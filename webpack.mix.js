const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .less('resources/less/credito-real-usa.less', 'public/credito-real/css/credito-real-usa.css');

mix.babel([
    'public/credito-real/js/registro.js',
    'public/credito-real/js/validacionesFront.js',
    'public/credito-real/js/login.js'
], 'public/credito-real/js/all.js').options({
        uglify: {
            uglifyOptions: {
            mangle: false
            }
        },
        compact:false
});
mix.babel([
    'public/credito-real/js/webapp/jquery-3.4.1.min.js',
    'public/credito-real/js/webapp/popper.min.js',
    'public/credito-real/js/webapp/bootstrap4.0.min.js',
    'public/credito-real/js/webapp/axios.min.js',
    'public/credito-real/js/webapp/dropzone.min.js',
    'public/credito-real/js/lib/jquery.magnific-popup.js',
    'public/credito-real/js/lib/sweetalert2.all.min.js',
    'public/credito-real/js/comprobante_ingresos.js',
    'public/credito-real/js/proceso_facematch.js'
], 'public/credito-real/js/all_facematch.js').options({
     uglify: {
         uglifyOptions: {
           mangle: false
         }
     },
     compact:false
});
mix.js('resources/js/app_backoffice.js', 'public/credito-real/js').options({
    uglify: {
        uglifyOptions: {
          mangle: true
        }
    },
    compact:true
});