<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistroController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('credito-real');
});
Route::get('/', [App\Http\Controllers\ProductoController::class, 'creal'])->name('index');
Route::get('/registro', function () {
	return view('registro');

});
Route::get('/equipos-leasing', function () {
	return view('equipos-leasing');
});
Route::get('/faqs', function () {
	return view('faqs');
});
Route::get('/aprobacion', function () {
	return view('parts.aprobacion');
});

Route::get('/getForm', [App\Http\Controllers\RegistroController::class, 'getForm']);
Route::post('/validaciones', [App\Http\Controllers\RegistroController::class, 'validaciones']);
Route::post('/solicitud/registro', [App\Http\Controllers\RegistroController::class, 'registroProspecto']);
Route::post('/solicitud/registro/solicitud', [App\Http\Controllers\RegistroController::class, 'registroSolicitud']);
Route::post('/solicitud/registro/sms', [App\Http\Controllers\RegistroController::class, 'envioSMS']);
Route::post('/solicitud/conf_sms_code', [App\Http\Controllers\RegistroController::class, 'confSMSCode']);
Route::post('/solicitud/resend_code', [App\Http\Controllers\RegistroController::class, 'resendSMSCode']);
Route::post('/solicitud/datos_empresa', [App\Http\Controllers\RegistroController::class, 'datosEmpresa']);
Route::post('/solicitud/datos_empresa_info', [App\Http\Controllers\RegistroController::class, 'datosEmpresaInfo']);
Route::post('/solicitud/datos_socio', [App\Http\Controllers\RegistroController::class, 'datosSocio']);
Route::post('/solicitud/datos_socio_info', [App\Http\Controllers\RegistroController::class, 'datosSocioInfo'])->name('socios_info');
Route::post('/solicitud/datos_credito', [App\Http\Controllers\RegistroController::class, 'datosNegocio']);
Route::get('/solicitud/oferta', [App\Http\Controllers\RegistroController::class, 'oferta'])->middleware('auth:prospecto');
Route::post('/solicitud/status_oferta', [App\Http\Controllers\SolicitudController::class, 'statusOferta']);
Route::post('/solicitud/rechazar_oferta', [App\Http\Controllers\SolicitudController::class, 'rechazarOferta']);
Route::post('/login/prospecto', [App\Http\Controllers\SessionController::class, 'login']);
Route::get('analytics', [App\Http\Controllers\AnalyticsController::class, 'getDataAnalytics'])->middleware('auth');

Route::get('/password-restore', function () {
	return view('password-restore');
});
Route::post('/email-password-restore', [App\Http\Controllers\RestorePasswordController::class, 'emailPasswordRestore']);
Route::post('/temporary-password', [App\Http\Controllers\RestorePasswordController::class, 'validateTemporaryPassword']);
Route::post('/password-update', [App\Http\Controllers\RestorePasswordController::class, 'passwordUpdate']);

Route::get('/webapp/facematch/selfie', [App\Http\Controllers\FaceMatchController::class, 'capturaSelfie'])->middleware('auth:prospecto');
Route::get('/webapp/facematch/id_front', [App\Http\Controllers\FaceMatchController::class, 'capturaIdentificacionFront'])->middleware('auth:prospecto');
Route::get('/webapp/facematch/id_back', [App\Http\Controllers\FaceMatchController::class, 'capturaIdentificacionBack'])->middleware('auth:prospecto');
Route::post('/webapp/facematch/procesaFaceMatch', [App\Http\Controllers\FaceMatchController::class, 'procesaFaceMatch'])->middleware('auth');
Route::post('/webapp/subirDocumento',  [App\Http\Controllers\FaceMatchController::class, 'subirDocumento'])->middleware('auth:prospecto');
Route::get('/webapp/subirDocumentoRepositorio', [App\Http\Controllers\FaceMatchController::class, 'subirDocumentoRepositorio']);

Route::get('/webapp/comprobante_ingresos', [App\Http\Controllers\ComprobanteIngresosController::class, 'index'])->middleware('auth:prospecto');
Route::post('/webapp/comprobante_ingresos/save', [App\Http\Controllers\ComprobanteIngresosController::class, 'save'])->middleware('auth:prospecto');
Route::post('/webapp/comprobante_ingresos/procesar', [App\Http\Controllers\ComprobanteIngresosController::class, 'procesar'])->middleware('auth');

//Auth::routes();
Route::get('/login', function () {
	return redirect('auth/login');
});
//============== PRIVATE ROUTES - REQUIRE AUTHENTICATION =======================
//admin home
Route::get('/panel', [App\Http\Controllers\BackOfficeController::class, 'panelDashboard'])->middleware('auth')->name('panel');
Route::post('/panel', [App\Http\Controllers\BackOfficeController::class, 'panelDashboard'])->middleware('auth');
// Detalle del prospecto
Route::get('/panel/prospecto/{prospecto_id}', [App\Http\Controllers\BackOfficeController::class, 'detalleProspecto'])->middleware('auth');
Route::get('/panel/prospecto/getDetalleProspecto/{prospecto_id}', [App\Http\Controllers\BackOfficeController::class, 'getDetalleProspecto'])->middleware('auth');
// Detalle de solicitud
Route::get('/panel/prospecto/solicitud/{solicitud_id}', [App\Http\Controllers\BackOfficeController::class, 'detalleSolicitud'])->middleware('auth');
Route::get('/panel/prospecto/getDetalleSolicitud/{solicitud_id}', [App\Http\Controllers\BackOfficeController::class, 'getDetalleSolicitud'])->middleware('auth');
//prospect detail
Route::get('/panelva/prospecto/{prospect_id}', [App\Http\Controllers\BackOfficeController::class, 'panelProspectDetail'])->middleware('auth');
//Auth::routes();
//download report csv for a single solicitud
//Route::get('/panel/report-csv/{prospect_id}/{solic_id}', 'BackOfficeController@downloadCsvReport')->middleware('auth');
//download complete report for all solicitudes for a given month of year
/*Route::get('/panel/report-completo-csv/', 'BackOfficeController@toViewFullCsvReport')->middleware('auth');
Route::post('/panel/report-completo-csv/', 'BackOfficeController@postToViewFullCsvReport')->middleware('auth');
Route::get('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');
Route::post('/panel/report-completo-csv/{month}/{year}', 'BackOfficeController@viewCsvReportFull')->middleware('auth');*/
//report csv daily
Route::post('/panel/report-daily/download', [App\Http\Controllers\BackOfficeController::class, 'downloadReportDaily'])->name('downloadReport')->middleware('auth');
Route::get('/panel/report-completo-csv-daily', [App\Http\Controllers\BackOfficeController::class, 'viewCsvReportDaily'])->middleware('auth');

//encrypt any non encrypted values (sensetive fields only) in the Prospects Table
Route::get('/panel/verify-encrypted/prospects', [App\Http\Controllers\BackOfficeController::class, 'cleanEncryptProspects'])->middleware('auth');
//encrypt any non encrypted values (sensetive fields only) in the Solicitations Table
//Route::get('/panel/verify-encrypted/solics', 'BackOfficeController@cleanEncryptSolics')->middleware('auth');
//=========== PRODUCTOS =============
Route::get('/productos', [App\Http\Controllers\ProductoController::class, 'index'])->name('productos')->middleware('auth');
Route::get('/productos/nuevo', [App\Http\Controllers\ProductoController::class, 'nuevo'])->name('productos.nuevo')->middleware('auth');
Route::post('/productos/save', [App\Http\Controllers\ProductoController::class, 'save'])->middleware('auth');
Route::get('/productos/{id}', [App\Http\Controllers\ProductoController::class, 'editar'])->middleware('auth');
Route::post('/productos/{id}', [App\Http\Controllers\ProductoController::class, 'update'])->middleware('auth');
Route::get('/productos/{id}/cobertura', [App\Http\Controllers\ProductoController::class, 'cobertura'])->middleware('auth');
Route::post('/productos/{id}/cobertura', [App\Http\Controllers\ProductoController::class, 'updateCobertura'])->middleware('auth');
Route::post('/productos/plazos/agregar', [App\Http\Controllers\ProductoController::class, 'savePlazos'])->middleware('auth');
Route::post('/productos/finalidad/agregar', [App\Http\Controllers\ProductoController::class, 'saveFinalidad'])->middleware('auth');
Route::post('/productos/ocupacion/agregar', [App\Http\Controllers\ProductoController::class, 'saveOcupacion'])->middleware('auth');
Route::post('/productos/equipos/agregar', [App\Http\Controllers\ProductoController::class, 'saveEquipos'])->middleware('auth');

Route::get('alta-clientes', [App\Http\Controllers\T24Controller::class, 'altaClientes'])->middleware('auth');
Route::post('carga-layout', [App\Http\Controllers\T24Controller::class, 'cargaLayout'])->middleware('auth');
Route::post('alta-cliente-t24', [App\Http\Controllers\T24Controller::class, 'altaCliente_T24'])->middleware('auth');
Route::post('alta-solicitud-t24', [App\Http\Controllers\T24Controller::class, 'altaSolicitud_T24'])->middleware('auth');
Route::post('ligar-usuario-ldap', [App\Http\Controllers\T24Controller::class, 'ligarUsuarioLDAP'])->middleware('auth');
Route::post('enviar-email', [App\Http\Controllers\T24Controller::class, 'enviarEmail'])->middleware('auth');
Route::get('search/autocomplete', [App\Http\Controllers\T24Controller::class, 'autoComplete']);
Route::get('alta-clientes/update_lista', [App\Http\Controllers\T24Controller::class,'updateLista']);

Route::get('auth/login', [App\Http\Controllers\Auth\AuthController::class, 'getLogin']);
Route::post('auth/login', [App\Http\Controllers\Auth\AuthController::class, 'login']);
Route::get('logout', [App\Http\Controllers\Auth\AuthController::class, 'logout']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/prospecto/logout', [App\Http\Controllers\Auth\AuthProspectoController::class, 'logout']);

Route::get('login/{provider}', [App\Http\Controllers\Auth\LoginController::class, 'redirectToProvider']);
Route::get('login/{provider}/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleProviderCallback']);

Route::get('/panel/usuarios', [App\Http\Controllers\UsersController::class, 'index'])->name('usuarios');
Route::get('/panel/usuarios/perfiles', [App\Http\Controllers\RolesController::class, 'index'])->name('perfiles');

Route::post('/panel/usuarios/save/', [App\Http\Controllers\UsersController::class, 'save']);
Route::get('/panel/usuarios/{id}', [App\Http\Controllers\UsersController::class, 'editar']);
Route::post('/panel/usuarios/{id}', [App\Http\Controllers\UsersController::class, 'actualizar']);

Route::post('/panel/usuarios/perfiles/save/', [App\Http\Controllers\RolesController::class,'save']);
Route::get('/panel/usuarios/perfiles/{id}', [App\Http\Controllers\RolesController::class, 'editar']);
Route::post('/panel/usuarios/perfiles/{id}', [App\Http\Controllers\RolesController::class, 'actualizar']);
//Auth::routes();

Route::get('/token', function () {

	JWTAuth::getJWTProvider()->setSecret(env('JWT_SECRET_PO'));
	$factory = JWTFactory::customClaims([
		'jti' => env('JWT_SECRET_PO')
	]);
	$payload = $factory->make();
	$token = JWTAuth::encode($payload);

	return $token;

})->middleware('auth');
Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade');
	 Route::get('map', function () {return view('pages.maps');})->name('map');
	 Route::get('icons', function () {return view('pages.icons');})->name('icons');
	 Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});
