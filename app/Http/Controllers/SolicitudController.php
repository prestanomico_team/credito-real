<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Models\Prospecto;
use App\Models\Solicitud;
use App\Models\RespuestaMaquinaRiesgo;
use App\Repositories\SolicitudRepository;

class SolicitudController extends Controller
{
    private $solicitudRepository;
    /**
     * Constructor de clase
     *
     * @param Request $request
     */
    public function __construct(Request $request) {

        $this->solicitudRepository = new SolicitudRepository;
    }
    

    /**
     * Guarda el status de oferta Aceptada
     * 
     *
     * @param  Request $request Datos capturados en el modal oferta
     *
     * @return json             Resultado del guardado del status
     */
    public function statusOferta(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            $status_oferta = $request->status_oferta;
            $eventTM = [];

            /*$ofertaPredominante = OfertaPredominante::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->first();

            Cookie::queue(Cookie::forget('producto'));
            Cookie::queue(Cookie::forget('logo'));
            Cookie::queue(Cookie::forget('checa_calificas'));*/

            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = $request->status_oferta;
            $solicitud->save();

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                $request->status_oferta,
                1,
                'Se ha guardado el status de la solicitud con éxito',
                null
            );


            if ($status_oferta == 'Oferta Aceptada') {

                $evento = 'OfertaAceptada';
                if ($solicitud->producto()->exists()) {
                    $campaña = $solicitud->producto[0]['pivot']['lead'];
                    $evento = $this->solicitudRepository->getEvento($campaña, 'OfertaAceptada');
                }

                $resultado = $this->solicitudRepository->ofertaAceptada($prospecto, $solicitud);
                if ($resultado['success'] == true) {
                    $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    
                    $eventTM[] = ['event'=> 'OfertaRegular', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    $resultado['eventTM'] = $eventTM;
                }

                if ($resultado['carga_documentos'] == true) {
                    $request->session()->put('isRedirected', true);
                }
            }
            
            return response()->json($resultado);

        } else {

            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);

        }

    }

    public function rechazarOferta(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $status_oferta = 'Oferta Rechazada';

            $msj = 'Se ha guardado el motivo de recahzo de la solicitud con éxito';
            $motivo = $request->motivo_rechazo;
            $descripcion_otro = $request->descripcion_otro;
            $modelo = $request->modelo;


            $resultado = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->update([
                    'status_oferta'     => $status_oferta,
                    'elegida'           => 0,
                    'motivo_rechazo'    => mb_strtoupper($motivo),
                    'descripcion_otro'  => mb_strtoupper($descripcion_otro)
                ]);


            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = $status_oferta;
            $solicitud->save();

            // Actualizando el campo ult_punto_reg de la solicitud
            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                $request->status_oferta,
                1,
                'Se ha guardado el status de la solicitud con éxito',
                null
            );

            Auth::guard('prospecto')->logout();

            return response()->json([
                'success' => true,
                'message' => 'Oferta rechazada'
            ]);

        } else {
            return response()->json([
                'success'   => false,
                'message'   => 'La sesión expiro',
                'reload'    => true,
            ]);
        }

    }


}
