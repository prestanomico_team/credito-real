<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CognitoRepository;
use App\Models\Prospecto;
use Validator;
use Hash;

class RestorePasswordController extends Controller
{
    /**
     * Constructor de la clase
     *
     * @param CognitoRepository $cognito Contiene las funciones que realizan las llamadas a Cognito
     */
    public function __construct(CognitoRepository $cognito)
    {
        $this->cognito = $cognito;
    }

    /**
     * Envia el email de restauración de contraseña.
     *
     * @param  Request $request Contiene la información enviada del formulario.
     *
     * @return json             Resultado del envio del email.
     */
    public function emailPasswordRestore(Request $request) {

        $messages = [
            'required'  => '* El e-mail es obligatorio',
            'email'     => '* El formato del campo es incorrecto',
            'exists'    => '* El e-mail no es válido como usuario de CREDITO REAL USA.',
        ];

        // Validaciones de los datos recibidos
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:prospectos,email',
        ], $messages);
        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            $response = $this->cognito->sendResetLink($request->email);
            $prospecto = Prospecto::where('email', $request->email)->first();

            if ($response['success'] == true) {
                return response()->json([
                    'success'   => true,
                    'message'   => 'SMS de restauración de contraseña enviado',
                    'email'     => $request->email,
                    'celular'   => $prospecto->celular
                ]);
            } else {

                return response()->json([
                    'success'   => false,
                    'message'   => $response['message'],
                ]);

            }

        } else {
            return json_encode([
                'success' => false,
                'errores' => $errores
            ]);
        }

    }

    /**
     * Realiza la validación del password temporal enviado en email.
     *
     * @param  Request $request Contiene la información enviada del formulario.
     *
     * @return json             Resultado de validar el password temporal.
     */
    public function validateTemporaryPassword(Request $request) {
        $messages = [
            'required'                  => '* El campo es obligatorio',
            'email'                     => '* El formato del campo es incorrecto',
            'codigo_verificacion.min'   => '* El campo debe tener mínimo 6 caracteres.',
            'new_password.min'          => '* El campo debe tener mínimo 8 caracteres.',
            'max'                       => '* El campo debe tener máximo 6 caracteres.',
            'new_password.regex'        => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
        ];

        // Validaciones de los datos recibidos
        $validator = Validator::make($request->all(), [
            'email_usuario'         => 'required|email',
            'codigo_verificacion'   => 'required|min:6|max:6',
            'new_password'          => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
            'confirm_password'      => 'required|same:new_password',
        ], $messages);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            $response = $this->cognito->restorePassword($request->email_usuario, $request->codigo_verificacion, $request->new_password);
            // print_r($response);

            if ($response['success'] == true) {

                $prospecto = Prospecto::where('email', $request->email_usuario)->first();
                $prospecto->password = Hash::make($request->new_password);
                $prospecto->encryptd = encrypt($request->new_password);
                $prospecto->save();

                return response()->json([
                    'success'       => true,
                    'message'       => 'Contraseña actualizada correctamente',
                ]);

            } else {

                return response()->json([
                    'success'   => false,
                    'message'   => $response['message'],
                ]);
            }

        } else {
            return json_encode([
                'success' => false,
                'errores' => $errores
            ]);
        }
    }

    /**
     * Actualiza la contraseña al usuario LDAP
     *
     * @param  Request $request Contiene la información enviada del formulario.
     *
     * @return json             Resultado de actualizar el password.
     */
    public function passwordUpdate(Request $request) {

        $messages = [
            'required'  => '* El campo es obligatorio',
            'min'       => '* El campo debe tener mínimo 8 caracteres.',
            'same'      => '* Las contraseñas deben ser iguales.'
        ];

        // Validaciones de los datos recibidos
        $validator = Validator::make($request->all(), [
            'new_password'      => 'required|min:8',
            'confirm_password'  => 'required|same:new_password',
            'name'              => 'required',
            'lastName'          => 'required',
            'fullName'          => 'required',
            'phoneNumber'       => 'required',
            'token'             => 'required',
            'timeWhenGenerated' => 'required',
        ], $messages);

        $errores = $validator->errors()->messages();

        if (count($errores) == 0) {

            $response = $this->_ldap->updatePassword($request);
            $response = json_decode($response);

            if ($response->success == true) {

                $email = mb_strtoupper($request->idUser);
                $prospecto = Prospect::where('email', $email)
                    ->update([
                        'encryptd' => encrypt($request->new_password)
                    ]);

                return response()->json([
                    'success'       => true,
                    'message'       => 'Contraseña actualizada con éxito',
                ]);

            } else {
                return response()->json([
                    'success'   => false,
                    'response'  => $response,
                ]);
            }

        } else {
            return json_encode([
                'success' => false,
                'errores' => $errores
            ]);
        }
    }
}
