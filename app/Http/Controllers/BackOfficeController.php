<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prospecto;
use App\Models\Solicitud;
use App\Models\Plazo;
use App\Models\RespuestaMaquinaRiesgo;
use App\Exports\ReporteSolicitudesExport;
use Excel;
use Carbon\Carbon;
use Auth;

class BackOfficeController extends Controller
{
    /**
     * Muestra la vista principal del panel, que contiene la lista de prospectos
     *
     * @param request $request
     * @return view
     */

    public function panelDashboard(request $request)
    {
        // Obteniendo la ip del usuario para determinar si puede visualizar
        // el panel
        //$user_ip = self::getUserIP();
        $authorized_ips = json_decode(env('APP_PANEL_IPS'));

        $filtered = false;
        $filter_type = '';
        $filter_val = '';
        if ($request->input('filter_prospect_id') != '') {
            $filtered = true;
            $filter_type = 'id';
            $filter_val = $request->input('filter_prospect_id');
            //filter the list by id
            $prospects = Prospecto::with('ultima_solicitud')->where('id', $filter_val)->limit(10)->paginate(10);
        } elseif ($request->input('filter_prospect_email') != '') {
            $filtered = true;
            $filter_type = 'email';
            $filter_val = $request->input('filter_prospect_email');
            //filter the list by email
            $prospects = Prospecto::with('ultima_solicitud')->where('email', 'like', '%'.$filter_val.'%')->paginate(20);

        } elseif ($request->input('filter_prospect_name') != '') {

            $filtered = true;
            $filter_type = 'name';
            $filter_val = $request->input('filter_prospect_name');
            $prospects = Prospecto::all()->filter(function($record) use($filter_val) {
            if(stripos($record->nombres, $filter_val) !== false) {
                    return $record;
                }
            });
            
            $prospects = $prospects->paginate(10);


        } else {
            //don't filter the list
            $prospects = Prospecto::orderBy('updated_at', 'desc')->paginate(10);
        }

        return view('crm.dashboard', [
            'prospects'         => $prospects,
            'filtered'          => $filtered,
            'filter_type'       => $filter_type,
            'filter_val'        => $filter_val,
            'can_prospectos'    => Auth::user()->can('prospectos'),
            'can_solicitudes'   => Auth::user()->can('solicitudes')
        ]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $prospecto_id
     * @return view
     */
    public function detalleProspecto($prospecto_id)
    {
        return view('crm.detalleProspecto', [
            'prospecto_id' => $prospecto_id
        ]);
    }

    public function detalleSolicitud($solicitud_id)
    {
        return view('crm.detalleSolicitud', [
            'solicitud_id' => $solicitud_id
        ]);
    }

    public function getDetalleProspecto($prospect_id)
    {
        $prospecto = Prospecto::with('solicitudes.producto')
            ->find($prospect_id);

        foreach ($prospecto->solicitudesV2 as $solicitud) {
            $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
            $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
            $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
            //$solicitud->encontrado = ($solicitud->encontrado) ? 'Si' : 'No';
            $solicitud->created_at_disp = $solicitud->created_at->toDateTimeString();
            $solicitud->updated_at_disp = $solicitud->updated_at->toDateTimeString();

            // Obteniendo el producto
            if (isset($solicitud->producto[0])) {
                $solicitud->nombre_producto = $solicitud->producto[0]->nombre_producto;
            } else {
                $solicitud->nombre_producto = 'MERCADO ABIERTO';
            }
        }

        return response()
                ->json($prospecto)
                ->header('Content-Type', 'json');
    }

    /**
     * Obtiene el detalle de la solcitud del prospecto
     *
     * @param [type] $solicitud_id
     * @return void
     */
    public function getDetalleSolicitud($solicitud_id) {

        $solicitud = Solicitud::with(
                'prospecto', 'datos_socios', 'datos_empresa', 'producto', 'tracking', 'respuesta_maquina_riesgos','log')
            ->select('id', 'prospecto_id', 'status', 'sub_status',  'equipo_leasing',
                'tipo_persona', 
                'prestamo', 'plazo', 'finalidad', 'pago_estimado',
                'created_at', 'updated_at')
            ->find($solicitud_id);
        try {
            $solicitud->prospecto->encryptd = decrypt($solicitud->prospecto->encryptd);
        } catch (DecryptException $e) {
            $solicitud->prospecto->encryptd = '';
        }   

        $solicitud->prestamo = '$'.number_format($solicitud->prestamo, 0, '.', ',');
        $solicitud->plazo = self::getPlazoLabel($solicitud->plazo);
        $solicitud->pago_estimado = '$'.number_format($solicitud->pago_estimado, 2, '.', ',');
        $solicitud->equipo_leasing = $solicitud->equipo_leasing;
        
        
        $solicitud->created_at_disp = self::displayMySqlDate($solicitud->created_at->toDateTimeString());
        

        if (isset($solicitud->datos_socios)) {
            $solicitud->datos_socios = $solicitud->datos_socios->toArray();
            
        }

        if (isset($solicitud->datos_empresa)) {
            $solicitud->datos_empresa = $solicitud->datos_empresa->toArray();
        }
        
        if ($solicitud->log->exists()) {
            $solicitud->ultimo_mensaje_usuario = json_decode($solicitud->log['ultimo_mensaje_usuario'], true);
        }
    
        return response()
                ->json($solicitud)
                ->header('Content-Type', 'json');
    }

    public function getPlazoLabel($clave){

        $plazo = Plazo::where('clave', $clave)->get();

        if (count($plazo) == 1) {
            $duracion = $plazo[0]->duracion;
            $plazo = $plazo[0]->duracion.' '.mb_strtoupper($plazo[0]->plazo);
        } else {
            $plazo = '';
        }

        return $plazo;
    }

    public function viewCsvReportDaily()
    {
        return view('crm.full_data_table_daily');
    }

    public function downloadReportDaily(Request $request)
    {

        ini_set('memory_limit','768M');
        $dates = $request->dateRange;
        $range = explode(' ', $dates);
        $startDate = $range['0'];
        $endDate = $range['2'];
        $sDate= Carbon::parse($range['0'])->startOfDay();
        $eDate= Carbon::parse($range['2'])->endOfDay();

        $diff = $eDate->diffInDays($sDate);

        if ($diff <= 30) {
            return (new ReporteSolicitudesExport)
            ->start_date($sDate)
            ->end_date($eDate)
            ->download('tabla-solicitudes-diaras-'.$startDate.'-'.$endDate.'.xlsx');
        } else {
            $error = 'No se pueden seleccionar mas de 30 días';
            return view('crm.full_data_table_daily')->with(['error' => $error]);
        }

    }

    public function displayMySqlDate($string){
        $date_time_arr = explode(' ', $string);
        //check if there is a time
        $time = '';
        if(isset($date_time_arr[1])){
            $time = $date_time_arr[1];
        }
        $date = '';
        if(isset($date_time_arr[0])){
            $date_arr = explode('-', $date_time_arr[0]);
            $y = $date_arr[0];
            $m = $date_arr[1];
            $d = $date_arr[2];
            $date = $d.'/'.$m.'/'.$y;
        }
        if($date != ''){
            return $date.' '.$time;
        }else{
            return '';
        }
    
    }
    
}
