<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Facades\Agent;
use Carbon\Carbon;
use Twilio\Rest\Client;
use Cookie;
use DB;
use Auth;
use App\Models\Prospecto;
use App\Models\Solicitud;
use App\Models\CodigosSMS;
use App\Models\EquipoLeasing;
use App\Models\Producto;
use App\Models\Analytic;
use App\Models\CatalogoZipCode;
use App\Models\Plazo;
use App\Models\Socio;
use App\Models\Empresa;
use App\Models\W_USER;
use App\Models\MotivoRechazo;
use App\Models\RespuestaMaquinaRiesgo;
use App\Repositories\CalixtaRepository;
use App\Repositories\CognitoRepository;
use App\Repositories\SolicitudRepository;

class RegistroController extends Controller
{

    private $solicitudRepository;

    public function __construct() {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function getForm(Request $request) {

        if (Auth::guard('prospecto')->check() && $request->has('paso') == false) {
            $prospecto = Auth::guard('prospecto')->user();
            $siguientePaso = $this->solicitudRepository->siguientePaso(null, $prospecto);

        } elseif ($request->has('paso')) {

            $siguientePaso = $this->solicitudRepository->siguientePaso($request->paso, null);
            $ultimoStatus = null;
            $modalConfianza = null;
            $nombreProspecto = null;
            if ($prospecto = Auth::guard('prospecto')->user()) {
                $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
                $ultimoStatus = $solicitud->status;
                if ($ultimoStatus == 'Registro') {
                    //$modalConfianza = view("modals.modalConfianza")->render();
                    $nombreProspecto = "{$prospecto->nombres} {$prospecto->apellido_paterno} {$prospecto->apellido_materno}";
                    $siguientePaso['ultimo_status'] = $ultimoStatus;
                    $siguientePaso['modal_confianza'] = $modalConfianza;
                    $siguientePaso['nombre_prospecto'] = $nombreProspecto;
                    $siguientePaso['sesionIniciada'] = true;
                }
            }
        } else {
            $siguientePaso = [
                'formulario'    => 'simulador',
                'oculta'        => null
            ];
        }

        return response()->json($siguientePaso);
    }


    public function validaciones(Request $request, $formulario = null) {

        if ($request->has('datos') && $request->has('formulario')) {

            $datos = $request->datos;
            $formulario = $request->formulario;

            $validaciones = $this->validacionesSolicitud($datos, $formulario);
            if ($validaciones->fails()) {
                return response()->json([
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ]);
            } else {
                return response()->json([
                    'success'   => true,
                    'errores'   => []
                ]);
            }

        } elseif ($formulario != null) {

            $validaciones = $this->validacionesSolicitud($request->toArray(), $formulario);
            if ($validaciones->fails()) {
                return [
                    'success'   => false,
                    'errores'   => $validaciones->errors()
                ];
            }

        } else {

            return response()->json([
                'success' => false,
                'message' => 'Operacion no permitida'
            ]);

        }
    }

    public function registroProspecto(Request $request) {

        $validaciones = $this->validaciones($request, 'registro-simulador');
        if ($validaciones != '') {
            return response()->json($validaciones);
        }
        $apellidos = explode(" ", $request->apellidos);
        if (count($apellidos) > 1) {
            $apellido_paterno = $apellidos[0];
            $apellido_materno = $apellidos[1];
        } else {
            $apellido_paterno = $request->apellidos;
            $apellido_materno = '';
        }

        $cognito_id = null;
        $responseCognito = $this->registroCognito($request);
        if ($responseCognito['success'] == false) {
            return response()->json($responseCognito);
        } else {
            $cognito_id = $responseCognito['cognito_id'];
        }

        /*if ($request->celular = "5578511626" || $request->celular = "5618474957" || $request->celular = "5566690083" || $request->celular = "5581573000" || $request->celular = "5510682890") {
            $area_code = '+52';
        } else {
            $area_code = '+1';
        }*/

        $prospecto = Prospecto::firstOrCreate([
            'email' => mb_strtolower($request->email)
        ]);

        // Si el prospecto fue creado recientemente, se actualizan atributos
        // faltantes y se crea solicitud
        if ($prospecto->wasRecentlyCreated) {

            $prospecto->nombres               = mb_strtoupper($request->nombres);
            $prospecto->apellido_paterno      = mb_strtoupper($apellido_paterno);
            $prospecto->apellido_materno      = mb_strtoupper($apellido_materno);
            $prospecto->celular               = $request->celular;
            $prospecto->password              = Hash::make($request->contraseña);
            $prospecto->encryptd              = encrypt($request->contraseña);
            $prospecto->sms_verificacion      = 0;
            $prospecto->usuario_confirmado    = 0;
            $prospecto->referencia            = 'SITIO';
            $prospecto->cognito_id            = $cognito_id;

            $prospecto->save();

            $w_user = W_USER::updateOrCreate([
                'ID_USER'       => mb_strtolower($request->email)
            ], [
                'PHONE_NUMBER'  => $request->celular,
                'CREATE_DATE'   => Carbon::now(),
                'UPDATE_DATE'   => Carbon::now()
            ]);

            // Obteniendo la descripcion del equipo
            $equipo = $request->input('equipo_leasing');
            $equipo = EquipoLeasing::find($equipo);
            if (isset($equipo->equipo_leasing)) {
                $equipo = mb_strtoupper($equipo->equipo_leasing);
            } else {
                $equipo = Equipo::find(1);
                $equipo = mb_strtoupper($equipo->equipo_leasing);
            }

            // Obteniendo la clave del plazo
            $plazo = $request->input('plazo');
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (isset($plazo->clave)) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $solicitud = Solicitud::create([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'tipo_persona'      => 'EMPRESA',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $this->str_replaceChars($request->monto_prestamo),
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $request->pago_estimado,
                'finalidad'  => mb_strtoupper($request->finalidad_custom),
                'equipo_leasing'    => $equipo,
            ]);

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($request->email, ['custom:idProspecto' => 'CREAL-'.$prospecto->id]);

            Auth::guard('prospecto')->loginUsingId($prospecto->id);

            $r_parameter = 'none';
            $campaña = 'none';
            if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                $r_parameter = Cookie::get('r_parameter');
            } else if (Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña');
            }

            $producto = Producto::with('plazos', 'equipo_leasing')
                ->where('alias', 'creal')
                ->first();

            $campaña = $campaña == 'none' ? '' : $campaña;
            $r_parameter = $r_parameter == 'none' ? '' : $r_parameter;
            $producto_id = $producto->id;
            $version_producto = Producto::find($producto_id)->currentVersion();

            $producto->version_producto = $version_producto;

            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                    'lead'             => $campaña,
                    'lead_id'          => $r_parameter,
                ]
            ]);

            Cookie::queue(Cookie::forget('campaña'));
            Cookie::queue(Cookie::forget('r_parameter'));

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if (Agent::isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = Agent::device();
            }
            if (Agent::isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = Agent::device();
            }
            if (Agent::isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = Agent::device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $request->input('clientId'),
                'email'             => $request->input('email'),
                'ip'                => $this->getUserIP(),
                'telefono'          => $request->input('celular'),
                'sistema_operativo' => Agent::platform(),
                'navegador'         => Agent::browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'msj'               => 'Prospecto y solicitud creados',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id
            ];

            $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Usuario y Solicitud creados con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $evento = $this->solicitudRepository->getEvento($campaña, 'RegistroUsuario');

            $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $mnsj_str['eventTM'] = $eventTM;

            return response()->json($mnsj_str, 201);

        } elseif($prospecto->id) {

            if ($prospecto->cognito_id == null) {
                $prospecto->cognito_id = $cognito_id;
                $prospecto->save();
            } else {

                return response()->json([
                    'success'           => false,
                    'msg'               => 'El usuario ya esta registrado',
                    'siguiente_paso'    => 'login'
                ]);
            }

        } else {

            return response()->json([
                'success' => false,
                'msg'     => 'Hubo un error al crear el prospecto y la solicitud',
            ]);

        }
    }

    public function registroSolicitud(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::findOrFail($prospecto_id);

            $validaciones = $this->validaciones($request, 'simulador');
            if ($validaciones != '') {
                return response()->json($validaciones);
            }

            // Obteniendo la descripcion del equipo
            $equipo = $request->input('equipo_leasing');
            $equipo = EquipoLeasing::find($equipo);
            if (isset($equipo->equipo_leasing)) {
                $equipo = mb_strtoupper($equipo->equipo_leasing);
            } else {
                $equipo = Equipo::find(1);
                $equipo = mb_strtoupper($equipo->equipo_leasing);
            }

            // Obteniendo la clave del plazo
            $plazo = $request->input('plazo');
            $duracion = null;
            $clave_plazo = null;
            $plazo = Plazo::find($plazo);

            if (isset($plazo->clave)) {
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            } else {
                $plazo = Plazo::find(1);
                $clave_plazo = $plazo->clave;
                $duracion = $plazo->duracion;
                $plazo = mb_strtoupper($plazo->plazo);
            }

            $solicitud = Solicitud::create([
                'status'            => 'Registro',
                'sub_status'        => 'Crear Usuario',
                'tipo_persona'      => 'EMPRESA',
                'prospecto_id'      => $prospecto->id,
                'prestamo'          => $this->str_replaceChars($request->monto_prestamo),
                'plazo'             => $clave_plazo,
                'pago_estimado'     => $request->pago_estimado,
                'finalidad'  => mb_strtoupper($request->finalidad_custom),
                'equipo_leasing'    => $equipo,
            ]);

            $cognito = new CognitoRepository;
            $responseCognito = $cognito->setUserAttributes($request->email, ['custom:idProspecto' => 'CREAL-'.$prospecto->id]);

            Auth::guard('prospecto')->loginUsingId($prospecto->id);

            $r_parameter = 'none';
            $campaña = 'none';
            if (Cookie::get('r_parameter') !== null && Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña') == 'askrobin' ? 'askrobin' : 'none';
                $r_parameter = Cookie::get('r_parameter');
            } else if (Cookie::get('campaña') !== null) {
                $campaña = Cookie::get('campaña');
            }

            $producto = Producto::with('plazos', 'equipo_leasing')
                ->where('alias', 'creal')
                ->first();

            $campaña = $campaña == 'none' ? '' : $campaña;
            $r_parameter = $r_parameter == 'none' ? '' : $r_parameter;
            $producto_id = $producto->id;
            $version_producto = Producto::find($producto_id)->currentVersion();

            $producto->version_producto = $version_producto;

            $solicitud->producto()->attach([
                $producto_id => [
                    'version_producto' => $version_producto,
                    'lead'             => $campaña,
                    'lead_id'          => $r_parameter,
                ]
            ]);

            Cookie::queue(Cookie::forget('campaña'));
            Cookie::queue(Cookie::forget('r_parameter'));

            $agent = new Agent();
            $tipo_dispositivo = null;
            $marca =  null;

            if (Agent::isDesktop()) {
                $tipo_dispositivo = 'desktop';
                $marca = Agent::device();
            }
            if (Agent::isMobile()) {
                $tipo_dispositivo = 'mobile';
                $marca = Agent::device();
            }
            if (Agent::isTablet()) {
                $tipo_dispositivo = 'tablet';
                $marca = Agent::device();
            }

            $analytic = Analytic::insert([
                'prospecto_id'      => $prospecto->id,
                'solicitud_id'      => $solicitud->id,
                'client_id'         => $request->input('clientId'),
                'email'             => $prospecto->email,
                'ip'                => $this->getUserIP(),
                'telefono'          => $prospecto->celular,
                'sistema_operativo' => Agent::platform(),
                'navegador'         => Agent::browser(),
                'tipo_dispositivo'  => $tipo_dispositivo,
                'marca'             => $marca,
                'datos_obtenidos'   => 'Sin datos',
                'fecha_hora'        => date('YmdHi'),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $mnsj_str = [
                'success'           => true,
                'login'             => false,
                'nueva_solicitud'   => true,
                'msj'               => 'Nueva solicitud creada con exito',
                'siguiente_paso'    => 'sms',
                'prospecto_id'      => $prospecto->id,
                'celular'           => $prospecto->celular
            ];

            $name_string = $request->input('nombres').' '.$request->input('apellido_paterno').' '.$request->input('apellido_materno');
            $this->solicitudRepository->setUltPuntoReg($solicitud->id,
                $solicitud->status,
                'Crear Usuario',
                1,
                'Nueva solicitud creada con éxito',
                ['prospecto_id' => $prospecto->id, 'solicitud_id' => $solicitud->id, 'nombre' => $name_string]
            );

            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));
            $solicitud->save();

            $evento = $this->solicitudRepository->getEvento($campaña, 'RegistroUsuario');

            $eventTM[] = ['event'=> $evento, 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
            $mnsj_str['eventTM'] = $eventTM;

            session()->forget('nueva_solicitud');
            return response()->json($mnsj_str, 201);

        } else {

            return response()->json([
                'message' => 'La sessión expiro',
                'success' => false,
            ]);

        }

    }

    public function envioSMS(Request $request, CalixtaRepository $calixta) {

        $prospecto_id = $request->prospecto_id;

        if ($request->celular == "5578511626" || $request->celular == "5618474957" || $request->celular == "5566690083" || $request->celular == "5581573000" || $request->celular == "5510682890") {
            $area_code = '+52';

        } else {
            $area_code = '+1';
        }
        $celular = $area_code.$request->celular;
        
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($celular, "sms");
        $twilio_response = true;
        

        $prospecto = Prospecto::findOrFail($prospecto_id);
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($request->has('actualizar_celular')) {
            $prospecto->celular = $celular;
        }

        $prospecto->sms_verificacion = true;
        $prospecto->save();

        $codigo_sms = CodigosSMS::create([
            'prospecto_id'  => $prospecto_id,
            //'codigo'        => $codigo,
            'celular'       => $celular,
            'nombre'        => $prospecto->nombres.' '.$prospecto->apellido_paterno,
            'email'         => $request->email,
            //'enviado'       => $responsecalixta->success,
            //'descripcion'   => $responsecalixta->message,
            'verificado'    => false,
        ]);

        if ($twilio_response == true) {

            return response()->json([
                'success'           => true,
                'message'           => 'Código de verificacón enviado con éxito.',
                'celular'           => $celular
            ]);

        } else {

            $mnsj_str = [
                'success'           => false,
                'message'           => 'Código de verificación sin enviar: ',
            ];
            $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));

            
            return response()->json([
                'success'               => false,
                'message'               => 'Código de verificación sin enviar: '
                ]);   
        }

    }

    public function confSMSCode(request $request, CalixtaRepository $calixta) {

        if (Auth::guard('prospecto')->check()) {
            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $conf_code = $request->input('conf_code');
            $is_login = $request->input('is_login');

            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();


            if ($prospecto->celular == "5578511626" || $prospecto->celular == "5618474957" || $prospecto->celular == "5566690083" || $prospecto->celular == "5581573000" || $prospecto->celular == "5510682890") {
                $area_code = '+52';
    
            } else {
                $area_code = '+1';
            }
            $celular = $area_code.$prospecto->celular;

            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_SID");
            $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
            $twilio = new Client($twilio_sid, $token);
            $verification = $twilio->verify->v2->services($twilio_verify_sid)
                ->verificationChecks
                ->create($conf_code, array('to' => $celular));

            
            if ($verification->valid) {

                $prospecto->usuario_confirmado = 1;
                $prospecto->save();

                if ($solicitud->status == 'Registro') {
                    $codigo_enviado = $calixta->sendSMS(
                        $prospecto->celular,
                        env('SMS_CONFIANZA')
                    );
                }

                if ($prospecto->password_temporal == true) {

                    $cognito = new CognitoRepository;
                    $responseCognito = $cognito->sendResetLink($prospecto->email);
                }

                if ($prospecto->login == false) {

                    $cognito = new CognitoRepository;
                    $responseCognito = $cognito->setUserAttributes($prospecto->email, ['phone_number_verified' => 'true']);
                    //$response = $cognito->sendResetLink($prospecto->email);

                    $solicitud->status = 'Registro Confirmado';
                    $solicitud->sub_status = 'Validar SMS';
                    $solicitud->save();

                    $mnsj_str = [
						'success'         => true,
                        'message'         => 'El usuario confirmó su celular con éxito.',
                        'celular'         => $prospecto->celular,
                        'siguiente_paso'  => $this->solicitudRepository->siguientePaso(null, $prospecto)
					];

                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        'Registro Confirmado',
                        'Validar SMS',
                        1,
                        'El usuario confirmó su celular con éxito.',
                        ['cel' => $prospecto->celular, 'code' => '']
                    );
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));

                } else {

                    $prospecto->login = false;
                    $prospecto->save();

                    if ($solicitud->status == 'Registro') {

                        $solicitud->status = 'Registro Confirmado';
                        $solicitud->sub_status = 'Validar SMS';
                        $solicitud->save();

                       $this->solicitudRepository->setUltPuntoReg(
                            $solicitud->id,
                            'Registro Confirmado',
                            'Validar SMS',
                            1,
                            'El usuario confirmó su celular con éxito al hacer Login',
                            ['cel' => $prospecto->celular, 'code' => '']
                        );

                    }

                    $mnsj_str = [
                        'success'           => true,
                        'message'           => 'Login: El usuario confirmó su celular con éxito.',
                    ];
                    $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($mnsj_str));

                    $siguiente_paso = $this->solicitudRepository->siguientePaso(null, $prospecto);
                    if (isset($siguiente_paso['cuestionario'])) {
                        $mnsj_str['cuestionario'] = $siguiente_paso['cuestionario'];
                    }
                    $mnsj_str['siguiente_paso'] = $siguiente_paso;

                    if ($siguiente_paso['redirect'] != null) {
                        $request->session()->put('isRedirected', true);
                    }

                }

                return response()->json($mnsj_str);

            } else {

                $mnsj_str = [
                    'success'   => false,
                    'message'   => 'Código Inválido',
                ];

                return response()->json($mnsj_str);
            }
        
        } else {
            return response()->json([
                'success'   => false,
                'message' => 'La sesión expiro'
            ]);
        }
    }

    /**
     * Reenvia el SMS con el código de activación al usuario
     *
     * @param  request $request Contiene los datos del usuario
     *
     * @return json Resultado del envío del SMS
     */
    public function resendSMSCode(request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            
            if ($prospecto->celular == "5578511626" || $prospecto->celular == "5618474957" || $prospecto->celular == "5566690083" || $prospecto->celular == "5581573000" || $prospecto->celular == "5510682890") {
                $area_code = '+52';
    
            } else {
                $area_code = '+1';
            }
            $celular = $area_code.$prospecto->celular;
            
            $token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_sid = getenv("TWILIO_SID");
            $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
            $twilio = new Client($twilio_sid, $token);
            $twilio->verify->v2->services($twilio_verify_sid)
                ->verifications
                ->create($celular, "sms");
            $twilio_response = true;

            /*$codigo_sms = CodigosSMS::create([
                'prospecto_id'  => $prospecto_id,
                'codigo'        => $codigo,
                'celular'       => $prospecto->celular,
                'nombre'        => $prospecto->nombres.' '.$prospecto->apellido_paterno,
                'email'         => $prospecto->email,
                'enviado'       => $responsecalixta->success,
                'descripcion'   => $responsecalixta->message,
                'verificado'    => false,
            ]);*/

            if ($twilio_response == true) {

                return response()->json([
                    'success'               => true,
                    'message'               => 'Código de verificacón reenviado con éxito',
                ]);
    
            } else {
                
                return response()->json([
                    'success'               => false,
                    'message'               => 'Código de verificación sin enviar: '
                    ]);   
            }            

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }

    public function datosEmpresa(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            
            if ($solicitud) {

                $validaciones = $this->validaciones($request, 'datos_empresa');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                // Guardando los datos de la solicitud desencriptados en la nueva tabla
                $empresa = Empresa::firstOrNew(
                    [
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ]);

                $empresa->nombre_empresa           = $request->nombre_empresa;
                $empresa->tipo_propiedad           = mb_strtoupper($request->tipo_propiedad);
                $empresa->fecha_fundacion          = mb_strtoupper($request->fecha_fundacion);
                $empresa->descripcion_empresa      = mb_strtoupper($request->descripcion_empresa);
                $empresa->calle                    = mb_strtoupper($request->location);
                $empresa->ciudad                   = $request->locality;
                $empresa->estado                   = mb_strtoupper($request->administrative_area_level_1);
                $empresa->cp                       = $request->postal_code;
                $empresa->save();

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Datos Empresa',
                    1,
                    'Los datos de empresa se han guardado con éxito',
                );

                $mnsj_str = [
                    'success'       => true
                ];
                //busqueda en BD codigo TEXAS
                $cp_data = CatalogoZipCode::where('zip_code', $request->postal_code)->get();
                
                if(isset($cp_data[0]->cobertura)) {

                    $eventTM = ['event' => 'DatosEmpresa', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                    $mnsj_str['eventTM'] = $eventTM;
                    // Guardando el mensaje en el campo ult_mensj_a_usuario
                    $this->solicitudRepository->setUltMnsjUsr(
                        $solicitud,
                        json_encode($mnsj_str)
                    );
                    $solicitud->sub_status = 'Datos Empresa';
                    $solicitud->save();

                    return response()->json($mnsj_str);

                    // Actualizando el campo ult_punto_reg de la solicitud
                    /*$this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        $request->status_oferta,
                        1,
                        'Se ha guardado el status de la solicitud con éxito',
                        null
                    );*/

                } else {

                    $mnsj_str = [
                        'success'       => false,
                        'stat'          => 'No Califica',
                        'result'        => 'No califica por su: plaza',
                        'estado'        => $empresa->estado
                    ];

                    $solicitud->status = 'Rechazado';
                    $solicitud->sub_status = 'Rechazado';
                    $solicitud->save();

                    Auth::guard('prospecto')->logout();
                    $modal = view("modals.modalPlazaCobertura")->render();
                    $mnsj_str['modal'] = $modal;
                    //session()->flash('nueva_solicitud', true);
                    return response()->json($mnsj_str);
                }

            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }


    public function datosEmpresaInfo(Request $request)
    {
        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $validaciones = $this->validaciones($request, 'empresa_info');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                // Guardando los datos de la solicitud desencriptados en la nueva tabla
                $empresa = Empresa::firstOrNew(
                    [
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ]);

                $empresa->ventas_mensuales              = $request->ventas_mensuales;
                $empresa->margen_ventas                 = mb_strtoupper($request->margen_ventas);
                $empresa->numero_empleados              = mb_strtoupper($request->numero_empleados);
                $empresa->unico_dueño                   = ($request->input("unico_dueno") == 'Si') ? 1 : 0;
                //$empresa->otros_nombres_comerciales     = mb_strtoupper($request->otros_nombres_comerciales);
                $empresa->save();

                //Guarda informacion de nombres comerciales
                $nombres_comerciales = [
                    'nombre_1' => $request->nombre_comercial_1,
                    'nombre_2' => $request->nombre_comercial_2,
                    'nombre_3' => $request->nombre_comercial_3,
                    'nombre_4' => $request->nombre_comercial_4,
                    'nombre_5' => $request->nombre_comercial_5
                ];

                $this->guardaNombresComerciales($solicitud, $nombres_comerciales);

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Datos negocio',
                    1,
                    'Los datos del negocio se han guardado con éxito',
                );

                $mnsj_str = [
                    'success'       => true
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->sub_status = 'Datos Negocio';
                $solicitud->save();

                $eventTM = ['event' => 'DatosNegocio', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $mnsj_str['eventTM'] = $eventTM;

                return response()->json($mnsj_str);
            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }

    public function datosSocio(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $validaciones = $this->validaciones($request, 'datos_socios');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                // Guardando los datos de la solicitud desencriptados en la nueva tabla
                $socio = Socio::firstOrNew(
                    [
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ]);

                $socio->nombre_completo             = $request->nombre_completo;
                $socio->fecha_nacimiento            = $request->fecha_nacimiento;
                $socio->email                       = mb_strtoupper($request->email_socio);
                $socio->telefono                    = $request->telefono;
                $socio->direccion_completa          = mb_strtoupper($request->direccion_completa);
                $socio->porcentaje_participacion    = $request->porcentaje_participacion;
                $socio->save();

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Datos socio',
                    1,
                    'Los datos de empresa se han guardado con éxito',
                );

                $mnsj_str = [
                    'success'       => true
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->sub_status = 'Datos Socio';
                $solicitud->save();

                $eventTM = ['event' => 'DatosSocio', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $mnsj_str['eventTM'] = $eventTM;

                return response()->json($mnsj_str);
            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }

    public function datosSocioInfo(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($solicitud) {

                $validaciones = $this->validaciones($request, 'socios_info');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                // Guardando los datos de la solicitud desencriptados en la nueva tabla
                $empresa = Empresa::firstOrNew(
                    [
                    'prospecto_id'  => $prospecto->id,
                    'solicitud_id'  => $solicitud->id
                ]);

                $modal = '';

                $empresa->monto_cuentas_cobrar        = $request->monto_cuentas_cobrar;
                $empresa->deudas_impuestos            = $request->deudas_impuestos;
                $empresa->monto_impuestos             = $request->monto_impuestos;
                $empresa->nss                         = $request->nss;
                $empresa->licencia_conducir           = $request->licencia_conducir;
                $empresa->deudas_comerciales          = $request->deudas_comerciales;

                $empresa->save();

                //Guarda informacion de nombres comerciales
                $deudas_empresa = [
                    'deuda_1' => $request->deuda_empresa_1,
                    'deuda_2' => $request->deuda_empresa_2,
                    'deuda_3' => $request->deuda_empresa_3,
                    'deuda_4' => $request->deuda_empresa_4,
                    'deuda_5' => $request->deuda_empresa_5
                ];

                $this->guardaDeudas($solicitud, $deudas_empresa);

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Datos extra negocio',
                    1,
                    'Los datos extra del negocio se han guardado con éxito',
                );

                $mnsj_str = [
                    'success'       => true,
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->sub_status = 'Datos Extra Negocio';
                $solicitud->save();
                $eventTM = ['event' => 'DatosExtraNegocio', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $mnsj_str['eventTM'] = $eventTM;

                return response()->json($mnsj_str);

            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }

    public function datosNegocio(Request $request) {

        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();
            $empresa = Empresa::where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($empresa) {

                $validaciones = $this->validaciones($request, 'cuentas_credito');
                if ($validaciones != '') {
                    return response()->json($validaciones);
                }

                $empresa->depositos_mensuales      = $request->deposito_mensual_promedio;
                $empresa->saldo_mensual            = $request->saldo_final;
                $empresa->save();

                // Actualizando el campo ult_punto_reg de la solicitud
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    'Cuentas de Crédito',
                    1,
                    'Los datos de cuentas de crédito se han guardado con éxito',
                );

                $mnsj_str = [
                    'success'       => true,
                    'message'           => 'Los datos de cuentas de crédito se han guardado con éxito',
                ];

                // Guardando el mensaje en el campo ult_mensj_a_usuario
                $this->solicitudRepository->setUltMnsjUsr(
                    $solicitud,
                    json_encode($mnsj_str)
                );
                $solicitud->sub_status = 'Cuentas de Crédito';
                $solicitud->autorizado = ($request->input("acepto_consulta")) ? 1 : 0;
                $solicitud->save();

                $eventTM = ['event' => 'PreAprobado', 'userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $mnsj_str['eventTM'] = $eventTM;

                return response()->json($mnsj_str);
            } else {
                return response()->json([
                    'success'   => false,
                    'message'   => 'La solicitud no existe'
                ]);
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }
    }

    public function oferta() {
        if (Auth::guard('prospecto')->check()) {

            $prospecto_id = Auth::guard('prospecto')->user()->id;
            $prospecto = Prospecto::find($prospecto_id);
            $solicitud = Solicitud::with('producto','datos_socios','datos_empresa')
                ->where('prospecto_id', $prospecto_id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $monto = $solicitud->prestamo;
            $plazo = $solicitud->plazo;
            $pago_estimado = $solicitud->pago_estimado;

            //Guardando el detalle de la solicitud en la MR
            $detalleSolicitud = $this->solicitudRepository->saveDetalleSolicitud($solicitud, $prospecto);
            try {
                $resultado = DB::connection('mysql_maquina_riesgos')
                    ->select("CALL SP_CReal_Pago ({$solicitud->id})");
            } catch (\Exception $e) {
                $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                    'prospecto_id'  => $prospecto_id,
                    'solicitud_id'  => $solicitud->id,
                ], [
                    'ejecucion_sp'        => 0,
                    'status_ejecucion_sp' => $e->getMessage()
                ]);

                $mnsj_str = [
                    'success' => false,
                    'error_message' => $e->getMessage()
                ];

                return response()->json($mnsj_str);

            }
            // Si el store procedure arroja un resultado se mostrara el modal segun
        // el cálculo
        if (count($resultado) > 0) {
            if (!isset($resultado[0]->TipoError)) {

                //update the solics ult_punto_reg
                $this->solicitudRepository->setUltPuntoReg(
                    $solicitud->id,
                    $solicitud->status,
                    "Stored Procedure",
                    1,
                    'El stored procedure se ejecuto con éxito. Obteniendo plantilla de comunicación'
                );

                    $monto = $resultado[0]->Monto_Mod;
                    setlocale(LC_MONETARY, 'en_US');
                    $monto = number_format($monto);
                    $plazo = $resultado[0]->Plazo_Mod;
                    $tasa = $this->tasaOferta($resultado[0]->Tasa_Mod);
                    $pago = $this->pagoOferta($resultado[0]->Pago_Mod);


                    $resultadoMR = RespuestaMaquinaRiesgo::updateOrCreate([
                        'prospecto_id'  => $prospecto_id,
                        'solicitud_id'  => $solicitud->id,
                        //'tipo_oferta'   => $tipoOferta
                    ], [
                        'ejecucion_sp'                  => 1,
                        'status_ejecucion_sp'           => 'Resultado Máquina de Riesgos',
                        'decision'                      => $resultado[0]->Decision,
                        'stored_procedure'              => $resultado[0]->stored_procedure,
                        'plantilla_comunicacion'        => $resultado[0]->plantilla_comunicacion,
                        'pantallas_extra'               => $resultado[0]->pantallas_extra,
                        'situaciones'                   => '',
                        'monto'                         => $resultado[0]->Monto_Mod,
                        'plazo'                         => $resultado[0]->Plazo_Mod,
                        'pago'                          => $resultado[0]->Pago_Mod,
                        'tasa'                          => $resultado[0]->Tasa_Mod,
                        'simplificado'                  => $resultado[0]->simplificado,
                        'facematch'                     => '1',
                        'carga_identificacion_selfie'   => '',
                        'subir_documentos'              => $resultado[0]->subir_documentos,
                        'oferta_minima'                 => '',
                        'tipo_poblacion'                => $resultado[0]->tipo_poblacion,
                    ]);


            } else {

                // Regresa un error si la ejecución del stored procedure falla
                $mnsj_str = [
                    'success' => false,
                    'error_message' => 'Error al ejecutar máquina riesgos: '.$resultado[0]->DescripcionError
                ];

                return response()->json($mnsj_str);
                }
            } else {

                // Regresa un error si la ejecución del stored procedure no arroja
                // nigún resultado
                $mnsj_str = [
                    'success' => false,
                    'error_message' => 'La máquina de riesgos no arrojo ningun resultado'
                ];

                return response()->json($mnsj_str);

            }


            $solicitud->status = 'Invitación a Continuar';
            $solicitud->sub_status = 'Invitación a Continuar';
            $solicitud->save();

            $this->solicitudRepository->setUltPuntoReg(
                $solicitud->id,
                "Invitacón a Continuar",
                "Invitación a Continuar",
                1,
                "Se mostró la pantalla de oferta con éxito."
            );

            $motivos = MotivoRechazo::pluck('motivo');

             return view('parts.aprobacion',[
                'monto' => $monto,
                'plazo' => $plazo,
                'pago_estimado' => $pago,
                'nombre' => $prospecto->nombres,
                'motivos'   => $motivos,

             ]);



        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'La sesión expiro'
            ]);

        }

    }

    public function registroCognito(Request $request) {

        $cognito = new CognitoRepository;
        $apellidos = explode(" ", $request->apellidos);
        
        if (count($apellidos) > 1)  {
            $apellidos = mb_strtoupper("$apellidos[0]} {$apellidos[0]}");
        } else {
            $apellidos = mb_strtoupper($request->apellidos);
        }
        $fecha = Carbon::now('UTC');

        if ($request->celular == "5578511626" || $request->celular == "5618474957" || $request->celular == "5566690083" || $request->celular == "5581573000" || $request->celular == "5510682890") {
            $area_code = '+52';
        } else {
            $area_code = '+1';
        }

        $datos_usuario = [
            'name'                  => mb_strtoupper($request->nombres),
            'middle_name'           => $apellidos,
            'phone_number'          => $area_code.$request->celular,
        ];

        $cognito = $cognito->createUserCognito(mb_strtolower($request->email), $request->contraseña, $datos_usuario);

        return $cognito;

    }

    public function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }

    function guardaNombresComerciales($solicitud, $nombres) {
        Empresa::updateOrCreate([
            'solicitud_id'              => $solicitud->id,
        ], [
            'otros_nombres_comerciales'    => json_encode($nombres, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        ]);

    }

    function guardaDeudas($solicitud, $deudas) {
        Empresa::updateOrCreate([
            'solicitud_id'              => $solicitud->id,
        ], [
            'deudas_empresa'    => json_encode($deudas, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        ]);

    }

    function str_replaceChars ($str){
        return str_replace(array("$", " ", ","), "", $str);
      }

    /**
     * Determina el contenido del monto a mostrar en el modal oferta
     *
     * @param  string $monto Valor regresado por la máquina de riesgos
     * @return retun         Valor a mostrar en el modal oferta
     */
    public function montoOferta($monto) {

        switch ($monto) {
            case '-1':
                $monto = 'Por definir en tu oferta final';
                break;

            default:
                $monto = '$'.number_format($monto, 2, ".", ",");
            break;
        }

        return $monto;

    }

    /**
     * Determina el contenido de la tasa a mostrar en el modal oferta
     *
     * @param  string $tasa Valor regresado por la máquina de riesgos
     * @return retun        Valor a mostrar en el modal oferta
     */
    public function tasaOferta($tasa) {

        switch ($tasa) {
            case '-1':
                $tasa = '15 puntos menos que la tasa de la tarjeta que deseas pagar';
                break;

            default:
                $tasa = number_format($tasa, 2, ".", ",").'%';
            break;
        }

        return $tasa;

    }

    /**
     * Determina el contenido del pago a mostrar en el modal oferta
     *
     * @param  string $pago  Valor regresado por la máquina de riesgos
     * @param  string $plazo Valor regresado por la máquina de riesgos
     * @param  string $monto Valor regresado por la máquina de riesgos
     * @return retun         Valor a mostrar en el modal oferta
     */
    public function pagoOferta($pago) {

        $etiquetafin = '';
        $etiquetainicio = '';
        $pago = $etiquetainicio.'$'.number_format($pago, 2, ".", ",").$etiquetafin;

        return $pago;

    }

    function validacionesSolicitud($datosSolicitud, $formulario) {

        $reglas = null;
        $mensajes = [
            'required'                      => 'El campo es obligatorio.',
            'calle.required'                => 'El campo calle es obligatorio.',
            'no_exterior.required'          => 'El campo número exterior es obligatorio.',
            'password.regex'                => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
            'confirma_password.same'        => 'El campo contraseña y confirma contraseña deben ser iguales.',
            'new_password.regex'            => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y debe ser al menos de 8 dígitos.',
            'confirm_password.same'         => 'El campo contraseña y confirma contraseña deben ser iguales.',
            'numeric'                       => 'El campo debe contener solo números.',
            'email'                         => 'El email no es válido.',
            'digits'                        => 'El campo debe tener :digits números.',
            'ingreso_mensual.min'           => 'El ingreso mensual debe tener al menos 4 dígitos',
            'alpha_numeric_spaces_not_html' => 'El campo solo puede contener caracteres válidos: [A-Z][0-9] o espacios',
            'validacion_combos'             => 'El campo solo puede contener caracteres válidos: [A-Z][0-9], -, parentesis o espacios',
            'alpha_spaces_not_html'         => 'El campo solo puede contener caracteres válidos: [A-Z]',
            'max'                           => 'El campo no puede ser mayor a :max caracteres',
            'min'                           => 'El campo no puede ser menor a :max caracteres',
            'rfc'                           => 'El RFC esta formado por 4 letras y 6 números (2 del año, 2 del mes y 2 del día de la fecha de nacimiento)',
            'uuid'                          => 'El valor uuid es incorrecto.',
        ];

        switch ($formulario) {
            case 'simulador':
                $reglas = [
                    'monto_prestamo'    => 'required',
                    'finalidad_custom'  => 'required|alpha_numeric_spaces_not_html'
                ];
                /*$mensajesSimulador = [
                    'monto_prestamo.max' => 'El monto no puede ser mayor a $'.number_format($datosSolicitud['monto_maximo'], 0),
                    'monto_prestamo.min' => 'El monto no puede ser menor a $'.number_format($datosSolicitud['monto_minimo'], 0),
                ];*/
                //$mensajes = array_merge($mensajes, $mensajesSimulador);
            break;
            case 'registro-simulador':
                $reglas = [
                    'monto_prestamo'            => 'required',
                    //'plazo'                     => 'required',
                    'finalidad_custom'          => 'required|alpha_numeric_spaces_not_html',
                    'nombres'                   => 'required|alpha_spaces_not_html',
                    'apellidos'                 => 'required|alpha_spaces_not_html',
                    //'apellido_paterno'          => 'required|alpha_spaces_not_html',
                    'celular'                   => 'required|max:13',
                    'email'                     => 'required|email',
                    'contraseña'                => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                    'confirmacion_contraseña'   => 'required|same:contraseña',
                ];
            break;
            case 'registro-actualizar-password':
                $reglas = [
                    'new_password'          => 'required|regex:/(^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})$)/u',
                    'confirm_password'      => 'required|same:new_password',
                    'codigo_verificacion'   => 'required|numeric|digits:6',
                ];
            break;
            case 'datos_empresa':
                $reglas = [
                    'nombre_empresa'   => 'required|alpha_spaces_not_html',
                    'tipo_propiedad'   => 'required|validacion_combos',
                    'descripcion_empresa'     => 'required|alpha_spaces_not_html',
                    'location'         => 'required|alpha_numeric_spaces_not_html|max:100',
                    'postal_code'      => 'required|digits:5',
                    'locality'         => 'required',
                    'administrative_area_level_1'        => 'required',
                ];
            break;
            case 'empresa_info':
                $reglas = [
                    'ventas_mensuales'    => 'required|max:20',
                    'margen_ventas'       => 'required|alpha_numeric_spaces_not_html|max:5',
                    'numero_empleados'    => 'required|validacion_combos',
                    'unico_dueno'         => 'required',
                    'otros_nombres'        => 'alpha_numeric_spaces_not_html|max:50',
                ];
            break;
            case 'datos_socios':
                $reglas = [
                    'nombre_completo'           => 'required|alpha_spaces_not_html',
                    'fecha_nacimiento'          => 'required|date',
                    'email_socio'               => 'required|email',
                    'telefono'                  => 'required|digits:10',
                    'direccion_completa'        => 'required|alpha_numeric_spaces_not_html',
                    'porcentaje_participacion'  => 'digits:2',
                ];
            break;
            case 'socios_info':
                $reglas = [
                    'monto_cuentas_cobrar'      => 'required|max:20',
                    'deudas_impuestos'          => 'required|alpha_numeric_spaces_not_html',
                    'deudas_comerciales'        => 'required',
                    'monto_impuestos'           => 'required',
                    'nss'                       => 'required|alpha_numeric_spaces_not_html|digits:9',
                    'licencia_conducir'         => 'required|alpha_numeric_spaces_not_html|digits:10',
                ];
            break;
            case 'cuentas_credito':
                $reglas = [
                    'acepto_consulta'                => 'required',
                    'deposito_mensual_promedio'       => 'required|max:20',
                    'saldo_final'             => 'required|max:20',
                ];
            break;
        }

        $validaciones = Validator::make($datosSolicitud, $reglas, $mensajes);
        return $validaciones;

    }
}
