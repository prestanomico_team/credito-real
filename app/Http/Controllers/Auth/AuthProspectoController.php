<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Models\Solicitud;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthProspectoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Path to login.
     *
     * @var string
     */
    protected $loginPath = '/';


    /**
     * Handle an logout.
     *
     * @return Response
     */
    public function logout(Request $request) {

        $redireccion = null;
        if ($prospecto = Auth::guard('prospecto')->user()) {
            $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
                ->orderBy('created_at', 'DESC')
                ->first();
            if ($solicitud->producto()->exists()) {
                $redireccion = $solicitud->producto[0]['redireccion'];
            }
        }

        Auth::guard('prospecto')->logout();

        if ($redireccion != null || $redireccion != '') {
            return redirect($redireccion);
        } else {
            return redirect('/');
        }

    }

    /**
     * Guard captura
     */
    protected function guard()
    {
        return Auth::guard('prospecto');
    }
}
