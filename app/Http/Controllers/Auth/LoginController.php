<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Str;
use Socialite;
use App\Models\User;
use App\Models\Role;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Crea una nueva instancia del controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Redirige al usuario a la pagina de autenticación del provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::with($provider)->redirect();
    }

    /**
     * Obtiene la información del usuario del provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        if ($provider == 'live') {

            $datosUserLogin = Socialite::driver($provider)->fields([
                'id',
                'displayName',
                'userPrincipalName',
                'mail',
                'jobTitle',
                'officeLocation',
                'department',
                'givenName',
                'surname',
            ])->stateless()->user();

            $email = $datosUserLogin->email;
            $dominio = explode('@', $email, 2);

            if ($dominio[1] == 'prestanomico.com') {

                $area =  mb_strtolower($datosUserLogin->user['department']);
                $puesto = mb_strtolower($datosUserLogin->user['jobTitle']);

                $usuario = User::updateorCreate(
                    [
                        'email' => $email,
                    ],
                    [
                        'name'      => $datosUserLogin->name,
                        'area'      => Str::title($area),
                        'puesto'    => Str::title($puesto),
                    ]
                );

                $rol = [];
                if ($area == 'it') {
                    $rol = Role::where('name', $area)->get();
                } else {
                    $nombre_rol = $puesto.'.'.$area;
                    $rol = Role::where('name', $nombre_rol)->get();
                }

                if (count($rol) == 1) {
                    $roles_usuario = $usuario->roles()->get();
                    $roles = [];
                    foreach ($roles_usuario as $rol_usuario) {
                        $roles[] = $rol_usuario->id;
                    }
                    $roles[] = $rol[0]->id;
                    $usuario->roles()->sync($roles);
                }

                auth()->login($usuario, true);
                return redirect()->to('panel');

            } else {
                abort(403, 'El correo con el que iniciaste sesión no es válido');
            }
        } else {
            abort(403, 'El agente no es válido');
        }

    }

    /**
     * Muestra el formulario de login para el usuario del panel
     *
     * @return view Vista del formulario
     */
    protected function showLoginCaptura()
    {
        return view('auth.login');
    }

    /**
     * Login prospecto
     *
     * @param  Request $request  Datos enviados del formulario
     *
     * @return redirect          Redirección a la pantalla correspondiente según
     * el resultado de autenticar al usuario
     */
    protected function loginCaptura(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::guard('captura_convenio')->attempt($credentials)) {

            return redirect('/panel/producto/captura/'.$request->producto);

        } else {
            echo 'El usuario no existe o la contraseña es incorrecta';
            return redirect('/captura_solicitud/'.$request->producto)->with('notLogin', 'El usuario no existe o la contraseña es incorrecta');
        }
    }

    protected function logoutCaptura(Request $request)
    {
        Auth::guard('captura_convenio')->logout();
        return redirect('/captura_solicitud/'.$request->producto)->with('logut', 'El usuario cerro la sesión');
    }
}
