<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Solicitud;
use App\Repositories\SolicitudRepository;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Models\PlantillaComunicacion;
use App\Models\CargaDocumentos;
use App\Models\ClientesAltaEmpresa;
use App\Jobs\ComprobantesIngreso;
use Log;

class ComprobanteIngresosController extends Controller
{
    private $solicitudRepository;

    public function __construct()
    {
        $this->solicitudRepository = new SolicitudRepository;
    }

    public function index() {

        $prospecto = Auth::guard('prospecto')->user();
        $prospecto_id = $prospecto->id;
        $solicitud = $this->solicitudActual($prospecto->id);
        $solicitud_id = $solicitud->id;
        $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id);
        //$pasos_habilitados = $this->solicitudRepository->pasosHabilitadosProducto($solicitud);
        $producto = null;
        if ($solicitud->producto()->exists()) {
            $producto = $solicitud->producto[0]['alias'];
        }

        \GoogleTagManager::set(['solicId' => $solicitud_id, 'userId' => $prospecto_id, 'producto' => $producto]);

        if ($siguientePaso == null) {
            return view('webapp.comprobante_ingresos', ['producto' => $producto]);
        } else {
            return $siguientePaso;
        }

    }

    public function save(Request $request) {

        ini_set('memory_limit','256M');
        if (Auth::guard('prospecto')->check()) {

            $prospecto = Auth::guard('prospecto')->user();
            $prospecto_id = $prospecto->id;
            $solicitud = $this->solicitudActual($prospecto->id);
            $solicitud_id = $solicitud->id;
            $id = 0;

            if ($request->hasFile('file')) {

                $files = $request->file('file');
                $finfo = finfo_open(FILEINFO_MIME); // Devuelve el tipo mime del tipo extensión

                foreach ($files as $key => $file) {

                    $tipo_archivo = finfo_file($finfo, $file);
                    $archivo = explode(";", $tipo_archivo);

                    if ($archivo[0] == "image/icon" || $archivo[0] =="image/jpeg" || $archivo[0]== "image/png" || $archivo[0]== "image/tiff" || $archivo[0]== "image/bpm" ||$archivo[0]== "application/pdf") {

                        if ($file->isValid()) {
                            $id = $id + 1;
                            $extension = $file->getClientMimeType();
                            $comprobante = $file;
                            if (strpos($extension, 'image') !== false) {
                                $extension = 'jpg';
                            } elseif (strpos($extension, 'pdf') !== false) {
                                $extension = 'pdf';
                            }

                            $tipo_documento = 'comprobante_ingresos';
                            
                            $detalle_documento = 'quin_detail';
                    
                            $root = "proceso_simplificado";
                            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$id}.jpg")) {
                                Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$id}.jpg");
                            }
                            if (Storage::disk('s3')->exists("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$id}.pdf")) {
                                Storage::disk('s3')->delete("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$id}.pdf");
                            }
                            Storage::disk('s3')->put("{$root}/{$prospecto_id}_{$solicitud_id}_{$tipo_documento}_{$detalle_documento}_{$id}.{$extension}", file_get_contents($comprobante));
                        }
                    } else {
                        return response()->json([
                            'success'           => false,
                            'message'           => 'Archivo no valido',
                            'archivo_invalido'  => true,
                        ]);
                    }

                }

                CargaDocumentos::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->update([
                        'comprobante_ingresos_completo' => 1,
                        //'frecuencia'                    => $request->frecuencia,
                        //'tipo_comprobante'              => $request->tipo_comprobante,
                        'detalle_documento'             => $detalle_documento,
                        'numero_comprobantes'           => $id
                    ]);

                $clienteAlta = ClientesAltaEmpresa::where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $job = (new ComprobantesIngreso($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
                dispatch($job);

                $log = [
                    'success' => true,
                    'msj'     => 'Comprobantes de ingreso cargados',
                ];
                $this->solicitudRepository->setUltMnsjUsr($solicitud, json_encode($log));
                $solicitud->save();

                $siguientePaso = $this->siguientePaso($prospecto_id, $solicitud_id, false);

                //$siguientePaso = null;
                if ($siguientePaso == null) {

                    $modal = view("modals.modalFinalizar")->render();
                    $this->solicitudRepository->setUltPuntoReg(
                        $solicitud->id,
                        $solicitud->status,
                        'Oferta Aceptada - Carga Documentos Completada',
                        1,
                        'Se ha completado la carga de documentos en el Panel Operativo'
                    );
                    $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
                    $solicitud->save();

                    Auth::guard('prospecto')->logout();

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Comprobantes de ingresos guardado exitosamente',
                        'modal'             => $modal,
                    ]);

                } else {

                    return response()->json([
                        'success'           => true,
                        'message'           => 'Comprobantes de ingresos guardado exitosamente',
                        'siguiente_paso'    => $siguientePaso,
                    ]);

                }
            }

        } else {

            return response()->json([
                'success'   => false,
                'reload'    => true,
                'message'   => 'Sesión expirada',
            ]);

        }

    }

    /**
     * Genera el job para procesamiento del alta de Comprobantes de Ingreso mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function procesar(Request $request) {

        $clienteAlta = ClientesAltaEmpresa::find($request->idAltaCliente);
        $job = (new ComprobantesIngreso($clienteAlta))->onQueue(env('QUEUE_NAME', 'default'))->delay(10);
        dispatch($job);

        return response()->json(['success' => true]);

    }

    public function solicitudActual($prospecto_id) {

        // Obteniendo la última solicitud del prospecto
        $solicitud = Solicitud::where('prospecto_id', $prospecto_id)
            ->orderBy('id', 'desc')
            ->first();

        return $solicitud;

    }

    public function siguientePaso($prospecto_id, $solicitud_id, $redirect = true) {

        $siguientePaso = $this->solicitudRepository->siguientePasoDocumentos($prospecto_id, $solicitud_id);

        if ($siguientePaso != 'comprobante_ingresos' && $redirect == true) {
            return redirect('/webapp/'.$siguientePaso);
        } elseif ($siguientePaso != 'comprobante_ingresos' && $redirect == false) {
            return $siguientePaso;
        } else {
            return null;
        }

    }
}
