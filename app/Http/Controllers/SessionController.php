<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Solicitud;
use App\Models\Prospecto;
use App\Models\Plazo;
use Twilio\Rest\Client;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Models\Analytic;
use App\Models\Situacion;
use App\Models\PlantillaComunicacion;
use Jenssegers\Agent\Agent;
use Cookie;
use App\Models\CodigosSMS;
use App\Models\ClientesAlta;
use App\MB_UPLOADED_DOCUMENT;
use Illuminate\Support\Facades\Storage;
use App\Repositories\SolicitudRepository;
use App\Repositories\LDAPRepository;
use App\Models\RespuestaMaquinaRiesgo;
use App\Models\MotivoRechazo;
use App\Repositories\CalixtaRepository;
use App\Repositories\CognitoRepository;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Lang;

class SessionController extends Controller
{
    use ThrottlesLogins;

    private $solicitudRepository;
    private $ldap;
    private $cognito;
    public $maxAttempts = 3;
    public $decayMinutes = 15;

    public function __construct () {
        $this->solicitudRepository = new SolicitudRepository;
        $this->cognito = new CognitoRepository;
    }

    /**
    * Hace el login de un usuario registrado en LDAP
    *
    * @param  request $request Arreglo con los datos del usuario que se quiere loguear
    *
    * @return json Resultado de realizar el login
    */
    public function login(request $request, CognitoRepository $cognito, CalixtaRepository $calixta) {

        $times = $this->incrementLoginAttempts($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $data = $request->toArray();
        $data['email'] = mb_strtolower($data['email']);
        
        $login = $cognito->authenticate($data['email'], $data['password']);

        if ($login['success'] == true) {

            // Si el login fue exitoso en Cognito, buscamos los datos del prospecto en la BD
            $prospecto = Prospecto::where('email', $data['email'])->first();
            if ($prospecto) {

                $this->clearLoginAttempts($request);

                $solicitud = Solicitud::where('prospecto_id', $prospecto->id)
                    ->orderBy('created_at', 'DESC')
                    ->first();

                // Actualizando la contraseña
                $password  = $data['password'];
                Prospecto::where('email', $data['email'])
                    ->update(['encryptd' => encrypt($password)]);

                $agent = new Agent();
                $prospecto_id = $prospecto->id;
                $tipo_dispositivo = null;
                $marca =  null;

                if ($agent->isDesktop()) {
                    $tipo_dispositivo = 'desktop';
                    $marca = $agent->device();
                }
                if ($agent->isMobile()) {
                    $tipo_dispositivo = 'mobile';
                    $marca = $agent->device();
                }
                if ($agent->isTablet()) {
                    $tipo_dispositivo = 'tablet';
                    $marca = $agent->device();
                }

                $analytic = Analytic::create([
                    'prospecto_id'      => $prospecto_id,
                    'solicitud_id'      => $solicitud->id,
                    'client_id'         => $request->input('clientId'),
                    'telefono'          => $prospecto->celular,
                    'email'             => $data['email'],
                    'ip'                => $this->getUserIP(),
                    'sistema_operativo' => $agent->platform(),
                    'navegador'         => $agent->browser(),
                    'navegador_version' => $agent->version($agent->browser()),
                    'tipo_dispositivo'  => $tipo_dispositivo,
                    'marca'             => $marca,
                    'datos_obtenidos'   => 'Sin datos',
                    'fecha_hora'        => date('YmdHi'),
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);

                $prospecto->login = true;
                $prospecto->save();
                $siguientePaso = $this->solicitudRepository->siguientePaso(null, $prospecto, true);

                // La solicitud esta inconclusa y necesita autenticarse para continuar
                if ($siguientePaso['formulario'] == 'verificar_codigo') {
                    if  (isset($siguientePaso['view'])) {
                        $respuesta = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                            ->where('solicitud_id', $solicitud->id)
                            ->get();
                        $motivos = MotivoRechazo::pluck('motivo');

                        $siguientePaso['view'] = view('parts.aprobacion',[
                            'monto' => $respuesta[0]->monto,
                            'plazo' => $respuesta[0]->plazo,
                            'pago_estimado' => $respuesta[0]->pago,
                            'nombre' => $prospecto->nombres,
                            'motivos'   => $motivos,
                        ])->render();
                    }
                    $codigo = mt_rand(10000, 99999);
                    $msj = "Código de Verificación: ". $codigo;
                    //$responsecalixta = $calixta->sendSMS($prospecto->celular, $msj);
                    //$responsecalixta = json_decode($responsecalixta);

                    if ($prospecto->celular == '5578511626' || $prospecto->celular == '5618474957' || $prospecto->celular == '5566690083' || $prospecto->celular == '5581573000' || $prospecto->celular == '5510682890') {
                        $area_code = '+52';
                        
                    } else {
                        $area_code = "+1";
                    }
                    
                    $token = getenv("TWILIO_AUTH_TOKEN");
                    $twilio_sid = getenv("TWILIO_SID");
                    $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
                    $twilio = new Client($twilio_sid, $token);
                    $twilio->verify->v2->services($twilio_verify_sid)
                        ->verifications
                        ->create($area_code.$prospecto->celular, "sms");

                    $siguientePaso['success'] = true;
                    $plazo = Plazo::find($prospecto->ultima_solicitud->plazo);
                    if (isset($prospecto->ultima_solicitud->oferta_aceptada->monto)) {
                        $prestamo =  $prospecto->ultima_solicitud->oferta_aceptada->monto;
                        $plazo = $prospecto->ultima_solicitud->oferta_aceptada->plazo;
                    } elseif (isset($prospecto->ultima_solicitud->oferta_preaprobada->monto)) {
                        $prestamo =  $prospecto->ultima_solicitud->oferta_preaprobada->monto;
                        $plazo = $prospecto->ultima_solicitud->oferta_preaprobada->plazo;
                    } else {
                        $prestamo =  '$ '.number_format($prospecto->ultima_solicitud->prestamo, 2);
                        $plazo = "{$plazo->duracion} {$plazo->plazo}";
                    }
                    $siguientePaso['prestamo'] = $prestamo;
                    $siguientePaso['plazo'] = $plazo;
                    $siguientePaso['finalidad'] = $prospecto->ultima_solicitud->finalidad;
                    $siguientePaso['celular'] = $prospecto->celular;
                    Auth::guard('prospecto')->loginUsingId($prospecto_id);

                    

                // La solicitud esta en un status de terminación exitoso o fallido y
                // debe de empezar una nueva
                } else {
                    $prospecto->login = false;
                    $prospecto->save();
                    $siguientePaso['success'] = true;
                    $siguientePaso['formulario'] = 'simulador';
                    session()->flash('nueva_solicitud', true);
                    Auth::guard('prospecto')->loginUsingId($prospecto_id);
                }

                $eventTM[] = ['userId'=> $prospecto->id, 'solicId' => $solicitud->id, 'clientId' => $request->clientId];
                $siguientePaso['eventTM'] = $eventTM;

                $siguientePaso['nombre_prospecto'] = "{$prospecto->nombres} {$prospecto->apellido_paterno} {$prospecto->apellido_materno}";
                return response()->json($siguientePaso);

            } else {

                $login = (array) $login;
                $login['message'] = 'El usuario o contraseña son incorrectos.';
                $login['success'] = false;
                return response()->json($login);
            }

        } else {

            if ($login['message'] == 'PasswordResetRequiredException') {
                $login['message'] = 'Se debe completar la solicitud de restauración de contraseña.';
            } else if ($login['message'] == 'UserNotFoundException') {
                $login['message'] = 'Usuario y/o contraseña incorrectos.';
            } else if ($login['message'] == 'Incorrect username or password.') {
                $login['message'] = 'Usuario y/o contraseña incorrectos.';
            }

            return response()->json($login);

        }

    }

    public function restaurarContraseña($datos) {
        /*
        $response = \EmailChecker::check('ferry.germanc_e615p@anom.xyz');
        print_r($response);
        die();
        $response = $this->cognito->sendResetLink('deivguerrero@gmail.com');
        print_r($response);
        */
    }

    public function username() { return 'email'; }

    protected function incrementLoginAttempts(Request $request)
    {
        return ($this->limiter()->hit(
            $this->throttleKey($request), $this->decayMinutes()
        ));
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        if ($seconds > 60) {
            $minutes = gmdate('i', $seconds);
            $message = Lang::get('auth.throttle_minutes', ['minutes' => $minutes]);
        } else {
            $message = Lang::get('auth.throttle', ['seconds' => $seconds]);
        }

        $errors = [
            'success' => false,
            'message' => $message
        ];

        if ($request->expectsJson()) {
            return response()->json($errors, 423);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }
}
