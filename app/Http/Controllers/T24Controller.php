<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClientesAltaEmpresa;

class T24Controller extends Controller
{
    // Contiene la consulta que se ejecuta para la obtencion de datos de la
    // tabla clientes_alta
    private $sql_alta_cliente = "*,
        IF(aplica_cliente is null AND alta_cliente = 0 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is null, 'Alta de cliente en proceso',
        IF(aplica_cliente is null AND alta_cliente = 0 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is not null, 'Alta de cliente erronea',
        IF(aplica_cliente is null AND alta_cliente = 1, 'Alta de cliente exitosa',
        IF(aplica_cliente = 1 AND alta_cliente = 0 AND error is null, 'Alta de cliente en proceso',
        IF(aplica_cliente = 1 AND alta_cliente = 0 AND error is not null, 'Alta de cliente erronea',
        IF(aplica_cliente = 1 AND alta_cliente = 1 , 'Alta de cliente exitosa', 'NA'))))))
        as 'status_alta',
        IF(aplica_solicitud is null AND alta_cliente = 1 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is null, 'Alta de solicitud en proceso',
        IF(aplica_solicitud is null AND alta_cliente = 1 AND alta_solicitud = 0 AND usuario_ligado = 0 AND error is not null, 'Alta de solicitud erronea',
        IF(aplica_solicitud is null AND alta_solicitud = 1, 'Alta de solicitud exitosa',
        IF((aplica_cliente = 1 AND alta_cliente = 0) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud por procesar',
        IF((aplica_cliente = 1 AND alta_cliente = 1) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud en proceso',
        IF((aplica_cliente = 1 AND alta_cliente = 1) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is not null, 'Alta de solicitud erronea',
        IF((aplica_cliente = 1 AND alta_cliente = 0) AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is not null, 'Alta de solicitud no procesada',
        IF(aplica_solicitud = 1 AND panel_operativo_id != null , 'Alta de solicitud exitosa',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND error is null, 'Alta de solicitud en proceso',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Alta de solicitud erronea',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 0 AND (error is not null AND error = 'El cliente se registro con un correo diferente'), 'Alta de solicitud en proceso',
        IF(aplica_cliente = 0 AND aplica_solicitud = 1 AND alta_solicitud = 1, 'Alta de solicitud exitosa', 'NA'))))))))))))
        as 'status_solicitud',
        IF(aplica_ligue is null AND alta_cliente = 1 AND alta_solicitud = 1 AND usuario_ligado = 0 AND error is null, 'Ligue de usuario en proceso',
        IF(aplica_ligue is null AND alta_cliente = 1 AND alta_solicitud = 1 AND usuario_ligado = 0 AND error is not null, 'Ligue de usuario erroneo',
        IF(aplica_ligue is null AND usuario_ligado = 1, 'Ligue de usuario exitoso',
        IF(aplica_ligue = 0, 'NA',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 0) AND usuario_ligado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Ligue de usuario por procesar',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Ligue de usuario en proceso',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Ligue de usuario erroneo',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 0) AND usuario_ligado = 0 AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Ligue de usuario no procesado',
        IF(aplica_ligue = 1 AND (aplica_solicitud = 1 AND alta_solicitud = 1) AND usuario_ligado = 1, 'Ligue de usuario exitoso',
        IF(usuario_ligado = 1, 'Ligue de usuario exitoso', 'NA'))))))))))
        as 'status_ligue',
        IF(aplica_facematch = 0 AND facematch is null AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND documentos_facematch is null AND registro_facematch is null, 'Identificación/Selfie pendiente',
        IF(aplica_facematch = 0 AND facematch = 1 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND documentos_facematch = 1 AND registro_facematch = 1, 'Identificación/Selfie exitoso',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 0 OR registro_facematch = 0) AND facematch_error is not null, 'Identificación/Selfie erroneo',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 1 AND registro_facematch = 0) AND facematch_error is not null, 'Identificación/Selfie-Documentos exitoso',
        IF(aplica_facematch = 0 AND facematch = 0 AND solo_carga_identificacion_selfie = 1 AND alta_solicitud = 1 AND (documentos_facematch = 0 AND registro_facematch = 1) AND facematch_error is not null, 'Identificación/Selfie-Registro exitoso',
        IF(aplica_facematch = 1 AND facematch is null AND facematch_error is null AND alta_solicitud = 1, 'Facematch pendiente',
        IF(aplica_facematch = 1 AND facematch = 0 AND facematch_error is null, 'Facematch no procesado',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch is null AND registro_facematch is null AND facematch_error is not null, 'Facematch erroneo',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 1 AND registro_facematch = 1 AND facematch_error is not null, 'Facematch erroneo-Procesos exitos',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 0 AND registro_facematch = 1 AND facematch_error is not null, 'Facematch erroneo-Registro exitoso',
        IF(aplica_facematch = 1 AND facematch = 0 AND documentos_facematch = 1 AND registro_facematch = 0 AND facematch_error is not null, 'Facematch erroneo-Documentos exitoso',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 1 AND registro_facematch = 1, 'Facematch exitoso',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 1 AND registro_facematch = 0, 'Facematch exitoso-Error Registro',
        IF(aplica_facematch = 1 AND facematch = 1 AND documentos_facematch = 0 AND registro_facematch = 1, 'Facematch exitoso-Error Documentos', 'NA'))))))))))))))
        as 'fachematch',
        IF((aplica_ligue = 1 AND usuario_ligado = 0) AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email por procesar',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email en proceso',
        IF((aplica_ligue = 1 AND usuario_ligado = 0) AND aplica_email = 1 AND email_enviado is null AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Envio de email no procesado',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 0 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email erroneo',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 1 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email exitoso',
        IF((aplica_ligue = 1 AND usuario_ligado = 1) AND aplica_email = 1 AND email_enviado = 1 AND (error is not null AND id_email is not null), 'Envio de email exitoso',
        IF(aplica_ligue = 0 AND alta_solicitud = 0 AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email por procesar',
        IF(aplica_ligue = 0 AND alta_solicitud = 1 AND aplica_email = 1 AND email_enviado is null AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email en proceso',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado is null AND (error is not null AND error != 'El cliente se registro con un correo diferente'), 'Envio de email no procesado',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado = 0 AND error is not null, 'Envio de email erroneo',
        IF(aplica_ligue = 0 AND aplica_email = 1 AND email_enviado = 1 AND (error is null OR error = 'El cliente se registro con un correo diferente'), 'Envio de email exitoso',
        IF(email_enviado = 1, 'Envio de email exitoso', 'NA'))))))))))))
        as 'status_email',
        IF ((aplica_comprobante_ingresos = 0 OR aplica_comprobante_ingresos is null) AND alta_comprobante_ingresos is null, 'NA',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos is null and comprobante_ingresos_error is null, 'Alta comprobante ingresos pendiente',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos = 0 and comprobante_ingresos_error is not null, 'Alta comprobante ingresos erronea',
        IF (aplica_comprobante_ingresos = 1 AND alta_comprobante_ingresos = 1, 'Alta comprobante ingresos exitosa', 'NA'))))
        as 'status_comprobante_ingresos'";

    /**
     * Constructor de la clase
     */
    public function __construct()
    {
        date_default_timezone_set('America/Mexico_City');
     }

    /**
     * Muestra la vista para el alta de clientes
     *
     * @param  Request $request Arreglo que contiene los datos que se envian a
     * la vista
     *
     * @return View            Vista para el alta de clientes
     */
    public function altaClientes(Request $request)
    {
        if (!isset($request['search'])) {
            $altas = ClientesAltaEmpresa::selectRaw($this->sql_alta_cliente)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            return view('crm.alta_clientes')->with('altas', $altas);
        } else {
            $term = $request['search'];
            $altas = ClientesAltaEmpresa::selectRaw($this->sql_alta_cliente)
                ->orHavingRaw('prospecto_id LIKE "%'.$term.'%"')
                ->orHavingRaw('solicitud_id LIKE "%'.$term.'%"')
                ->orHavingRaw('status_alta LIKE "%'.$term.'%"')
                ->orHavingRaw('status_solicitud LIKE "%'.$term.'%"')
                ->orHavingRaw('status_ligue LIKE "%'.$term.'%"')
                ->orHavingRaw('CONCAT(SHORTNAME," ",NAME1," ",NAME2) LIKE "%'.$term.'%"')
                ->orHavingRaw('EMAIL LIKE "%'.$term.'%"')
                ->orderBy('created_at', 'desc')
                ->simplePaginate(10);
            $altas->withPath('alta-clientes?search='.$term);

            return view('crm.alta_clientes')->with([
                'altas'=>$altas,
                'search'=>$term
            ]);
        }
    }

    /**
     * Guarda los datos que se mandan en el archivo CSV en la tabla alta clientes
     *
     * @param  Request $request Arreglo que contiene el archivo CSV enviado
     *
     * @return Json             Resultado de la carga del CSV
     */
    public function cargaLayout(Request $request)
    {
        $file = $request->file('file');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $file = $file->move(storage_path().'/app/public', $filename);

            if ($file->getExtension() == 'csv') {
                $location = $file->getPathName();

                Excel::load($location, function ($reader) {
                    $headers = $reader->first()->keys()->toArray();
                    $results = $reader->formatDates(false)->toArray();

                    $clientesAlta = [];

                    foreach ($results as $result) {
                        $customerLoan = [];
                        foreach ($result as $key => $value) {

                            // Construyendo el arreglo CustomerInput (Cliente) y LoanApplication (Solicitud)
                            if (array_key_exists($key, $this->_columnasCustomerLoan) == true) {
                                $campo = $this->_columnasCustomerLoan[$key];

                                if (is_array($campo) == false) {
                                    $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                                } else {
                                    foreach ($campo as $nombre) {
                                        $customerLoan[$nombre] = mb_strtoupper(Self::limpiar_caracteres($value));
                                    }
                                }
                            }
                        }

                        $clientesAlta = $this->procesaDatos($customerLoan);

                        $existe = ClientesAlta::where('MNEMONIC', $clientesAlta['MNEMONIC'])
                            ->where('alta_cliente', 1)
                            ->get()
                            ->count();

                        if ($existe == 0) {
                            $clientesAlta = ClientesAlta::create($clientesAlta);
                            $job = (new AltaCliente($clientesAlta))->delay(30);
                            dispatch($job);
                        } else {
                            $clientesAlta['error'] = 'RFC del prospecto ya procesado';
                            $clientesAlta = ClientesAlta::create($clientesAlta);
                        }
                    }
                });

                return response()->json([
                    'success' => 'true'
                ]);
            } else {
                return response()->json([
                    'success' => 'false',
                    'msg' => 'Solo se permiten archivos con extensión .csv'
                ]);
            }
        } else {
            return response()->json([
                'success' => 'false',
                'msg' => 'No se selecciono ningún archivo'
            ]);
        }
    }

    public function arregloDatosAlta($datos) {
        $customerLoan = [];

        foreach ($datos as $key => $value) {
            // Construyendo el arreglo CustomerInput (Cliente) y LoanApplication (Solicitud)
            if (array_key_exists($key, $this->_columnasCustomerLoanAlta) == true) {
                $campo = $this->_columnasCustomerLoanAlta[$key];

                if (is_array($campo) == false) {

                    if ($campo == 'FECNACIMIENTO') {
                        $value = Carbon::createFromFormat('Y-m-d', $datos['fecha_nacimiento'])->format('Ymd');
                        $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                    } else {
                        $customerLoan[$campo] = mb_strtoupper(Self::limpiar_caracteres($value));
                    }

                } else {

                    foreach ($campo as $nombre) {
                        $customerLoan[$nombre] = mb_strtoupper(Self::limpiar_caracteres($value));
                    }

                }
            }
        }

        $customerLoan['DIRPAIS'] = 'MX';

        return $customerLoan;
    }

    /**
     * Convierte los datos enviados en el arreglo de alta al formato necesario
     * por T24
     *
     * @param  array $customerLoan Contiene los datos que se envian en el arreglo
     *
     * @return array               Contiene el array con los datos en el formato
     * necesario para T24
     */
    public function procesaDatosAlta($customerLoan)
    {
        foreach ($customerLoan as $key => $valor) {
            if (array_key_exists($key, $this->_reglasCustomerLoanAlta)) {
                $reglas = $this->_reglasCustomerLoanAlta[$key];
                if ($reglas['parametros'] == 'nombre') {
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['NAME2'] = $valores['NAME2'];
                    $customerLoan['FORMERNAME'] = $valores['FORMERNAME'];
                } elseif ($reglas['funcion'] == 'ids_domicilio') {
                    $valor = $customerLoan;
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['DIRDELMUNI'] = $valores['DIRDELMUNI'];
                    $customerLoan['DIRCIUDAD'] = $valores['DIRCIUDAD'];
                }   else {
                    $nuevo_valor = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan[$key] = $nuevo_valor;
                }
            }
        }

        return $customerLoan;
    }


    /**
     * Convierte los datos enviados en el CSV al formato necesario por T24
     *
     * @param  array $customerLoan Contiene los datos que se envian en el CSV
     *
     * @return array               Contiene el array con los datos en el formato
     * necesario para T24
     */
    public function procesaDatos($customerLoan)
    {
        foreach ($customerLoan as $key => $valor) {
            if (array_key_exists($key, $this->_reglasCustomerLoan)) {
                $reglas = $this->_reglasCustomerLoan[$key];
                if ($reglas['parametros'] == 'direccion') {
                    $valor = $customerLoan;
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['DIRCODPOS'] = $valores['DIRCODPOS'];
                    $customerLoan['DIRCDEDO'] = $valores['DIRCDEDO'];
                    $customerLoan['DIRDELMUNI'] = $valores['DIRDELMUNI'];
                    $customerLoan['DIRCIUDAD'] = $valores['DIRCIUDAD'];
                    $customerLoan['DIRCOLONIA'] = $valores['DIRCOLONIA'];
                    //if ($valores['COLONIACORRECTA'] == 0) {
                    $customerLoan['error'] = $valores['MSGCOLONIA'];
                    //}
                } elseif ($reglas['parametros'] == 'nombre') {
                    $valores = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan['NAME2'] = $valores['NAME2'];
                    $customerLoan['FORMERNAME'] = $valores['FORMERNAME'];
                } else {
                    $nuevo_valor = call_user_func('self::'.$reglas['funcion'], $reglas['parametros'], $valor, $key);
                    $customerLoan[$key] = $nuevo_valor;
                }
            }
        }

        return $customerLoan;
    }

    /**
     * Realiza el filtrado de los datos que se muestran en la lista de altas
     * cuando se realiza una búsqueda
     *
     * @param  Request $request Arreglo que contiene los parametros que se envían
     * para la búsqueda
     *
     * @return json             Resultado del filtrado
     */
    public function autoComplete(Request $request)
    {
        $term = $request->term;

        $results = array();

        $altas = ClientesAltaEmpresa::selectRaw($this->sql_alta_cliente)
            ->orHavingRaw('prospecto_id LIKE "%'.$term.'%"')
            ->orHavingRaw('solicitud_id LIKE "%'.$term.'%"')
            ->orHavingRaw('status_alta LIKE "%'.$term.'%"')
            ->orHavingRaw('status_solicitud LIKE "%'.$term.'%"')
            ->orHavingRaw('status_ligue LIKE "%'.$term.'%"')
            ->orHavingRaw('CONCAT(SHORTNAME," ",NAME1," ",NAME2) LIKE "%'.$term.'%"')
            ->orHavingRaw('EMAIL LIKE "%'.$term.'%"')
            ->orderBy('created_at', 'desc');

        $altas_paginadas = $altas->simplePaginate(10);
        $altas = $altas->take(0)->get();

        $altas_paginadas->withPath('alta-clientes?search='.$term);

        foreach ($altas as $alta) {
            $results['content'][] = [
                'id' => $alta->id,
                'value' => $alta->SHORTNAME.' '.$alta->NAME1
            ];
        }

        $results['view'] =  view('crm.lista_clientes')
            ->with('altas', $altas_paginadas)
            ->render();

        return response()->json($results);
    }

    /**
     * Actualiza la lista de altas de clientes
     *
     * @return json Resultado de actualizar la lista
     */
    public function updateLista()
    {
        $results = array();
        $altas = ClientesAltaEmpresa::selectRaw($this->sql_alta_cliente)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $altas->withPath('alta-clientes');

        $results['view'] =  view('crm.lista_clientes')
            ->with('altas', $altas)
            ->render();
        return response()->json($results);
    }

    /**
     * Establece la fecha a la que se encunetra T24
     */
    public function setDatesT24() {

        if (App::environment(['produccion'])) {

            $holidays = Holiday::select('fecha')->get()->toArray();
            $status_t24 = StatusT24::first();

            $holidays_dates = [];
            foreach ($holidays as $holiday) {
                $holidays_dates[] = strtotime($holiday['fecha']);
            }

            $dia = Carbon::today();

            if (in_array($dia->timestamp, $holidays_dates) || $dia->isWeekend() == true) {
                $status_t24->fecha_alta = 'fecha_t24';
                $status_t24->fecha_actual = date('Ymd');
                $status_t24->save();
            } else {
                $status_t24->fecha_alta = null;
                $status_t24->fecha_actual = date('Ymd');
                $status_t24->save();
            }

        }  else {

            echo 'Disponible solo en Producción';

        }

    }

    /**
     * Genera el job para procesamiento del alta de cliente en T24 mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function altaCliente_T24(Request $request) {

        self::altaClienteT24(null, null, $request->idAltaCliente, null, null);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del alta de solicitud en T24 mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function altaSolicitud_T24(Request $request) {

        self::altaSolicitudT24(null, null, $request->idAltaCliente, null, null);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del ligue de usuario en LDAP mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function ligarUsuarioLDAP(Request $request) {

        self::ligueUsuarioLDAP($request->idAltaCliente);

        return response()->json(['success' => true]);

    }

    /**
     * Genera el job para procesamiento del envío del email y sms mediante el
     * botón que esta en la vista Alta de clientes T24
     *
     * @param  Request $request Object con los parámetros que son enviados
     *
     * @return json             Resultado de generar el job
     */
    public function enviarEmail(Request $request) {

        self::EnvioEmail($request->idAltaCliente);

        return response()->json(['success' => true]);

    }
}
