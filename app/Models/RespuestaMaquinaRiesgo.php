<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestaMaquinaRiesgo extends Model
{
    protected $table = 'respuestas_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'ejecucion_sp',
        'status_ejecucion_sp',
        'decision',
        'stored_procedure',
        'status_oferta',
        'motivo_rechazo',
        'descripcion_otro',
        'plantilla_comunicacion',
        'pantallas_extra',
        'situaciones',
        'tipo_poblacion',
        'tipo_oferta',
        'monto',
        'plazo',
        'pago',
        'tasa',
        'cuestionario_dinamico_guardado',
        'status_guardado',
        'motivo_rechazo',
        'descripcion_otro',
        'oferta_minima',
        'simplificado',
        'facematch',
        'carga_identificacion_selfie',
        'subir_documentos',
        'elegida'
    ];
}
