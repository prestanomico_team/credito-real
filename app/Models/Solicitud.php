<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $columns = array('id','prospecto_id','status');
    protected $table = 'solicitudes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'sub_status',
        'tipo_persona',
        'prospecto_id',
        'plazo',
        'prestamo',
        'finalidad',
        'pago_estimado',
        'equipo_leasing',
        'autorizado',
    ];

    /**
     * Obtiene el prospecto de cada solicitud
     *
     * @return object Datos del prospecto
     */
    public function prospecto() {
        return $this->hasOne('App\Models\Prospecto', 'id', 'prospecto_id');
    }

    /**
     * Obtiene el producto al que pertenece la solicitud
     *
     * @return object Datos del producto
     */
    public function producto() {
        return $this->belongsToMany('App\Models\Producto')->withPivot('version_producto', 'lead', 'lead_id');
    }

    /**
     * Obtiene el tracking de la solcitud
     *
     * @return object Datos del tracking
     */
    public function tracking() {
        return $this->hasMany('App\Models\TrackingSolicitud', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los analytics de la solcitud
     *
     * @return object Datos de analytics
     */
    public function analytic() {
        return $this->hasMany('App\Models\Analytic', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos de la empresa de cada solicitud
     *
     * @return object Datos Empresa
     */
    public function datos_empresa() {
        return $this->hasOne('App\Models\Empresa', 'solicitud_id', 'id');
    }

    /**
     * Obtiene los datos del socio de cada solicitud
     *
     * @return object Datos Socio
     */
    public function datos_socios() {
        return $this->hasOne('App\Models\Socio', 'solicitud_id', 'id');
    }

    /**
     * Obtiene el log de la solcitud
     *
     * @return object Datos del log
     */
    public function log() {
        return $this->hasOne('App\Models\LogSolicitud', 'solicitud_id', 'id');
    }

    /**
     * Obtiene la respuesta de la máquina de riesgos de cada solicitud
     *
     * @return object Respuesta de la máquina de riesgos
     */
    public function respuesta_maquina_riesgos() {
        return $this->hasMany('App\Models\RespuestaMaquinaRiesgo', 'solicitud_id', 'id');
    }
}
