<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientesAlta extends Model
{
    protected $table = 'clientes_alta';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'fecha_alta',
        'simplificado',
        'SHORTNAME',
        'NAME1',
        'NAME2',
        'FECNACIMIENTO',
        'GENDERNC',
        'MARITALSTSNC',
        'STREET',
        'DIRNUMEXT',
        'DIRCDEDO',
        'DIRDELMUNI',
        'DIRCOLONIA',
        'DIRPAIS',
        'TELDOM',
        'TELOFI',
        'TELCEL',
        'EMAIL',
        'MNEMONIC',
        'RFCCTE',
        'FORMERNAME',
        'LUGNAC',
        'VALCURP',
        'NATIONALITY',
        'RESIDENCE',
        'DIRNUMINT',
        'DIRCIUDAD',
        'DIRCODPOS',
        'ESTUDIOS',
        'OCUPACION',
        'MAININCOME',
        'DOMANOS',
        'EGRORDMEN',
        'TIPODOM',
        'NOOFDEPEND',
        'LOANAMOUNT',
        'TIPOTASA',
        'TERM',
        'LOANPURPOSE',
        'CUREMPMTEXPNCYR',
        'HLDMORTGAGE',
        'HLDAUTOLOAN',
        'HLDTDC',
        'TDCCODE',
        'INCSNDSOURCE',
        'IDTYPE',
        'id_oferta_renovacion',
        'rfc_calculado',
        'aplica_cliente',
        'alta_cliente',
        'alta_cliente_at',
        'no_cliente_t24',
        'aplica_solicitud',
        'alta_solicitud',
        'alta_solicitud_at',
        'no_solicitud_t24',
        'aplica_ligue',
        'usuario_ligado',
        'ligue_usuario_at',
        'aplica_email',
        'email_enviado',
        'id_email',
        'status_email',
        'aplica_facematch',
        'error',
        'aplica_referencias',
        'aplica_cuenta_clabe',
        'aplica_comprobante_domicilio',
        'aplica_comprobante_ingresos',
        'aplica_certificados_deuda'
    ];
}
