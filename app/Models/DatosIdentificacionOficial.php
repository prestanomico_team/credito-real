<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatosIdentificacionOficial extends Model
{
    protected $table = 'datos_id_oficial';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'solicitudT24',
        'index',
        'type',
        'code',
        'value',
        'description',
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];
}
