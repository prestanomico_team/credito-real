<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MB_UPLOADED_DOCUMENT extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'MB_UPLOADED_DOCUMENT';
    public $timestamps = false;
    protected $primaryKey = 'idMB_UploadedDocument';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'idLoanApp',
        'email',
        'name',
        'extension',
        'ip',
        'source',
        'businessFormat',
        'createdAt'
    ];
}
