<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    /**
     * Tabla a la que se conecta
     */
    protected $table = 'ocupaciones';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ocupacion',
    ];
}
