<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatosIdentificacionOficialR extends Model
{
    protected $table = 'datos_id_oficial_r';

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $guarded = [];


    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
