<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipoLeasing extends Model
{
    protected $table = 'equipo_leasing';

    protected $fillable = [
        'equipo_leasing'
    ];
    
}
