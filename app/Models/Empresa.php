<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'nombre_empresa',
        'tipo_propiedad',
        'fecha_fundacion',
        'descripcion_empresa',
        'calle',
        'ciudad',
        'estado',
        'cp',
        'pais',
        'ventas_mensuales',
        'margen_ventas',
        'numero_empleados',
        'unico_dueño',
        'otros_nombres_comerciales',
        'deudas_empresa'
    ];
}