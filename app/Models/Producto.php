<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Versionable\VersionableTrait;

/**
 * Modelo que se conecta con la tabla productos
 */
class Producto extends Model
{
    use VersionableTrait;

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'nombre_producto',
        'alias',
        'tipo',
        'empresa',
        'monto_minimo',
        'monto_maximo',
        'bc_score',
        'condicion',
        'micro_score',
        'edad_minima',
        'edad_maxima',
        'cat',
        'tasa_minima',
        'tasa_maxima',
        'comision_apertura',
        'stored_procedure',
        'doble_oferta',
        'tipo_monto_do',
        'do_monto_maximo',
        'proceso_simplificado',
        'tipo_monto_simplificado',
        'simplificado_monto_minimo',
        'simplificado_monto_maximo',
        'facematch_simplificado',
        'carga_identificacion_selfie_simplificado',
        'carga_identificacion_selfie',
        'facematch',
        'captura_referencias',
        'captura_cuenta_clabe',
        'carga_comprobante_domicilio',
        'carga_comprobante_ingresos',
        'carga_certificados_deuda',
        'logo',
        'campo_cobertura',
        'consulta_alp',
        'consulta_buro',
        'proceso_experian',
        'producto_experian',
        'garantia',
        'seguro',
        'vigencia_de',
        'vigente',
        'vigencia_hasta',
        'default',
        'sin_historial',
        'sin_cuentas_recientes',
        'tasa_fija',
        'captura_referencias',
        'sms_referencias',
        'captura_cuenta_clabe',
        'carga_comprobante_domicilio',
        'carga_comprobante_ingresos',
        'redireccion',
        'redireccion_portal',
        'no_aplica_oferta'
    ];

    /**
     * Atributos que estan ocultos
     */
    protected $hidden = [

    ];


    /**
     * Obtiene las solicitudes del producto
     *
     * @return object Datos de las solicitudes
     */
    public function solicitudes() {
        return $this->belongsToMany(Solicitud::class)->withPivot('lead');
    }

    /**
     * Obtiene los plazos relacionados al producto
     *
     * @return object Datos de los plazos
     */
    public function plazos() {
        return $this->belongsToMany(Plazo::class)->withPivot('default');
    }

    /**
     * Obtiene las finalidades relacionados al producto
     *
     * @return object Datos de las finalidades
     */
    public function finalidades() {
        return $this->belongsToMany(Finalidad::class);
    }

    /**
     * Obtiene las ocupaciones relacionados al producto
     *
     * @return object Datos de las ocupaciones
     */
    public function ocupaciones() {
        return $this->belongsToMany(Ocupacion::class);
    }

    /**
     * Obtiene el equipo leasing relacionados al producto
     *
     * @return object Datos de las ocupaciones
     */
    public function equipo_leasing() {
        return $this->belongsToMany(EquipoLeasing::class);
    }

}
