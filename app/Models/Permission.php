<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * Modelo que se conecta con la tabla permisos
 */
class Permission extends EntrustPermission
{

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];
}
