<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class W_ICAR_VALIDATION extends Model
{
    protected $connection = 'mysql_app';
    protected $table = 'W_ICAR_VALIDATION';
    public $timestamps = false;
    protected $primaryKey = 'ID_ICAR_VALIDATION';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'ID_LOAN_APP',
        'STATUS_RESPONSE',
        'TEST_FACE_REC_VAL',
        'TEST_EXPIRY_DATE',
        'TEST_LOBAL_ATUH_RAT',
        'TEST_LOBAL_ATUH_VAL',
        'OBSERVATIONS',
        'EXECUTION_DATE',
        'CREATE_DATE',
        'UPDATE_DATE'
    ];
}
