<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CargaDocumentos extends Model
{
    
    protected $table = 'carga_documentos';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'solicitud_id',
        'prospecto_id',
        'aplica_facematch',
        'id_front',
        'id_back',
        'selfie',
        'facematch_completo',
        'aplica_comprobante_ingresos',
        'frecuencia',
        'tipo_comprobante',
        'detalle_documento',
        'numero_comprobantes',
        'comprobante_ingresos_completo',
        
    ];
}
