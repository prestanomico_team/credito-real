<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleSolicitud extends Model
{
    protected $table = 'detalle_solicitudes_mr';
    protected $connection = 'mysql_maquina_riesgos';

    /**
     * Atributos que son asignados masivamente
     *
     * @var array
     */
    protected $fillable = [
        'ID_PROSPECT',
        'ID_SOLIC',
        'FECHA_REGISTRO',
        'STATUS',
        'ULT_ACT',
        'PRESTAMO',
        'PLAZO',
        'FINALIDAD',
        'EQUIPO_LEASING',
        'NOMBRE',
        'APELLIDO_P',
        'APELLIDO_M',
        'EMAIL',
        'CELULAR',
        'ORIGEN',
        'NOMBRE_EMPRESA',
        'TIPO_PROPIEDAD',
        'FECHA_FUNDACION',
        'DESCRIPCION_EMPRESA',
        'DOM_CALLE',
        'NUM_INT',
        'CIUDAD',
        'DOM_ESTADO',
        'COD_POSTAL',
        'DOM_PAIS',
        'VENTAS_MENSUALES',
        'MARGEN_VENTAS',
        'NUMERO_EMPLEADOS',
        'UNICO_DUEÑO',
        'OTROS_NOMBRES_COMERCIALES',
        'MONTO_CUENTAS_COBRAR',
        'DEUDAS_IMPUESTOS',
        'DEUDAS_COMERCIALES',
        'MONTO_IMPUESTOS',
        'DEUDAS_EMPRESA',
        'NSS',
        'LICENCIA_CONDUCIR',
        'DEPOSITOS_MENSUALES',
        'SALDO_MENSUAL',
        'NOMBRE_SOCIO',
        'FECHA_NAC_SOCIO',
        'EMAIL_SOCIO',
        'TEL_SOCIO',
        'DOM_SOCIO',
        'PORCENTAJE_PARTICIPACION',
        'IP_ADDRESS',
        
    ];
}
