<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodigosSMS extends Model
{
    protected $table = 'codigos_enviados';

    // Atributos que son asignables
    protected $fillable = [
        'prospecto_id',
        'nombre',
        'email',
        'celular',
        'codigo',
        'enviado',
        'descripcion',
        'verificado',
    ];
}
