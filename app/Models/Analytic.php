<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
    use HasFactory;
    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'client_id',
        'email',
        'ip',
        'navegador',
        'navegador_version',
        'sistema_operativo',
        'sistema_version',
        'tipo_dispositivo',
        'marca',
        'modelo',
        'modelo_detalle',
        'idioma',
        'campaña',
        'origen',
        'medio',
        'duracion_sesion',
        'pais',
        'estado',
        'ciudad',
        'proveedor_internet',
        'datos_obtenidos',
        'fecha_hora',
        'contenido_anuncio'
    ];
}
