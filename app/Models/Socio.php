<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    use HasFactory;

    protected $fillable = [
        'prospecto_id',
        'solicitud_id',
        'nombre_completo',
        'fecha_nacimiento',
        'email',
        'telefono',
        'direccion_completa',
        'porcentaje_participacion',
        'monto_cuentas_cobrar',
        'deudas_impuestos',
        'deudas_comerciales',
        'monto_impuestos',
        'nss',
        'licencia_conducir',
        'deudas_empresa'
    ];
}
