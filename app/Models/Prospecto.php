<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Prospecto extends Authenticatable
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $encryptable = ['nombres', 'apellido_paterno', 'apellido_materno'];

    protected $fillable = [
        'nombres',
        'apellido_paterno',
        'apellido_materno',
        'email',
        'celular',
        'password',
        'cognito_id',
        'sms_verificacion',
        'usuario_confirmado',
        'referencia',
        'encryptd'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * Obtiene las solicitudes del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function solicitudes() {
        return $this->hasMany('App\Models\Solicitud', 'prospecto_id', 'id')
            ->select('id', 'prospecto_id', 'status', 'sub_status')
            ->orderBy('id', 'desc')
            ->limit(1);
    }

    /**
     * Obtiene la ultima solicitud modificada del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function ultima_solicitud() {

        return $this->hasOne('App\Models\Solicitud', 'prospecto_id', 'id')
            ->select('id', 'prospecto_id', 'status', 'sub_status', 'prestamo', 'plazo', 'finalidad')
            ->orderBy('updated_at', 'desc');
    }

    /**
     * Obtiene las solicitudes del prospecto
     *
     * @return object Datos de la solicitud
     */
    public function solicitudesV2() {
        return $this->hasMany('App\Models\Solicitud', 'prospecto_id', 'id')
        ->select(
            'id',
            'prospecto_id',
            'status',
            'sub_status',
            'autorizado',
            //'user_ip',
            'prestamo',
            'plazo',
            'finalidad',
            'pago_estimado',
            'created_at',
            'updated_at'
        )
            ->orderBy('id', 'desc');
    }


   /* public function attributesToArray() {
        $attributes = parent::attributesToArray();
        foreach ($this->encryptable as $key) {
            if (isset($attributes[$key])){
                try {
                    $attributes[$key] = decrypt($attributes[$key]);
                } catch (DecryptException $e) {
                    $attributes[$key] = $attributes[$key];
                }
            }
        }
        return $attributes;
    }


    public function getAttribute($key) {
        $value = parent::getAttribute($key);
        if (in_array($key, $this->encryptable) && $value != '') {
            try {
                $value = decrypt($value);
            } catch (DecryptException $e) {
                $value = $value;
            }
            return $value;

        }
        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable) && $value != '') {
            $value = encrypt($value);
        }
        return parent::setAttribute($key, $value);
    }

    public function nombreCompleto() {
        if ($this->apellido_materno != '') {
            return "{$this->nombres} {$this->apellido_paterno} {$this->apellido_materno}";
        } else {
            return "{$this->nombres} {$this->apellido_paterno}";
        }
    }*/
}

