<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Finalidad extends Model
{
    /**
     * Tabla a la que se conecta
     */
    protected $table = 'finalidades';

    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'finalidad',
    ];
}
