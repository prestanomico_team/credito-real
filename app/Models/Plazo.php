<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plazo extends Model
{
    /**
     * Atributos que pueden ser asignados
     */
    protected $fillable = [
        'plazo',
        'duracion',
    ];
}
