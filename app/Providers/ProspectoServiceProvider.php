<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\Str;

class ProspectoServiceProvider extends EloquentUserProvider
{
    public function retrieveById($identifier) {
        $user = parent::retrieveById($identifier);
        return $user;
    }
}