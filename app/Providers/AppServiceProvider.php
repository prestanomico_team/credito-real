<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Validación customizada que permite solo letras y espacios
        Validator::extend('alpha_numeric_spaces', function ($attribute, $value) {

            return preg_match('/[\p{L}\p{N} .-]+$/u', $value);

        });

        Validator::extend('alpha_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'.\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric_spaces_not_html', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N} ,.-]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('validacion_combos', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*+=\\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N} ,.()]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });

        Validator::extend('alpha_numeric', function ($attribute, $value) {

            if (preg_match('/(?<=<)\w+(?=[^<]*?>)/', $value) || preg_match('/[#$%^&*()+=\-\[\]\'\/{}|":<>?~\\\\]/', $value)) {
                return false;
            } else {
                if (preg_match('/[\p{L}\p{N}]+$/u', $value)) {
                    return true;
                } else {
                    return false;
                }
            }

        });


        // Validación customizada que verifica si existe el plazo
        Validator::extend('unique_duracion_plazo', function ($attribute, $value, $parameters) {

            $plazo = \DB::select("SELECT count(*) as total
            FROM plazos
            WHERE duracion ='$value' AND plazo='$parameters[0]'");

            if ($plazo[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Validación customizada que verifica si existe el rol/perfil
        Validator::extend('unique_rol', function ($attribute, $value, $parameters) {

            $rol = \DB::select("SELECT count(*) as total
            FROM roles
            WHERE name LIKE '%$parameters[0]%'");

            if ($rol[0]->total == 0) {
                return true;
            } else {
                return false;
            }

        });

        // Validación de imagen en base 64
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $explode = explode(',', $value);
            $allow = ['png', 'jpg', 'jpeg', 'svg', 'bmp', 'gif'];
            $format = str_replace(
                [
                    'data:image/',
                    ';',
                    'base64',
                ],
                [
                    '', '', '',
                ],
                $explode[0]
            );

            // check file format
            if (!in_array($format, $allow)) {
                return false;
            }

            // check base64 format
            if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                return false;
            }

            return true;
        });

        Validator::extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/[a-f0-9]{8}\-[a-f0-9]{4}\-4[a-f0-9]{3}\-(8|9|a|b)[a-f0-9]{3}\-[a-f0-9]{12}/', $value);
        });
    }
}
