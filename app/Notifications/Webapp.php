<?php

namespace App\Notifications;
use Sebbmyr\Teams\AbstractCard as Card;
/**
 * Simple card for microsoft teams
 */
class Webapp extends Card
{
    public function getMessage()
    {
        return [
            "@context" => "http://schema.org/extensions",
            "@type" => "MessageCard",
            "themeColor" => '01BC36',
            "summary" => "Webapp",
            "title" => $this->data['title'],
            "sections" => [
                [
                    "activityTitle" => "Carga de Documentos Webapp",
                    "activitySubtitle" => $this->data['subtitle'],
                    "activityImage" => "",
                    "text" => $this->data['text'],
                    "facts" => [
                        [
                            "name" => "Producto: ",
                            "value" => $this->data['producto']
                        ]
                    ]
                ]
            ]
        ];
    }
}