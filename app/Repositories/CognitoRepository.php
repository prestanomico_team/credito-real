<?php
namespace App\Repositories;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Config;

class CognitoRepository
{
    const NEW_PASSWORD_CHALLENGE = 'NEW_PASSWORD_REQUIRED';
    const FORCE_PASSWORD_STATUS  = 'FORCE_CHANGE_PASSWORD';
    const RESET_REQUIRED         = 'PasswordResetRequiredException';
    const USER_NOT_FOUND         = 'UserNotFoundException';
    const USERNAME_EXISTS        = 'UsernameExistsException';
    const INVALID_PASSWORD       = 'InvalidPasswordException';
    const CODE_MISMATCH          = 'CodeMismatchException';
    const EXPIRED_CODE           = 'ExpiredCodeException';

    /**
     * @var CognitoIdentityProviderClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @var string
     */
    protected $clientSecret;

    /**
     * @var string
     */
    protected $poolId;

    /**
     * CognitoClient constructor.
     * @param CognitoIdentityProviderClient $client
     * @param string $clientId
     * @param string $clientSecret
     * @param string $poolId
     */
    public function __construct() {
        $args = [
            'credentials' => [
                'key' => Config::get('cognito.credentials.key'),
                'secret' => Config::get('cognito.credentials.secret'),
            ],
            'region' => Config::get('cognito.region'),
            'version' => Config::get('cognito.version'),
            'app_client_id' => Config::get('cognito.app_client_id'),
            'app_client_secret' => Config::get('cognito.app_client_secret'),
            'user_pool_id' => Config::get('cognito.user_pool_id'),
        ];
        $client = new CognitoIdentityProviderClient($args);
        $this->client       = $client;
        $this->clientId     = Config::get('cognito.app_client_id');
        $this->clientSecret = Config::get('cognito.app_client_secret');
        $this->poolId       = Config::get('cognito.user_pool_id');
    }

    /**
     * Checks if credentials of a user are valid
     *
     * @see http://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminInitiateAuth.html
     * @param string $email
     * @param string $password
     * @return \Aws\Result|bool
     */
    public function authenticate($email, $password)
    {
        try {
            $response = $this->client->adminInitiateAuth([
                'AuthFlow'       => 'ADMIN_NO_SRP_AUTH',
                'AuthParameters' => [
                    'USERNAME'     => $email,
                    'PASSWORD'     => $password,
                    'SECRET_HASH'  => $this->cognitoSecretHash($email)
                ],
                'ClientId'   => $this->clientId,
                'UserPoolId' => $this->poolId
            ]);

            return [
                'success'   => true,
                'message'   => $response,
            ];

        } catch (CognitoIdentityProviderException $e) {
            if ($e->getAwsErrorCode() === self::RESET_REQUIRED ||
                $e->getAwsErrorCode() === self::USER_NOT_FOUND) {
                return [
                    'success'   => false,
                    'message'   => $e->getAwsErrorCode(),
                ];
            }

            return [
                'success'   => false,
                'message'   => $e->get('message'),
            ];

        }
    }

    /**
     * Registers a user in the given user pool
     *
     * @param $email
     * @param $password
     * @param array $attributes
     * @return bool
     */
    public function createUserCognito($email, $password, array $attributes = [])
    {
        $attributes['email'] = $email;

        try {

            $response = $this->client->signUp([
                'ClientId' => $this->clientId,
                'Password' => $password,
                'SecretHash' => $this->cognitoSecretHash($email),
                'UserAttributes' => $this->formatAttributes($attributes),
                'Username' => $email
            ]);

            return [
                'success'       => true,
                'message'       => 'Usuario creado en Cognito',
                'cognito_id'    => $response->get('UserSub'),
            ];

        } catch (CognitoIdentityProviderException $e) {

            if ($e->getAwsErrorCode() === self::USERNAME_EXISTS) {

                return [
                    'success'           => false,
                    'message'           => 'El usuario ya existe',
                    'siguiente_paso'    => 'login'
                ];
            }

            return [
                'success'   => false,
                'message'   => $e->get('message')
            ];

        }

    }

    /**
     * Send a password reset code to a user.
     * @see http://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_ForgotPassword.html
     *
     * @param  string $username
     * @return string
     */
    public function sendResetLink($username)
    {
        try {
            $result = $this->client->forgotPassword([
                'ClientId' => $this->clientId,
                'SecretHash' => $this->cognitoSecretHash($username),
                'Username' => $username,
            ]);

            return [
                'success'   => true,
                'message'   => 'Mensaje enviado'
            ];

        } catch (CognitoIdentityProviderException $e) {
            $message = $e->get('message');
            if ($e->getAwsErrorCode() === self::USER_NOT_FOUND) {

                return [
                    'success'   => false,
                    'message'   => 'Usuario inválido'
                ];
            } elseif ($message == 'Attempt limit exceeded, please try after some time.') {
                return [
                    'success'   => false,
                    'message'   => 'Número máximo de intentos excedido, por favor intenta más tarde.'
                ];
            } elseif ($message == 'Cannot reset password for the user as there is no registered/verified email or phone_number') {
                return [
                    'success'   => false,
                    'message'   => 'No se puede restaurar la contraseña a un usuario con el teléfono celular sin verificar.'
                ];
            } else {
                return [
                    'success'   => false,
                    'message'   => $message
                ];
            }
        }
    }

    public function restorePassword($username, $confirmation_code, $newPassword)
    {
        try {
            $result = $this->client->confirmForgotPassword([
                'ClientId' => $this->clientId,
                'ConfirmationCode' => $confirmation_code,
                'SecretHash' => $this->cognitoSecretHash($username),
                'Password'  => $newPassword,
                'Username' => $username,
            ]);

            return [
                'success'   => true,
                'message'   => 'Password actualizado'
            ];

        } catch (CognitoIdentityProviderException $e) {
            $message = $e->get('message');
            if ($message == 'Invalid verification code provided, please try again.' || $message == 'Invalid code provided, please request a code again.') {
                return [
                    'success'   => false,
                    'message'   => 'Código de verificación inválido'
                ];
            } else {
                return [
                    'success'   => false,
                    'message'   => $message
                ];
            }

        }
    }

    # HELPER FUNCTIONS

    /**
     * Set a users attributes.
     * http://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminUpdateUserAttributes.html
     *
     * @param string $username
     * @param array  $attributes
     * @return bool
     */
    public function setUserAttributes($username, array $attributes)
    {
        try {

            $this->client->AdminUpdateUserAttributes([
                'Username' => $username,
                'UserPoolId' => $this->poolId,
                'UserAttributes' => $this->formatAttributes($attributes),
            ]);

            return [
                'success'   => true,
                'message'   => 'Usuario actualizado'
            ];

        } catch (\Exception $e) {

            return [
                'success'   => true,
                'message'   => $e->getMessage()
            ];

        }

    }


    /**
     * Creates the Cognito secret hash
     * @param string $username
     * @return string
     */
    protected function cognitoSecretHash($username)
    {
        return $this->hash($username . $this->clientId);
    }

    /**
     * Creates a HMAC from a string
     *
     * @param string $message
     * @return string
     */
    protected function hash($message)
    {
        $hash = hash_hmac(
            'sha256',
            $message,
            $this->clientSecret,
            true
        );

        return base64_encode($hash);
    }

    /**
     * Get user details.
     * http://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_GetUser.html
     *
     * @param  string $username
     * @return mixed
     */
    public function getUser($username)
    {
        try {
            $user = $this->client->AdminGetUser([
                'Username' => $username,
                'UserPoolId' => $this->poolId,
            ]);
        } catch (CognitoIdentityProviderException $e) {
            return false;
        }

        return $user;
    }

    /**
     * Format attributes in Name/Value array
     *
     * @param  array $attributes
     * @return array
     */
    protected function formatAttributes(array $attributes)
    {
        $userAttributes = [];

        foreach ($attributes as $key => $value) {
            $userAttributes[] = [
                'Name' => $key,
                'Value' => $value,
            ];
        }

        return $userAttributes;
    }
}
