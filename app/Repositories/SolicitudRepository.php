<?php
namespace App\Repositories;
use \Exception as Exception;
use App\Models\LogSolicitud;
use App\Models\TrackingSolicitud;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Models\Solicitud;
use App\Models\PlantillaComunicacion;
use App\Models\MotivoRechazo;
use App\Models\DetalleSolicitud;
use App\Models\Empresa;
use App\Http\Controllers\FuncionesT24;
use App\Models\RespuestaMaquinaRiesgo;
use App\Models\CargaDocumentos;
use App\Models\ClientesAltaEmpresa;
use DB;
use Validator;
use PDO;
use App\Models\Producto;
use App\Repositories\CurlCaller;
use App\Repositories\PanelOperativoRepository;
use App\Models\Plazo;
use App\Models\Prospecto;
use App\Jobs\EnvioEmail_InvitacionContinuar;
use Log;

class SolicitudRepository {

    use FuncionesT24;
    protected $altaPanel = false;
    protected $idPanelOperativo = false;
    protected $generaProcesos = true;

    /**
     * Genera el log para cada paso del proceso de registro
     * @param  string $nombre     Nombre del prospecto que se esta registrando
     * @param  string $mnsj_str   Mensaje que sera registrado
     * @param  array $user_input  Captura de datos del prospecto
     *
     * @return void
     */
    private function logRegisterError($nombre, $mnsj_str, $user_input)
    {
        if ($crea_registrolog == true) {
            LogRegistro::updateOrCreate([
                'email'         => $user_input['email'],
                'fecha'         => $date->format('Y-m-d H:i:s')
            ], [
                'client_id'     => $user_input['clientId'],
                'nombre'        => $user_input['nombre'],
                'apellido_p'    => $user_input['apellido_p'],
                'apellido_m'    => $user_input['apellido_m'],
                'cel'           => $cel,
                'error_message' => $mnsj_data->message
            ]);
        }
    }

    /**
     * Funcion que determina que parte del flujo de la solicitud debe mostrar
     * @param  string $paso         Paso actual en el que se encuentra la solicitud
     * @param  integer $prospecto   Id del prospecto Autenticado
     * @param  boolean $login       Para identificar si el método se esta invocando del login
     *
     * @return array                Arreglo con la información del siguiente paso
     */
    public function siguientePaso($paso = null, $prospecto = null, $login = null) {

        // Si la variable $prospecto esta definida es por que la función se esta invocando cuando la página
        // se actualiza, cuando se invoca de un método interno (Llamada a BC, ALP,
        // Status Oferta, Verificación de código SMS) o cuando se hace login.
        if ($prospecto != null) {
            $solicitud = Solicitud::with('tracking', 'log','datos_empresa', 'datos_socios')->where('prospecto_id', $prospecto->id)
                ->orderBy('created_at', 'DESC')
                ->first();

            $status = $solicitud->status;
            $sub_status = $solicitud->sub_status;
            $cuestionario = null;
            $tipoPoblacion = null;
            $redirect = null;
            $formulario = null;
            $mostrarOferta = false;
            $modal = null;
            $nueva_solicitud = false;
            $carga_documentos = false;
            $datos_capturados = null;
            $modelo = null;
            $info_socios = isset($solicitud->datos_empresa->unico_dueño) ? true : false;

            if ($prospecto->password_temporal == true) {
                $formulario = 'restaurar_contraseña';
            } else if ($status == 'Registro' && $prospecto->sms_verificacion == true) {
                $formulario = 'verificar_codigo';
                if ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                }
            } elseif ($status == 'Registro Confirmado') {

                if ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                } else {
                    switch ($sub_status) {
                        case 'Validar SMS':
                            $formulario = 'datos_empresa';
                            break;
                        case 'Datos Empresa':
                            $formulario = 'datos_negocio';
                            break;
                        case 'Datos Negocio':

                            $unico_d = Empresa::where('solicitud_id', $solicitud->id)
                                ->first();

                            if ($unico_d->unico_dueño == 1 ) {
                                $formulario = 'info_socios';
                            } else {
                                $formulario = 'datos_socio';
                            }

                            break;
                        case 'Datos Socio':
                            $formulario = 'info_socios';
                            break;
                        case 'Datos Extra Negocio':
                            $formulario = 'datos_credito';
                            break;
                        case 'Datos Cuenta Credito':
                            $formulario = 'datos_credito';
                            break;
                        default:
                            $formulario = 'simulador';
                            break;
                    }
                }

            } elseif($status == 'Store Procedure') {

                switch ($sub_status) {
                    case 'Check Bc Score Elegible':
                        $formulario = 'datos_ingreso';
                        break;
                    case 'Elegible':
                        $formulario = 'datos_empleo';
                        break;
                    case 'No Elegible':
                        $formulario = 'simulador';
                        $nueva_solicitud = true;
                        session()->flash('nueva_solicitud', true);
                        break;
                    default:
                        $formulario = 'simulador';
                        $nueva_solicitud = true;
                        session()->flash('nueva_solicitud', true);
                        break;
                }

            } elseif ($status == 'Invitación a Continuar') {

                $respuesta = RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
                    ->where('solicitud_id', $solicitud->id)
                    ->get();

                if ($login == true && $sub_status == 'Oferta Rechazada') {
                    $sub_status = $sub_status.'_LoginOR';
                } elseif (($login == true && ($sub_status == 'Oferta Aceptada' || $sub_status == 'Oferta Aceptada - FaceMatch Completado'
                    || $sub_status == 'Oferta Aceptada - Carga Documentos Completada'))) {
                    $sub_status = $sub_status.'_LoginOA';
                } elseif ($login == true) {
                    $modal = view("modals.modalSolicitudPendiente")->render();
                }

                switch ($sub_status) {
                    case 'Invitación a Continuar':
                        $mostrarOferta = true;
                        $plantillaComunicacion = null;
                        $view = 'aprobacion';
                        $datosOferta = [];
                        foreach ($respuesta as $oferta) {
                            $datosOferta[] = [
                                'monto'         => $oferta->monto,
                                'plazo'         => $oferta->plazo,
                                'pago_estimado' => $oferta->pago_estimado,
                            ];
                            //$tipoPoblacion = $oferta->tipo_poblacion;
                            //$plantillaComunicacion = $oferta->plantilla_comunicacion;
                        }
                        if ($tipoPoblacion == 'oferta_doble') {
                            $modal = 'modalDobleOferta';
                        }
                        $motivos = MotivoRechazo::pluck('motivo');
                        $view = view('parts.aprobacion',[
                            'monto' => $respuesta[0]->monto,
                            'plazo' => $respuesta[0]->plazo,
                            'pago_estimado' => $respuesta[0]->pago,
                            'nombre' => $prospecto->nombres,
                            'motivos'   => $motivos,
                        ])->render();
                        break;
                    case 'Oferta Aceptada - FaceMatch':
                        $carga_documentos = true;
                        $this->crearRegistroCargaDocumentos($solicitud);
                        break;
                    case 'Oferta Rechazada':
                    case 'Oferta Aceptada':
                    case 'Oferta Rechazada_LoginOR':
                        session()->flash('nueva_solicitud', true);
                        $nueva_solicitud = true;
                        break;
                    case 'Oferta Aceptada_LoginOA':
                    case 'Oferta Aceptada - FaceMatch Completado_LoginOA':
                    case 'Oferta Aceptada - Carga Documentos Completada_LoginOA':
                        $modal = view("modals.modalSolicitudTerminada")->render();
                        $nueva_solicitud = true;
                        break;
                    case 'Oferta Aceptada - Carga Documentos Completada':
                        session()->flash('nueva_solicitud', true);
                        $nueva_solicitud = true;
                        break;
                    case 'Oferta Aceptada - Carga Documentos':
                        $carga_documentos = true;
                        break;
                }

                if ($carga_documentos == true) {
                    $siguiente_paso = $this->siguientePasoDocumentos($prospecto->id, $solicitud->id);
                    if ($siguiente_paso != null) {
                        $redirect = "/webapp/{$siguiente_paso}";
                    } else {
                        $modal = view("modals.modalSolicitudTerminada")->render();
                        $nueva_solicitud = true;
                    }
                }

            } else {
                // Casos: Oferta Diferida, Rechazado o cualquier otro status no especificado
                // Si el prospecto inicio sesion y fue oferta diferida o rechazado
                // tiene que iniciar una nueva solicitud
                if ($prospecto->login == true) {
                    $nueva_solicitud = true;
                    $modal = null;
                } else {
                     // Si recarga la página y la ultima solicitud es oferta diferida
                     // o rechazado se tiene que redirigir al simulador con una nueva
                     // solicitud
                    session()->flash('nueva_solicitud', true);
                    $nueva_solicitud = true;
                }

            }

            // Si el prospecto tiene el campo login en true es por que o inicio sesion
            // o no ha validado el sms que se envia al hacer login, por lo que
            // se tiene que mostrar el formulario de validacion de código sms y cualquier
            // redirect se tiene que anular siempre y cuando no se tenga que
            // iniciar con una nueva solicitud
            if ($prospecto->login == true && $nueva_solicitud == false) {
                $formulario = 'verificar_codigo';
                $redirect = null;
            }

            return [
                'formulario'        => $formulario,
                'oculta'            => 'simulador',
                'cuestionario'      => $cuestionario,
                'tipo_poblacion'    => $tipoPoblacion,
                'redirect'          => $redirect,
                'mostrar_oferta'    => $mostrarOferta,
                'modal'             => $modal,
                'nueva_solicitud'   => $nueva_solicitud,
                'datos_capturados'  => $datos_capturados,
                //'view'              => $view,
            ];

        } elseif ($paso != null) {
            // Si la variable $paso esta definida es por que se esta invocando del botón siguiente
            // que se encuentra en cada paso del flujo a excepción del Cuestionario Dinámico

            $formulario = '';
            switch ($paso) {
                case 'registro':
                    $formulario = 'registro';
                    break;
                case 'verificar_codigo':
                    $formulario = 'verificar_codigo';
                    break;
                case 'datos_empresa':
                    $formulario = 'datos_empresa';
                    break;
                case 'datos_negocio':
                    $formulario = 'datos_negocio';
                    break;
                case 'datos_socio':
                    $formulario = 'datos_socio';
                    break;
                case 'info_socios':
                    $formulario = 'info_socios';
                    break;
                case 'datos_credito':
                    $formulario = 'datos_credito';
                    break;
                case 'restaurar_contraseña':
                    $formulario = 'restaurar_contraseña';
                    break;
                default:
                    $formulario = 'simulador';
                    break;
            }

            return [
                'formulario'        => $formulario,
                'oculta'            => 'restaurar_contraseña',
            ];
        }
    }

    public function setUltMnsjUsr($solicitud, $log) {

        $log = $this->limpiaLog($log);

        if ($solicitud->log()->exists() == false) {

            $ultimoMensaje = [
                'datetime'      => Carbon::now()->format('Y-m-d H:i:s'),
                'console_log'   => json_decode($log),
            ];

            LogSolicitud::updateOrCreate([
                'solicitud_id'              => $solicitud->id,
            ], [
                'ultimo_mensaje_usuario'    => json_encode($ultimoMensaje)
            ]);

        } else {

            $logfile = [];
            $logActual = $solicitud->log()->get()->toArray();
            $logActual = json_decode($logActual[0]['ultimo_mensaje_usuario'], true);

            $ultimoMensaje = [
                'datetime'      => Carbon::now()->format('Y-m-d H:i:s'),
                'console_log'   => json_decode($log),
            ];

            if (isset($logActual[0])) {
                $logfile = $logActual;
                array_push($logfile, $ultimoMensaje);
            } else {
                array_push($logfile, $logActual);
                array_push($logfile, $ultimoMensaje);
            }

            LogSolicitud::updateOrCreate([
                'solicitud_id'              => $solicitud->id,
            ], [
                'ultimo_mensaje_usuario'    => json_encode($logfile, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            ]);
        }

    }

    public function setUltPuntoReg($solicitud_id, $status, $sub_status, $success, $result, $extra = NULL) {

        $decision = false;
        $tipoOferta = '';
        if ($extra != null) {
            if (isset($extra['tipo_oferta'])) {
                $decision = true;
            }
            $extra = json_encode($extra, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        if ($decision == false || ($decision == true && $tipoOferta == 'oferta_normal')) {

            TrackingSolicitud::insert([
                'solicitud_id'  => $solicitud_id,
                'status'        => $status,
                'sub_status'    => $sub_status,
                'success'       => $success,
                'descripcion'   => $result,
                'extra'         => $extra,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);

        }


    }

    public function limpiaLog($log) {

        $log = json_decode($log);
        $log = json_encode($log,  JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return $log;

    }

    function json_validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);
        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }
        if ($error !== '') {
            return $error;
        }
        return true;
    }

    /**
     * Genera el registro para la tabla clientes_alta para procesar el alta
     * automática del cliente
     *
     * @param  integer $prospecto_id  Id del prospecto
     * @param  integer $solicitud_id  Id de la solicitud del prospecto
     * @param  integer $idClienteT24  Id del cliente en T24
     * @param  boolean $error         Identifica si el registro tiene un error
     * @param  string  $msj           Mensaje de error
     *
     * @return array                  Resultado del registro en tabla clientes_alta
     */
    public function arregloAlta($prospecto_id, $solicitud_id)
    {
        $datosAlta = [];

        // Obteniendo los datos del prospecto
        $prospecto = Prospecto::select('nombres', 'apellido_paterno', 'apellido_materno',
                'celular', 'email', 'referencia')
            ->where('id', $prospecto_id)
            ->get();
        $prospecto = $prospecto[0];
        $datosAlta = array_merge($datosAlta, $prospecto->toArray());

        // Obteniendo los datos de la solicitud
        $solicitud = Solicitud::select('id', 'prospecto_id', 'tipo_persona',
                'prestamo', 'plazo', 'finalidad', 'pago_estimado', 'equipo_leasing')
            ->where('id', $solicitud_id)
            ->get();

        $solicitud = $solicitud[0];
        // Obteniendo los datos de la direccion
        $empresa = Empresa::select('id', 'calle', 'ciudad',
                'estado', 'cp', 'pais', 'ventas_mensuales')
            ->where('solicitud_id', $solicitud_id)
            ->get();

        $empresa = $empresa[0];


        // Obteniendo los datos de la oferta de la respuesta de la máquina de
        // riesgos para mandar los datos de monto y plazo a la solicitud T24

        /*$ofertaPredominante = OfertaPredominante::select('monto', 'plazo', 'simplificado', 'carga_identificacion_selfie', 'facematch', 'tipo_tasa')
            ->where('solicitud_id', $solicitud_id)
            ->where('prospecto_id', $prospecto_id)
            ->where('status_oferta', 'Oferta Aceptada')
            ->where('elegida', 1)
            ->get();

        if (count($ofertaPredominante) == 0) {
            $ofertaPredominante = OfertaPredominante::select('monto', 'plazo', 'simplificado', 'carga_identificacion_selfie', 'facematch', 'tipo_tasa')
                ->where('solicitud_id', $solicitud_id)
                ->where('prospecto_id', $prospecto_id)
                ->where('status_oferta', 'Oferta Aceptada')
                ->get();
        }*/

        $facematch = 1;
        $carga_identificacion_selfie = 1;
        $simplificado = 1;
        $aplica_facematch = 1;
        $aplica_comprobante_ingresos = 1;

        $tipoTasa = null;


        /*if ($solicitud->producto()->exists()) {
            $solicitud->nombre_producto = mb_strtoupper($solicitud->producto['0']['nombre_producto']);
            if ($solicitud->producto[0]['pivot']['lead'] != null) {
                $datosAlta['referencia'] = $solicitud->producto[0]['pivot']['lead'];
                if ($datosAlta['referencia'] == 'COMPARA GURU') {
                    $facematch = 0;
                    $solo_carga_identificacion_selfie = 0;
                }
            }
        } else {
            $solicitud->nombre_producto = 'MERCADO ABIERTO';
        }*/

        if ($facematch == 1) {
            $solo_carga_identificacion_selfie = 0;
        }

        if ($facematch == 1 || $solo_carga_identificacion_selfie == 1) {
            $aplica_facematch = 1;
        }

        /*if ($solicitud->producto()->exists()) {

            if ($simplificado == 0) {
                $aplica_referencias = $solicitud->producto[0]['captura_referencias'];
                $aplica_cuenta_clabe = $solicitud->producto[0]['captura_cuenta_clabe'];
                $aplica_comprobante_domicilio = $solicitud->producto[0]['carga_comprobante_domicilio'];
                $aplica_comprobante_ingresos = $solicitud->producto[0]['carga_comprobante_ingresos'];
                $aplica_certificados_deuda = $solicitud->producto[0]['carga_certificados_deuda'];
            }

            if ($solicitud->producto[0]['alias'] == 'renovaciones') {

                $oferta_renovacion = OfertaRenovacion::where('id_solic', $solicitud->id)->first();
                $id_oferta_renovacion = $oferta_renovacion->Id_Oferta;

            }

        }*/

        $datosAlta = array_merge($datosAlta, $solicitud->toArray());
        $datosAlta = Arr::dot($datosAlta);


        $datosUnicos['prospecto_id'] = $datosAlta['prospecto_id'];
        $datosUnicos['solicitud_id'] = $datosAlta['id'];

        // Generando el registro en la tala clientes_alta
        $clientesAlta = ClientesAltaEmpresa::updateOrCreate(
            $datosUnicos,
            $datosAlta
        );

        //obtención LOANPURPOSE
        $lp = $this->getLOANPURPOSE($solicitud['equipo_leasing']);

        if ($clientesAlta->wasRecentlyCreated) {
            // Si el $idClienteT24 es diferente a nulo los campos de control para el
            // procesamiento de Jobs comienzan en la solicitud, de lo contrario
            // comienzan en el cliente
            $clientesAlta->SHORTNAME = mb_strtoupper($prospecto['apellido_paterno']);
            $clientesAlta->NAME1 = mb_strtoupper($prospecto['apellido_materno']);
            $clientesAlta->NAME2 = $prospecto['nombres'];
            $clientesAlta->fecha_alta = date('Y-m-d');
            $clientesAlta->TELCEL = $prospecto['celular'];
            $clientesAlta->EMAIL = $prospecto['email'];
            $clientesAlta->LOANAMOUNT = $solicitud['prestamo'];
            $clientesAlta->TERM = $solicitud['plazo'];
            $clientesAlta->LOANPURPOSE = $lp;
            $clientesAlta->INCSNDSOURCE = 'SITIO';
            $clientesAlta->STREET = $empresa['calle'];
            $clientesAlta->DIRNUMEXT = $empresa['calle'];
            $clientesAlta->DIRCDEDO = $empresa['estado'];
            $clientesAlta->DIRPAIS = $empresa['pais'];
            $clientesAlta->DIRCODPOS = $empresa['cp'];
            $clientesAlta->IDTYPE = 'CREAL';
            $clientesAlta->no_cliente_t24 = $prospecto_id;
            $clientesAlta->no_solicitud_t24 = $solicitud_id;
            $clientesAlta->aplica_cliente = 0;
            $clientesAlta->aplica_solicitud = 1;
            $clientesAlta->aplica_ligue = 1;
            $clientesAlta->aplica_email = ($simplificado == false ? 1 : 0);
            $clientesAlta->aplica_facematch = $facematch;
            $clientesAlta->solo_carga_identificacion_selfie = $solo_carga_identificacion_selfie;
            $clientesAlta->aplica_comprobante_ingresos = $aplica_comprobante_ingresos;

            $clientesAlta->save();


            CargaDocumentos::updateOrCreate([
                'solicitud_id'                  => $clientesAlta['solicitud_id'],
                'prospecto_id'                  => $clientesAlta['prospecto_id']
            ], [
                'aplica_facematch'                  => $aplica_facematch,
                'facematch_completo'                => ($aplica_facematch == true) ? 0 : null,
                'aplica_comprobante_ingresos'       => $aplica_comprobante_ingresos,
                'comprobante_ingresos_completo'     => ($aplica_comprobante_ingresos == true) ? 0 : null,

            ]);

        }

        return $clientesAlta;
    }

    public function ofertaAceptada($prospecto, $solicitud) {

        RespuestaMaquinaRiesgo::where('prospecto_id', $prospecto->id)
            ->where('solicitud_id', $solicitud->id)
            ->update(['elegida' => 1, 'status_oferta' => 'Oferta Aceptada']);


        $modal = null;
        $carga_documentos = $this->aplicaCargaDocumentos($solicitud);
        $this->curl = new CurlCaller;

        if ($carga_documentos == true) {

            $this->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Oferta Aceptada - Carga Documentos',
                1,
                'Se ha guardado el status de la solicitud con éxito'
            );
            $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos';
            $solicitud->save();

        // Se agrega substatus Oferta Aceptada - FaceMatch para que todos los
        // prospectos que tengan carga_identificacion_selfie como true
        } /*elseif ($carga_identificacion_selfie == true) {

            $this->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Oferta Aceptada - FaceMatch',
                1,
                'Se ha guardado el status de la solicitud con éxito'
            );
            $solicitud->sub_status = 'Oferta Aceptada - FaceMatch';
            $solicitud->save();

        } else {

            $id_plantilla = $ofertaPredominante->plantilla_comunicacion;
            $plantilla = PlantillaComunicacion::select('id', 'modal_encabezado', 'modal_cuerpo', 'modal_img')
                        ->where('plantilla_id', $id_plantilla)
                        ->get()
                        ->toArray();

            $tituloModal = $plantilla[0]['modal_encabezado'];
            $imgModal = $plantilla[0]['modal_img'];
            $cuerpoModal = $plantilla[0]['modal_cuerpo'];
            $modal = view("modals.modalStatusSolicitudDocumentos", ['tituloModal' => $tituloModal, 'imgModal' => $imgModal, 'cuerpoModal' => $cuerpoModal, 'continua' => false])->render();

        }*/

        $arregloAlta = $this->arregloAlta($prospecto->id, $solicitud->id);

        $siguiente_paso = $this->siguientePasoDocumentos($prospecto->id, $solicitud->id);

        $clienteAlta = ClientesAltaEmpresa::where('prospecto_id', $prospecto->id)
                ->where('solicitud_id', $solicitud->id)
                ->orderBy('id', 'desc')
                ->first();

        if ($clienteAlta->panel_operativo_id === null) {
            $panelOperativoRepository = new PanelOperativoRepository($this->curl);
            $id_clienteAlta = [
                'idp' => $clienteAlta->id,
            ];
            $response = $panelOperativoRepository->forzarAlta($id_clienteAlta);
            
            if ($response['success'] == true) {
                if (isset($response['response']->idp[0])) {
                    $this->altaPanel = true;
                    ClientesAltaEmpresa::where('id', $clienteAlta->id)->update([
                        'panel_operativo_id' => $response['response']->idp[0],
                        'alta_solicitud'    => 1,
                        'alta_solicitud_at'  => date('Y-m-d H:i:s'),
                    ]);
                }
            } else {
                $this->generaProcesos = false;
                throw new Exception("No se pudo dar de alta en el Panel Operativo");
            }

            $job = (new LigueUsuario($clienteAlta))->delay(10);
            dispatch($job);
        }
        
        $job = (new EnvioEmail_InvitacionContinuar($clienteAlta))->onQueue(env('QUEUE_NAME'));
        dispatch($job);

        if ($siguiente_paso == null) {
            $siguiente_paso = 'facematch/id_front';
        }

        return [
            'success'           => true,
            'carga_documentos'  => $carga_documentos == 1,
            'siguiente_paso'    => $siguiente_paso,
            //'modal'             => $modal,

        ];

    }

    /**
     * Se ocupa para todas las solicitudes que hacen Login y se encuentran en status
     * Invitación a Continuar - Carga de Documentos
     *
     * Verifica el status de la solicitud en el Panel Operativo, si la solicitud
     * esta abierta para integración checa que pasos de la carga de documentos se pudieron
     * completar en el Panel Operativo y los pone como completados en el sitio.
     *
     * Si la solicitud ya esta cerrada para integración pone el status de la solicitud
     * Invitación a Continuar - Carga de Documentos Completada
     *
     * @param  integer $prospecto_id Id del prospecto
     * @param  integer $solicitud_id Id de la solicitud
     *
     * @return string                Nombre del siguiente paso
     */
    public function siguientePasoDocumentos($prospecto_id, $solicitud_id) {

        // Consultamos que el status open_integration de la solicitud en el panel
        // operativo sea true para poder seguir cargando la documentación a traves
        // del web app
        $clientes_alta = ClientesAltaEmpresa::where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->first();

        $open_integration = true;
        $pasos_validados = [];

        try {

            if ($clientes_alta->no_solicitud_t24 != null) {

                $curl = new CurlCaller();
                $datos = [
                    'd' => 'onig',
                    'i' => $clientes_alta->no_solicitud_t24,
                ];

                $response = $curl->syncWebapp($datos);
                $response = json_decode($response);

                if ($response->statusCode == 200) {
                    $open_integration = $response->open_integration;
                    if (isset($response->validated)) {
                        $pasos_validados = $response->validated;
                    }
                }

            }

        } catch (\Exception $e) {
            Log::info('No existe el registro CA para el prospecto: '.$prospecto_id. ' y solicitud:' .$solicitud_id);
        }

        // Obteniendo los campos que terminan en la palabra completo de la tabla
        // carga_documentos exceptuando el campo referencias
        $columnas = DB::connection('mysql_lower')
            ->table('information_schema.columns')
            ->select('column_name')
            ->whereRAW('table_name = "carga_documentos" AND column_name LIKE "%completo"')
            ->whereRAW('table_schema = "'.env('DB_DATABASE').'"')
            ->orderBy('ordinal_position', 'asc')
            ->get();

        // Armando el listado de campos a seleccionar
        $sql = '';
        $last = count($columnas);
        foreach ($columnas as $key => $columna) {
            $sql .= $columna->column_name;
            if (($key + 1) < $last) {
                $sql .= ', ';
            }
        }

        // Obteniendo el registro de la tabla carga_documentos de la solicitud y
        // el prospecto correspondiente
        $pasos_faltantes = CargaDocumentos::selectRAW($sql)
            ->where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->get()
            ->first();

        // Filtrando los campos que tengan el valor 0, que son los que
        // faltan por cargar
        $pasos_faltantes = collect($pasos_faltantes)->filter(function($value, $key) {
            return $value === 0;
        });

        // Si la solicitud esta abierta para integración y aun tiene pasos
        // por completar se regresa el ultimo paso punto donde se quedo
        if ($open_integration == true && $pasos_faltantes != null) {

            // Verificando si los pasos válidos en el panel operativo estan
            // incompletos en el sitio
            if ($pasos_validados != null) {

                $pasos_desactualizados = null;
                foreach ($pasos_validados as $key => $paso) {
                    switch ($paso) {
                        case 'face':
                            $paso = 'facematch_completo';
                            break;
                        case 'references':
                            $paso = 'referencias_completo';
                            break;
                        case 'cta':
                            $paso = 'cuenta_clabe_completo';
                            break;
                        case 'location':
                            $paso = 'comprobante_domicilio_completo';
                            break;
                        case 'receipts':
                            $paso = 'comprobante_ingresos_completo';
                            break;
                    }

                    $paso_desactualizado = $pasos_faltantes->keys()->contains($paso);
                    if ($paso_desactualizado == true) {
                        $pasos_desactualizados[$paso] = true;
                        if ($paso == 'facematch_completo') {
                            $pasos_desactualizados['id_front'] = true;
                            $pasos_desactualizados['id_back'] = true;
                            $pasos_desactualizados['selfie'] = true;
                        }
                        $pasos_faltantes = $pasos_faltantes->forget($paso);
                    }
                }

                if ($pasos_desactualizados != null) {

                    CargaDocumentos::selectRAW($sql)
                        ->where('prospecto_id', $prospecto_id)
                        ->where('solicitud_id', $solicitud_id)
                        ->update($pasos_desactualizados);

                }

            }

            // Obteniendo el primer documento que falta por cargar
            $siguiente_paso = null;
            if ($pasos_faltantes != null) {
                $siguiente_paso = $pasos_faltantes->keys()->first();
                $siguiente_paso = str_replace('_completo', '', $siguiente_paso);
            }

            // Si $siguiente_paso es facematch entonces se verifica cual de los sub pasos
            // esta incompleto
            $sub_paso = '';
            if ($siguiente_paso == 'facematch') {

                $sub_pasos = CargaDocumentos::select('id_front', 'id_back', 'selfie')
                    ->where('prospecto_id', $prospecto_id)
                    ->where('solicitud_id', $solicitud_id)
                    ->get()
                    ->first();

                if ($sub_pasos->id_front == 0) {
                    $sub_paso = "/id_front";
                } elseif ($sub_pasos->id_back == 0) {
                    $sub_paso = "/id_back";
                } elseif ($sub_pasos->selfie == 0) {
                    $sub_paso = "/selfie";
                }

            }

            return $siguiente_paso.$sub_paso;

        // Si la solicitud esta cerrada para integración y aun tiene pasos
        // por completar se completan los pasos pendientes y el ultimo punto se
        // regresa como null
        } elseif ($open_integration == false && $pasos_faltantes != null) {

            $update = [];
            foreach ($columnas as $key => $columna) {
                $update[$columna->column_name] = 1;
                if ($columna->column_name == 'facematch_completo') {
                    $pasos_desactualizados['id_front'] = true;
                    $pasos_desactualizados['id_back'] = true;
                    $pasos_desactualizados['selfie'] = true;
                }
            }

            $pasos_faltantes = CargaDocumentos::selectRAW($sql)
                ->where('prospecto_id', $prospecto_id)
                ->where('solicitud_id', $solicitud_id)
                ->update($update);

            $solicitud = Solicitud::find($solicitud_id);
            $this->setUltPuntoReg(
                $solicitud->id,
                $solicitud->status,
                'Oferta Aceptada - Carga Documentos Completada',
                1,
                'Se ha completado la carga de documentos en el Panel Operativo'
            );
            $solicitud->sub_status = 'Oferta Aceptada - Carga Documentos Completada';
            $solicitud->save();

            return null;

        } else {
            return null;
        }

    }

    /**
     * Determina los documentos que no estan habilitados para cargar y mostrar
     * en el modal de status solicitud el listado de documentos faltantes.
     *
     * @param  integer $prospecto_id Id del prospecto
     * @param  integer $solicitud_id Id de la solicitud
     *
     * @return string                Listado de documentos que faltan por enviar
     * a traves de un medio alterno con estilo de html <ul></ul>
     */
    public function faltanDocumentos($prospecto_id, $solicitud_id, $producto = null) {

        // Obteniendo los campos que comienza con la palabra aplica de la tabla
        // carga_documentos exceptuando el campo referencias
        $columnas = DB::connection('mysql_lower')
            ->table('information_schema.columns')
            ->select('column_name')
            ->whereRAW('table_name = "carga_documentos" AND column_name LIKE "aplica%"')
            ->orderBy('ordinal_position', 'asc')
            ->get();

        // Armando el listado de campos a seleccionar
        $sql = '';
        $last = count($columnas);
        foreach ($columnas as $key => $columna) {
            $sql .= $columna->column_name;
            if (($key + 1) < $last) {
                $sql .= ', ';
            }
        }

        // Obteniendo el registro de la tabla carga_documentos de la solicitud y
        // el prospecto correspondiente
        $documentos_pendientes = CargaDocumentos::selectRAW($sql)
            ->where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->get()
            ->first();

        // Filtrando los campos que tengan el valor null o 0, que son los que
        // faltan por cargar
        $documentos_pendientes = collect($documentos_pendientes)->filter(function($value, $key) {
            return $value == null || $value == 0;
        });

        // Armando el listado de documentos que faltan por caergar.
        $documentos = null;
        $last = count($documentos_pendientes);
        $key = 1;
        foreach ($documentos_pendientes as $documento => $value) {

            if ($key == 1) {
                $documentos .= '<ul style="list-style: decimal; text-align:  justify;"">';
            }

            $documento = str_replace('aplica_', '', $documento);

            switch ($documento) {
                case 'facematch':
                    $documentos .= '<li>Identificación oficial vigente</li>';
                    break;
                case 'comprobante_ingresos':
                    $documentos .= '<li>Comprobante de ingresos</li>';
                    break;
            }

            if ($key  == $last) {
               $documentos .= '</ul>';
            } else {
               $key = $key + 1;
            }
        }

        return $documentos;

    }

    /**
     * Determina los documentos que estan habilitados para cargar y estan pendientes
     * por completar
     *
     * @param  integer $prospecto_id Id del prospecto
     * @param  integer $solicitud_id Id de la solicitud
     *
     * @return string                Listado de documentos que faltan por completar
     * en el webapp con estilo de html <ul></ul>
     */
    public function documentosPorCargar($prospecto_id, $solicitud_id) {

        // Obteniendo los campos que comienza con la palabra aplica de la tabla
        // carga_documentos
        $columnas = DB::connection('mysql_lower')
            ->table('information_schema.columns')
            ->select('column_name')
            ->whereRAW('table_name = "carga_documentos" AND column_name LIKE "%completo"')
            ->orderBy('ordinal_position', 'asc')
            ->get();

        // Armando el listado de campos a seleccionar
        $sql = '';
        $last = count($columnas);
        foreach ($columnas as $key => $columna) {
            $sql .= $columna->column_name;
            if (($key + 1) < $last) {
                $sql .= ', ';
            }
        }

        // Obteniendo el registro de la tabla carga_documentos de la solicitud y
        // el prospecto correspondiente
        $documentos_pendientes = CargaDocumentos::selectRAW($sql)
            ->where('prospecto_id', $prospecto_id)
            ->where('solicitud_id', $solicitud_id)
            ->get()
            ->first();

        // Filtrando los campos que tengan el valor 0, que son los que
        // faltan por cargar
        $documentos_pendientes = collect($documentos_pendientes)->filter(function($value, $key) {
            return $value === 0;
        });

        // Armando el listado de documentos que faltan por caergar.
        $documentos = null;
        $last = count($documentos_pendientes);
        $key = 1;
        foreach ($documentos_pendientes as $documento => $value) {

            if ($key == 1) {
                $documentos .= '<ol style="list-style: decimal; text-align:  justify;"">';
            }

            $documento = str_replace('_completo', '', $documento);

            switch ($documento) {
                case 'facematch':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">Identificación Oficial.</span></li>';
                    break;
                case 'referencias':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">3 referencias personales.</span></li>';
                    break;
                case 'cuenta_clabe':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">Cuenta CLABE a tu nombre donde te depositaremos.</span></li>';
                    break;
                case 'comprobante_domicilio':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">Comprobante de domicilio</span></li>';
                    break;
                case 'comprobante_ingresos':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">Comprobantes de ingreso</span></li>';
                    break;
                case 'certificados_deuda':
                    $documentos .= '<li style="text-align: justify;"><span style="font-size:13px">Certificados de deuda</span></li>';
                    break;
            }

            if ($key  == $last) {
               $documentos .= '</ol>';
            } else {
               $key = $key + 1;
            }
        }

        return $documentos;

    }

    /**
     * Determina si aplica la carga de documentos en base a las opciones que esten
     * habilitadas en el administrador de productos
     *
     * @param  object $solicitud Solicitud de la que se desea saber si el producto
     * relacionado tiene habilitada alguna opción de la carga de documentos.
     *
     * @return boolean           Resultado de la verificación
     */
    function aplicaCargaDocumentos($solicitud) {

        if ($solicitud->producto()->exists()) {
            if ($solicitud->producto[0]['facematch'] == 1 || $solicitud->producto[0]['carga_identificacion_selfie'] == 1) {
                return true;
            } elseif($solicitud->producto[0]['captura_referencias'] == 1) {
                return true;
            } elseif($solicitud->producto[0]['captura_cuenta_clabe'] == 1) {
                return true;
            } elseif($solicitud->producto[0]['carga_comprobante_domicilio'] == 1)  {
                return true;
            } elseif($solicitud->producto[0]['carga_comprobante_ingresos'] == 1)  {
                return true;
            } elseif($solicitud->producto[0]['carga_certificados_deuda'] == 1)  {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    function pasosHabilitadosProducto($solicitud) {

        $pasos = [];
        if ($solicitud->producto()->exists()) {
            if ($solicitud->producto[0]['facematch'] == 1 || $solicitud->producto[0]['carga_identificacion_selfie'] == 1) {
                $pasos[] = 'Identificación y Selfie';
            }
            if ($solicitud->producto[0]['captura_referencias'] == 1) {
                $pasos[] = 'Referencias';
            }
            if ($solicitud->producto[0]['captura_cuenta_clabe'] == 1) {
                $pasos[] = 'Cuenta Clabe';
            }
            if ($solicitud->producto[0]['carga_comprobante_domicilio'] == 1)  {
                $pasos[] = 'Comprobante de Domicilio';
            }
            if ($solicitud->producto[0]['carga_comprobante_ingresos'] == 1)  {
                $pasos[] = 'Comprobante de Ingresos';
            }
            if ($solicitud->producto[0]['carga_certificados_deuda'] == 1)  {
                $pasos[] = 'Certificados de Deuda';
            }
        }

        return $pasos;

    }

    /**
     * Función para crear el registro en la tabla de carga de documentos cuando
     * la solicitud se encuenta en status Oferta Aceptada - FaceMatch
     *
     * @param  integer $solicitud Solicitud de la que se creara el registro en la
     * tabla Carga Documentos
     *
     * @return void
     */
    function crearRegistroCargaDocumentos($solicitud) {

        $producto_id = $solicitud->producto[0]['id'];
        $producto = Producto::find($producto_id);

        $aplica_facematch = 0;
        $aplica_referencias = 0;
        $aplica_cuenta_clabe = 0;
        $aplica_comprobante_domicilio = 0;
        $aplica_comprobante_ingresos = 0;
        $aplica_certificados_deuda = 0;

        if ($producto->facematch == 1 || $producto->carga_identificacion_selfie == 1) {
            $aplica_facematch = 1;
        }

        if ($producto->captura_referencias == 1) {
            $aplica_referencias = 1;
        }

        if ($producto->captura_cuenta_clabe == 1) {
            $aplica_cuenta_clabe = 1;
        }

        if ($producto->carga_comprobante_domicilio == 1) {
            $aplica_comprobante_domicilio = 1;
        }

        if ($producto->carga_comprobante_ingresos == 1) {
            $aplica_comprobante_ingresos = 1;
        }

        if ($producto->carga_certificados_deuda == 1) {
            $aplica_certificados_deuda = 1;
        }

        $carga_documentos = CargaDocumentos::updateOrCreate([
            'solicitud_id' => $solicitud->id,
            'prospecto_id' => $solicitud->prospecto_id
        ]);

        if ($carga_documentos->wasRecentlyCreated) {
            $carga_documentos->aplica_facematch = $aplica_facematch;
            $carga_documentos->facematch_completo = ($aplica_facematch == true) ? 0 : null;
            $carga_documentos->aplica_referencias = $aplica_referencias;
            $carga_documentos->referencias_completo = ($aplica_referencias == true) ? 0 : null;
            $carga_documentos->aplica_cuenta_clabe = $aplica_cuenta_clabe;
            $carga_documentos->cuenta_clabe_completo = ($aplica_cuenta_clabe == true) ? 0 : null;
            $carga_documentos->aplica_comprobante_domicilio = $aplica_comprobante_domicilio;
            $carga_documentos->comprobante_domicilio_completo = ($aplica_comprobante_domicilio == true) ? 0 : null;
            $carga_documentos->aplica_comprobante_ingresos = $aplica_comprobante_ingresos;
            $carga_documentos->comprobante_ingresos_completo = ($aplica_comprobante_ingresos == true) ? 0 : null;
            $carga_documentos->aplica_certificados_deuda= $aplica_certificados_deuda;
            $carga_documentos->certificados_deuda_completo = ($aplica_certificados_deuda == true) ? 0 : null;
            $carga_documentos->save();
        }

    }

    public function getEvento($campaña, $evento) {

        $nombreEvento = null;
        switch ($campaña) {
            case 'adsalsa':
                $nombreEvento = $evento.'-AdSalsa';
                break;
            case 'antevenio':
                $nombreEvento = $evento.'-Antevenio';
                break;
            case 'sem':
                $nombreEvento = $evento.'-SEM';
                break;
            default:
                $nombreEvento = $evento;
                break;
        }

        return $nombreEvento;

    }

    public function getLOANPURPOSE($eq) {
        $equipos = [
            'MINI EXCAVADORAS' => 100,
            'BACKHOES' => 200,
            'MONTACARGAS' => 300,
            'SKID STEERS Y TRACK LOADERS' => 400,
            'HORIZONTAL DRILLING MACHINES' => 500,
            'TRAILERS' => 600,
            'OTRO EQUIPO PESADO' => 700,
        ];
        if(isset($equipos[$eq])){
            return $equipos[$eq];
        }else{
            return '';
        }
    }

    public function saveDetalleSolicitud($solic, $prospecto) {

        $detalleSolicitd = DetalleSolicitud::updateOrCreate(
            [
                'ID_PROSPECT'                   => $prospecto->id,
                'ID_SOLIC'                      => $solic->id,
            ], [
                'FECHA_REGISTRO'	            => $solic->created_at,
                'STATUS'			            => $solic->status,
                'ULT_ACT'			            => $solic->updated_at,
                'PRESTAMO'			            => $solic->prestamo,
                'PLAZO'				            => mb_strtoupper($this->getPlazoLabel($solic->plazo)),
                'FINALIDAD'			            => mb_strtoupper($solic->finalidad),
                'EQUIPO_LEASING'	            => mb_strtoupper($solic->equipo_leasing),
                'NOMBRE'			            => mb_strtoupper($prospecto->nombres),
                'APELLIDO_P'		            => mb_strtoupper($prospecto->apellido_paterno),
                'APELLIDO_M'		            => mb_strtoupper($prospecto->apellido_materno),
                'EMAIL'				            => mb_strtoupper($prospecto->email),
                'CELULAR'			            => $prospecto->celular,
                'ORIGEN'			            => 'SITIO',
                'NOMBRE_EMPRESA'                => $solic->datos_empresa->nombre_empresa,
                'TIPO_PROPIEDAD'                => $solic->datos_empresa->tipo_propiedad,
                'FECHA_FUNDACION'               => $solic->datos_empresa->fecha_fundacion,
                'DESCRIPCION_EMPRESA'           => $solic->datos_empresa->descripcion_empresa,
                'DOM_CALLE'                     => $solic->datos_empresa->calle,
                'NUM_INT'                       => $solic->datos_empresa->num_int,
                'CIUDAD'                        => $solic->datos_empresa->ciudad,
                'DOM_ESTADO'                    => $solic->datos_empresa->estado,
                'COD_POSTAL'                    => $solic->datos_empresa->cp,
                'DOM_PAIS'                      => $solic->datos_empresa->pais,
                'VENTAS_MENSUALES'              => $solic->datos_empresa->ventas_mensuales,
                'MARGEN_VENTAS'                 => $solic->datos_empresa->margen_ventas,
                'NUMERO_EMPLEADOS'              => $solic->datos_empresa->numero_empleados,
                'UNICO_DUEÑO'                   => $solic->datos_empresa->unico_dueño,
                'OTROS_NOMBRES_COMERCIALES'     => $solic->datos_empresa->otros_nombres_comerciales,
                'MONTO_CUENTAS_COBRAR'          => $solic->datos_empresa->monto_cuentas_cobrar,
                'DEUDAS_IMPUESTOS'              => $solic->datos_empresa->deudas_impuestos,
                'DEUDAS_COMERCIALES'            => $solic->datos_empresa->deudas_comerciales,
                'MONTO_IMPUESTOS'               => $solic->datos_empresa->monto_impuestos,
                'DEUDAS_EMPRESA'                => $solic->datos_empresa->deudas_empresa,
                'NSS'                           => $solic->datos_empresa->nss,
                'LICENCIA_CONDUCIR'             => $solic->datos_empresa->licencia_conducir,
                'DEPOSITOS_MENSUALES'           => $solic->datos_empresa->depositos_mensuales,
                'SALDO_MENSUAL'                 => $solic->datos_empresa->saldo_mensual,
                'NOMBRE_SOCIO'                  => isset($solic->datos_socios) ? $solic->datos_socios->nombre_completo : '',
                'FECHA_NAC_SOCIO'               => isset($solic->datos_socios) ? $solic->datos_socios->fecha_nacimiento : '',
                'EMAIL_SOCIO'                   => isset($solic->datos_socios) ? $solic->datos_socios->email : '',
                'TEL_SOCIO'                     => isset($solic->datos_socios) ? $solic->datos_socios->telefono : '',
                'DOM_SOCIO'                     => isset($solic->datos_socios) ? $solic->datos_socios->domicilio_completo : '',
                'PORCENTAJE_PARTICIPACION'      => isset($solic->datos_socios) ? $solic->datos_socios->porcentaje_participacion : '',
                'IP_ADDRESS'                    ,
            ]
        )->save();

        return $detalleSolicitd;
    }

    public function getPlazoLabel($clave){

        $plazo = Plazo::where('clave', $clave)->get();

        if (count($plazo) == 1) {
            $duracion = $plazo[0]->duracion;
            $plazo = $plazo[0]->duracion.' '.mb_strtoupper($plazo[0]->plazo);
        } else {
            $plazo = '';
        }

        return $plazo;
    }



}
