<?php
namespace App\Repositories;
use App\Repositories\CurlCaller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\MB_UPLOADED_DOCUMENT;
use \Exception as Exception;

class DocumentosRepository {

    private $documentUploaded = '';
    /**
     * Constructor de la clase
     *
     * @param CurlCaller $curl Realiza las llamadas curl
     */
    public function __construct(CurlCaller $curl)
    {
        $this->_curl = $curl;
    }

    function subirDocumentoS3($data) {

        $documentos = $data['documentos'];
        $email = $data['email'];
        $no_solicitud_t24 = $data['no_solcitud_t24'];
        foreach ($documentos as $key => $documento) {

            $ruta = $data['ruta_local'];
            $archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'].$documento['formato_documento'];
            $nombre_archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'];
            if (Storage::disk('s3')->exists("{$ruta}_{$archivo}") == true) {
                $image = "{$ruta}_{$archivo}";
                if (Storage::disk('s3')->exists("{$email}/{$no_solicitud_t24}/{$archivo}")) {
                    Storage::disk('s3')->delete("{$email}/{$no_solicitud_t24}/{$archivo}");
                }

                $response = Storage::disk('s3')->move($image, "{$email}/{$no_solicitud_t24}/{$archivo}", 'private');

                if ($response == 1) {
                     $responseDB = MB_UPLOADED_DOCUMENT::updateOrCreate([
                         'idLoanApp'         => $no_solicitud_t24,
                         'email'             => $email,
                         'name'              => $nombre_archivo,
                     ], [
                         'extension'         => '.jpg',
                         'ip'                => $this->getUserIP(),
                         'source'            => env('AWS_S3_BUCKET')."/{$email}/{$no_solicitud_t24}/{$archivo}",
                         'businessFormat'    => env('BUSINESSFORMAT'),
                         'createdAt'         => Carbon::now(),
                     ]);

                 }

            }
        }
    }

    function subirComprobantesS3($data) {

        $documentos = $data['documentos'];
        $email = $data['email'];
        $no_solicitud_t24 = $data['no_solicitud_t24'];
        foreach ($documentos as $key => $documento) {

            $ruta = $data['ruta_local'];
            $archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'].$documento['formato_documento'];
            $nombre_archivo = $documento['tipo_documento'].'_'.$documento['detalle_documento'];
            if (Storage::disk('s3')->exists("{$ruta}_{$archivo}") == true) {
                $image = "{$ruta}_{$archivo}";

                if (Storage::disk('s3')->exists("{$email}/{$no_solicitud_t24}/$nombre_archivo.jpg")) {
                    Storage::disk('s3')->delete("{$email}/{$no_solicitud_t24}/$nombre_archivo.jpg");
                }
                if (Storage::disk('s3')->exists("{$email}/{$no_solicitud_t24}/$nombre_archivo.pdf")) {
                    Storage::disk('s3')->delete("{$email}/{$no_solicitud_t24}/$nombre_archivo.pdf");
                }

                $response = Storage::disk('s3')->move($image, "{$email}/{$no_solicitud_t24}/{$archivo}", 'private');

                if ($response == 1) {
                     $responseDB = MB_UPLOADED_DOCUMENT::updateOrCreate([
                         'idLoanApp'         => $no_solicitud_t24,
                         'email'             => $email,
                         'name'              => $nombre_archivo,
                     ], [
                         'extension'         => $documento['formato_documento'],
                         'ip'                => $this->getUserIP(),
                         'source'            => env('AWS_S3_BUCKET')."/{$email}/{$no_solicitud_t24}/{$archivo}",
                         'businessFormat'    => env('BUSINESSFORMAT'),
                         'createdAt'         => Carbon::now(),
                     ]);
                 }

            }
        }
    }

    public function getUserIP() {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];
        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }
        return $ip;
    }

}
