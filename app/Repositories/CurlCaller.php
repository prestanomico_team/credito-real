<?php
namespace App\Repositories;
use \Exception as Exception;
use JWTFactory;
use JWTAuth;
use Carbon\Carbon;

class CurlCaller {

    function callCurlCalixta($celular, $msj) {

        $calixta_config = [
            "cte"       =>  env('CTE'),
            "encpwd"    =>  env('ENCPWD'),
            "email"     =>  env('EMAIL'),
            "numtel"    =>  $celular,
            "msg"       =>  $msj,
            "mtipo"     =>  env('MTIPO'),
            "idivr"     =>  env('IDIVR'),
            "auxiliar"  =>  env('AUXILIAR')
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, env('CALIXTA_URL'));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($calixta_config));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);

        if (!$curl_response) {
            $curl_response = curl_error($curl);
        }
        curl_close($curl);
        return $curl_response;

    }

    public function callCurlPanelOperativo($url, $curl_post_data) {

        JWTAuth::getJWTProvider()->setSecret(env('JWT_SECRET_PO'));
        $factory = JWTFactory::customClaims([
            'jti' => env('JWT_JTI_PO')
        ]);
        $payload = $factory->make();
        $token = JWTAuth::encode($payload);

        $data_string = json_encode($curl_post_data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$token
            )
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }

    public function syncWebapp($datos) {

        JWTAuth::getJWTProvider()->setSecret(env('JWT_SECRET_PO'));
        $factory = JWTFactory::customClaims([
            'jti' => env('JWT_JTI_PO')
        ]);
        $payload = $factory->make();
        $token = JWTAuth::encode($payload);

        $data_string = json_encode($datos);
        $url = env('URL_NOTIFICACIONES_PO');

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$token
            )
        );
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return $curl_response;

    }

}
