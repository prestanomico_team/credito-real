<?php

namespace App\Exports;

use App\Models\Solicitud;
use Schema;
use App\Http\Controllers\cleanStrMethods;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\Encryption\DecryptException;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;

class ReporteSolicitudesExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function start_date($date)
    {
        $this->start_date = $date;

        return $this;
    }

    public function end_date($date)
    {
        $this->end_date = $date;

        return $this;
    }

    public function decryptProspect($pr)
    {
        //decrypt the nombre field if it is encrypted
        try {
            $pr->nombre = decrypt($pr->nombre);
        } catch (DecryptException $e) {
            //the value is not encrypted, use the normal string
        }

        //decrypt the apellido_p field if it is encrypted
        try {
            $pr->apellido_p = decrypt($pr->apellido_p);
        } catch (DecryptException $e) {
            //the value is not encrypted, use the normal string
        }

        //decrypt the apellido_m field if it is encrypted
        try {
            $pr->apellido_m = decrypt($pr->apellido_m);
        } catch (DecryptException $e) {
            //the value is not encrypted, use the normal string
        }

        //decrypt the cel field if it is encrypted
        try {
            $pr->cel = decrypt($pr->cel);
        } catch (DecryptException $e) {
            //the value is not encrypted, use the normal string
        }

        //decrypt the activate_code field if it is encrypted
        try {
            $pr->activate_code = decrypt($pr->activate_code);
        } catch (DecryptException $e) {
            //the value is not encrypted, use the normal string
        }
        return $pr;
    }

    public function query()
    {

        $return = Solicitud::with('prospecto', 'datos_empresa', 'producto', 'datos_socios')
            ->where('updated_at', '>=', $this->start_date)
            ->where('updated_at', '<=', $this->end_date)
            ->orderBy('updated_at', 'desc');

        return $return;

    }

    public function headings(): array
    {
        return [
            'SOLICITUD ID',
            'PROSPECTO ID',
            'FECHA REGISTRO',
            'PRODUCTO',
            'STATUS',
            'ULTIMO PUNTO REGISTRO',
            'ÚLTIMA ACTUALIZACIÓN',
            'PRESTAMO',
            'PLAZO',
            'EQUIPO LEASING',
            'NOS GUSTARÍA CONOCERTE',
            'NOMBRE',
            'APELLIDO P',
            'APELLIDO M',
            'EMAIL',
            'CELULAR',
            //'(LOG DE CONSOLA) ENCONTRADO',
            'NOMBRE EMPRESA',
            'PROPIEDAD',
            'FECHA DE FUNDACION',
            'CALLE',
            'NUM INT',
            'CIUDAD',
            'ESTADO',
            'CODIGO POSTAL',
            'PAIS',
            'DESCRIPCION DE LA EMPRESA',
            'VENTAS MENSUALES',
            'MARGEN DE VENTAS',
            'NÚMERO DE EMPLEADOS',
            'UNICO DUEÑO',
            'OTROS NOMBRES COMERCIALES',
            'MONTO DE CUENTAS POR COBRAR',
            'DEUDAS DE IMPUESTOS',
            'DEUDAS COMERCIALES',
            'MONTO DE IMPUESTOS',
            'DEUDAS DE LA EMPRESA',
            'NÚMERO SEGURIDAD SOCIAL',
            'NÚMERO DE LICENCIA DE CONDUCIR',
            'DEPOSITOS MENSUALES',
            'SALDO MENSUAL',
            'NOMBRE DEL SOCIO',
            'FECHA NAC DEL SOCIO',
            'EMAIL DEL SOCIO',
            'TELEFONO DEL SOCIO',
            'DIRECCIÓN DEL SOCIO',
            'PORCENTAJE DE PARTICIPACION',
        ];
    }


    public function map($solicitud): array {

        //$solicitud = $solicitud;
        $prospecto = self::decryptProspect($solicitud->prospecto);

        $referencia = null;
        $producto = $solicitud->producto;
        $nombre_empresa = isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->nombre_empresa : '';
        if (count($producto) != 0) {
            $nombre_producto = mb_strtoupper($producto[0]->nombre_producto);
            if ($producto[0]->pivot['lead'] != null) {
                $referencia = mb_strtoupper($producto[0]->pivot['lead']);
            }
        } else {
            $nombre_producto = 'MERCADO ABIERTO';
        }

        $calle = '';
        $num_interior = '';
        $ciudad = '';
        $estado = '';
        $cp = '';
        $pais = '';
        
        if (isset($solicitud->datos_empresa['calle'])) {
            $calle = $solicitud->datos_empresa['calle'];
            $num_interior = $solicitud->datos_empresa['num_interior'];
            $colonia = $solicitud->datos_empresa['ciudad'];
            $estado = $solicitud->datos_empresa['estado'];
            $cp = $solicitud->datos_empresa['cp'];
            $pais = $solicitud->datos_empresa['pais'];
            
        }

        
        return [
            $solicitud->id,
            $solicitud->prospecto_id,
            $solicitud->created_at,
            $nombre_producto,
            $solicitud->status,
            $solicitud->sub_status,
            $solicitud->updated_at,
            $solicitud->prestamo,
            $solicitud->plazo,
            $solicitud->equipo_leasing,
            $solicitud->finalidad,
            $solicitud->prospecto->nombres,
            $solicitud->prospecto->apellido_paterno,
            $solicitud->prospecto->apellido_materno,
            $solicitud->prospecto->email,
            $solicitud->prospecto->celular,
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->nombre_empresa : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->tipo_propiedad : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->fecha_fundacion : '',
            $calle,
            $num_interior,
            $ciudad,
            $estado,
            $cp,
            $pais,
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->descripcion_empresa : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->ventas_mensuales : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->margen_ventas : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->numero_empleados : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->unico_dueño : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->otros_nombres_comerciales : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->monto_cuentas_cobrar : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->deudas_impuestos : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->deudas_comerciales : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->monto_impuestos : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->deudas_empresa : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->nss : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->licencia_conducir : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->depositos_mensuales : '',
            isset($solicitud->datos_empresa) ? $solicitud->datos_empresa->saldo_mensual : '', 
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->nombre_completo : '', 
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->fecha_nacimiento : '', 
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->email : '',
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->telefono : '',
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->direccion_completa : '',
            isset($solicitud->datos_socios) ? $solicitud->datos_socios->porcentaje_participacion : '',  
            
        ];
    }
}
