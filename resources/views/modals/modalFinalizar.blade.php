
    <div id="solicFound">
      <div class="modal__heading bg-blue">
          <h5 class="modal-title" id="AvisoImportante" style="color: #fff">¡Muchas gracias por cargar tus documentos!</h5>
      </div>
        <div class="modal__body">
           <div class="Img"><img src="/credito-real/img/credito-real-usa.svg" height="120px" alt=""></div> <br>
           <div class="modal__text">
            <p>Nuestro equipo estará revisando la documentación compartida.</p>
            <p>Una vez que la documentación este completa y correcta, nos pondremos en contacto contigo para ayudar a concluir tu solicitud.</p>
            <p><strong> Contáctanos <br>soporte@crealusabc.com  <br></strong>   </p>
            <a href="/" type="button" class="btn BtnCredito Back1" data-bs-dismiss="modal" aria-label="Cerrar">Cerrar</a>
        </div>
      </div>
    </div>