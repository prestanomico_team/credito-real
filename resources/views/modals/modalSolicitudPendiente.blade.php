<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="AvisoImportante">Solicitud Pendiente</h5>
    </div>
    <div class="modal-body">
        <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="80px" alt=""></div> <br>
        <b>¡La encontramos!</b>
        <p>Te informamos que tienes una solicitud pendiente.</p>
        <p>Debes terminar la solicitud <br>para continuar con el proceso.</p>

    </div>
</div>
