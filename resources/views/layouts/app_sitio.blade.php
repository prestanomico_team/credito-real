<!Doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Crédito Real Usa - Business Capital">
    <meta name="author" description="Prestanomico"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="pingback" href="Crédito Real Usa"/>
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="{{ asset('credito-real') }}/favicon/credito-real-usa.ico" rel="SHORTCUT ICON">

    <!-- compatilibdad con webapp iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Crédito Real Usa">
    <link rel="apple-touch-icon"  href="{{ asset('credito-real') }}/favicon/apple.png">

    <!-- compatilibdad con webapp Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#004a98">
    <meta name="application-name" content="Conectatest">
    <link rel="icon" type="image/png" href="{{ asset('credito-real') }}/favicon/android.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('credito-real') }}/css/credito-real-usa.css">
    <link rel="stylesheet" type="text/css" href="/css/modals.css?v=<?php echo microtime(); ?>">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-glyphicons.css?v=<?php echo microtime(); ?>">

    <title>{{ config('app.name', 'Credito Real USA') }}</title>

    <!-- Facebook Pixel Code -->

    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '4480572125352853');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=4480572125352853&ev=PageView&noscript=1"/>
</noscript>

<!-- End Facebook Pixel Code -->
</head>
    <body class="{{ $class ?? '' }}">
    @if (Auth::guard('prospecto')->check())
        <script>
             window.onload = function() {
                 $('.sesionIniciada').show()
                 $('.iniciarSesion').hide()
                 getForm();
             };
        </script>
    @endif
    @include('googletagmanager::head')
         <!-- Google Analytics -->
         <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
                @verbatim
                    ga('create', 'UA-217784197-1', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @else
                 @verbatim
                    ga('create', 'UA-217784197-2', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @endif
            ga(function(tracker) {
                var clientId = tracker.get('clientId');
                tracker.set('dimension1', tracker.get('clientId'));
            });
            ga('send', 'pageview');
         </script>
        @include('googletagmanager::body')

        <section class="MenuPrincipal">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="Logo">
                            <a href="/"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-9">
                        <div class="">
                            <ul class="Desktop">
                                <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                                <li><a href="/faqs">Preguntas frecuentes</a></li>
                                @if (!Auth::guard('prospecto')->check())
                                    <li class="iniciarSesion"><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                                    <li class="iniciarSesion"><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                                    <li class="sesionIniciada" style="display: none"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                                    <li class="sesionIniciada"><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                                @endif
                                @if (Auth::guard('prospecto')->check())
                                <li class="sesionIniciada"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                                @endif

                            </ul>
                            <ul class="Mobile">
                                <li>
                                    <a id="BtnMovil" href="#">
                                        <i class="material-icons" style="vertical-align: middle;">menu</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="SideNav">
            <ul>
                <div class="AvatarLogo">
                    <div class="Background">
                        <div class="Img">
                            <img src="{{ asset('credito-real') }}/img/sidenav.jpg" alt="">
                        </div>
                    </div>
                </div>
                <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                <li><a href="/faqs">Preguntas frecuentes</a></li>
                @if (!Auth::guard('prospecto')->check())
                    <li class="iniciarSesion"><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                    <li class="iniciarSesion"><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                    <li class="sesionIniciada" style="display: none"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                    <li class="sesionIniciada"><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                @endif
                @if (Auth::guard('prospecto')->check())
                <li class="sesionIniciada"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                @endif
            </ul>
        </div>
        <div class="main-content">
            @yield('content')
        </div>
            <!-- Modal -->
        <div class="modal fade" id="AvisoImportante" tabindex="-1">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="AvisoImportante">Aviso Importante</h5>
                </div>
                <div class="modal-body">
                <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="120px" alt=""></div> <br>
                <p>Crédito Real USA no pide depósitos ni cualquier tipo de pago anticipado para otorgar financiamientos.</p>
                <p>Al comunicarte con nosotros y enviar documentos utiliza nuestros medios oficiales</p>
                <p><strong> App móvil Prestanómico <br>soporte@crealusabc.com <br>(55) 5512-34356</strong>   </p>
                <button type="button" class="btn BtnCredito Back1" data-bs-dismiss="modal" aria-label="Cerrar">Cerrar</button>
                </div>
            </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="LoginForm" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-body" id="formLogin">
                    <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="70px" alt=""></div>
                    <h3>Bienvenidos</h3>
                    <form id="formLogin">
                        <div class="mb-3">
                            <label class="form-label"></label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Usuario/Correo electrónico">
                        </div>
                        <div class="mb-3">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                        </div>
                        <span id="loginError" class="error" style="border-bottom: none !important; font-size: 13px; color: red !important;"></span>
                        <div class="Olvidar">
                            <a onclick="focusSimulador()">¿No eres cliente? Checa si calificas</a> <br>
                            <a href="/password-restore" id="olvidePassword">¿Olvidaste tu contraseña?</a> <br><br>
                        </div>
                        <a onclick="iniciarSesion()" type="submit" class="btn BtnCredito Back1">Entrar <i class="material-icons" style="vertical-align: middle;">lock</i></a>
                    </form>
                </div>
                </div>
            </div>
        </div>

        <div class="ScrollTop">
            <a href="#" id="irArriba">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        </div>

        <div class="SidenavOverlay"></div>
        @guest()
            @include('layouts.footers.guest')
        @endguest

        <script src="{{ asset('credito-real') }}/js/jquery-3.6.0.min.js"></script>
        <script src="{{ asset('credito-real') }}/js/lib/maskmoney.min.js"></script>
        <script src="{{ asset('credito-real') }}/js/bootstrap.js"></script>
        <script src="{{ asset('credito-real') }}/js/myscripts.js"></script>
        <script src="{{ asset('credito-real') }}/js/all.js?v=<?php echo microtime(); ?>"></script>
        <script type="text/javascript" src="{{ asset('credito-real') }}/js/password_restore.js?v=<?php echo microtime(); ?>"></script>
        <script src="{{ asset('credito-real') }}/js/lib/axios.min.js"></script>
        <script src="{{ asset('credito-real') }}/js/lib/sweetalert2.all.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2QFwl7UlbpYib_hUh--dBfVjdrfqiJEM&libraries=places&callback=initMap&solution_channel=GMP_QB_addressselection_v1_cAC" async defer></script>
        @stack('js')

        <!-- Argon JS -->
        <!-- <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script> -->
        @include('googletagmanager::body')
    </body>
</html>
