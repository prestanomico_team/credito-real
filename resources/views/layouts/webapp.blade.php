<!Doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Crédito Real Usa - Business Capital">
    <meta name="author" description="Prestanomico"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="pingback" href="Crédito Real Usa"/>
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="{{ asset('credito-real') }}/favicon/credito-real-usa.ico" rel="SHORTCUT ICON">

    <!-- compatilibdad con webapp iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Crédito Real Usa">
    <link rel="apple-touch-icon"  href="{{ asset('credito-real') }}/favicon/apple.png">

    <!-- compatilibdad con webapp Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#004a98">
    <meta name="application-name" content="Conectatest">
    <link rel="icon" type="image/png" href="{{ asset('credito-real') }}/favicon/android.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('credito-real') }}/css/credito-real-usa.css">

    <title>{{ config('app.name', 'Credito Real USA') }}</title>

    <script src="{{ asset('credito-real') }}/js/all_facematch.js?v=<?php echo microtime(); ?>"></script>
    @if(str_contains(url()->current(), 'facematch'))
        <script async defer src="{{ asset('credito-real') }}/js/webapp/icarSDK.js?v=1611"></script>
    @endif
    <link rel="stylesheet" href="/credito-real/css/webapp/font-awesome.css">
	<!-- <link rel='stylesheet' href='/credito-real/icon-webapp2/styles.css'> -->
	<link rel="stylesheet" href="/credito-real/css/webapp/webapp.css?v=<?php echo microtime(); ?>">
    <link rel="stylesheet" href="/credito-real/css/webapp/dropzone.min.css">
    <link rel="stylesheet" href="/credito-real/css/modals.css?v=<?php echo microtime(); ?>">
    <script src="{{ asset('credito-real') }}/js/myscripts.js"></script>

</head>
    <body class="{{ $class ?? '' }}">
    @include('googletagmanager::head')
         <!-- Google Analytics -->
         <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
                @verbatim
                    ga('create', 'UA-217784197-1', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @else
                 @verbatim
                    ga('create', 'UA-217784197-2', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @endif
            ga(function(tracker) {
                var clientId = tracker.get('clientId');
                tracker.set('dimension1', tracker.get('clientId'));
            });
            ga('send', 'pageview');
         </script>
        @include('googletagmanager::body')
        <section class="MenuPrincipal">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="Logo">
                            <a href="/"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-9">
                        <div class="">
                            <ul class="Desktop">
                                <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                                <li><a href="/faqs">Preguntas frecuentes</a></li>
                                @if (!Auth::guard('prospecto')->check())
                                    <li class="iniciarSesion"><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                                    <li class="iniciarSesion"><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                                    <li class="sesionIniciada" style="display: none"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                                    <li class="sesionIniciada"><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                                @endif
                                @if (Auth::guard('prospecto')->check())
                                <li class="sesionIniciada"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                                @endif

                            </ul>
                            <ul class="Mobile">
                                <li>
                                    <a id="BtnMovil" href="#">
                                        <i class="material-icons" style="vertical-align: middle;">menu</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="SideNav">
            <ul>
                <div class="AvatarLogo">
                    <div class="Background">
                        <div class="Img">
                            <img src="{{ asset('credito-real') }}/img/sidenav.jpg" alt="">
                        </div>
                    </div>
                </div>
                <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                <li><a href="/faqs">Preguntas frecuentes</a></li>
                @if (!Auth::guard('prospecto')->check())
                    <li class="iniciarSesion"><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                    <li class="iniciarSesion"><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                    <li class="sesionIniciada" style="display: none"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                    <li class="sesionIniciada"><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                @endif
                @if (Auth::guard('prospecto')->check())
                <li class="sesionIniciada"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                @endif
            </ul>
        </div>
        <div class="main-content">
            
            @yield('content')
        </div>
            <!-- Modal -->
        <div class="modal fade" id="AvisoImportante" tabindex="-1">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="AvisoImportante">Aviso Importante</h5>
                </div>
                <div class="modal-body">
                <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="120px" alt=""></div> <br>
                <p>Crédito Real USA no pide depósitos ni cualquier tipo de pago anticipado para otrogar financiamientos.</p>
                <p>Al comunicarte con nosotros y enviar documentos utiliza nuestros medios oficiales</p>
                <p><strong> App móvil Prestanómico <br>atencion@creditoreal.com  <br>(55) 5512-34356</strong>   </p>
                <a href="/" type="button" class="btn BtnCredito Back1" data-bs-dismiss="modal" aria-label="Cerrar">Cerrar</a>
                </div>
            </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="LoginForm" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-body" id="formLogin">
                    <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="70px" alt=""></div> 
                    <h3>Bienvenidos</h3>
                    <form id="formLogin">
                        <div class="mb-3">
                            <label class="form-label"></label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Usuario/Correo electrónico">
                        </div>
                        <div class="mb-3">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                        </div>
                        <div class="Olvidar">
                            <a href="#">¿No eres cliente? Checa si calificas</a> <br>
                            <a href="#">¿Olvidaste tu contraseña?</a> <br><br>
                        </div>
                        <a onclick="iniciarSesion()" type="submit" class="btn BtnCredito Back1">Entrar <i class="material-icons" style="vertical-align: middle;">lock</i></a>
                    </form> 
                </div>
                </div>
            </div>
        </div>    
            
        <div class="ScrollTop">
            <a href="#" id="irArriba">
                <i class="material-icons">keyboard_arrow_up</i>
            </a>
        </div>

        <div class="SidenavOverlay"></div>
        @guest()
            @include('layouts.footers.guest')
        @endguest

        
        @stack('js')
        
        <!-- Argon JS -->
        <!-- <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script> -->
        @include('googletagmanager::body')
    </body>
</html>