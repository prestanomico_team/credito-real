@extends('layouts.app_sitio')
@section('content')
<section id="carouselCreditoUsa" class="carousel slide carousel-fade CarouselCreditoUsa" data-bs-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('credito-real') }}/img/maquinaria.jpg" class="d-block w-100" alt="Arrendamiento Real Usa">
            <div class="carousel-caption d-none d-md-block">
                <h3>Arrendamiento de máquinas accesible especialmente <span>para ti.</span></h3>
                <h5>Consigue el equipo que necesitas crecer</h5>
                <p>Estabiliza tu flujo de caja, acepta más trabajo y aumenta los beneficios, con el arrendamiento de equipos de Crédito <strong>Real USA Business Capital.</strong></p>
                <a class="btn Back1" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</a>
            </div>
        </div>
    </div>
</section>

<section class="EquiposFinanciar">
    <div class="container">
        <div class="row" id="restablecer-pass">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Restablecer contraseña</h2>
                    <h6>¿Olvidaste tu contraseña?</h6>
                    <h6>No te preocupes, reestablecer tu contraseña es muy fácil.</h6>                   
                </div>
            </div>
        </div>
        <div class="row" id="password-restore">
            <form id="form-password-restore">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"> <br>
                    <h6>Solo ingresa el correo electrónico con el que estás registrado en <b>CREDITO REAL USA</b></h6>
                        <div class="Codigo">
                            <input type="text" class="form-control required" id="email" name="email">
                            <small id="email-help" class="help"></small>
                        </div>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12 BtnsGo">
                        <a class="btn BtnCredito Back4" onclick="emailPasswordRestore()">Enviar</a><br><br><br><br>
                    </div> 
                </div>
            </form>
        </div>
        <div class="row" id="temporary-password" style="display:none">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <!-- <h2 class="secondary-title txt-center">Restablecer contraseña</h2> -->
                <div class="calculadoras-productos"><small>Te hemos enviado un SMS al celular <b id="celular_enviado"></b> con un código de verificación.</small></div>
                <div class="calculadoras-productos"><small>Ingresa ese código en campo Código de verificación y tu nueva contraseña, después da click en actualizar contraseña</b></small></div>
            </div>
            <div class="col-sm-4"></div>
            <div class="row">
                <form id="form-temporary-password">
                    <div class="row">
                        <div class="col-sm-4"></div>
                            <div class="col-sm-4"> <br>
                    
                                <div class="Codigo">
                                    <input type="text" class="form-control required" id="codigo_verificacion" name="codigo_verificacion" placeholder="Código de verificación">
                                    <input type="hidden" id="email_usuario" name="email_usuario"/>
                                    <small id="codigo_verificacion-help" class="help"></small>
                                </div>
                            </div>
                        <div class="col-sm-4"></div>
                    </div>
                   <div class="row"> 
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                            <div class="Formula">
                                <div class="FormInput">
                                    <label class="form-label">Crear tu nueva contraseña</label>
                                    <input type="password" class="form-control required" id="new_password" name="new_password" maxlength="50" type="password">
                                    <small id="new_password-help" class="help"></small>
                                    <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
                                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
                                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
                                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Mínimo 8 caracteres</div>
                                </div><br><br>
                               
                            </div>
                        </div> 
                        <div class="col-sm-4">
                            <div class="FormInput">
                                <label class="form-label">Confirma tu nueva contraseña</label>
                                <input type="password" class="form-control required" id="confirm_password" name="confirm_password" maxlength="50" type="password">
                                <small id="new_password-help" class="help"></small>
                            </div>
                        </div>
                   </div><br />
                    <div class="row">
                        <div class="col-sm-12 BtnsGo">
                            <a class="btn BtnCredito Back4" onclick="validateTemporaryPassword()">Actualizar contraseña</a><br><br><br><br>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="password-update-ok" style="display:none">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" style="text-align:center">
            <h2 class="secondary-title txt-center">Contraseña actualizada</h2>
                <p>La contraseña se ha actualizado con éxito.</p>
                <p>Ya puedes ingresar con tu nueva contraseña.</p>
                <p>Asegurate de memorizarla o anotarla en un lugar seguro.</p>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</section>
@endsection