@extends('crm.app')
@section('content')
<?php
	$msg_arr = [];
	$indexed_msgs = [];
	foreach($logs as $lg){
		$lg = json_decode($lg);
		$msg = '';//default
		if(is_object($lg)){
			if(is_object($lg->console)){
				foreach($lg->console as $key => $val){
					if($key == 'message'){
						$msg = $val;
						if(!in_array($val, $msg_arr)){
							array_push($msg_arr, $val);
						}
					}
				}
			}
		}
		array_push($indexed_msgs,$msg);
	}
	$msg_counts = [];
	foreach($msg_arr as $unique_msg){
		$msg_occurrance = 0;
		//count the number of times this message appears in logs
		foreach($indexed_msgs as $m){
			if($m == $unique_msg){
				$msg_occurrance++;
			}
		}
		$msg_counts[$unique_msg] = $msg_occurrance;
	}
	// echo count($logs).' logs<br>';
	// echo count($indexed_msgs).' indexed<br>';
?>
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="row">
			<div class="col-sm-4">
				<div class="title">
					<h2>ERRORES DE REGISTRO</h2>
				</div>
			</div>
			@if(Auth::user()->can('logs-registro'))
			<div class="col-sm-4">
				<form class="form" id="filter-errors-form">
					<div class="form-group">
						<label for="filter-by-message">Filtrar Por Error</label>
						<select id="filter-by-message" class="form-control">
							<option value=""></option>
							<?php
								$msg_id = 0;
								foreach($msg_arr as $msg){
									echo '<option value="'.$msg_id.'">('.$msg_counts[$msg].') &nbsp; '.$msg.'</option>';
									$msg_id++;
								}
							?>
						</select>
					</div>
				</form>
			</div>
			@endif
			<div class="col-sm-4">
				&nbsp;
			</div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Log de errores</h3>
		  </div>
		  <div class="panel-body">
		    <table class="table table-striped" style="font-size:12px;">
					<tr>
						<th>FECHA HORA</th>
						<th>NOMBRE</th>
						<th>CONSOLE</th>
						<th>DATOS</th>
					</tr>
					@if(Auth::user()->can('logs-registro'))
					<?php
						foreach($logs as $index => $lg){
							$lg = json_decode($lg);
								if(is_object($lg)){
									if(is_object($lg->console)){
									//get the message id for this log
									$lg_msg = $indexed_msgs[$index];
									//get the msg type id for filtering errors
									$this_type = 999999;//default
									$msg_id = 0;
									foreach($msg_arr as $msg){
										if($msg == $lg_msg){
											$this_type = $msg_id;
										}
										$msg_id++;
									}
									echo '
										<tr class="error_row tipo_'.$this_type.'">
											<td width="200px">
												'.$lg->datetime.'
											</td>
											<td>
												'.$lg->name.'
											</td>
											<td width="300px">
												';
												$mnsj_obj = $lg->console;
												if(is_object($mnsj_obj)){
													foreach($mnsj_obj as $key => $val){
														echo '<strong>'.$key.': </strong>'.$val.'<br>';
													}
												}
											echo '
											</td>
											<td>';
												foreach($lg->data as $key => $val){
													if($key != '_token' && $key != 'password' && $key != 'password_conf'){
														echo '<strong>'.$key.': </strong>'.$val.'<br>';
													}
												}
											echo '
											</td>
										</tr>
									';
								}
							}
						}
					?>
					@else
						<center>
							<tr>
								<td colspan="4">No tienes privilegios para ver los logs de registro</td>
							</tr>
						</center>
					@endif
				</table>
		  </div>
		</div>
	</div>
</div>
@endsection
