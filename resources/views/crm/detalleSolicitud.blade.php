@extends('crm.app')
@section('content')
<link href="/back/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<div class="container" style="width:100%">
	<div id="app" class="content">

	    <detalle_solicitud :solicitud_id={{ $solicitud_id }}></detalle_solicitud>

	</div>
</div>
@endsection
@section('app_backoffice')
	<script type="text/javascript" src="/credito-real/js/app_backoffice.js?v=<?php echo microtime(); ?>"></script>
@append
