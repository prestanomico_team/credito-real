@extends('crm.app')
@section('content')
	<div class="container" style="width:100%">
		<div id="dashboard-container" class="content">
			<div class="title">
				<h2>Prospecto</lab></h2>
			</div>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Solicitudes de Cliente # {{ $prospect->id }} {{ $prospect->nombre }} {{ $prospect->apellido_p }} {{ $prospect->apellido_m }}</h3>
			  </div>
			  <div class="panel-body">
			    <table class="table table-striped">
						<th>ID</th>
						<th>FECHA INICIO</th>
						<th>ACTUALIZACIÓN</th>
						<th>PRODUCTO</th>
						<th>STATUS</th>
						<th>PRESTAMO</th>
						<th>PLAZO</th>
						<th>FINALIDAD</th>
						<th>PAGO EST.</th>
						<th>ENCONTRADO</th>
						<th>BC SCORE</th>
						<th>MICRO SCORE</th>
						@if(Auth::user()->can('solicitudes'))
							@foreach($solics as $sl)
							<tr>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->id }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->created_at_disp }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->updated_at_disp }}</a>
								</td>
								@if (count($sl->producto) == 1)
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ mb_strtoupper($sl->producto[0]->nombre_producto) }}</a>
								</td>
								@else
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">MERCADO ABIERTO</a>
								</td>
								@endif
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ mb_strtoupper($sl->type) }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->prestamo }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ mb_strtoupper($sl->plazo) }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ mb_strtoupper($sl->finalidad) }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->pago_estimado }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ mb_strtoupper($sl->encontrado) }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->bc_score }}</a>
								</td>
								<td>
									<a href="#" data-toggle="modal" data-target="#solic_modal_{{ $sl->id }}">{{ $sl->micro_valor }}</a>
								</td>
							</tr>
							@endforeach
						@else
							<tr>
								<td colspan="12" style="text-align:center">No tienes privilegios para ver las solicitudes del prospecto</td>
							</tr>
						@endif
					</table>
			  </div>
			</div>
		</div>
	</div>
	@if(Auth::user()->can('detalle-solicitud'))
		@foreach($solics as $sl)
		<div id="solic_modal_{{ $sl->id }}" class="modal fade" tabindex="-1" role="dialog">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Solicitud # {{ $sl->id }}</h4>
		      </div>
		      <div class="modal-body">
		      	<strong>FECHA DE SOLICITUD:</strong> {{ $sl->created_at_disp }}<br><br>
		      	<strong>STATUS:</strong> {{ mb_strtoupper($sl->type) }}<br><br>
						<!--==================================================================== -->
		      	<!--======================== DATOS DEL PRESTAMO ======================== -->
		      	<!--==================================================================== -->
		      	<a href="#datos-prestamo_{{ $sl->id }}" data-toggle="collapse" class="btn btn-success btn-lg reporte-section-btn" style="width:100%">
							DATOS DEL PRESTAMO
						</a>
		        <div class="reporte-panel collapse" id="datos-prestamo_{{ $sl->id }}">
		        	<table class="table table-striped">
								<tr>
									<th width="40%">PRESTAMO:</th>
									<td>{{ $sl->prestamo }}</td>
								</tr>
								<tr>
									<th width="40%">PLAZO:</th>
									<td>{{ mb_strtoupper($sl->plazo) }}</td>
								</tr>
								<tr>
									<th width="40%">FINALIDAD:</th>
									<td>{{ mb_strtoupper($sl->finalidad) }}</td>
								</tr>
								<tr>
									<th width="40%">NOS GUSTARÍA CONOCERTE:</th>
									<td>{{ mb_strtoupper($sl->finalidad_custom) }}</td>
								</tr>
								<tr>
									<th width="40%">PAGO ESTIMADO:</th>
									<td>{{ $sl->pago_estimado }}</td>
								</tr>
							</table>
						</div>

						@if(count($sl->respuesta_maquina_riesgos) == 1)
							@if($sl->respuesta_maquina_riesgos->monto != null)
							<!--==================================================================== -->
					      	<!--======================== DATOS DE LA OFERTA ========================= -->
					      	<!--==================================================================== -->
								<a href="#datos-oferta_{{ $sl->id }}" data-toggle="collapse" class="btn btn-success btn-lg reporte-section-btn" style="width:100%">
									DATOS DE LA OFERTA
								</a>
					        	<div class="reporte-panel collapse" id="datos-oferta_{{ $sl->id }}">
				        			<table class="table table-striped">
										<tr>
											<th width="40%">STATUS OFERTA:</th>
											<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->status_oferta) }}</td>
										</tr>
										@if($sl->respuesta_maquina_riesgos->status_oferta == 'Oferta Rechazada')
											<tr>
												<th width="40%">MOTIVO RECHAZO:</th>
												<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->motivo_rechazo) }}</td>
											</tr>
											@if($sl->respuesta_maquina_riesgos->motivo_rechazo == 'OTRO')
												<tr>
													<th width="40%">DESCRIPCION:</th>
													<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->descripcion_otro) }}</td>
												</tr>
											@endif
										@endif
										<tr>
											<th width="40%">MONTO:</th>
											<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->monto) }}</td>
										</tr>
										<tr>
											<th width="40%">PLAZO:</th>
											<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->plazo) }}</td>
										</tr>
										<tr>
											<th width="40%">PAGO ESTIMADO:</th>
											<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->pago) }}</td>
										</tr>
										<tr>
											<th width="40%">TASA:</th>
											<td>{{ mb_strtoupper($sl->respuesta_maquina_riesgos->tasa) }}</td>
										</tr>
									</table>
								</div>
							@endif
						@endif

						<!--==================================================================== -->
		      	<!--======================== DATOS DEL USUARIO ========================= -->
		      	<!--==================================================================== -->
		      	<a href="#datos-usuario-panel_{{ $sl->id }}" data-toggle="collapse" class="btn btn-primary btn-lg reporte-section-btn" style="width:100%">
							DATOS DEL USUARIO
						</a>
		        <div class="reporte-panel collapse" id="datos-usuario-panel_{{ $sl->id }}">
		        	<table class="table table-striped">
		        		<tr>
									<th width="40%">ID:</th>
									<td>{{ $prospect->id }}</td>
								</tr>
								<tr>
									<th width="40%">Corréo Electrónico:</th>
									<td>{{ $prospect->email }}</td>
								</tr>
								<tr>
									<th width="40%">Contraseña:</th>
									<td>
										<?php
											if($prospect->password_decrypted != ''){
												echo '<input class="decryptd_pw form-control" type="password" value="'.$prospect->password_decrypted.'" style="width:200px;float:left;margin-right:15px;"> <a href="#" class="view_decryptd_pw btn btn-default btn-xs">Mostrar</a> <a href="#" class="hide_decryptd_pw btn btn-default btn-xs hidden">Ocultar</a>';
											}else{
												echo 'N/A';
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="40%">NOMBRE:</th>
									<td>{{ mb_strtoupper($prospect->nombre) }}</td>
								</tr>
								<tr>
									<th width="40%">APELLIDO P:</th>
									<td>{{ mb_strtoupper($prospect->apellido_p) }}</td>
								</tr>
								<tr>
									<th width="40%">APELLIDO M:</th>
									<td>{{ mb_strtoupper($prospect->apellido_m) }}</td>
								</tr>
								<tr>
									<th width="40%">GENERO:</th>
									<td>{{ mb_strtoupper($sl->sexo) }}</td>
								</tr>
								<tr>
									<th width="40%">FECHA DE NACIMIENTO:</th>
									<td>{{ $sl->fecha_nac }}</td>
								</tr>
								<tr>
									<th width="40%">LUGAR DE NACIMIENTO:</th>
									<td>{{ mb_strtoupper($sl->lugar_nac_ciudad) }} {{ mb_strtoupper($sl->lugar_nac_estado) }}</td>
								</tr>
								@if( count($sl->domicilio) > 0 )
									@php
										$calle = mb_strtoupper($sl->domicilio['calle']);
										$num_exterior =  mb_strtoupper($sl->domicilio['num_exterior']);
										$num_interior = mb_strtoupper($sl->domicilio['num_interior']);
										$colonia = mb_strtoupper($sl->domicilio['colonia']);
										$delegacion = mb_strtoupper($sl->domicilio['delegacion']);
										$ciudad = mb_strtoupper($sl->domicilio['ciudad']);
										$cp = $sl->domicilio['cp'];
										$estado = mb_strtoupper($sl->domicilio['estado']);
									@endphp
								@else
								@php
									$calle = mb_strtoupper($sl->dom_calle);
									$num_exterior =  mb_strtoupper($sl->dom_num_ext);
									$num_interior = mb_strtoupper($sl->dom_num_int);
									$colonia = mb_strtoupper($sl->dom_colonia);
									$delegacion = mb_strtoupper($sl->dom_deleg);
									$ciudad = mb_strtoupper($sl->dom_ciudad);
									$cp = $sl->dom_cp;
									$estado = mb_strtoupper($sl->dom_estado);
								@endphp
								@endif
								<tr>
									<th width="40%">DOMICILIO:</th>
									<td>{{ $calle }} {{ $num_exterior }} {{ $num_interior }}</td>
								</tr>
								<tr>
									<th width="40%">COLONIA:</th>
									<td>{{ $colonia }}</td>
								</tr>
								<tr>
									<th width="40%">DELEGACION:</th>
									<td>{{ $delegacion }}</td>
								</tr>
								<tr>
									<th width="40%">CIUDAD:</th>
									<td>{{ $ciudad }}</td>
								</tr>
								<tr>
									<th width="40%">CP:</th>
									<td>{{ $cp }}</td>
								</tr>
								<tr>
									<th width="40%">ESTADO:</th>
									<td>{{ $estado }}</td>
								</tr>
								<tr>
									<th width="40%">RFC:</th>
									<td>{{ mb_strtoupper($sl->rfc) }}</td>
								</tr>
								<tr>
									<th width="40%">CURP:</th>
									<td>{{ mb_strtoupper($sl->curp) }}</td>
								</tr>
								<tr>
									<th width="40%">TELEFONO CASA:</th>
									<td>{{ $sl->tel_casa }}</td>
								</tr>
								<tr>
									<th width="40%">ESTADO CIVIL:</th>
									<td>{{ mb_strtoupper($sl->estado_civil) }}</td>
								</tr>
								<tr>
									<th width="40%">NIVEL DE ESTUDIOS:</th>
									<td>{{ mb_strtoupper($sl->nivel_estudios) }}</td>
								</tr>
								<tr>
									<th width="40%">CREDITO HIPOTECARIO:</th>
									<td>{{ mb_strtoupper($sl->credito_hipo) }}</td>
								</tr>
								<tr>
									<th width="40%">CREDITO AUTOMOTRIZ:</th>
									<td>{{ mb_strtoupper($sl->credito_auto) }}</td>
								</tr>
								<tr>
									<th width="40%">CREDITO BANCARIO:</th>
									<td>{{ mb_strtoupper($sl->credito_banc) }}</td>
								</tr>
								<tr>
									<th width="40%">ULT 4 NUM TDC:</th>
									<td>{{ $sl->credito_banc_num }}</td>
								</tr>
								<tr>
									<th width="40%">TIPO DE RESIDENCIA:</th>
									<td>{{ mb_strtoupper($sl->tipo_residencia) }}</td>
								</tr>
								<tr>
									<th width="40%">INGRESO TOTAL MENSUAL ACTUAL:</th>
									<td>{{ $sl->ingreso_mensual }}</td>
								</tr>
								@if ($sl->nombre_empresa != null)
								<tr>
									<th width="40%">NOMBRE DE LA EMPRESA/DEPENDENCIA/NEGOCIO:</th>
									<td>{{ mb_strtoupper($sl->nombre_empresa) }}</td>
								</tr>
								@endif
								<tr>
									<th width="40%">ANTIGUEDAD DE EMPLEO ACTUAL:</th>
									<td>{{ mb_strtoupper($sl->antiguedad_empleo) }}</td>
								</tr>
								<tr>
									<th width="40%">ANTIGUEDAD EN DOMICILIO ACTUAL:</th>
									<td>{{ mb_strtoupper($sl->antiguedad_domicilio) }}</td>
								</tr>
								<tr>
									<th width="40%">MONTO GASTOS FAMILIARES MENSUALES:</th>
									<td>{{ $sl->gastos_familiares }}</td>
								</tr>
								<tr>
									<th width="40%">NUMERO DE DEPENDIENTES</th>
									<td>{{ $sl->numero_dependientes }}</td>
								</tr>
								<tr>
									<th width="40%">OCUPACION</th>
									<td>{{ mb_strtoupper($sl->ocupacion) }}</td>
								</tr>
								<tr>
									<th width="40%">PRINCIPAL FUENTE DE INGRESOS</th>
									<td>{{ mb_strtoupper($sl->fuente_ingresos) }}</td>
								</tr>
								<tr>
									<th width="40%">TELEFONO DE EMPLEO</th>
									<td>{{ $sl->telefono_empleo }}</td>
								</tr>
								<tr>
									<th width="40%">AUTORIZO CONSULTA BC:</th>
									<td>{{ mb_strtoupper($sl->autorizado) }}</td>
								</tr>
								<tr>
									<th width="40%">IP DE USUARIO</th>
									<td>{{ $sl->user_ip }}</td>
								</tr>
								<tr>
									<th colspan="2"><hr></th>
								</tr>
							</table>
						</div>

						<!-- =========== RESPUESTAS CUESTIONARIO DINÁMICO ============= -->
						@if(count($sl->respuesta_maquina_riesgos) == 1)
							<a href="#CuestionarioDinamico_{{ $sl->id }}" data-toggle="collapse" class="btn btn-info btn-lg reporte-section-btn" style="width:100%">
								CUESTIONARIO DINAMICO
							</a>
							<div class="reporte-panel collapse" id="CuestionarioDinamico_{{ $sl->id }}">
								<table class="table table-striped">
									<tr>
									@if ($sl->respuesta_maquina_riesgos->pantallas_extra == 1)
										<th width="20%">SITUACIONES:</th>
										<td>{{ $sl->respuesta_maquina_riesgos->situaciones }}</td>
									@else
										<th width="20%">Situaciones:</th>
										<td>No Aplica</td>
									@endif
									</tr>
								</table>
								@if ($sl->respuesta_maquina_riesgos->pantallas_extra == 1)
									@if ($sl->respuesta_maquina_riesgos->cuestionario_dinamico_guardado == 1)
										@foreach ($cuestionarios_dinamicos[$sl->id]['respuestas'] as $key => $respuesta)
											<center><h5>{{ $cuestionarios_dinamicos[$sl->id]['situaciones'][$key]['encabezado'] }}<h5></center>
											<br>
											<table class="table table-striped">
												@foreach ($respuesta as $keyRespuesta => $datoRespuesta)
													<tr  style="font-size: 13px">
														<th width="60%">
															{{ $cuestionarios_dinamicos[$sl->id]['preguntas'][$datoRespuesta['pregunta_id']]['pregunta'] }}
														</th>
														<td>
															{{ $datoRespuesta['respuesta'] }}
														</td>
													</tr>
												@endforeach
											</table>
										@endforeach
									@else
										No hay respuestas por parte del cliente
									@endif
								@endif
							</div>
						@endif
						<!--==================================================================== -->
		      	<!--======================== REPORTE BC SCORE ========================== -->
		      	<!--==================================================================== -->
						@if(in_array($sl->type, ['Consulta BC','Hit BC','Reporte Completo','Pre-aprobado','Pre-Aprobado','Revisión Comité','Elegible','No Elegible','Denegado','Invitación a Continuar', 'Oferta Diferida', 'Rechazado']))
						<a href="#reporte-bc-panel_{{ $sl->id }}" data-toggle="collapse" class="btn btn-info btn-lg reporte-section-btn" style="width:100%">
							REPORTE BC SCORE
						</a>
						<div class="reporte-panel collapse" id="reporte-bc-panel_{{ $sl->id }}">
							<table class="table table-striped">
								<tr>
									<th width="20%">ENCONTRADO:</th>
									<td>{{ $sl->encontrado }}</td>
								</tr>
								<tr>
									<th width="20%">BC SCORE:</th>
									<td>{{ $sl->bc_score }}</td>
								</tr>
								<tr>
									<th width="20%">BC EXCLUSION:</th>
									<td>{{ $sl->exclusion }}</td>
								</tr>
			        	<tr>
									<th width="20%">BC RAZON 1:</th>
									<td>{{ $sl->bc_razon1 }}</td>
								</tr>
								<tr>
									<th width="20%">BC RAZON 2:</th>
									<td>{{ $sl->bc_razon2 }}</td>
								</tr>
								<tr>
									<th width="20%">BC RAZON 3:</th>
									<td>{{ $sl->bc_razon3 }}</td>
								</tr>
								<tr>
									<th width="20%">MICRO SCORE:</th>
									<td>{{ $sl->micro_valor }}</td>
								</tr>
								<tr>
									<th width="20%">MICRO RAZON 1:</th>
									<td>{{ $sl->micro_razon1 }}</td>
								</tr>
								<tr>
									<th width="20%">MICRO RAZON 2:</th>
									<td>{{ $sl->micro_razon2 }}</td>
								</tr>
								<tr>
									<th width="20%">MICRO RAZON 3:</th>
									<td>{{ $sl->micro_razon3 }}</td>
								</tr>
								<tr>
									<th colspan="2" class="text-center">DESCONTINUADO</th>
								</tr>
								<tr>
									<th width="20%">ICC SCORE: </th>
									<td>{{ $sl->icc_score }}</td>
								</tr>
								<tr>
									<th width="20%">ICC RAZON 1:</th>
									<td>{{ $sl->icc_razon1 }}</td>
								</tr>
								<tr>
									<th width="20%">ICC RAZON 2:</th>
									<td>{{ $sl->icc_razon2 }}</td>
								</tr>
								<tr>
									<th width="20%">ICC RAZON 3:</th>
									<td>{{ $sl->icc_razon3 }}</td>
								</tr>
							</table>
		        </div>
		   			@endif

		   			<!--==================================================================== -->
		      	<!--======================== REPORTE COMPLETO ========================== -->
		      	<!--==================================================================== -->
		   			@if(in_array($sl->type, ['Reporte Completo','Pre-aprobado','Pre-Aprobado','Revisión Comité','Elegible','No Elegible','Denegado', 'Invitación a Continuar', 'Oferta Diferida', 'Rechazado']))
						<a href="#reporte-completo-panel_{{ $sl->id }}" data-toggle="collapse" class="btn btn-info btn-lg reporte-section-btn" style="width:100%">
							REPORTE COMPLETO
						</a>
		        <div class="reporte-panel collapse" id="reporte-completo-panel_{{ $sl->id }}">
							<table class="table table-striped">
								<tr>
									<th width="35%">TASA FINAL</th>
									<td>{{ $sl->calc_tasa_final }}%</td>
								</tr>
								<tr>
									<th width="35%">PAGO FINAL</th>
									<td>${{ $sl->calc_pago_final }}</td>
								</tr>

								<tr>
									<th colspan="2" class="text-center"><hr></th>
								</tr>

								<tr>
									<th width="35%">DEUDA A INGRESO</th>
									<td>
										<?php
											if(isset($sl->calculos['deuda_a_ingreso']) && $sl->calculos['deuda_a_ingreso'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['deuda_a_ingreso_pasa'])){
													$is_pass = '('. (($sl->calculos['deuda_a_ingreso_pasa'])? 'OK' : 'No Califica') .')';
												}
												echo number_format($sl->calculos['deuda_a_ingreso'], 2, '.', '').'% &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">DEUDA A INGRESO SOLO CONSUMO</th>
									<td>
										<?php
											if(isset($sl->calculos['deuda_a_ingreso_solo_consumo']) && $sl->calculos['deuda_a_ingreso_solo_consumo'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['deuda_a_ingreso_solo_consumo_pasa'])){
													$is_pass = '('. (($sl->calculos['deuda_a_ingreso_solo_consumo_pasa'])? 'OK' : 'No Califica') .')';
												}
												echo number_format($sl->calculos['deuda_a_ingreso_solo_consumo'], 2, '.', '').'% &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">INGRESO DISPONIBLE</th>
									<td>{{ number_format($sl->calc_ingr_disp, 2, '.', '') }}%</td>
								</tr>
								<tr>
									<th width="35%">USO DE LINEAS</th>
									<td>
										<?php
											if(isset($sl->calculos['uso_de_lineas']) && $sl->calculos['uso_de_lineas'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['uso_de_lineas_pasa'])){
													$is_pass = '('. (($sl->calculos['uso_de_lineas_pasa'])? 'OK' : 'No Califica') .')';
												}
												echo number_format($sl->calculos['uso_de_lineas'], 2, '.', '').'% &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">ABILITY TO PAY</th>
									<td>
										<?php
											if(isset($sl->calculos['ability_to_pay']) && $sl->calculos['ability_to_pay'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['ability_to_pay_pasa'])){
													$is_pass = '('. (($sl->calculos['ability_to_pay_pasa'])? 'OK' : 'No Califica') .')';
												}
												echo number_format($sl->calculos['ability_to_pay'], 2, '.', '').'% &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">CONSULTAS</th>
									<td>
										<?php
											if(isset($sl->calculos['consultas_count']) && $sl->calculos['consultas_count'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['consultas_count_pasa'])){
													$is_pass = '('. (($sl->calculos['consultas_count_pasa'])? 'OK' : 'No Califica') .')';
												}
												echo $sl->calculos['consultas_count'].' &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">MOP1</th>
									<td>
										<?php
											if(isset($sl->calculos['porcentaje_mop1_a_meses']) && $sl->calculos['porcentaje_mop1_a_meses'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['porcentaje_mop1_pasa'])){
													$is_pass = '('. (($sl->calculos['porcentaje_mop1_pasa'])? 'OK' : 'No Califica') .')';
												}else{
													//is older solic so use static login here
													$is_pass = '('. (($sl->calculos['porcentaje_mop1_a_meses'] >= 75)? 'OK' : 'No Califica').')';
												}
												echo number_format($sl->calculos['porcentaje_mop1_a_meses'], 2, '.', '').'% &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								<tr>
									<th width="35%">MOP2</th>
									<td>
										<?php
											if(isset($sl->calculos['mop2']) && $sl->calculos['mop2'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['mop2_pasa'])){
													$is_pass = '('. (($sl->calculos['mop2_pasa'])? 'OK' : 'No Califica') .')';
												}else{
													//is older solic so use static login here
													$is_pass = '('. (($sl->calculos['mop2'] < 2)? 'OK' : 'No Califica').')';
												}
												echo $sl->calculos['mop2'].' &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">MOP3</th>
									<td>
										<?php
											if(isset($sl->calculos['mop3']) && $sl->calculos['mop3'] !== ''){
												$is_pass = '';
												if(isset($sl->calculos['mop3_pasa'])){
													$is_pass = '('. (($sl->calculos['mop3_pasa'])? 'OK' : 'No Califica') .')';
												}else{
													//is older solic so use static login here
													$is_pass = '('. (($sl->calculos['mop3'] < 2)? 'OK' : 'No Califica').')';
												}
												echo $sl->calculos['mop3'].' &nbsp; &nbsp;'.$is_pass;
											}
										?>
									</td>
								</tr>
								<tr>
									<th width="35%">THICK FILE</th>
									<td>{{ mb_strtoupper($sl->calc_is_hit) }}</td>
								</tr>
								<tr>
									<th width="35%">INGRESO MENSUAL</th>
									<td>{{ $sl->calc_ingr_neto_mens }}</td>
								</tr>
								<tr>
									<th width="35%">PAGO MENSUAL A DEUDA</th>
									<td>{{ $sl->calc_pago_mens_deuda }}</td>
								</tr>
								<tr>
									<th width="35%">PAGO MENSUAL A DEUDA SOLO CONSUMO</th>
									<td>{{ $sl->calc_pago_mens_deuda_cons }}</td>
								</tr>
								<tr>
									<th width="35%">GASTO FAMILIAR ESTIMADO</th>
									<td>{{ $sl->calc_gasto_fam_est }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS TOTAL</th>
									<td>{{ $sl->calc_total_cuentas }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS HIPOTECA</th>
									<td>{{ $sl->calc_cuentas_hipo }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS AUTOMOTRIZ</th>
									<td>{{ $sl->calc_cuentas_auto }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS CONSUMO</th>
									<td>{{ $sl->calc_cuentas_consumo }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS 4 MESES +</th>
									<td>{{ $sl->calc_cuentas_4meses_plus }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS CERRADAS</th>
									<td>{{ $sl->calc_cuentas_cerradas }}</td>
								</tr>
								<tr>
									<th width="35%">CUENTAS ABIERTAS</th>
									<td>{{ $sl->calc_cuentas_abiertas }}</td>
								</tr>
								<tr>
									<th width="20%">PEOR CALIFICACION (PEOR MOP):</th>
									<td>{{ $sl->peor_mop }}</td>
								</tr>
								<tr>
									<th width="20%">FECHA MAS RECIENTE DE PEOR MOP:</th>
									<td>{{ $sl->fecha_peor_mop }}</td>
								</tr>
								<tr>
									<th width="20%">NUM MESES DESDE PEOR MOP:</th>
									<td>{{ $sl->num_meses_desde_peor_mop }}</td>
								</tr>
								<tr>
									<th width="20%">NUMERO DE CONSULTAS POR OTORGANTES EN LOS ULTIMOS 3 MESES:</th>
									<td>{{ $sl->calc_consultas }}</td>
								</tr>
								<tr>
									<th width="20%">FECHA DE LA CONSULTA MAS RECIENTE:</th>
									<td>{{ $sl->fecha_consulta_mas_reciente }}</td>
								</tr>
								<tr>
									<th width="20%">NUM MESES DESDE LA CONSULTA MAS RECIENTE:</th>
									<td>{{ $sl->meses_desde_consulta_mas_reciente }}</td>
								</tr>
								<tr>
									<th width="20%">FECHA DE REGISTRO EN BURO DE CREDITO:</th>
									<td>{{ $sl->fecha_integracion }}</td>
								</tr>
								<tr>
									<th width="20%">FECHA DE APERTURA DE LA LINEA MAS ANTIGUA:</th>
									<td>{{ $sl->fecha_apertura_linea_mas_antigua }}</td>
								</tr>
								<tr>
									<th width="20%">CLAVES DE ACTIVIDAD REPORTADAS BURO DE CREDITO:</th>
									<td>
										<ol>
										@foreach($sl->claves_general as $clv)
											<li>{{ $clv }}</li>
										@endforeach
										</ol>
									</td>
								</tr>
								<tr>
									<th width="20%">CLAVES DE FRAUDE REPORTADAS:</th>
									<td>{{ $sl->claves_fraude_reportadas }}</td>
								</tr>
								<tr>
									<th width="20%">CLAVES DE EXTRAVIO REPORTADAS:</th>
									<td>{{ $sl->claves_extravio_reportadas }}</td>
								</tr>
							</table>
		        </div>

		        <!--==================================================================== -->
		      	<!--======================== CADENA BURO DE CREDITO ==================== -->
		      	<!--==================================================================== -->
						<a href="#cadena-buro-panel_{{ $sl->id }}" data-toggle="collapse" class="btn btn-warning btn-lg reporte-section-btn" style="width:100%">
							CADENA BC REPORTE COMPLETO
						</a>
		        <div class="reporte-panel collapse" id="cadena-buro-panel_{{ $sl->id }}">
							<div class="col-sm-12">
								<table class="table table-striped">
									<?php
										if($sl->report_response != ''){
											$report_data = $sl->report_response;
		                  foreach($report_data as $key => $val){
		                  	echo '<tr>
		                  					<td width="25%">';
	                  							if(isset($tags_arr[$key])){
	                  								echo '('.$tags_arr[$key].') ';
	                  							}
		                  					echo mb_strtoupper($key).':
		                  					</td>';
	                  					if(!is_object($val) && !is_array($val)){
	                  						echo '<td>'.mb_strtoupper($val).'</td>';
	                  					}else{
	                  						echo '<td>';
	                  						if(is_array($val)){
	                  							echo '<ol>';
	                  							foreach($val as $obj){
	                  								echo '<br><li>';
	                  												foreach($obj as $k => $v){
		                  												echo '<strong>';
		                  													if(isset($tags_arr[$k])){
								                  								echo '('.$tags_arr[$k].') ';
								                  							}
		                  												echo mb_strtoupper($k).': </strong>'.mb_strtoupper($v).'<br>';
		                  											}
	                  								echo '</li>';
	                  							}
	                  							echo '</ol>';
	                  						}else{
	                  							//is the calculos object
	                  							if(isset($val->deuda_a_ingreso)){
	                  								echo '<strong>'; 								if(isset($tags_arr['deuda_a_ingreso'])){  echo '('.$tags_arr['deuda_a_ingreso'].') '; }
	                  											echo mb_strtoupper('deuda_a_ingreso').'</strong>: '.$val->deuda_a_ingreso.'<br>';
	                  							}
	                  							if(isset($val->deuda_a_ingreso_pasa)){
																		echo '<strong>'; if(isset($tags_arr['deuda_a_ingreso_pasa'])){	echo '('.$tags_arr['deuda_a_ingreso_pasa'].')'; }
						                  						$deuda_a_ingreso_pasa_str = ($val->deuda_a_ingreso_pasa)? 'SI' : 'NO';
																					echo mb_strtoupper('deuda_a_ingreso_pasa').'</strong>: '.$deuda_a_ingreso_pasa_str.'<br>';
																	}
																	if(isset($val->deuda_a_ingreso_solo_consumo)){
																		echo '<strong>'; if(isset($tags_arr['deuda_a_ingreso_solo_consumo'])){	echo '('.$tags_arr['deuda_a_ingreso_solo_consumo'].')'; }
																					echo mb_strtoupper('deuda_a_ingreso_solo_consumo').'</strong>: '.$val->deuda_a_ingreso_solo_consumo.'<br>';
																	}
																	if(isset($val->deuda_a_ingreso_solo_consumo_pasa)){
																		echo '<strong>'; if(isset($tags_arr['deuda_a_ingreso_solo_consumo_pasa'])){	echo '('.$tags_arr['deuda_a_ingreso_solo_consumo_pasa'].')'; }
						                  						$deuda_a_ingreso_solo_consumo_pasa_str = ($val->deuda_a_ingreso_solo_consumo_pasa)? 'SI' : 'NO';
																					echo mb_strtoupper('deuda_a_ingreso_solo_consumo_pasa').'</strong>: '.$deuda_a_ingreso_solo_consumo_pasa_str.'<br>';
																	}
																	if(isset($val->ingreso_disponible)){
																		echo '<strong>'; if(isset($tags_arr['ingreso_disponible'])){	echo '('.$tags_arr['ingreso_disponible'].')'; }
																					echo mb_strtoupper('ingreso_disponible').'</strong>: '.$val->ingreso_disponible.'<br>';
																	}
																	if(isset($val->uso_de_lineas)){
																		echo '<strong>'; if(isset($tags_arr['uso_de_lineas'])){	echo '('.$tags_arr['uso_de_lineas'].')'; }
																					echo mb_strtoupper('uso_de_lineas').'</strong>: '.$val->uso_de_lineas.'<br>';
																	}
																	if(isset($val->uso_de_lineas_pasa)){
																		echo '<strong>'; if(isset($tags_arr['uso_de_lineas_pasa'])){	echo '('.$tags_arr['uso_de_lineas_pasa'].')'; }
						                  						$uso_de_lineas_pasa_str = ($val->uso_de_lineas_pasa)? 'SI' : 'NO';
																					echo mb_strtoupper('uso_de_lineas_pasa').'</strong>: '.$uso_de_lineas_pasa_str.'<br>';
																	}
																	if(isset($val->ability_to_pay)){
																		echo '<strong>'; if(isset($tags_arr['ability_to_pay'])){	echo '('.$tags_arr['ability_to_pay'].')'; }
																					echo mb_strtoupper('ability_to_pay').'</strong>: '.$val->ability_to_pay.'<br>';
																	}
																	if(isset($val->ability_to_pay_pasa)){
																		echo '<strong>'; if(isset($tags_arr['ability_to_pay_pasa'])){	echo '('.$tags_arr['ability_to_pay_pasa'].')'; }
						                  						$ability_to_pay_pasa_str = ($val->ability_to_pay_pasa)? 'SI' : 'NO';
																					echo mb_strtoupper('ability_to_pay_pasa').'</strong>: '.$ability_to_pay_pasa_str.'<br>';
																	}
																	if(isset($val->consultas_count)){
																		echo '<strong>'; if(isset($tags_arr['consultas_count'])){	echo '('.$tags_arr['consultas_count'].')'; }
																					echo mb_strtoupper('consultas_count').'</strong>: '.$val->consultas_count.'<br>';
																	}
																	if(isset($val->consultas_count_pasa)){
																		echo '<strong>'; if(isset($tags_arr['consultas_count_pasa'])){	echo '('.$tags_arr['consultas_count_pasa'].')'; }
						                  						$consultas_count_pasa_str = ($val->consultas_count_pasa)? 'SI' : 'NO';
																					echo mb_strtoupper('consultas_count_pasa').'</strong>: '.$consultas_count_pasa_str.'<br>';
																	}
																	if(isset($val->mop2)){
																		echo '<strong>'; if(isset($tags_arr['mop2'])){	echo '('.$tags_arr['mop2'].')'; }
																					echo mb_strtoupper('mop2').'</strong>: '.$val->mop2.'<br>';
																	}
																	if(isset($val->mop3)){
																		echo '<strong>'; if(isset($tags_arr['mop3'])){	echo '('.$tags_arr['mop3'].')'; }
																					echo mb_strtoupper('mop3').'</strong>: '.$val->mop3.'<br>';
																	}
																	if(isset($val->is_hit)){
																		echo '<strong>'; if(isset($tags_arr['is_hit'])){	echo '('.$tags_arr['is_hit'].')'; }
						                  						$yes_or_no_thick = ($val->is_hit)? 'SI':'NO';
																					echo mb_strtoupper('THICK_FILE').'</strong>: '.$yes_or_no_thick.'<br>';
																	}
																	if(isset($sl->bc_score)){
																		echo '<strong>'; if(isset($tags_arr['bc_score'])){	echo '('.$tags_arr['bc_score'].')'; }
																					echo mb_strtoupper('bc_score').'</strong>: '.$sl->bc_score.'<br>';
																	}
																	if(isset($sl->calc_ingr_neto_mens)){
																		echo '<strong>'; if(isset($tags_arr['ingreso_neto_mensual_total'])){	echo '('.$tags_arr['ingreso_neto_mensual_total'].')'; }
																					echo mb_strtoupper('ingreso_neto_mensual_total').'</strong>: '.$sl->calc_ingr_neto_mens.'<br>';
																	}
																	if(isset($val->pago_mensual_total_de_deuda)){
																		echo '<strong>'; if(isset($tags_arr['pago_mensual_total_de_deuda'])){	echo '('.$tags_arr['pago_mensual_total_de_deuda'].')'; }
																					echo mb_strtoupper('pago_mensual_total_de_deuda').'</strong>: '.$val->pago_mensual_total_de_deuda.'<br>';
																	}
																	if(isset($val->pago_mensual_total_deuda_lineas_de_consumo)){
																		echo '<strong>'; if(isset($tags_arr['pago_mensual_total_deuda_lineas_de_consumo'])){	echo '('.$tags_arr['pago_mensual_total_deuda_lineas_de_consumo'].')'; }
																					echo mb_strtoupper('pago_mensual_total_deuda_lineas_de_consumo').'</strong>: '.$val->pago_mensual_total_deuda_lineas_de_consumo.'<br>';
																	}
																	if(isset($sl->calc_gasto_fam_est)){
																		echo '<strong>'; if(isset($tags_arr['gasto_familiar_estimado'])){	echo '('.$tags_arr['gasto_familiar_estimado'].')'; }
																					echo mb_strtoupper('gasto_familiar_estimado').'</strong>: '.$sl->calc_gasto_fam_est.'<br>';
																	}
																	if(isset($val->num_cuentas_hipoteca)){
																		echo '<strong>'; if(isset($tags_arr['num_cuentas_hipoteca'])){	echo '('.$tags_arr['num_cuentas_hipoteca'].')'; }
																					echo mb_strtoupper('num_cuentas_hipoteca').'</strong>: '.$val->num_cuentas_hipoteca.'<br>';
																	}
																	if(isset($val->num_cuentas_automotriz)){
																		echo '<strong>'; if(isset($tags_arr['num_cuentas_automotriz'])){	echo '('.$tags_arr['num_cuentas_automotriz'].')'; }
																					echo mb_strtoupper('num_cuentas_automotriz').'</strong>: '.$val->num_cuentas_automotriz.'<br>';
																	}
																	if(isset($val->num_cuentas_consumo)){
																		echo '<strong>'; if(isset($tags_arr['num_cuentas_consumo'])){	echo '('.$tags_arr['num_cuentas_consumo'].')'; }
																					echo mb_strtoupper('num_cuentas_consumo').'</strong>: '.$val->num_cuentas_consumo.'<br>';
																	}
																	if(isset($sl->calc_cuentas_4meses_plus)){
																		echo '<strong>'; if(isset($tags_arr['cuentas_4meses_plus'])){	echo '('.$tags_arr['cuentas_4meses_plus'].')'; }
																					echo mb_strtoupper('cuentas_6meses_plus').'</strong>: '.$sl->calc_cuentas_4meses_plus.'<br>';
																	}
																	if(isset($val->cuentas_cerradas)){
																		echo '<strong>'; if(isset($tags_arr['cuentas_cerradas'])){	echo '('.$tags_arr['cuentas_cerradas'].')'; }
																					echo mb_strtoupper('cuentas_cerradas').'</strong>: '.$val->cuentas_cerradas.'<br>';
																	}
																	if(isset($val->cuentas_abiertas)){
																		echo '<strong>'; if(isset($tags_arr['cuentas_abiertas'])){	echo '('.$tags_arr['cuentas_abiertas'].')'; }
																					echo mb_strtoupper('cuentas_abiertas').'</strong>: '.$val->cuentas_abiertas;
																	}
	                  						}
	                  						echo '</td>';
	                  					}
		                  	echo '</tr>';
			                }
			              }
									?>
								</table>
							</div>
		        </div>

		        <!--==================================================================== -->
		      	<!--======================== LOG DE FICO ALP =========================== -->
		      	<!--==================================================================== -->
						<a href="#log_fico_{{ $sl->id }}" data-toggle="collapse" class="btn btn-warning btn-lg reporte-section-btn" style="width:100%">
							FICO ALP:
						</a>
						<div class="collapse" id="log_fico_{{ $sl->id }}">
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>ALP Result:</strong></div>
								<div class="col-sm-10">
									<?php
										if( $sl->fico_alp != ''){
											echo $sl->fico_alp;
										}
									?>
								</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>FICO ALP REQUEST:</strong></div>
								<div class="col-sm-10">
									<?php
										if( isset($sl->calculos['fico_alp_request'])){
											// var_dump($sl->calculos['fico_alp_request']);
											echo $sl->calculos['fico_alp_request'];
										}
									?>
								</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>FICO ALP RESPONSE:</strong></div>
								<div class="col-sm-10">
									<?php
										if( isset($sl->calculos['fico_alp_response'])){
											echo $sl->calculos['fico_alp_response'];
										}
									?>
								</div>
							</div>
						</div>

						<!-- <a href="#comf-datos_{{ $sl->id }}" data-toggle="collapse" class="btn btn-default btn-lg reporte-section-btn" style="width:100%">
							CONFIRMACION DE DATOS
						</a>
		        <div class="reporte-panel collapse" id="comf-datos_{{ $sl->id }}">
							<div class="col-sm-12">
								<form id="gen_sms_register_form" method="POST" action="/register_prospect">
									{!! csrf_field() !!}
									<hr>
									<h3>CONFIRMAR DATOS DEL CLIENTE</h3>
									<br>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Tipo de Cliente:</label><br>
												<select id="" name="" class="form-control">
													<option>Nuevo</option>
													<option>Former</option>
												</select>
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Rating de la Experiencia Previa:</label><br>
												<select id="" name="" class="form-control">
													<option>Muy Bueno</option>
													<option>Buena</option>
													<option>Regular</option>
													<option>Malo</option>
													<option>Pérdida</option>
													<option>Fraude</option>
												</select>
											</div>
										</div>

										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>
									<div class="row">

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Tipo de identificación</label><br>
												<input type="text" id="" name="" class="form-control">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Número de Identificación</label><br>
												<input type="text" id="" name="" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>



									<hr>
									<h3>Ingresos del Cliente</h3>
									<br>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Frequencia de ingreso del cliente</label><br>
												<select id="" name="" class="form-control" placeholder="">
													<option>Semanal</option>
													<option>Quincenal</option>
													<option>Mensual</option>
												</select>
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Fechas de ingreso del cliente</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Fuente de segundo ingreso</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Empresa donde trabaja</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Primer ingreso mensual actual</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Segundo ingreso mensual actual</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Ingreso total mensual</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Empresa donde trabaja</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>

									<hr>
									<h3>Referencias del cliente</h3>
									<br>
									<div class="row">
										<h4>&nbsp; &nbsp;Referencia 1</h4>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Nombre</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Paterno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Materno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Tipo de relación</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Teléfono de la referencia</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>

									<div class="row">
										<h4>&nbsp; &nbsp;Referencia 2</h4>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Nombre</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Paterno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Materno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Tipo de relación</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Teléfono de la referencia</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>

									<div class="row">
										<h4>&nbsp; &nbsp;Referencia 3</h4>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Nombre</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Paterno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Apellido Materno</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Tipo de relación</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Teléfono de la referencia</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
										<div class="col-sm-4">
											&nbsp;<br>
										</div>
									</div>

									<hr>
									<h3>Pagos Prestanómico</h3>
									<br>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Numero de Cuenta Clabe - Chequera o Cuenta de Débito</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Institución Bancária de la Chequera o Cuenta de Débito</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Numero de Tarjeta de Crédito<br> &nbsp; &nbsp; &nbsp; &nbsp;</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Institución Bancária de la Tarjeta de Crédito</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Fechas de Pago<br> &nbsp; &nbsp; &nbsp; &nbsp;</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Frequencia de Pago Elegida<br> &nbsp; &nbsp; &nbsp; &nbsp;</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Número de Cuenta</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Fecha de Desembolso</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="form-group">
												<label for="">Fecha de Liquidación</label><br>
												<input type="text" id="" name="" class="form-control" placeholder="">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12 text-center">
											<a href="#" id="comf-datos-submit" class="btn btn-default" style="width:60%;margin-top:20px;">Guardar Datos</a>
										</div>
									</div>

								</form>
							</div>
		        </div> -->
		        @endif

		        <!--==================================================================== -->
		      	<!--======================== LOG DE REGISTRO =========================== -->
		      	<!--==================================================================== -->
		      	<a href="#ult-punto-panel_{{ $sl->id }}" data-toggle="collapse" class="btn btn-warning btn-lg reporte-section-btn" style="width:100%">
							ÚLTIMO PUNTO DE REGISTRO
						</a>
		        <div class="reporte-panel collapse" id="ult-punto-panel_{{ $sl->id }}">
		        	<table class="table table-striped">
								<?php
									if($sl->ult_punto_reg != ''){
										$log_reg = json_decode($sl->ult_punto_reg);
										foreach($log_reg as $rep){
											echo '
											<tr>
												<td>'.$rep->punto.'</td>
												<td>'.$rep->report->datetime.'</td>
												<td>'.$rep->report->result.'</td>
											</tr>';
										}
									}
								?>
							</table>
						</div>

						<!--==================================================================== -->
		      	<!--======================== LOG DE CONSOLA ============================ -->
		      	<!--==================================================================== -->
						<a href="#log_consola_{{ $sl->id }}" data-toggle="collapse" class="btn btn-warning btn-lg reporte-section-btn" style="width:100%">
							ÚLTIMO MENSAJE AL USUARIO (LOG DE CONSOLA)
						</a>
						<div class="collapse" id="log_consola_{{ $sl->id }}">
							<?php
								echo '<!-- ';
								var_dump($sl->ult_decoded);
								echo '!-->';
								if(is_array($sl->ult_decoded)){
									if(count($sl->ult_decoded)){
										echo '<table class="table">';
										foreach($sl->ult_decoded as $log_msg){
											if(isset($log_msg->datetime) && isset($log_msg->console_log)){
												echo '  <tr>
																				<th width="20%">'.$log_msg->datetime.'</th>';
																	echo '<td>';
																	$log_obj = json_decode($log_msg->console_log);
																	if(is_object($log_obj)){
																		foreach($log_obj as $key => $val){
																			echo '<strong>'.$key.'</strong> :';
																			if(is_object($val)){
																				echo '<br>';
																				foreach($val as $calc => $result){
																					if($calc !== 'mop1_report' && $calc !== 'cuentas_con_mop2_debug' && $calc !== 'cuentas_con_mop3_debug' && $calc !== 'pago_mensual_debug' && $calc !== 'fico_alp_request' && $calc !== 'fico_alp_response'){
					   																$res_str = (is_array($result))? implode(', ',$result) : $result;
																						echo '&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$calc.'</strong> : '.$res_str.'<br>';
																					}
																				}
																			}else{
																				if(is_array($val)){
																					echo '&nbsp;&nbsp;&nbsp;&nbsp;';
																					echo implode(', ',$val).'<br>';
																				}else{
																					echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$val.'<br>';
																				}
																			}
																		}
																	}else{
																		var_dump($log_obj);
																	}
																	echo '</td>
																</tr>';
											}else{//is old format
												foreach($log_msg as $key => $val){
													echo '  <tr>
																		<th width="20%">'.$key.'</th>';
																		if(is_object($val)){
																			echo '<td>';
																				foreach($val as $calc => $result){
																					echo '<strong>'.$calc.'</strong> : '.$result.'<br>';
																				}
																			echo '</td>';
																		}else{
																			echo '<td>'.$val.'</td>';
																		}
													echo '	</tr>';
												}
											}
										}
										echo '</table>';
									}
								}else{
									echo 'No hay log de consola.';
								}
							?>
						</div>


						<!--==================================================================== -->
		      	<!--======================== LOG DE CADENAS BC ========================= -->
		      	<!--==================================================================== -->
						<a href="#log_cadenas_{{ $sl->id }}" data-toggle="collapse" class="btn btn-warning btn-lg reporte-section-btn" style="width:100%">
							CADENAS BC:
						</a>
						<div class="collapse" id="log_cadenas_{{ $sl->id }}">
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>CONSULTA BC :</strong></div>
								<div class="col-sm-10">{{ $sl->str_request }}</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>RESPUESTA BC SCORE:</strong></div>
								<div class="col-sm-10">{{ $sl->str_response }}</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>CONSULTA REPORTE:</strong></div>
								<div class="col-sm-10">{{ $sl->report_request }}</div>
							</div>
							<div class="row" style="padding:20px;">
								<div class="col-sm-2"><strong>RESPUESTA REPORTE (JSON):</strong></div>
								<div class="col-sm-10">{{ json_encode($sl->report_response) }}</div>
							</div>
						</div>


		      </div>
		      <div class="modal-footer">
		        <a class="btn btn-default" href="/panel/report-csv/{{$sl->prospect_id}}/{{$sl->id}}">Descargar Reporte</a>
		        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		@endforeach
	@endif
@endsection
