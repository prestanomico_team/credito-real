<!DOCTYPE html>
<html ng-app="prestaburo">
	<head>
		<title>Credito Real</title>
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
		<link href="/back/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/main.css" rel="stylesheet" type="text/css">
		<link href="/back/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
		<link href="/back/css/fullcalendar.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
         <!-- Google Analytics -->
         <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
                @verbatim
                    ga('create', 'UA-80377318-3', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @else
                @verbatim
                    ga('create', 'UA-129520248-2', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @endif
            ga(function(tracker) {
                var clientId = tracker.get('clientId');
                tracker.set('dimension1', tracker.get('clientId'));
            });
            
         </script>
	</head>
	<style>
		.nav>li>a {
    		padding-right: 10px;
    		padding-left: 10px;
		}
	</style>
	<body>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-main-nav" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/">
		      	{{-- <img src="/back/img/logo_prestanomico.png"> --}}
		      </a>
		    </div>

		    <div class="collapse navbar-collapse" id="admin-main-nav">
		      @if(Auth::check())
			      <ul class="nav navbar-nav">
			      	<!-- <li><a>{{ $_SERVER['REQUEST_URI'] }}</a></li> -->
			        <li class="{{ ($_SERVER['REQUEST_URI'] == '/panel')? 'active' : '' }}"><a href="/panel">Dashboard</a></li>
					@if(Auth::user()->can('calendario-citas'))
						<li class="{{ ($_SERVER['REQUEST_URI'] == '/calendario-citas')? 'active' : '' }}">
							<a href="/calendario-citas">Calendario Citas</a>
						</li>
				 	@endif
					@if(Auth::user()->can('reportes'))
						<li class="dropdown {{ (strpos($_SERVER['REQUEST_URI'],'report-completo-csv') !== false)? 'active' : '' }}">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
				          <ul class="dropdown-menu">
							<!-- <li><a href="/panel/report-completo-csv/">Tabla Completa</a></li> -->
							<li><a href="/panel/report-completo-csv-daily">Tabla Diaria</a></li>
				          </ul>
				        </li>
					@endif
					@if(Auth::user()->can('logs-registro'))
				        <li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/registro-logs')? 'active' : '' }}">
				        	<a href="/panel/registro-logs">Logs de Registro</a>
				        </li>
					@endif
					@if(Auth::user()->can('codigos-postales'))
						<li class="dropdown {{ ($_SERVER['REQUEST_URI'] == '/panel/cp-console')? 'active' : '' }}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Códigos Postales<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li>
									<a href="/panel/cp-console">Mantenimento códigos postales</a>
								</li>
								<li>
									<a href="/panel/cobertura">Cobertura códigos postales</a>
								</li>
							</ul>
						</li>
					@endif
					@if(Auth::user()->can('alta-automatica') || Auth::user()->can('administrador-productos') || Auth::user()->can('administracion-usuarios') || Auth::user()->can('cliente-estrella'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Desarrollos<span class="caret"></span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->can('alta-automatica'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/alta-clientes')? 'active' : '' }}">
								<a href="/alta-clientes">Alta automática T24</a>
							</li>
							@endif
							@if(Auth::user()->can('administrador-productos'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/productos')? 'active' : '' }}">
								<a href="/productos">Administrador de productos</a>
							</li>
							@endif
							@if(Auth::user()->can('administracion-usuarios'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/usuarios')? 'active' : '' }}">
								<a href="/panel/usuarios">Usuarios</a>
							</li>
							@endif
							@if(Auth::user()->can('cliente-estrella'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/clienteEstrella')? 'active' : '' }}">
								<a href="/panel/clienteEstrella">Cliente estrella</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if(Auth::user()->can('captura'))
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Captura<span class="caret"></span></a>
						<ul class="dropdown-menu">
							@if(Auth::user()->can('captura-dentalia'))
							<li class="{{ ($_SERVER['REQUEST_URI'] == '/panel/producto/captura/dentalia')? 'active' : '' }}">
								<a href="/panel/producto/captura/dentalia">Solicitud Dentalia</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="/logout">Cerrar Sessión</a></li>
			      </ul>
			  	@endif
				
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
					  	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></a>
					  	<ul class="dropdown-menu">
						  	<li><a href="/logout/captura/{{ request()->segment(4) }}">Cerrar Sessión</a></li>
					  	</ul>
					</li>
				</ul>
				
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		@yield('content')
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/jquery.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/moment.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/fullcalendar.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/jquery-ui.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/bootstrap.min.js"></script>
		<!--<script type="text/javascript" src="/back/js/angular.min.js"></script>
		<script type="text/javascript" src="/back/js/app.js"></script>
		<script type="text/javascript" src="/back/js/main.js"></script> -->
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/datetimepicker.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/datetimepicker.es.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/multiselect.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/sweetalert2.all.min.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/jquery.inputmask.bundle.js"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/productos.js?v=<?php echo microtime(); ?>"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/usuarios.js?v=<?php echo microtime(); ?>"></script>
		<script type="text/javascript" src="{{ asset('credito-real') }}/js/perfiles.js?v=<?php echo microtime(); ?>"></script>
		
		@yield('scripts')
		@yield('app_backoffice')
		

	</body>
</html>
