@extends('crm.app')
@section('content')

<div class="container" style="margin-right:150px;">
	<h2>Reporte de solicitudes completas</h2>
	@if(Auth::user()->can('reportes'))
	<p>
		<div class="row">

			<div class="col-md-4">
				<p>Por favor seleccione un rango de fechas</p>
				 @if (isset($error))
					<div class="alert alert-danger" role="alert">
						<span class="alert-link">{{ $error }}</span>
					</div>
				@endif
				<form action="/panel/report-daily/download" method="POST">
					{!! csrf_field() !!}
					<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
						<i class="fa fa-calendar"></i>&nbsp;
						<span></span> <i class="fa fa-caret-down"></i>
					</div>
					<input id="dateRange" type="hidden" name="dateRange" value="">

					<label for="">&nbsp;&nbsp;</label><br>
					<button class="btn btn-success" id="downloadReport" >Descargar Solicitudes</button>
				</form>
			</div>
		</div>
	</p>
	@else
		<center>
			<br>
			<h4>No tienes privilegios para realizar esta acción</h4>
		</center>
	@endif
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(function() {

		var start = moment().subtract(29, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$('#dateRange').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
			'Hoy': [moment(), moment()],
			'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
			'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
			'Mes actual': [moment().startOf('month'), moment().endOf('month')],
			'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);

	});

</script>
@endsection
