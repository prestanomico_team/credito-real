@extends('crm.app')
@section('content')
<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="title">
			<h2>CRM DASHBOARD</h2>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<form id="prospect-filter-id-form" method="POST" action="/panel">
					{!! csrf_field() !!}
					<table class="table">
						<tr>
							<td width="160">
								<label>Buscar por ID:</label>
							</td>
							<td width="100">
								<input type="text" id="filter_prospect_id" name="filter_prospect_id" value="{{ ($filter_type == 'id')? $filter_val : '' }}" style="margin-left:5px;width:100px;">
							</td>
							<td>
								<input type="submit" value="Buscar ID">
							</td>
						</tr>
					</table>
				</form>
			</div>
			<div class="col-sm-6">
				<form id="prospect-filter-email-form" method="POST" action="/panel">
					{!! csrf_field() !!}
					<table class="table">
						<tr>
							<td width="160">
								<label>Buscar por Email:</label>
							</td>
							<td width="100">
								<input type="text" id="filter_prospect_email" name="filter_prospect_email" value="{{ ($filter_type == 'email')? $filter_val : '' }}" style="margin-left:5px;width:200px;">
							</td>
							<td>
								<input type="submit" value="Buscar Email">
							</td>
						</tr>
						<!-- <tr>
							<td width="160">
								<label>Buscar por Nombre:</label>
							</td>
							<td width="100">
								<input type="text" id="filter_prospect_name" name="filter_prospect_name" value="{{ ($filter_type == 'name')? $filter_val : '' }}" style="margin-left:5px;width:200px;">
							</td>
							<td>
								<input type="submit" value="Buscar Nombre">
							</td>
						</tr> -->
					</table>
				</form>
			</div>
			<?php
				if($filtered){
					echo '
					<div class="col-sm-12">
						&nbsp;&nbsp;<a href="/panel">Quitar Filtro</a>
						<br><br>
					</div>
					';
				}
			?>
		</div>
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Prospectos</h3>
		  </div>
		  <div class="panel-body">
		    <table class="table table-striped" style="font-size:12px;">
					<tr>
						<th>ID</th>
						<th>ACTUALIZADO</th>
						<th>NOMBRE</th>
						<th>APELL P</th>
						<th>APELL M</th>
						<th>CEL</th>
						<th>EMAIL</th>
						<th>PRODUCTO</th>
						<th>REF</th>
						<th>ULT PUNTO REG</th>
						<th>ULT STATUS</th>
					</tr>
					@if($can_prospectos)
					@foreach($prospects as $pr)
					<tr>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ $pr->id }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ $pr->updated_at }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->nombres) }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->apellido_paterno) }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->apellido_materno) }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ $pr->celular }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ $pr->email }}</a>
						</td>
						@if($pr->ultima_solicitud != null)
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->ultima_solicitud->producto[0]['nombre_producto']) }}</a>
						</td>
						@else
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif > NONE </a>
						</td>
						@endif
 						<td>
 							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ $pr->referencia }}</a>
 						</td>
						
						@if($pr->ultima_solicitud != null)
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->ultima_solicitud->status) }}</a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif >{{ mb_strtoupper($pr->ultima_solicitud->sub_status) }}</a>
						</td>
						@else
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif > NONE </a>
						</td>
						<td>
							<a @if($can_solicitudes) href="/panel/prospecto/{{ $pr->id }}" @endif > NONE </a>
						</td>
						@endif

					</tr>
					@endforeach
					@else
						<tr>
							<td colspan="11"> No tienes privilegios para ver los prospectos </td>
						</tr>
					@endif
				</table>
				@if($can_prospectos)
					{{ $prospects->links() }}
				@endif
		  </div>
		</div>
	</div>
</div>
@endsection
