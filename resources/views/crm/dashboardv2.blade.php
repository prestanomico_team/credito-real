@extends('crm.app')
@section('content')
<div class="container" style="width:100%">
	<div id="app" class="content">

		<div class="panel panel-default" style="border-color: transparent;">
			
		  	<div class="panel-body">

				@if(Auth::user()->can('prospectos'))
					<panel></panel>
				@else

					<h3>No tienes privilegios para ver los prospectos</h3>

				@endif

		  	</div>
		</div>
	</div>
</div>
@endsection
