<!-- ================================================================================== -->
<!-- ================================= SEGMENTO NOMBRE ================================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Nombre del Cliente - PN</strong>  <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_nombre">?</span></h2>
  	<div id="desc_nombre" class="collapse">
  		<p>Este segmento se ocupa para incluir el nombre completo del Cliente o Prospecto. Conjuntamente con los datos solicitados en el segmento de Autenticación (AU), así como en los segmentos subsecuentes (domicilio – PA, domicilio de empleo – PE, números de referencia – PI), se busca la información de la persona consultada en la base de datos, se autentica y se entrega el BC Score.</p>
  		<p>Contiene los datos generales del Cliente: nombre(s) y apellidos, fecha de nacimiento, RFC, CURP.</p>
			<p>Es un segmento requerido compuesto por 20 etiquetas.</p>
		</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-nombre">Formulario</button>
  </div>
  <div id="segmento-nombre" class="panel-body collapse">
  	<section id="nombre_preview">
	  	Preview: 
			<ul id="segmento_nombre_cliente" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta_apellido_pat">PN</li>
				<li class="seg-count" id="seg-len_apellido_pat"><% segs.nombre.apellido_pat | stringLength %></li>
				<li id="seg-apellido_pat"><% segs.nombre.apellido_pat %></li>
				
				<li class="seg-label" id="seg-etiqueta_apellido_mat">00</li>
				<li class="seg-count" id="seg-len_apellido_mat"><% segs.nombre.apellido_mat | stringLength %></li>
				<li id="seg-apellido_mat"><% segs.nombre.apellido_mat %></li>
				
				<span ng-show="segs.nombre.apellido_adl">
					<li class="seg-label" id="seg-etiqueta_apellido_adl">01</li>
					<li class="seg-count" id="seg-len_apellido_adl"><% segs.nombre.apellido_adl | stringLength %></li>
					<li id="seg-apellido_adl"><% segs.nombre.apellido_adl %></li>
				</span>
				
				<li class="seg-label" id="seg-etiqueta_prim_nombre">02</li>
				<li class="seg-count" id="seg-len_prim_nombre"><% segs.nombre.prim_nombre | stringLength %></li>
				<li id="seg-prim_nombre"><% segs.nombre.prim_nombre %></li>
				
				<span ng-show="segs.nombre.segundo_nombre">
					<li class="seg-label" id="seg-etiqueta_seg_nombre">03</li>
					<li class="seg-count" id="seg-len_seg_nombre"><% segs.nombre.segundo_nombre | stringLength %></li>
					<li id="seg-seg_nombre"><% segs.nombre.segundo_nombre %></li>
				</span>
				
				<span ng-show="segs.nombre.fecha_nac">
					<li class="seg-label" id="seg-etiqueta_fecha_nac">04</li>
					<li class="seg-count" id="seg-len_fecha_nac"><% segs.nombre.fecha_nac | stringLength %></li>
					<li id="seg-fecha_nac"><% segs.nombre.fecha_nac %></li>
				</span>
				
				<span ng-show="segs.nombre.rfc">
					<li class="seg-label" id="seg-etiqueta_rfc">05</li>
					<li class="seg-count" id="seg-len_rfc"><% segs.nombre.rfc | stringLength %></li>
					<li id="seg-rfc"><% segs.nombre.rfc %></li>
				</span>
				<span ng-show="segs.nombre.prefijo">
					<li class="seg-label" id="seg-etiqueta_prefijo">06</li>
					<li class="seg-count" id="seg-len_prefijo"><% segs.nombre.prefijo | stringLength %></li>
					<li id="seg-prefijo"><% segs.nombre.prefijo %></li>
				</span>
				<span ng-show="segs.nombre.sufijo">
					<li class="seg-label" id="seg-etiqueta_sufijo">07</li>
					<li class="seg-count" id="seg-len_sufijo"><% segs.nombre.sufijo | stringLength %></li>
					<li id="seg-sufijo"><% segs.nombre.sufijo %></li>
				</span>
				<span ng-show="segs.nombre.nacionalidad">
					<li class="seg-label" id="seg-etiqueta_nacionalidad">08</li>
					<li class="seg-count" id="seg-len_nacionalidad"><% segs.nombre.nacionalidad | stringLength %></li>
					<li id="seg-nacionalidad"><% segs.nombre.nacionalidad %></li>
				</span>
				<span ng-show="segs.nombre.tipo_residencia">
					<li class="seg-label" id="seg-etiqueta_tipo_residencia">09</li>
					<li class="seg-count" id="seg-len_tipo_residencia"><% segs.nombre.tipo_residencia | stringLength %></li>
					<li id="seg-tipo_residencia"><% segs.nombre.tipo_residencia %></li>
				</span>
				<span ng-show="segs.nombre.lic_conducir">
					<li class="seg-label" id="seg-etiqueta_lic_conducir">10</li>
					<li class="seg-count" id="seg-len_lic_conducir"><% segs.nombre.lic_conducir | stringLength %></li>
					<li id="seg-lic_conducir"><% segs.nombre.lic_conducir %></li>
				</span>
				<span ng-show="segs.nombre.estado_civil">
					<li class="seg-label" id="seg-etiqueta_estado_civil">11</li>
					<li class="seg-count" id="seg-len_estado_civil"><% segs.nombre.estado_civil | stringLength %></li>
					<li id="seg-estado_civil"><% segs.nombre.estado_civil %></li>
				</span>
				<span ng-show="segs.nombre.genero">
					<li class="seg-label" id="seg-etiqueta_genero">12</li>
					<li class="seg-count" id="seg-len_genero"><% segs.nombre.genero | stringLength %></li>
					<li id="seg-genero"><% segs.nombre.genero %></li>
				</span>
				<span ng-show="segs.nombre.cedula_prof">
					<li class="seg-label" id="seg-etiqueta_cedula_prof">13</li>
					<li class="seg-count" id="seg-len_cedula_prof"><% segs.nombre.cedula_prof | stringLength %></li>
					<li id="seg-cedula_prof"><% segs.nombre.cedula_prof %></li>
				</span>
				<span ng-show="segs.nombre.num_ife">
					<li class="seg-label" id="seg-etiqueta_num_ife">14</li>
					<li class="seg-count" id="seg-len_num_ife"><% segs.nombre.num_ife | stringLength %></li>
					<li id="seg-num_ife"><% segs.nombre.num_ife %></li>
				</span>
				<span ng-show="segs.nombre.curp">
					<li class="seg-label" id="seg-etiqueta_curp">15</li>
					<li class="seg-count" id="seg-len_curp"><% segs.nombre.curp | stringLength %></li>
					<li id="seg-curp"><% segs.nombre.curp %></li>
					
					<li class="seg-label" id="seg-etiqueta_clave_pais">16</li>
					<li class="seg-count" id="seg-len_clave_pais">2</li>
					<li id="seg-clave_pais">MX</li>
				</span>
				<span ng-show="segs.nombre.dependientes">
					<li class="seg-label" id="seg-etiqueta_dependientes">17</li>
					<li class="seg-count" id="seg-len_dependientes"><% segs.nombre.dependientes | stringLength %></li>
					<li id="seg-dependientes"><% segs.nombre.dependientes %></li>
				</span>
				<span ng-show="segs.nombre.deps_edades">
					<li class="seg-label" id="seg-etiqueta_deps_edades">18</li>
					<li class="seg-count" id="seg-len_deps_edades"><% segs.nombre.deps_edades | stringLength %></li>
					<li id="seg-deps_edades"><% segs.nombre.deps_edades %></li>
				</span>
			</ul>
			<hr>
		</section>
		<section id="nombre_settings">
			
			<div class="form-group">
				<label for="nom_apellido_pat">PN - Apellido Paterno <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-apellido-paterno">?</span></label>
				<div id="tip-nom-apellido-paterno" class="collapse">
					<p>
						Reportar el apellido paterno completo del Cliente, sin abreviaturas.<br>
						Si el apellido paterno contiene múltiples palabras, deberá separarse con espacios.<br>
						Ejemplos: Pérez Nieto, Del Campo, Martínez de Escobar<br>
						<ul>
							<li>Ningún nombre propio o apellido debe abreviarse.</li>
  						<li>Los nombres deben ingresarse en mayúsculas.</li>
  						<li>Si el apellido contiene múltiples palabras, debe separarse con espacios. Ejemplo: PEREZ NIETO, DEL CAMPO, MARTINEZ DE ESCOBAR, DE LA O.</li>
  						<li>El dato debe contener 3 letras o más.</li>
  						<li>El dato no debe contener caracteres especiales.</li>
  						<li>El dato no debe contener prefijos personales o profesionales; ejemplos, Jr., Tercero ó III, Lic., Ing.</li>
  						<li>Preferentemente deben ingresarse los datos de acuerdo con documentos oficiales de identificación como la credencial del IFE, pasaporte vigente, forma FM2 para extranjeros.</li>
  						<li><strong>Es un dato requerido, de no incluirse se rechazará la solicitud de consulta.</strong></li>
						</ul>
					</p>
				</div>
				<input type="text" id="nom_apellido_pat" name="nom_apellido_pat" class="form-control" ng-model="segs.nombre.apellido_pat" maxlength="26">
				<small><strong class="req-label">Requerido</strong> | Etiqueta: PN | Logitud Variable | 26 caracteres max | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_apellido_mat">00 - Apellido Materno <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-apellido-materno">?</span></label>
				<div id="tip-nom-apellido-materno" class="collapse">
					<p>
						Se reporta el apellido materno completo del Cliente, sin abreviaturas.<br>
						Si el apellido materno contiene múltiples palabras, deberá separarse con espacios.<br>
						Por ejemplo: Pérez Nieto, Del Campo, Martínez de Escobar<br>
						Si no existe apellido materno o el paterno debido a que es extranjero o no lo lleva 
						en sus documentos oficiales, deberá colocarse el único apellido en el campo de 
						“Apellido Paterno” e incluirse en este campo la frase “<strong>NO PROPORCIONADO</strong>”
						<ul>
							<li>Debe contener 3 letras o más</li>
							<li>No debe contener caracteres especiales</li>
							<li>No debe haber más de un espacio entre palabras</li>
							<li>No reportar el apellido de casada</li>
							<li>Reportar tal como está en los documentos oficiales de identificación como la credencial del IFE, pasaporte vigente, forma FM2 para extranjeros.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="nom_apellido_mat" name="nom_apellido_mat" class="form-control" ng-model="segs.nombre.apellido_mat" maxlength="26">
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 00 | Logitud Variable | 26 caracteres max | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_apellido_adl">01 - Apellido Adicional <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-apellido-adicional">?</span></label>
				<div id="tip-nom-apellido-adicional" class="collapse">
					<p>
						Para mujeres, se puede reportar el apellido de casada, sin abreviaturas.<br>
						Si el apellido adicional contiene dos o más palabras deberán separarse con espacios.<br>
						Por ejemplo: Pérez Nieto, Del Campo, Martínez de Escobar<br>
						Si no se cuenta con este dato o no existe, no es necesario incluirlo.
					</p>
				</div>
				<input type="text" id="nom_apellido_adl" name="nom_apellido_adl" class="form-control" ng-model="segs.nombre.apellido_adl" maxlength="26" disabled>
				<small>Opcional | Etiqueta: 01 | Logitud Variable | 26 caracteres max | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_prim_nombre">02 - Primer Nombre <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-primer-nombre">?</span></label>
				<div id="tip-nom-primer-nombre" class="collapse">
					<p>
						<ul>
							<li>Reportar el primer nombre completo del Cliente, solo letras</li>
							<li>Sin abreviaturas</li>
							<li>Debe contener 3 letras o más</li>
						</ul>
					</p>
				</div>
				<input type="text" id="nom_prim_nombre" name="nom_prim_nombre" class="form-control" ng-model="segs.nombre.prim_nombre" maxlength="26">
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 02 | Logitud Variable | 26 caracteres max | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_segundo_nombre">03 - Segundo Nombre <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-segundo-nombre">?</span></label>
				<div id="tip-nom-segundo-nombre" class="collapse">
					<p>
						Si el Cliente tiene 2 o más nombres, del segundo en adelante se reportarán en este campo separándose con espacios.<br>
						Por ejemplo: del Rosario, de la Asunción, Silvia Inés.
					</p>
				</div>
				<input type="text" id="nom_segundo_nombre" name="nom_segundo_nombre" class="form-control" ng-model="segs.nombre.segundo_nombre" maxlength="26">
				<small>Opcional | Etiqueta: 03 | Logitud Variable | 26 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_fecha_nac">04 - Fecha de Nacimiento <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-fecha-nac">?</span></label>
				<div id="tip-nom-fecha-nac" class="collapse">
					<p>
						Contiene la fecha de nacimiento del Cliente. <br>
						<strong>Este dato es altamente recomendable que se informe ya que es importante para asegurar la localización del Cliente correcto.</strong>
						<ul>
							<li>DD: número entre 01- 31</li>
							<li>MM: número entre 01-12</li>
							<li>AAAA: año 4 num.</li>
							<li>Nota: No se deben reportar fechas de nacimiento para menores de 18 años.</li>
							<li>Si no se tiene disponible, no reportar.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="nom_fecha_nac" name="nom_fecha_nac" class="form-control" ng-model="segs.nombre.fecha_nac" maxlength="8">
				<small>Opcional (<strong>recomendable</strong>) | Etiqueta: 04 |  Logitud Fija | 8 caracteres | Numérico | Formato DDMMAAAA</small>
			</div>
			
			<div class="form-group">
				<label for="nom_rfc">05 - RFC <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-rfc">?</span></label>
				<div id="tip-nom-rfc" class="collapse">
					<p>
						Reportar el RFC (Registro Federal de Contribuyentes) del Cliente, al menos las 10 primeras posiciones y si es posible con los 3 caracteres de la homoclave. <br>
						*Para cuentas con fecha de apertura posterior a enero de 1998, el RFC del Cliente es requerido<br>
						Se validará que tenga la siguiente estructura:<br>
						<strong>AAAANNNNNNZZZ:</strong>
						<ul>
							<li>A – Alfabético (Letras del nombre)</li>
							<li>N – Número (Fecha de nacimiento)</li>
							<li>Z – Alfanumérico (Homoclave)</li>
						</ul>
					</p>
				</div>
				<input type="text" id="nom_rfc" name="nom_rfc" class="form-control" ng-model="segs.nombre.rfc" maxlength="13">
				<small>Opcional* | Etiqueta: 05 | Logitud Variable | 13 caracteres max 10 characters min | Alfanumérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_prefijo">06 - Prefijo Personal o Profesional <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-prefijo">?</span></label>
				<div id="tip-nom-prefijo" class="collapse">
					<p>
						Se puede incluir valores mostrados en el ANEXO 9 de “PREFIJOS PERSONALES Y PROFESIONALES”.
					</p>
				</div>
				<select id="nom_prefijo" name="nom_prefijo" class="form-control" ng-model="segs.nombre.prefijo" disabled>
					<option value="ACT">ACT - Actuario</option>
					<option value="ADM">ADM - Administrador</option>
					<option value="ARQ">ARQ - Arquitecto</option>
					<option value="CP">CP - Contador publico</option>
					<option value="DENT">DENT - Dentista</option>
					<option value="DIP">DIP - Diputado</option>
					<option value="DIR">DIR - Director</option>
					<option value="DON">DON - Don</option>
					<option value="DONA">DONA - Doña</option>
					<option value="DR">DR - Doctor</option>
					<option value="DRA">DRA - Doctora</option>
					<option value="FIS">FIS - Físico</option>
					<option value="GIN">GIN - Ginecólogo</option>
					<option value="ING">ING - Ingeniero</option>
					<option value="ISC">ISC - Ing. en sistemas cómputo</option>
					<option value="LAE">LAE - Lic. en administración de empresas</option>
					<option value="LIC">LIC - Licienciado / Abogado</option>
					<option value="LRI">LRI - Lic. relaciones internacionales</option>
					<option value="MAE">MAE - Maestro</option>
					<option value="MR">MR - Mr.</option>
					<option value="MRS">MRS - Mrs.</option>
					<option value="MS">MS - Ms</option>
					<option value="MVZ">MVZ - Médico Veterinario Zootecnista</option>
					<option value="NOT">NOT - Notario</option>
					<option value="OFTA">OFTA - Oftalmólogo</option>
					<option value="ORTO">ORTO - Ortodontista</option>
					<option value="PROF">PROF - Profesor(a)</option>
					<option value="PSIC">PSIC - Psicólogo</option>
					<option value="PSIQ">PSIQ - Psiquiatra</option>
					<option value="QF">QF - Químico Físico</option>
					<option value="QFB">QFB - Químico fármaco biólogo</option>
					<option value="QUIM">QUIM - Químico</option>
					<option value="SR">SR - Señor</option>
					<option value="SRA">SRA - Señora</option>
					<option value="SRTA">SRTA - Señorita</option>
					<option value="VDA">VDA - Viuda</option>
				</select>
				<small>Opcional | Etiqueta: 06 | Logitud Variable | 4 caracteres max | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_sufijo">07 - Sufijo Personal <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-sufijo">?</span></label>
				<div id="tip-nom-sufijo" class="collapse">
					<p>
						Sufijo personal del cliente
					</p>
				</div>
				<select id="nom_sufijo" name="nom_sufijo" class="form-control" ng-model="segs.nombre.sufijo" disabled>
					<option value="JR">JR - Junior</option>
					<option value="II">II - Segundo</option>
					<option value="III">III - Tercero</option>
				</select>
				<small>Opcional | Etiqueta: 07 | Logitud Variable | 4 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_nacionalidad">08 - Nacionalidad <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-nacionalidad">?</span></label>
				<div id="tip-nom-nacionalidad" class="collapse">
					<p>
						Reportar la Nacionalidad del Cliente de acuerdo al ANEXO 10 de “PAÍSES Y MONEDAS”.
					</p>
				</div>
				<select id="nom_nacionalidad" name="nom_nacionalidad" class="form-control" ng-model="segs.nombre.nacionalidad" disabled>
					<option value="MX">MX - México</option>
				</select>
				<small>Opcional | Etiqueta: 08 | Logitud Fija | 2 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_tipo_residencia">09 - Tipo de Residencia <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-tipo-residencia">?</span></label>
				<div id="tip-nom-tipo-residencia" class="collapse">
					<p>
						Se refiere al estado de la vivienda del Cliente
					</p>
				</div>
				<select id="nom_tipo_residencia" name="nom_tipo_residencia" class="form-control" ng-model="segs.nombre.tipo_residencia" disabled>
					<option value="1">1 - Propietario</option>
					<option value="2">2 - Renta</option>
					<option value="3">3 - Pensión / Vive con familiares</option>
				</select>
				<small>Opcional | Etiqueta: 09 | Logitud Fija | 1 caracteres | Numérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_lic_conducir">10 - Num. Licencia de Conducir <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-lic-conducir">?</span></label>
				<div id="tip-nom-lic-conducir" class="collapse">
					<p>
						Contiene el número de licencia de conducir.
					</p>
				</div>
				<input type="text" id="nom_lic_conducir" name="nom_lic_conducir" class="form-control" ng-model="segs.nombre.lic_conducir" maxlength="20" disabled>
				<small>Opcional | Etiqueta: 10 | Logitud Variable | 20 caracteres max | Alfanumérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_estado_civil">11 - Estado Civil <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-estado-civil">?</span></label>
				<div id="tip-nom-estado-civil" class="collapse">
					<p>
						Reportar solo si la información está disponible.
					</p>
				</div>
				<select id="nom_estado_civil" name="nom_estado_civil" class="form-control" ng-model="segs.nombre.estado_civil">
					<option value="D">D - Divorciado</option>
					<option value="F">F - Unión Libre</option>
					<option value="M">M - Casado</option>
					<option value="S">S - Soltero</option>
					<option value="W">W - Viudo</option>
				</select>
				<small>Opcional | Etiqueta: 11 | Logitud Fija | 1 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_genero">12 - Género <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-genero">?</span></label>
				<div id="tip-nom-genero" class="collapse">
					<p>
						Género del cliente
					</p>
				</div>
				<select id="nom_genero" name="nom_genero" class="form-control" ng-model="segs.nombre.genero">
					<option value="F">F - Femenino</option>
					<option value="M">M - Masculino</option>
				</select>
				<small>Opcional | Etiqueta: 12 | Logitud Fija | 1 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_cedula_prof">13 - Num. de Cedula Profesional <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-cedula-prof">?</span></label>
				<div id="tip-nom-cedula-prof" class="collapse">
					<p>
						Reportar el dato si se tiene disponible.
					</p>
				</div>
				<input type="text" id="nom_cedula_prof" name="nom_cedula_prof" class="form-control" ng-model="segs.nombre.cedula_prof" maxlength="20" disabled>
				<small>Opcional | Etiqueta: 13 | Logitud Variable | 20 caracteres max | Alfanumérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_num_ife">14 - Num. de Registro Electoral (IFE/INE) <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-ife-ine">?</span></label>
				<div id="tip-nom-ife-ine" class="collapse">
					<p>
						Reportar el dato si se tiene disponible.<br>
						El dato a reportar se encuentra ubicado en las partes marcadas de las imágenes.<br>
						<img src="/back/img/ife-back-sample.png">
					</p>
				</div>
				<input type="text" id="nom_num_ife" name="nom_num_ife" class="form-control" ng-model="segs.nombre.num_ife" maxlength="20" disabled>
				<small>Opcional | Etiqueta: 14 | Logitud Variable | 20 caracteres max | Alfanumérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_curp">15 - CURP <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-curp">?</span></label>
				<div id="tip-nom-curp" class="collapse">
					<p>
						Anotar el número de identificación única, si se tiene disponible <br>
						*<strong>Esto dependerá de la Nacionalidad del Cliente</strong>
						*<strong>Si el Cliente es de México, se podrá capturar la CURP</strong><br>
						*<strong>Si se reporta un dato en este campo, será requerido el campo 16 “CLAVE DE PAÍS”</strong>
					</p>
				</div>
				<input type="text" id="nom_curp" name="nom_curp" class="form-control" ng-model="segs.nombre.curp" maxlength="20">
				<small>Opcional | Etiqueta: 15 | Logitud Variable | 20 caracteres max | Alfanumérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_clave_pais">16 - Clave de País <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-clave-pais">?</span></label>
				<div id="tip-nom-clave-pais" class="collapse">
					<p>
						Reportar la clave del país de ciudadanía del Cliente, si se tiene disponible, de acuerdo al ANEXO 10 de “PAÍSES Y MONEDAS”.
					</p>
				</div>
				<input id="nom_clave_pais" name="nom_clave_pais" class="form-control" value="MX" disabled>
				<small><strong class="req-label">Requerido (con CURP)</strong> | Etiqueta: 16 | Logitud Fija | 2 caracteres | Alfabético</small>
			</div>
			
			<div class="form-group">
				<label for="nom_dependientes">17 - Num. de Dependientes <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-dependientes">?</span></label>
				<div id="tip-nom-dependientes" class="collapse">
					<p>
						Reportar el número de personas que dependen económicamente del Cliente, hasta 15 dependientes.<br>
						Si el número es de solo un dígito, colocar un cero (0) a la izquierda.
					</p>
				</div>
				<select id="nom_dependientes" name="nom_dependientes" class="form-control" ng-model="segs.nombre.dependientes" disabled>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
				</select>
				<small>Opcional | Etiqueta: 17 | Logitud Fija | 2 caracteres | Numérico</small>
			</div>
			
			<div class="form-group">
				<label for="nom_deps_edades">18 - Edades de los dependientes <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-nom-edades-dependientes">?</span></label>
				<div id="tip-nom-edades-dependientes" class="collapse">
					<p>
						Se reportan las edades de los dependientes mencionados en el campo anterior, hasta 15 dependientes.<br>
						Se reportan de forma consecutiva usando 2 caracteres por cada edad de dependiente.<br>
						Ejemplo: si se reportaron 3 dependientes, la edad de uno es menor de 1 año, la
						edad del segundo es de 19 años y la edad del tercero es de 75 años, las edades
						se reportaran 011975, consecutivos, sin espacios.<br>
						Las edades menores de 1 año se reportarán como 01.
					</p>
				</div>
				<input type="text" id="nom_deps_edades" name="nom_deps_edades" class="form-control" ng-model="segs.nombre.deps_edades" maxlength="30" disabled>
				<small>Opcional | Etiqueta: 18 | Logitud Variable | 30 caracteres max | Numérico</small>
			</div>
		</section>
  </div>
</div>