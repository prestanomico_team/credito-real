<!-- ================================================================================== -->
<!-- ================================= SEGMENTO ENCABEZADO ============================ -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Encabezado - INTL</strong> <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_encabezado">?</span></h2>
  	<div id="desc_encabezado" class="collapse">
  		<p>El Segmento de Encabezado o inicio – INTL, sirve para: identificar la versión en uso del Formato INTL, incluir la referencia del operador y enviar la clave de acceso y contraseña del Usuario, entre otros datos que deben ingresarse. Se trata de un segmento requerido para que el sistema acepte y procese la petición de consulta.</p>
  		<p>A diferencia de otros segmentos, este contiene una sola etiqueta: “INTL”. Los datos complementarios están distribuidos a largo del segmento, de acuerdo con una posición y extensión predeterminada.</p>
  		<p>Es un segmento requerido compuesto por 17 datos.</p>
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-encabezado">Formulario</button>
  </div>
  <div id="segmento-encabezado" class="panel-body collapse">
  	<section id="encabezado_preview">
	  	Preview: 
			<ul id="segmento_nombre_cliente" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta">INTL</li>
				<li id="seg-version">13</li>
				<li id="seg-referencia">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
				<li id="seg-producto">007</li>
				<li id="seg-pais">MX</li>
				<li id="seg-reservado">0000</li><!-- 4 ceros -->
				<li id="seg-clave-usuario">ZM78211002</li><!-- 10 chars -->
				<li id="seg-password-usuario">GKL6GKL6</li><!-- 8 chars -->
				<li id="seg-responsabilidad">I</li>
				<li id="seg-tipo_contrato">PL</li>
				<li id="seg-tipo_moneda">MX</li>
				<li id="seg-importe">000000000</li>
				<li id="seg-idioma">SP</li>
				<li id="seg-tipo_salida">01</li>
				<li id="seg-tamanio-bloque">&nbsp;</li><!-- 1 espacio -->
				<li id="seg-impresora">&nbsp;&nbsp;&nbsp;&nbsp;</li><!-- 4 espacios -->
				<li id="seg-reservado">0000000</li><!-- 7 ceros -->
			</ul>
			<hr>
		</section>
		<section id="encabezado_settings">
			<div class="form-group">
				<label for="intl_etiqueta">INTL - ETIQUETA DEL SEGMENTO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-etiqueta-encabezado">?</span></label>
				<div id="tip-etiqueta-encabezado" class="collapse">
					<p>Debe contener las letras INTL</p>
					<p><strong>Si el dato se omite o se incluye algo diferente, se rechaza la solicitud de consulta.</strong></p>
				</div>
				<input type="text" id="intl_etiqueta" name="intl_etiqueta" class="form-control" value="INTL" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Alfabético | Posición 1-4</small>
			</div>

			<div class="form-group">
				<label for="intl_version">VERSIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-encabezado-version">?</span></label>
				<div id="tip-encabezado-version" class="collapse">
					<p>Indica la versión del formato de registro de consulta. INTL13.</p>
					<p>Las versiones válidas para procesar consultas Autenticador – Prospector son 11 12 ó 13;</p>
					<p><strong>Cuando se omite el número de versión o se incluye un dato diferente, se rechaza la solicitud de consulta.</strong></p>
				</div>
				<input type="text" id="intl_version" name="intl_version" class="form-control" value="13" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Numérico | Posición 5-6</small>
			</div>

			<div class="form-group">
				<label for="intl_num_referencia">Referencia Interna <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-ref-interna">?</span></label>
				<div id="tip-ref-interna" class="collapse">
					<p>Este campo es de uso exclusivo para el Usuario. Sirve para ingresar un dato de identificación propio del Usuario para referenciar la consulta del Cliente; el dato se presentará en la respuesta.</p>
					<p>Consideraciones:</p>
					<ul>
						<li><strong>Debe ingresarse el mismo dato indicado en la etiqueta 01 del segmento AU.</strong></li>
						<li>El campo es REQUERIDO, en caso de NO hacer uso de la referencia, SÍ debe incluirse la etiqueta en el archivo de consulta enviando 25 espacios en blanco.</li>
						<li>Si la referencia ingresada es menor a 25 caracteres, debe incluir espacios a la izquierda hasta cumplir la longitud máxima.</li>
						<li><strong>Si se omite la etiqueta, se rechaza la solicitud de consulta.</strong></li>
					</ul>
				</div>
				<input type="text" id="intl_num_referencia" name="intl_num_referencia" class="form-control" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 25 caracteres | Numérico | Posición 7-31</small>
			</div>

			<div class="form-group">
				<label for="intl_producto">Clave del Producto <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-producto">?</span></label>
				<div id="tip-clave-producto" class="collapse">
					<p>El valor permitido es: <strong>007 = BC Score</strong></p>
					<p>Consideraciones:</p>
					<ul>
						<li>Para solicitar Prospector, la clave de consulta del Usuario debe tener el privilegio de uso previamente contratado (consultar al Ejecutivo Comercial).</li>
						<li><strong>Si el dato se omite o se incluye uno diferente, se rechaza la solicitud de consulta.</strong></li>
					</ul>		
				</div>
				<input type="text" id="intl_producto" name="intl_producto" class="form-control" value="007 - BC Score" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 3 caracteres | Numérico | Posición 32-34</small>
			</div>

			<div class="form-group">
				<label for="intl_pais">Clave del País <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-pais">?</span></label>
				<div id="tip-clave-pais" class="collapse">
					<p>Deberá contener las letras MX</p>
				</div>
				<input type="text" id="intl_pais" name="intl_pais" class="form-control" value="MX" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 35-36</small>
			</div>

			<div class="form-group">
				<label for="intl_reservado">RESERVADO</label>
				<input type="text" id="intl_reservado" name="intl_reservado" class="form-control" value="0000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Numbers | posición 37-40</small>
			</div>

			<div class="form-group">
				<label for="intl_clave_usuario">CLAVE DEL USUARIO o MEMBER CODE <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-clave-usuario">?</span></label>
				<div id="tip-clave-usuario" class="collapse">
					<p>
						Contiene la clave única del Usuario para consulta, la cual fue asignada por Buró de Crédito.
						Esta clave está formada como sigue:
						<ul>
							<li>
									Las primeras 2 posiciones son alfabéticas y corresponden a la
									clave de Tipo de Negocio o KOB (Kind of Business) del
									Usuario
							</li>
							<li>
									Los siguientes 4 números identifica al Usuario.
							</li>
							<li>
									Los últimos 4 números puede identificar productos, sucursales o área interna del Usuario.
							</li>
						</ul>
					</p>
				</div>
				<input type="text" id="intl_clave_usuario" name="intl_clave_usuario" class="form-control" value="ZM78211002" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 10 caracteres | Alfanumérico | Posición 41-50</small>
			</div>
			<div class="form-group">
				<label for="intl_password_usuario">CONTRASEÑA O PASSWORD DE ACCESO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-password-usuario">?</span></label>
				<div id="tip-password-usuario" class="collapse">
					<p>
						Incluir la “Contraseña” o “Password” cifrada o encriptada.
						Para obtener y dar mantenimiento a esta contraseña o password, seguir las
						indicaciones del ANEXO 13 “MANTENIMIENTO A CONTRASEÑA O
						PASSWORD”.
					</p>
				</div>
				<input type="text" id="intl_password_usuario" name="intl_password_usuario" class="form-control" value="GKL6GKL6" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 8 caracteres | Alfanumérico | Posición 51-58</small>
			</div>

			<div class="form-group">
				<label for="intl_responsabilidad">Tipo de Responsabilidad <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-responsabilidad">?</span></label>
				<div id="tip-responsabilidad" class="collapse">
					<p>Indicar si la solicitud del Informe Buró es para un crédito individual o mancomunado.</p>
				</div>
				<input id="intl_responsabilidad" name="intl_responsabilidad" class="form-control" value="I - Individual" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 1 caracter | Alfabético | Posición 59</small>
			</div>

			<div class="form-group">
				<label for="intl_tipo_contrato">Tipo de Contrato o Producto <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-contrato">?</span></label>
				<div id="tip-tipo-contrato" class="collapse">
					<p>Indicar el producto de la solicitud del Cliente.</p>
				</div>
				<input type="text" id="intl_tipo_contrato" name="intl_tipo_contrato" class="form-control" value="PL" disabled>
				<!--	<option value="AF">AF - Aparatos / Muebles</option>
					<option value="AG">AG - Agropecuario (PFAE)</option>
					<option value="AL">AL - Arrendamiento Automotriz</option>
					<option value="AP">AP - Aviación</option>
					<option value="AU">AU - Compra de Automóvil</option>
					<option value="BD">BD - Fianza</option>
					<option value="BT">BT - Bote / Lancha</option>
					<option value="CC">CC - Tarjeta de Crédito</option>
					<option value="CE">CE - Cartas de Crédito (PFAE)</option>
					<option value="CF">CF - Crédito Fiscal</option>
					<option value="CL">CL - Linea de Crédito</option>
					<option value="CO">CO - Consolidación</option>
					<option value="CS">CS - Crédito Simple (PFAE)</option>
					<option value="CT">CT - Con Colateral (PFAE)</option>
					<option value="DE">DE - Descuentos (PFAE)</option>
					<option value="EQ">EQ - Equipo</option>
					<option value="FI">FI - Fideicomiso (PFAE)</option>
					<option value="FT">FT - Factoraje</option>
					<option value="HA">HA - Habilitación o Avio (PFAE)</option>
					<option value="HE">HE - Préstamo tipo "Home Equity"</option>
					<option value="HI">HI - Mejoras a la casa</option>
					<option value="LS">LS - Arrendamiento</option>
					<option value="LR">LR - Línea de Crédito Reinstalable</option>
					<option value="MI">MI - Otros</option>
					<option value="OA">OA - Otros adeudos vencidos (PFAE)</option>
					<option value="PA">PA - Préstamo para Personas Físicas con Actividades Empresariales (PFAE)</option>
					<option value="PB">PB - Editorial</option>
					<option value="PG">PG - PGUE - Préstamo como garantía de unidades industriales para PFAE</option>
					<option value="PL">PL - Préstamo Personal</option>
					<option value="PN">PN - Préstamo de nómina</option>
					<option value="PQ">PQ - Quirografario (PFAE)</option>
					<option value="PR">PR - Prendario (PFAE)</option>
					<option value="PS">PS - Pago de Servicios</option>
					<option value="RC">RC - Reestructurado (PFAE)</option>
					<option value="RD">RD - Redescuento (PFAE)</option>
					<option value="RE">RE - Bienes Raíces</option>
					<option value="RF">RF - Refaccionario (PFAE)</option>
					<option value="RN">RN - Renovado (PFAE)</option>
					<option value="RV">RV - Vehículo Recreativo</option>
					<option value="SC">SC - Tarjeta Garantizada</option>
					<option value="SE">SE - Préstamo Garantizado</option>
					<option value="SG">SG - Seguros</option>
					<option value="SM">SM - Segunda Hipoteca</option>
					<option value="ST">ST - Préstamo para estudiante</option>
					<option value="TE">TE - Tarjeta de Crédito Empresarial</option>
					<option value="UK">UK - Desconocido</option>
					<option value="US">US - Préstamo no garantizado</option>
				-->
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 60-61</small>
			</div>

			<div class="form-group">
				<label for="intl_tipo_moneda">Moneda del crédito <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-moneda">?</span></label>
				<div id="tip-tipo-moneda" class="collapse">
					<p>Indica la moneda del crédito que solicita el Cliente.</p>
				</div>
				<input type="text" id="intl_tipo_moneda" name="intl_tipo_moneda" class="form-control" value="MX" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 62-63</small>
			</div>

			<div class="form-group">
				<label for="intl_importe_contrato">IMPORTE DEL CONTRATO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-encabezado-importe_contrato">?</span></label>
				<div id="tip-encabezado-importe_contrato" class="collapse">
					<p>Reportar 9 ceros.</p>
				</div>
				<input type="text" id="intl_importe_contrato" name="intl_importe_contrato" class="form-control" value="000000000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 9 caracteres | Numérico | Posición 64-72</small>
			</div>
			
			<div class="form-group">
				<label for="intl_idioma">Idioma <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-idioma">?</span></label>
				<div id="tip-idioma" class="collapse">
					<p>Indica el idioma en que deberá emitirse los informes de Crédito del archivo</p>
				</div>
				<input type="text" id="intl_idioma" name="intl_idioma" class="form-control" value="SP" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Alfabético | Posición 73-74</small>
			</div>

			<div class="form-group">
				<label for="intl_tipo_salida">Tipo de salida <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-tipo-salida">?</span></label>
				<div id="tip-tipo-salida" class="collapse">
					<p>Indica el tipo de formato en el que se entregarán los Informes Buró</p>
				</div>
				<input type="text" id="intl_tipo_salida" name="intl_tipo_salida" class="form-control" value="01" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 2 caracteres | Numérico | Posición 75-76</small>
			</div>

			<div class="form-group">
				<label for="intl_bloque_registro">TAMAÑO DEL BLOQUE DEL REGISTRO DE RESPUESTA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-bloque-registro">?</span></label>
				<div id="tip-bloque-registro" class="collapse">
					<p>Reportar 1 blanco o espacio.</p>
				</div>
				<input type="text" id="intl_bloque_registro" name="intl_bloque_registro" class="form-control" value=" " disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 1 caracter | Alfabético | Posición 77</small>
			</div>

			<div class="form-group">
				<label for="intl_id_impresora">IDENTIFICACIÓN DE LA IMPRESORA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-id-impresora">?</span></label>
				<div id="tip-id-impresora" class="collapse">
					<p>Deberá reportarse 4 espacios o blancos.</p>
				</div>
				<input type="text" id="intl_id_impresora" name="intl_id_impresora" class="form-control" value="    " disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 4 caracteres | Alfanumérico | Posición 78-81</small>
			</div>
			
			<div class="form-group">
				<label for="intl_uso_futuro">RESERVADO PARA USO FUTURO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-uso-futuro">?</span></label>
				<div id="tip-uso-futuro" class="collapse">
					<p>Reportar siete ceros.</p>
				</div>
				<input type="text" id="intl_uso_futuro" name="intl_uso_futuro" class="form-control" value="0000000" disabled>
				<small><strong class="req-label">Requerido</strong> | Logitud Fija | 7 caracteres | Numérico | Posición 82-88</small>
			</div>
		</section>
  </div>
</div>