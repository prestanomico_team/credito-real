<!-- ================================================================================== -->
<!-- ================================= SEGMENTO DIRECCION ============================= -->
<!-- ================================================================================== -->
<div class="panel panel-default">
  <div class="panel-heading">
  	<h2 class="panel-title"><strong>Segmento de Dirección del Cliente - PA</strong>  <span class="badge tip-btn" data-toggle="collapse" data-target="#desc_direccion">?</span></h2>
  	<div id="desc_direccion" class="collapse">
  		<p>Contiene los datos de la dirección proporcionada por el Cliente a consultar: calle, número exterior, colonia, delegación o municipio, ciudad, estado y código postal.</p>
			<p>Es un segmento requerido compuesto por 15 etiquetas.</p>
  	</div>
  	<button class="btn btn-default" data-toggle="collapse" data-target="#segmento-direccion">Formulario</button>
  </div>
  <div id="segmento-direccion" class="panel-body collapse">
  	<section id="direccion_preview">
	  	Preview: 
			<ul id="segmento_direccion_cliente" class="preview_segmento_list">
				<li class="seg-label" id="seg-etiqueta_primer_linea">PA</li>
				<li class="seg-count" id="seg-len_primer_linea"><% segs.dir.primer_linea | stringLength %></li>
				<li id="seg-primer_linea"><% segs.dir.primer_linea %></li>
				<span ng-show="segs.dir.segunda_linea">
					<li class="seg-label" id="seg-etiqueta_segunda_linea">00</li>
					<li class="seg-count" id="seg-len_segunda_linea"><% segs.dir.segunda_linea | stringLength %></li>
					<li id="seg-segunda_linea"><% segs.dir.segunda_linea %></li>
				</span>
				<span ng-show="segs.dir.colonia">
					<li class="seg-label" id="seg-etiqueta_colonia">01</li>
					<li class="seg-count" id="seg-len_colonia"><% segs.dir.colonia | stringLength %></li>
					<li id="seg-colonia"><% segs.dir.colonia %></li>
				</span>
				<span ng-show="segs.dir.delegacion">
					<li class="seg-label" id="seg-etiqueta_delegacion">02</li>
					<li class="seg-count" id="seg-len_delegacion"><% segs.dir.delegacion | stringLength %></li>
					<li id="seg-delegacion"><% segs.dir.delegacion %></li>
				</span>
				<span ng-show="segs.dir.ciudad">
					<li class="seg-label" id="seg-etiqueta_ciudad">03</li>
					<li class="seg-count" id="seg-len_ciudad"><% segs.dir.ciudad | stringLength %></li>
					<li id="seg-ciudad"><% segs.dir.ciudad %></li>
				</span>
				<li class="seg-label" id="seg-etiqueta_estado">04</li>
				<li class="seg-count" id="seg-len_estado"><% segs.dir.estado | stringLength %></li>
				<li id="seg-estado"><% segs.dir.estado %></li>

				<li class="seg-label" id="seg-etiqueta_codigo_postal">05</li>
				<li class="seg-count" id="seg-len_codigo_postal"><% segs.dir.codigo_postal | stringLength %></li>
				<li id="seg-codigo_postal"><% segs.dir.codigo_postal %></li>

				<span ng-show="segs.dir.fecha_residencia">
					<li class="seg-label" id="seg-etiqueta_fecha_residencia">06</li>
					<li class="seg-count" id="seg-len_fecha_residencia"><% segs.dir.fecha_residencia | stringLength %></li>
					<li id="seg-fecha_residencia"><% segs.dir.fecha_residencia %></li>
				</span>

				<span ng-show="segs.dir.telefono">
					<li class="seg-label" id="seg-etiqueta_telefono">07</li>
					<li class="seg-count" id="seg-len_telefono"><% segs.dir.telefono | stringLength %></li>
					<li id="seg-telefono"><% segs.dir.telefono %></li>
				</span>

				<span ng-show="segs.dir.ext_tel">
					<li class="seg-label" id="seg-etiqueta_ext_tel">08</li>
					<li class="seg-count" id="seg-len_ext_tel"><% segs.dir.ext_tel | stringLength %></li>
					<li id="seg-ext_tel"><% segs.dir.ext_tel %></li>
				</span>

				<span ng-show="segs.dir.fax">
					<li class="seg-label" id="seg-etiqueta_fax">09</li>
					<li class="seg-count" id="seg-len_fax"><% segs.dir.fax | stringLength %></li>
					<li id="seg-fax"><% segs.dir.fax %></li>
				</span>

				<span ng-show="segs.dir.tipo_domicilio">
					<li class="seg-label" id="seg-etiqueta_tipo_domicilio">10</li>
					<li class="seg-count" id="seg-len_tipo_domicilio"><% segs.dir.tipo_domicilio | stringLength %></li>
					<li id="seg-tipo_domicilio"><% segs.dir.tipo_domicilio %></li>
				</span>

				<span ng-show="segs.dir.indicador">
					<li class="seg-label" id="seg-etiqueta_indicador">11</li>
					<li class="seg-count" id="seg-len_indicador"><% segs.dir.indicador | stringLength %></li>
					<li id="seg-indicador"><% segs.dir.indicador %></li>
				</span>

				<span ng-show="segs.dir.fecha_reporte">
					<li class="seg-label" id="seg-etiqueta_fecha_reporte">12</li>
					<li class="seg-count" id="seg-len_fecha_reporte"><% segs.dir.fecha_reporte | stringLength %></li>
					<li id="seg-fecha_reporte"><% segs.dir.fecha_reporte %></li>
				</span>

				<li class="seg-label" id="seg-etiqueta_origen">13</li>
				<li class="seg-count" id="seg-len_origen">2</li>
				<li id="seg-origen">MX</li>
			</ul>
			<hr>
		</section>
		<section id="direccion_settings">
			<div class="form-group">
				<label for="dir_primer_linea">PA - PRIMER LÍNEA DE DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-primer-linea">?</span></label>
				<div id="tip-dir-primer-linea" class="collapse">
					<p>
						Contiene la dirección de la casa del Cliente.<br>
						Incluir: calle o similar, número exterior e interior cuando existan.
						<ul>
							<li>En caso de reportar únicamente la calle sin el número la dirección será rechazada.</li>
							<li>Si el domicilio no tiene número, especificar “SN”.</li>
							<li>Para un domicilio “conocido” reportar como: “DOMICLIO CONOCIDO SN”</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_primer_linea" name="dir_primer_linea" class="form-control" ng-model="segs.dir.primer_linea" maxlength="40">
				<small><strong class="req-label">Requerido</strong> | Etiqueta: PA | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="dir_segunda_linea">00 - SEGUNDA LÍNEA DE DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-segunda-linea">?</span></label>
				<div id="tip-dir-segunda-linea" class="collapse">
					<p>
						Este campo es usado cuando no es suficiente el campo anterior de “Primer Línea de Dirección”
					</p>
				</div>
				<input type="text" id="dir_segunda_linea" name="dir_segunda_linea" class="form-control" ng-model="segs.dir.segunda_linea" maxlength="40">
				<small>Opcional | Etiqueta: 00 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="dir_colonia">01 - COLONIA O POBLACIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-colonia">?</span></label>
				<div id="tip-dir-colonia" class="collapse">
					<p>
						Reportar la Colonia o población si se tiene disponible.
					</p>
				</div>
				<input type="text" id="dir_colonia" name="dir_colonia" class="form-control" ng-model="segs.dir.colonia" maxlength="40">
				<small>Opcional | Etiqueta: 01 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>

			<div class="form-group">
				<label for="dir_delegacion">02 - DELEGACIÓN O MUNICIPIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-delegacion">?</span></label>
				<div id="tip-dir-delegacion" class="collapse">
					<p>
						Reportar la Delegación o el Municipio si es que se tiene disponible.
						<ul>
							<li>En caso de no reportar la Delegación o el Municipio, el campo 03 de Ciudad se hace requerido.</li>
							<li>En caso de no reportar la Ciudad, el campo de Delegación o Municipio se hace requerido.</li>
							<li>Si no se aplica uno de los 2 puntos anteriores, la consulta se rechazará.</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_delegacion" name="dir_delegacion" class="form-control" ng-model="segs.dir.delegacion" maxlength="40">
				<small><strong class="req-label">Requerido (sin Ciudad)</strong> Opcional (con Ciudad) | Etiqueta: 02 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>
	
			<div class="form-group">
				<label for="dir_ciudad">03 - CIUDAD <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-ciudad">?</span></label>
				<div id="tip-dir-ciudad" class="collapse">
					<p>
						Reportar el nombre de la ciudad.
						<ul>
							<li>En caso de no reportar el campo 02 de “Delegación o Municipio”, el campo de Ciudad se hace requerido.</li>
							<li>En caso de no reportar la Ciudad, el campo de Delegación o Municipio se hace requerido.</li>
							<li>Si no se aplica uno de los 2 puntos anteriores, la consulta se rechazará</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_ciudad" name="dir_ciudad" class="form-control" ng-model="segs.dir.ciudad" maxlength="40">
				<small><strong class="req-label">Requerido (sin Delegación o Municipio)</strong> Opcional (con Delegación o Municipio) | Etiqueta: 03 | Logitud Variable | 40 caracteres max | Alfanumérico</small>
			</div>
		
			<div class="form-group">
				<label for="dir_estado">04 - ESTADO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-estado">?</span></label>
				<div id="tip-dir-estado" class="collapse">
					<p>
						Contiene el código del estado de la República Mexicana donde tiene su residencia el cliente.
					</p>
				</div>
				<select id="dir_estado" name="dir_estado" class="form-control" ng-model="segs.dir.estado">
					<option value="AGS">AGS - Aguascalientes</option>
					<option value="BCN">BCN - Baja California Norte</option>
					<option value="BCS">BCS - Baja California Sur</option>
					<option value="CAM">CAM - Campeche</option>
					<option value="CHS">CHS - Chiapas</option>
					<option value="CHI">CHI - Chihuahua</option>
					<option value="COA">COA - Coahuila</option>
					<option value="COL">COL - Colima</option>
					<option value="DF">DF - Distrito Federal</option>
					<option value="DGO">DGO - Durango</option>
					<option value="EM">EM - Estado de México</option>
					<option value="GTO">GTO - Guanajuato</option>
					<option value="GRO">GRO - Guerrero</option>
					<option value="HGO">HGO - Hidalgo</option>
					<option value="JAL">JAL - Jalisco</option>
					<option value="MICH">MICH - Michoacán</option>
					<option value="MOR">MOR - Morelos</option>
					<option value="NAY">NAY - Nayarit</option>
					<option value="NL">NL - Nuevo León</option>
					<option value="OAX">OAX - Oaxaca</option>
					<option value="PUE">PUE - Puebla</option>
					<option value="QRO">QRO - Querétaro</option>
					<option value="QR">QR - Quintana Roo</option>
					<option value="SLP">SLP - San Luis Potosí</option>
					<option value="SIN">SIN - Sinaloa</option>
					<option value="SON">SON - Sonora</option>
					<option value="TAB">TAB - Tabasco</option>
					<option value="TAM">TAM - Tamaulipas</option>
					<option value="TLA">TLA - Tlaxcala</option>
					<option value="VER">VER - Veracruz</option>
					<option value="YUC">YUC - Yucatán</option>
					<option value="ZAC">ZAC - Zacatecas</option>
				</select>
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 04 | Logitud Variable | 04 caracteres max | Alfabético</small>
			</div>
		
			<div class="form-group">
				<label for="dir_codigo_postal">05 - CÓDIGO POSTAL <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-codigo-postal">?</span></label>
				<div id="tip-dir-codigo-postal" class="collapse">
					<p>
						Se reporta el Código Postal correspondiente, debe ser de exactamente 5
						posiciones, incluyendo ceros (0) a la izquierda si así lo indica el código.<br>
						Este dato se validará de acuerdo a la lista de SEPOMEX, disponible en Internet, y
						concordar con el Estado, Delegación o Municipio y Ciudad.<br>
						Si no concuerda o no se incluye un dato se rechazará la consulta.
					</p>
				</div>
				<input type="text" id="dir_codigo_postal" name="dir_codigo_postal" class="form-control" ng-model="segs.dir.codigo_postal" maxlength="5">
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 05 | Logitud Fija | 05 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="dir_fecha_residencia">06 - FECHA DE RESIDENCIA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-fecha-residencia">?</span></label>
				<div id="tip-dir-fecha-residencia" class="collapse">
					<p>
						Reportar la fecha desde la cual el Cliente vive en la dirección reportada.
						El formato es DDMMAAAA:
						<ul>
							<li>DD: número entre 01- 31</li>
							<li>MM: número entre 01-12</li>
							<li>AAAA: año de 4 dígitos</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_fecha_residencia" name="dir_fecha_residencia" class="form-control" ng-model="segs.dir.fecha_residencia" maxlength="8" disabled>
				<small>Opcional | Etiqueta: 06 | Logitud Fija | 08 caracteres max | Numérico | Formato DDMMAAAA</small>
			</div>

			<div class="form-group">
				<label for="dir_telefono">07 - NÚMERO DE TELÉFONO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-telefono">?</span></label>
				<div id="tip-dir-telefono" class="collapse">
					<p>
						Contiene el número telefónico de casa del Cliente.<br>
						El formato es:
						<ul>
							<li>Código de área + hasta 8 dígitos del teléfono</li>
							<li>Longitud mínima es de 5 dígitos, no usar guiones, no repetir el mismo número, ejemplo: 00000 ó 33333 ó 77777, etc.</li>
							<li>Ejemplo: número telefónico de la CD. de México se reporta: 5554494949</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_telefono" name="dir_telefono" class="form-control" ng-model="segs.dir.telefono" maxlength="11">
				<small>Opcional | Etiqueta: 07 | Logitud Variable | 11 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="dir_ext_tel">08 - EXTENSIÓN TELEFÓNICA <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-ext-tel">?</span></label>
				<div id="tip-dir-ext-tel" class="collapse">
					<p>
						Reportar si se cuenta con el dato.
					</p>
				</div>
				<input type="text" id="dir_ext_tel" name="dir_ext_tel" class="form-control" ng-model="segs.dir.ext_tel" maxlength="8" disabled>
				<small>Opcional | Etiqueta: 08 | Logitud Variable | 8 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="dir_fax">09 - NÚMERO DE FAX EN ESTA DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-fax">?</span></label>
				<div id="tip-dir-fax" class="collapse">
					<p>
						Contiene el número telefónico del Fax en casa del Cliente. Reportar si se tiene.<br>
						El formato es:
						<ul>
							<li>Código de área + hasta 8 dígitos del teléfono</li>
							<li>Longitud mínima es de 5 dígitos, no usar guiones, no repetir el mismo número, ejemplo: 00000 ó 33333 ó 77777, etc.</li>
							<li>Ejemplo: número telefónico de la CD. de México se reporta: 5554494949</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_fax" name="dir_fax" class="form-control" ng-model="segs.dir.fax" maxlength="11" disabled>
				<small>Opcional | Etiqueta: 09 | Logitud Variable | 11 caracteres max | Numérico</small>
			</div>

			<div class="form-group">
				<label for="dir_tipo_domicilio">10 - TIPO DE DOMICILIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-tipo-domicilio">?</span></label>
				<div id="tip-dir-tipo-domicilio" class="collapse">
					<p>
						Indica el tipo de Domicilio que el Cliente ha proporcionado.<br>
						Debe contener uno de los siguientes valores si es que se tiene disponible el dato:<br>
						<ul>
							<li>B = Negocio</li>
							<li>C = Domicilio del Usuario</li>
							<li>H = Casa</li>
							<li>P = Apartado Postal</li>
						</ul>
						Si se incluye un valor inválido, se tomará como blanco.
					</p>
				</div>
				<select id="dir_tipo_domicilio" name="dir_tipo_domicilio" class="form-control" ng-model="segs.dir.tipo_domicilio" disabled>
					<option value="B">B - Negocio</option>
					<option value="C">C - Domicilio del Usuario</option>
					<option value="H">H - Casa</option>
					<option value="P">P - Apartado Postal</option>
				</select>
				<small>Opcional | Etiqueta: 10 | Logitud Fija | 1 caracteres | Alfabético</small>
			</div>

			<div class="form-group">
				<label for="dir_indicador">11 - INDICADOR ESPECIAL DE DOMICILIO <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-indicador">?</span></label>
				<div id="tip-dir-indicador" class="collapse">
					<p>
						Indica el tipo de ubicación del domicilio del Cliente.<br>
						Debe contener uno de los siguientes valores si es que se tiene disponible el dato:<br>
						<ul>
							<li>M = Militar</li>
							<li>R = Rural</li>
							<li>K = Domicilio conocido</li>
						</ul>
					</p>
				</div>
				<select id="dir_indicador" name="dir_indicador" class="form-control" ng-model="segs.dir.indicador" disabled>
					<option value="M">M - Militar</option>
					<option value="R">R - Rural</option>
					<option value="K">K - Domicilio conocido</option>
				</select>
				<small>Opcional | Etiqueta: 11 | Logitud Fija | 1 caracteres | Alfabético</small>
			</div>

			<div class="form-group">
				<label for="dir_fecha_reporte">12 - FECHA DE REPORTE DE LA DIRECCIÓN <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-fecha-reporte">?</span></label>
				<div id="tip-dir-fecha-reporte" class="collapse">
					<p>
						Indica la fecha en que se reportó la dirección en BURO DE CREDITO.<br>
						El formato es DDMMAAAA:
						<ul>
							<li>DD: número entre 01- 31</li>
							<li>MM: número entre 01-12</li>
							<li>AAAA: año de 4 dígitos</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_fecha_reporte" name="dir_fecha_reporte" class="form-control" ng-model="segs.dir.fecha_reporte" maxlength="8" disabled>
				<small>Opcional | Etiqueta: 12 | Logitud Fija | 8 caracteres | Numérico</small>
			</div>

			<div class="form-group">
				<label for="dir_origen">13 - ORIGEN DEL DOMICILIO (PAÍS) <span class="badge tip-btn" data-toggle="collapse" data-target="#tip-dir-origen">?</span></label>
				<div id="tip-dir-origen" class="collapse">
					<p>
						<ul>
							<li>Indicar el origen de la dirección del Acreditado.</li>
							<li>El origen (país) del domicilio del Cliente se clasifica de acuerdo con el catálogo “PAÍSES” (ver Anexo 10 – Códigos de Países).</li>
						</ul>
					</p>
				</div>
				<input type="text" id="dir_origen" name="dir_origen" class="form-control" value="MX" disabled>
				<small><strong class="req-label">Requerido</strong> | Etiqueta: 13 | Logitud Fija | 2 caracteres | Alfabético</small>
			</div>
		</section>
  </div>
</div>