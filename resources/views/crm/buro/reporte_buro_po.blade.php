<table>
	<tr colspan="2">
		<td>
			{{ $solicitud->prospecto->nombres }} {{ $solicitud->prospecto->apellido_paterno }} {{ $solicitud->prospecto->apellido_materno }}
		</td>
	</tr>
	<tr colspan="2">
		<td>
			{{ $solicitud->ocupacion }}
		</td>
	</tr>
	<tr>
		<td>
			BC Score
		</td>
		<td>
			{{ intval($solicitud->bc_score->bc_score) }}
		</td>
	</tr>
	<tr>
		<td>
			Micro Score
		</td>
		<td>
			{{ intval($solicitud->bc_score->micro_valor) }}
		</td>
	</tr>
	<tr>
		<td>
			Folio Consulta
		</td>
		<td>
			{{ intval($solicitud->respuesta_segunda_llamadabc[0]['folio_consulta']) }}
		</td>
	</tr>
</table>
<table id="lineas_abiertas">
	<caption>Lineas Abiertas</caption>
	<tr>
		<th>
			Otorgante
		</th>
		<th>
			TC
		</th>
		<th>
			Fecha Apertura
		</th>
		<th>
			Meses Antigüedad
		</th>
		<th>
			MOP Actual
		</th>
		<th>
			Máximo MOP Últimos 6 Meses
		</th>
		<th>
			Histórico de Pagos
		</th>
		<th>
			Límite Crédito
		</th>
		<th>
			Saldo Actual
		</th>
		<th>
			Uso Línea Revolvente
		</th>
		<th>
			Pago Mensual
		</th>
		<th>
			Crédito Máximo
		</th>
		<th>
			Clave Observación
		</th>
	</tr>
@foreach ($bc_cuentas['lineas_abiertas'] as $key => $linea_abierta)
	@foreach ($linea_abierta as $keyL => $linea)
	<tr>
		@if ($loop->first)
		<td rowspan="{{ count($linea_abierta) }}">
			{{ $key }}
		</td>
		@endif
		<td>
			{{ $linea['CUENTA_CONTRATO_PRODUCTO'] }}
		</td>
		<td>
			{{ $linea['CUENTA_FECHA_APERTURA'] }}
		</td>
		<td>
			{{ $linea['MesesAntigCuenta'] }}
		</td>
		<td>
			{{ $linea['Cuenta_MOP_V2'] }}
		</td>
		<td>
			{{ $linea['MMop6'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Hist_Pagos'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Limite_Credito'] }}
		</td>
		<td>
			{{ $linea['Sdo_Act2'] }}
		</td>
		<td>
			{{ ($linea['UsoLRev'] * 100) }}%
		</td>
		<td>
			{{ $linea['PagoMensualBuro2'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Cred_Max_Aut'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Clave_Observacion'] }}
		</td>
	</tr>
	@endforeach
@endforeach
</table>

<table id="lineas_cerradas">
	<caption>Lineas Cerradas</caption>
	<tr>
		<th>
			Otorgante
		</th>
		<th>
			TC
		</th>
		<th>
			Fecha Apertura
		</th>
		<th>
			Fecha Cierre
		</th>
		<th>
			Historico Pagos
		</th>
		<th>
			Límite Crédito
		</th>
		<th>
			Crédito Máximo
		</th>
		<th>
			Clave Observación
		</th>
	</tr>
@foreach ($bc_cuentas['lineas_cerradas'] as $key => $linea_cerrada)
	@foreach ($linea_cerrada as $keyL => $linea)
	<tr>
		@if ($loop->first)
		<td rowspan="{{ count($linea_cerrada) }}">
			{{ $key }}
		</td>
		@endif
		<td>
			{{ $linea['CUENTA_CONTRATO_PRODUCTO'] }}
		</td>
		<td>
			{{ $linea['CUENTA_FECHA_APERTURA'] }}
		</td>
		<td>
			{{ $linea['cuenta_fecha_cierre'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Hist_Pagos'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Limite_Credito'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Cred_Max_Aut'] }}
		</td>
		<td>
			{{ $linea['Cuenta_Clave_Observacion'] }}
		</td>
	</tr>
	@endforeach
@endforeach
</table>
