@extends('crm.app')
@section('content')
<div style="margin:20px;">
	<div class="row">
		<div class="col-sm-6">
			<h2 class="title">TABLA LOCAL</h2>
		</div>
		@if(Auth::user()->can('tabla-local'))
		<div class="col-sm-6">
			<form method="post" action="/panel/tabla-local">
				{!! csrf_field() !!}
				<input type="hidden" name="create_new_mock" value="1">
				<input type="submit" class="btn btn-primary pull-right" value="&plus; Nueva Persona Mock">
			</form>
		</div>
		@endif
	</div>
	@if(Auth::user()->can('tabla-local'))
		<div class="row">

			<div class="col-sm-12">
				<?php
					if($last_action != ''){
						echo '<div class="alert alert-'.$action_type.'"><a class="btn btn-warning btn-xs" href="/panel/tabla-local/">&times;</a> &nbsp;'.$last_action.'</div>';
					}
				?>
			</div>

		</div>

		<table class="table table-striped" style="font-size:12px;margin-right:50px;">
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>SEG NOMBRE</th>
				<th>APELL P</th>
				<th>APELL M</th>
				<th>FECHA NAC</th>
				<th>DOM CIUDAD</th>
				<th>DOM ESTADO</th>
				<th>CRED HIP</th>
				<th>CRED AUTO</th>
				<th>CRED TDC</th>
				<th>BC SCORE</th>
				<th>ICC SCORE</th>
				<th>MICRO SCORE</th>
				<th>Borrar</th>
			</tr>
			@foreach($mock_people as $pr)
			<tr>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->id }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->nombre }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->seg_nombre }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->apellido_p }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->apellido_m }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->fecha_nac_dd.'/'.$pr->fecha_nac_mm.'/'.$pr->fecha_nac_yyyy }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->dom_ciudad }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->dom_estado }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ ($pr->credito_hipo)? 'Si' : 'No' }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ ($pr->credito_auto)? 'Si' : 'No' }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ ($pr->credito_banc)? 'Si' : 'No' }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->bc_valor }}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->icc_valor}}</a></td>
				<td><a data-toggle="modal" data-target="#mock-person-modal-{{$pr->id}}">{{ $pr->micro_valor}}</a></td>
				<td class="text-center">
					<form method="post" action="/panel/tabla-local">
						{!! csrf_field() !!}
						<input type="hidden" name="delete_mock_id" value="{{ $pr->id }}">
						<input type="submit" class="btn btn-danger btn-xs" value="&times;" style="padding:2px;line-height:8px;">
					</form>
				</td>
			</tr>
			@endforeach
		</table>

		<!-- modals each mock person's details -->
		@foreach($mock_people as $pr)
			<div class="modal fade" id="mock-person-modal-{{$pr->id}}" tabindex="-1" role="dialog">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Persona Mock ID:{{$pr->id}} {{ $pr->nombre }} {{ $pr->seg_nombre }} {{ $pr->apellido_p }} {{ $pr->apellido_m }}</h4>
			      </div>
			      <div class="modal-body">
			      	<!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active"><a href="#tab-datos-personales-{{$pr->id}}" role="tab" data-toggle="tab">Datos Personales</a></li>
						    <li role="presentation"><a href="#tab-cuentas-{{$pr->id}}" role="tab" data-toggle="tab">Cuentas</a></li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active" id="tab-datos-personales-{{$pr->id}}">
							    <form id="mock_edit_form" method="POST" action="/panel/tabla-local">
			      				{!! csrf_field() !!}
			      				<input type="hidden" name="id" value="{{ $pr->id }}" class="form-control">
							    	<table class="table table-striped" style="width:100%;">
											<tr><th>Nombre:</th>  						<td><input type="text" name="nombre" 				value="{{ $pr->nombre }}" class="form-control"></td></tr>
											<tr><th>Seg Nombre:</th>  				<td><input type="text" name="seg_nombre" 		value="{{ $pr->seg_nombre }}" class="form-control"></td></tr>
											<tr><th>Apellido P:</th>  				<td><input type="text" name="apellido_p" 		value="{{ $pr->apellido_p }}" class="form-control"></td></tr>
											<tr><th>Apellido M:</th>  				<td><input type="text" name="apellido_m" 		value="{{ $pr->apellido_m }}" class="form-control"></td></tr>
											<tr>
												<th>Fecha de Nac:</th>
												<td>
													<input type="text" name="fecha_nac_dd" value="{{ $pr->fecha_nac_dd }}"  class="form-control" style="display:inline;margin-right:5px;width:50px">
													<input type="text" name="fecha_nac_mm" value="{{ $pr->fecha_nac_mm }}"  class="form-control" style="display:inline;margin-right:5px;width:50px">
													<input type="text" name="fecha_nac_yyyy" value="{{ $pr->fecha_nac_yyyy }}"  class="form-control" style="display:inline;margin-right:5px;width:70px">
												</td>
											</tr>
											<tr><th>RFC:</th> <td><input type="text" name="rfc" value="{{ $pr->rfc }}" class="form-control"></td></tr>
											<tr>
												<th>Estado Civil:</th>
												<td>
													<select name="estado_civil" class="form-control">
													<option></option>
													<option value="D" {{ ($pr->estado_civil == "D")? "selected" : "" }}>D - Divorciado</option>
													<option value="F" {{ ($pr->estado_civil == "F")? "selected" : "" }}>F - Unión Libre</option>
													<option value="M" {{ ($pr->estado_civil == "M")? "selected" : "" }}>M - Casado</option>
													<option value="S" {{ ($pr->estado_civil == "S")? "selected" : "" }}>S - Soltero</option>
													<option value="W" {{ ($pr->estado_civil == "W")? "selected" : "" }}>W - Viudo</option>
												</select>
												</td>
											</tr>
											<tr>
												<th>Género:</th>
												<td>
													<select name="sexo" class="form-control">
														<option value="M" {{ ($pr->sexo == "M")? "selected" : "" }}>M</option>
														<option value="F" {{ ($pr->sexo == "F")? "selected" : "" }}>F</option>
													</select>
												</td>
											</tr>
											<tr><th>CURP:</th>  							<td><input type="text" name="curp" value="{{ $pr->curp }}" class="form-control"></td></tr>
											<tr><th>Dom Calle:</th>  					<td><input type="text" name="dom_calle" value="{{ $pr->dom_calle }}" class="form-control"></td></tr>
											<tr><th>Dom Num Ext:</th>  				<td><input type="text" name="dom_num_ext" value="{{ $pr->dom_num_ext }}" class="form-control"></td></tr>
											<tr><th>Dom Num Int:</th>  				<td><input type="text" name="dom_num_int" value="{{ $pr->dom_num_int }}" class="form-control"></td></tr>
											<tr><th>Dom Colonia:</th>  				<td><input type="text" name="dom_colonia" value="{{ $pr->dom_colonia }}" class="form-control"></td></tr>
											<tr><th>Dom Deleg:</th>  					<td><input type="text" name="dom_deleg" value="{{ $pr->dom_deleg }}" class="form-control"></td></tr>
											<tr><th>Dom Ciudad:</th>  				<td><input type="text" name="dom_ciudad" value="{{ $pr->dom_ciudad }}" class="form-control"></td></tr>
											<tr>
												<th>Dom Estado:</th>
											  <td>
											  	<select name="dom_estado" class="form-control">
											  		<option value=""></option>
											  		<option value="AGS" {{ ($pr->dom_estado == "AGS")? "selected" : "" }}>AGS - Aguascalientes</option>
											  		<option value="BCN" {{ ($pr->dom_estado == "BCN")? "selected" : "" }}>BCN - Baja California Norte</option>
											  		<option value="BCS" {{ ($pr->dom_estado == "BCS")? "selected" : "" }}>BCS - Baja California Sur</option>
											  		<option value="CAM" {{ ($pr->dom_estado == "CAM")? "selected" : "" }}>CAM - Campeche</option>
											  		<option value="CHS" {{ ($pr->dom_estado == "CHS")? "selected" : "" }}>CHS - Chiapas</option>
											  		<option value="CHI" {{ ($pr->dom_estado == "CHI")? "selected" : "" }}>CHI - Chihuahua</option>
											  		<option value="COA" {{ ($pr->dom_estado == "COA")? "selected" : "" }}>COA - Coahuila</option>
											  		<option value="COL" {{ ($pr->dom_estado == "COL")? "selected" : "" }}>COL - Colima</option>
											  		<option value="DF" {{ ($pr->dom_estado == "DF")? "selected" : "" }}>DF - Distrito Federal</option>
											  		<option value="DGO" {{ ($pr->dom_estado == "DGO")? "selected" : "" }}>DGO - Durango</option>
											  		<option value="EM" {{ ($pr->dom_estado == "EM")? "selected" : "" }}>EM - Estado de México</option>
											  		<option value="GTO" {{ ($pr->dom_estado == "GTO")? "selected" : "" }}>GTO - Guanajuato</option>
											  		<option value="GRO" {{ ($pr->dom_estado == "GRO")? "selected" : "" }}>GRO - Guerrero</option>
											  		<option value="HGO" {{ ($pr->dom_estado == "HGO")? "selected" : "" }}>HGO - Hidalgo</option>
											  		<option value="JAL" {{ ($pr->dom_estado == "JAL")? "selected" : "" }}>JAL - Jalisco</option>
											  		<option value="MICH" {{ ($pr->dom_estado == "ICH")? "selected" : "" }}>MICH - Michoacán</option>
											  		<option value="MOR" {{ ($pr->dom_estado == "MOR")? "selected" : "" }}>MOR - Morelos</option>
											  		<option value="NAY" {{ ($pr->dom_estado == "NAY")? "selected" : "" }}>NAY - Nayarit</option>
											  		<option value="NL" {{ ($pr->dom_estado == "NL")? "selected" : "" }}>NL - Nuevo León</option>
											  		<option value="OAX" {{ ($pr->dom_estado == "OAX")? "selected" : "" }}>OAX - Oaxaca</option>
											  		<option value="PUE" {{ ($pr->dom_estado == "PUE")? "selected" : "" }}>PUE - Puebla</option>
											  		<option value="QRO" {{ ($pr->dom_estado == "QRO")? "selected" : "" }}>QRO - Querétaro</option>
											  		<option value="QR" {{ ($pr->dom_estado == "QR")? "selected" : "" }}>QR - Quintana Roo</option>
											  		<option value="SLP" {{ ($pr->dom_estado == "SLP")? "selected" : "" }}>SLP - San Luis Potosí</option>
											  		<option value="SIN" {{ ($pr->dom_estado == "SIN")? "selected" : "" }}>SIN - Sinaloa</option>
											  		<option value="SON" {{ ($pr->dom_estado == "SON")? "selected" : "" }}>SON - Sonora</option>
											  		<option value="TAB" {{ ($pr->dom_estado == "TAB")? "selected" : "" }}>TAB - Tabasco</option>
											  		<option value="TAM" {{ ($pr->dom_estado == "TAM")? "selected" : "" }}>TAM - Tamaulipas</option>
											  		<option value="TLA" {{ ($pr->dom_estado == "TLA")? "selected" : "" }}>TLA - Tlaxcala</option>
											  		<option value="VER" {{ ($pr->dom_estado == "VER")? "selected" : "" }}>VER - Veracruz</option>
											  		<option value="YUC" {{ ($pr->dom_estado == "YUC")? "selected" : "" }}>YUC - Yucatán</option>
											  		<option value="ZAC" {{ ($pr->dom_estado == "ZAC")? "selected" : "" }}>ZAC - Zacatecas</option>
											  	</select>
											  </td>
											</tr>
											<tr><th>Dom CP:</th>  						<td><input type="text" name="dom_cp" value="{{ $pr->dom_cp }}" class="form-control"></td></tr>
											<tr><th>Dom Tel Casa:</th>  			<td><input type="text" name="dom_tel_casa" value="{{ $pr->dom_tel_casa }}" class="form-control"></td></tr>
											<tr>
												<th>Cred Hipo:</th>
												<td>
													<input type="radio" name="credito_hipo" value="1" {{ ($pr->credito_hipo)? 'checked' : ''}}> <label>SI</label>
													<input type="radio" name="credito_hipo" value="0" {{ ($pr->credito_hipo)? '' : 'checked'}}> <label>NO</label>
												</td>
											</tr>
											<tr>
												<th>Cred Auto:</th>
												<td>
													<input type="radio" name="credito_auto" value="1" {{ ($pr->credito_auto)? 'checked' : ''}}> <label>SI</label>
													<input type="radio" name="credito_auto" value="0" {{ ($pr->credito_auto)? '' : 'checked'}}> <label>NO</label>
												</td>
											</tr>
											<tr>
												<th>Cred TDC:</th>
												<td>
													<input type="radio" name="credito_banc" value="1" {{ ($pr->credito_banc)? 'checked' : ''}}> <label>SI</label>
													<input type="radio" name="credito_banc" value="0" {{ ($pr->credito_banc)? '' : 'checked'}}> <label>NO</label>
												</td>
											</tr>
											<tr><th>Cred Num TDC:</th>  			<td><input type="text" name="credito_banc_num" value="{{ $pr->credito_banc_num }}" class="form-control"></td></tr>
											<tr>
												<th>Ocupación:</th>
											  <td>
											  	<select name="ocupacion" class="form-control">
														<option></option>
														<option value="Empleado Sector Público" {{ ($pr->ocupacion == "Empleado Sector Público")? "selected" : "" }}>Empleado Sector Público</option>
														<option value="Empleado Sector Privado" {{ ($pr->ocupacion == "Empleado Sector Privado")? "selected" : "" }}>Empleado Sector Privado</option>
														<option value="Negocio Propio" {{ ($pr->ocupacion == "Negocio Propio")? "selected" : "" }}>Negocio Propio</option>
														<option value="Profesional Independiente" {{ ($pr->ocupacion == "Profesional Independiente")? "selected" : "" }}>Profesional Independiente</option>
														<option value="Arrendador" {{ ($pr->ocupacion == "Arrendador")? "selected" : "" }}>Arrendador</option>
														<option value="Pensionado" {{ ($pr->ocupacion == "Pensionado")? "selected" : "" }}>Pensionado</option>
														<option value="Jubilado" {{ ($pr->ocupacion == "Jubilado")? "selected" : "" }}>Jubilado</option>
														<option value="Otro" {{ ($pr->ocupacion == "Otro")? "selected" : "" }}>Otro</option>
													</select>
											  </td>
											</tr>
											<tr>
												<th>Fuente Ingr:</th>
											  <td>
											  	<select name="fuente_ingresos" class="form-control">
														<option></option>
														<option value="Salario" {{ ($pr->fuente_ingresos == "Salario")? "selected" : "" }}>Salario</option>
														<option value="Honorarios" {{ ($pr->fuente_ingresos == "Honorarios")? "selected" : "" }}>Honorarios</option>
														<option value="Renta" {{ ($pr->fuente_ingresos == "Renta")? "selected" : "" }}>Renta</option>
														<option value="Inmuebles" {{ ($pr->fuente_ingresos == "Inmuebles")? "selected" : "" }}>Inmuebles</option>
														<option value="Pensión" {{ ($pr->fuente_ingresos == "Pensión")? "selected" : "" }}>Pensión</option>
														<option value="Jubilación" {{ ($pr->fuente_ingresos == "Jubilación")? "selected" : "" }}>Jubilación</option>
														<option value="Otro" {{ ($pr->fuente_ingresos == "Otro")? "selected" : "" }}>Otro</option>
													</select>
											  </td>
											</tr>
											<tr>
												<th>Nivel Estudios:</th>
												<td>
													<select name="nivel_estudios" class="form-control">
														<option></option>
														<option value="Secundaria" {{ ($pr->nivel_estudios == "Secundaria")? "selected" : "" }}>Secundaria</option>
														<option value="Preparatoria" {{ ($pr->nivel_estudios == "Preparatoria")? "selected" : "" }}>Preparatoria</option>
														<option value="Carrera Técnica" {{ ($pr->nivel_estudios == "Carrera Técnica")? "selected" : "" }}>Carrera Técnica</option>
														<option value="Licenciatura" {{ ($pr->nivel_estudios == "Licenciatura")? "selected" : "" }}>Licenciatura</option>
														<option value="Posgrado" {{ ($pr->nivel_estudios == "Posgrado")? "selected" : "" }}>Posgrado</option>
														<option value="Otro" {{ ($pr->nivel_estudios == "Otro")? "selected" : "" }}>Otro</option>
													</select>
												</td>
											</tr>
											<tr>
												<th>Tipo Residencia:</th>
												<td>
													<select name="tipo_residencia" class="form-control">
														<option></option>
														<option value="Propia" {{ ($pr->tipo_residencia == "Propia")? "selected" : "" }}>Propia</option>
														<option value="Renta" {{ ($pr->tipo_residencia == "Renta")? "selected" : "" }}>Renta</option>
														<option value="Con Familiares" {{ ($pr->tipo_residencia == "Con Familiares")? "selected" : "" }}>Con Familiares</option>
													</select>
												</td>
											</tr>
											<tr><th>Ingr Mensual:</th>			<td><input type="text" name="ingreso_mensual" value="{{ $pr->ingreso_mensual }}" class="form-control"></td></tr>
											<tr>
												<th>Antig Empleo:</th>
												<td>
													<select name="antiguedad_empleo" class="form-control">
														<option></option>
														<option value="0" {{ ($pr->antiguedad_empleo == "0")? "selected" : "" }}>Menos de 1 año</option>
														<option value="1" {{ ($pr->antiguedad_empleo == "1")? "selected" : "" }}>1 año</option>
														<option value="2" {{ ($pr->antiguedad_empleo == "2")? "selected" : "" }}>2 años</option>
														<option value="3" {{ ($pr->antiguedad_empleo == "3")? "selected" : "" }}>3 años</option>
														<option value="4" {{ ($pr->antiguedad_empleo == "4")? "selected" : "" }}>4 años</option>
														<option value="5" {{ ($pr->antiguedad_empleo == "5")? "selected" : "" }}>5 años</option>
														<option value="6" {{ ($pr->antiguedad_empleo == "6")? "selected" : "" }}>6 años</option>
														<option value="7" {{ ($pr->antiguedad_empleo == "7")? "selected" : "" }}>7 años</option>
														<option value="8" {{ ($pr->antiguedad_empleo == "8")? "selected" : "" }}>8 años</option>
														<option value="9" {{ ($pr->antiguedad_empleo == "9")? "selected" : "" }}>9 años</option>
														<option value="10" {{ ($pr->antiguedad_empleo == "10")? "selected" : "" }}>10 años</option>
														<option value="11" {{ ($pr->antiguedad_empleo == "11")? "selected" : "" }}>Más de 10 años</option>
													</select>
												</td>
											</tr>
											<tr>
												<th>Antig Domicilio:</th>
												<td>
													<select name="antiguedad_domicilio" class="form-control">
														<option></option>
														<option value="0" {{ ($pr->antiguedad_domicilio == "0")? "selected" : "" }}>Menos de 1 año</option>
														<option value="1" {{ ($pr->antiguedad_domicilio == "1")? "selected" : "" }}>1 año</option>
														<option value="2" {{ ($pr->antiguedad_domicilio == "2")? "selected" : "" }}>2 años</option>
														<option value="3" {{ ($pr->antiguedad_domicilio == "3")? "selected" : "" }}>3 años</option>
														<option value="4" {{ ($pr->antiguedad_domicilio == "4")? "selected" : "" }}>4 años</option>
														<option value="5" {{ ($pr->antiguedad_domicilio == "5")? "selected" : "" }}>5 años</option>
														<option value="6" {{ ($pr->antiguedad_domicilio == "6")? "selected" : "" }}>6 años</option>
														<option value="7" {{ ($pr->antiguedad_domicilio == "7")? "selected" : "" }}>7 años</option>
														<option value="8" {{ ($pr->antiguedad_domicilio == "8")? "selected" : "" }}>8 años</option>
														<option value="9" {{ ($pr->antiguedad_domicilio == "9")? "selected" : "" }}>9 años</option>
														<option value="10" {{ ($pr->antiguedad_domicilio == "10")? "selected" : "" }}>10 años</option>
														<option value="11" {{ ($pr->antiguedad_domicilio == "11")? "selected" : "" }}>Más de 10 años</option>
													</select>
												</td>
											</tr>
											<tr><th>Gastos Familiares:</th>		<td><input type="text" name="gastos_familiares" value="{{ $pr->gastos_familiares }}" class="form-control"></td></tr>
											<tr>
												<th>Num Depend:</th>
												<td>
													<select name="numero_dependientes" class="form-control">
														<option></option>
														<option value="0" {{ ($pr->numero_dependientes == "0")? "selected" : "" }}>0</option>
														<option value="1" {{ ($pr->numero_dependientes == "1")? "selected" : "" }}>1</option>
														<option value="2" {{ ($pr->numero_dependientes == "2")? "selected" : "" }}>2</option>
														<option value="3" {{ ($pr->numero_dependientes == "3")? "selected" : "" }}>3</option>
														<option value="4" {{ ($pr->numero_dependientes == "4")? "selected" : "" }}>4</option>
														<option value="5" {{ ($pr->numero_dependientes == "5")? "selected" : "" }}>5</option>
														<option value="6" {{ ($pr->numero_dependientes == "6")? "selected" : "" }}>6</option>
														<option value="7" {{ ($pr->numero_dependientes == "7")? "selected" : "" }}>7</option>
														<option value="8" {{ ($pr->numero_dependientes == "8")? "selected" : "" }}>8</option>
														<option value="9" {{ ($pr->numero_dependientes == "9")? "selected" : "" }}>9</option>
														<option value="10" {{ ($pr->numero_dependientes == "10")? "selected" : "" }}>10</option>
														<option value="11" {{ ($pr->numero_dependientes == "11")? "selected" : "" }}>11</option>
														<option value="12" {{ ($pr->numero_dependientes == "12")? "selected" : "" }}>12</option>
														<option value="13" {{ ($pr->numero_dependientes == "13")? "selected" : "" }}>13</option>
														<option value="14" {{ ($pr->numero_dependientes == "14")? "selected" : "" }}>14</option>
														<option value="15" {{ ($pr->numero_dependientes == "15")? "selected" : "" }}>15</option>
													</select>
												</td>
											</tr>
											<tr><th>Tel Empleo:</th>  				<td><input type="text" name="telefono_empleo" value="{{ $pr->telefono_empleo }}" class="form-control"></td></tr>
											<tr><th>BC Score:</th>  					<td><input type="text" name="bc_valor" value="{{ $pr->bc_valor }}" class="form-control"></td></tr>
											<tr><th>ICC Score:</th>  					<td><input type="text" name="icc_valor" value="{{ $pr->icc_valor}}" class="form-control"></td></tr>
											<tr><th>Micro Valor:</th>  				<td><input type="text" name="micro_valor" value="{{ $pr->micro_valor}}" class="form-control"></td></tr>
										</table>
										<input type="submit" class="btn btn-success pull-right" value="Guardar Datos Personales">
										<a href="/panel/tabla-local" class="btn btn-danger pull-right" style="margin-right:20px;">Cancelar</a>
										<br><br>
									</form>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="tab-cuentas-{{$pr->id}}">
						    	<div class="row">
						    		<div class="col-sm-12" style="padding:10px 20px 20px 0px; border-bottom:thin solid #ccc">
						    			<a class="nueva_cuenta_btn btn btn-primary btn-sm pull-right" data-person="{{$pr->id}}" >&plus; Nueva Cuenta</a>
						    			<a class="go_to_end_btn btn btn-success btn-sm pull-right" style="margin-right:20px;" data-person="{{$pr->id}}" ><span class="glyphicon glyphicon-arrow-down"></span> Ir al Final</a>
						    		</div>
						    	</div>
						    	<form id="mock_cuentas_edit_form" method="POST" action="/panel/tabla-local-edit-cuentas">
							    	{!! csrf_field() !!}
							    	<input type="hidden" name="mock_person_id" value="{{ $pr->id }}" class="form-control">
							    	<input type="hidden" id="set_for_delete" name="set_for_delete" value="">
							    	<div id="cuentas_container_{{$pr->id}}"><!-- Used to for javascript (new accounts are apended to this element) -->
							    	<?php
							    		$cuentas = json_decode($pr->cuentas);
							    		$c_counter = 0;
							    		if(is_array($cuentas)){
								    		foreach($cuentas as $cn){
								    			$c_counter++;
								    			echo '<div id="cuenta_num_'.$c_counter.'">';
									    			echo '<h3>Cuenta Num. '.$c_counter.' <a href="#" data-target="'.$c_counter.'" data-person="'.$pr->id.'" class="cuenta_delete_btn btn btn-danger btn-xs pull-right">&times;</a></h3>';
									    			echo '<table class="table table-striped">';
									    				echo '<tr><th width="30%">cuenta_fecha_actualizacion</th>';
															if(isset($cn->cuenta_fecha_actualizacion)){ 			echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_actualizacion" value="'.$cn->cuenta_fecha_actualizacion.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_actualizacion" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_impugnado</th>';
															if(isset($cn->cuenta_impugnado)){									echo '<td><input type="text" name="'.$c_counter.'_cuenta_impugnado" value="'.$cn->cuenta_impugnado.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_impugnado" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_clave_member_code</th>';
															if(isset($cn->cuenta_clave_member_code)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_clave_member_code" value="'.$cn->cuenta_clave_member_code.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_clave_member_code" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_nombre_usuario</th>';
															if(isset($cn->cuenta_nombre_usuario)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_nombre_usuario" value="'.$cn->cuenta_nombre_usuario.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_nombre_usuario" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_num_tel</th>';
															if(isset($cn->cuenta_num_tel)){										echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_tel" value="'.$cn->cuenta_num_tel.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_tel" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_num_cuenta</th>';
															if(isset($cn->cuenta_num_cuenta)){								echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_cuenta" value="'.$cn->cuenta_num_cuenta.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_cuenta" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_responsabilidad</th>';
															if(isset($cn->cuenta_responsabilidad)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_responsabilidad" value="'.$cn->cuenta_responsabilidad.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_responsabilidad" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_tipo</th>';
															if(isset($cn->cuenta_tipo)){											echo '<td><input type="text" name="'.$c_counter.'_cuenta_tipo" value="'.$cn->cuenta_tipo.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_tipo" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_contrato_producto</th>';
															if(isset($cn->cuenta_contrato_producto)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_contrato_producto" value="'.$cn->cuenta_contrato_producto.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_contrato_producto" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_moneda</th>';
															if(isset($cn->cuenta_moneda)){										echo '<td><input type="text" name="'.$c_counter.'_cuenta_moneda" value="'.$cn->cuenta_moneda.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_moneda" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_importe_evaluo</th>';
															if(isset($cn->cuenta_importe_evaluo)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_importe_evaluo" value="'.$cn->cuenta_importe_evaluo.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_importe_evaluo" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_num_pagos</th>';
															if(isset($cn->cuenta_num_pagos)){									echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_pagos" value="'.$cn->cuenta_num_pagos.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_pagos" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_freguencia_pagos</th>';
															if(isset($cn->cuenta_freguencia_pagos)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_freguencia_pagos" value="'.$cn->cuenta_freguencia_pagos.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_freguencia_pagos" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_monto_pagar</th>';
															if(isset($cn->cuenta_monto_pagar)){								echo '<td><input type="text" name="'.$c_counter.'_cuenta_monto_pagar" value="'.$cn->cuenta_monto_pagar.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_monto_pagar" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_apertura</th>';
															if(isset($cn->cuenta_fecha_apertura)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_apertura" value="'.$cn->cuenta_fecha_apertura.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_apertura" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_ult_pago</th>';
															if(isset($cn->cuenta_fecha_ult_pago)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_ult_pago" value="'.$cn->cuenta_fecha_ult_pago.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_ult_pago" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_ult_compra</th>';
															if(isset($cn->cuenta_fecha_ult_compra)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_ult_compra" value="'.$cn->cuenta_fecha_ult_compra.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_ult_compra" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_cierre</th>';
															if(isset($cn->cuenta_fecha_cierre)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_cierre" value="'.$cn->cuenta_fecha_cierre.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_cierre" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_reporte</th>';
															if(isset($cn->cuenta_fecha_reporte)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_reporte" value="'.$cn->cuenta_fecha_reporte.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_reporte" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_modo_reporte</th>';
															if(isset($cn->cuenta_modo_reporte)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_modo_reporte" value="'.$cn->cuenta_modo_reporte.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_modo_reporte" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_ult_fecha_cero</th>';
															if(isset($cn->cuenta_ult_fecha_cero)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_ult_fecha_cero" value="'.$cn->cuenta_ult_fecha_cero.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_ult_fecha_cero" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_garantia</th>';
															if(isset($cn->cuenta_garantia)){									echo '<td><input type="text" name="'.$c_counter.'_cuenta_garantia" value="'.$cn->cuenta_garantia.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_garantia" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_cred_max_aut</th>';
															if(isset($cn->cuenta_cred_max_aut)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_cred_max_aut" value="'.$cn->cuenta_cred_max_aut.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_cred_max_aut" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_saldo_actual</th>';
															if(isset($cn->cuenta_saldo_actual)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_actual" value="'.$cn->cuenta_saldo_actual.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_actual" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_limite_credito</th>';
															if(isset($cn->cuenta_limite_credito)){						echo '<td><input type="text" name="'.$c_counter.'_cuenta_limite_credito" value="'.$cn->cuenta_limite_credito.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_limite_credito" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_saldo_vencido</th>';
															if(isset($cn->cuenta_saldo_vencido)){							echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_vencido" value="'.$cn->cuenta_saldo_vencido.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_vencido" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_num_pagos_vencidos</th>';
															if(isset($cn->cuenta_num_pagos_vencidos)){				echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_pagos_vencidos" value="'.$cn->cuenta_num_pagos_vencidos.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_num_pagos_vencidos" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_mop</th>';
															if(isset($cn->cuenta_mop)){												echo '<td><input type="text" name="'.$c_counter.'_cuenta_mop" value="'.$cn->cuenta_mop.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_mop" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_hist_pagos</th>';
															if(isset($cn->cuenta_hist_pagos)){								echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos" value="'.$cn->cuenta_hist_pagos.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_hist_pagos_fecha_reciente</th>';
															if(isset($cn->cuenta_hist_pagos_fecha_reciente)){	echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos_fecha_reciente" value="'.$cn->cuenta_hist_pagos_fecha_reciente.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos_fecha_reciente" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_hist_pagos_fecha_antigua</th>';
															if(isset($cn->cuenta_hist_pagos_fecha_antigua)){	echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos_fecha_antigua" value="'.$cn->cuenta_hist_pagos_fecha_antigua.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_hist_pagos_fecha_antigua" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_clave_observacion</th>';
															if(isset($cn->cuenta_clave_observacion)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_clave_observacion" value="'.$cn->cuenta_clave_observacion.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_clave_observacion" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_total_pagos</th>';
															if(isset($cn->cuenta_total_pagos)){								echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos" value="'.$cn->cuenta_total_pagos.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_total_pagos_mop2</th>';
															if(isset($cn->cuenta_total_pagos_mop2)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop2" value="'.$cn->cuenta_total_pagos_mop2.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop2" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_total_pagos_mop3</th>';
															if(isset($cn->cuenta_total_pagos_mop3)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop3" value="'.$cn->cuenta_total_pagos_mop3.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop3" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_total_pagos_mop4</th>';
															if(isset($cn->cuenta_total_pagos_mop4)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop4" value="'.$cn->cuenta_total_pagos_mop4.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop4" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_total_pagos_mop5_plus</th>';
															if(isset($cn->cuenta_total_pagos_mop5_plus)){			echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop5_plus" value="'.$cn->cuenta_total_pagos_mop5_plus.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_total_pagos_mop5_plus" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_saldo_morosidad_mas_alta</th>';
															if(isset($cn->cuenta_saldo_morosidad_mas_alta)){	echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_morosidad_mas_alta" value="'.$cn->cuenta_saldo_morosidad_mas_alta.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_saldo_morosidad_mas_alta" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_morosidad_mas_alta</th>';
															if(isset($cn->cuenta_fecha_morosidad_mas_alta)){	echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_morosidad_mas_alta" value="'.$cn->cuenta_fecha_morosidad_mas_alta.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_morosidad_mas_alta" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_clasif_puntualidad_de_pago</th>';
															if(isset($cn->cuenta_clasif_puntualidad_de_pago)){echo '<td><input type="text" name="'.$c_counter.'_cuenta_clasif_puntualidad_de_pago" value="'.$cn->cuenta_clasif_puntualidad_de_pago.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_clasif_puntualidad_de_pago" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_fecha_inicio_reestructura</th>';
															if(isset($cn->cuenta_fecha_inicio_reestructura)){	echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_inicio_reestructura" value="'.$cn->cuenta_fecha_inicio_reestructura.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_fecha_inicio_reestructura" value=""></td></tr>';}
															echo '<tr><th width="30%">cuenta_monto_ultimo_pago</th>';
															if(isset($cn->cuenta_monto_ultimo_pago)){					echo '<td><input type="text" name="'.$c_counter.'_cuenta_monto_ultimo_pago" value="'.$cn->cuenta_monto_ultimo_pago.'"><td></tr>';}else{echo '<td><input type="text" name="'.$c_counter.'_cuenta_monto_ultimo_pago" value=""></td></tr>';}
									    			echo '</table><hr><br>';
									    		echo '</div>';//this div will be deleted and cuentas_count adjusted when cuenta_delete_btn is clicked
								    		}
								    	}
							    	?>
							    	</div>
							    	<a id="cuentas_end_shortcut" name="cuentas_end_shortcut"></a>
						    		<input type="hidden" id="cuentas_count_{{$pr->id}}" name="cuentas_count" value="{{ $c_counter }}">
						    		<input type="submit" class="btn btn-success pull-right" value="Guardar Cuentas">
						    		<a href="/panel/tabla-local" class="btn btn-danger pull-right" style="margin-right:20px;">Cancelar</a>
						    		<br><br>
						    	</form>
						    </div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	@else
		<center>
			<br>
			<h4>No tienes privilegios para realizar esta acción</h4>
		</center>
	@endif
</div>
@endsection
