@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
<link rel="stylesheet" href="/back/css/multi-select.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<style>
.yellow {
    color: #FFF500;
}
.green {
    color: #28A745;
}
.glyphicon {
    font-size: 18px;
}
.box.box-primary {
	border-top-color: #f79020;
}
a:visited {
	color: #3c8dbc;
}
.btn-primary:visited {
	color: #fff;
}
label {
	text-transform: none;
}
.help-block {
    color: #dd4b39;
}
</style>

<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border" style="text-align:center;">
	            	<h3 class="box-title" style="font-weight: bold;">Listado de Perfiles</h3>
	            </div>
                @if(Auth::user()->can('administracion-usuarios'))
                <div class="box-body">
					<div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#perfilModal">
                                Nuevo
                            </button>
                        </div>
                        <hr>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 5%">#</th>
										<th>Nombre</th>
										<th>Área</th>
										<th>Puesto</th>
										<th style="width: 35%">Permisos</th>
										<th style="width: 5%">Opciones</th>
									</tr>
									@foreach ($roles as $rol)
                                        @php
                                            $permisos = [];
                                        @endphp
                                        @foreach ($rol->perms as $permiso)
                                            @php
                                                $permisos[] = $permiso->display_name;
                                            @endphp
                                        @endforeach
										<tr>
											<td> {{ $loop->iteration }} </td>
											<td> {{ $rol->display_name }} </td>
                                            <td> {{ $rol->area }} </td>
                                            <td> {{ $rol->description }} </td>
                                            <td> {{ implode(', ', $permisos) }} </td>
                                            <td style="text-align:center">
                                                <a href="/panel/usuarios/perfiles/{{ $rol->id }}"><li class="glyphicon glyphicon-edit"></li></a>
                                            </td>
                                        </tr>
									@endforeach


								</tbody>
							</table>

                            <center>{{ $roles->links() }}</center>
	    				</div>
	  				</div>
	    		</div>
                @else
					<center>
						<br>
						<h4>No tienes privilegios para realizar esta acción</h4>
                        <br>
					</center>
                @endif
			</div>

		</div>
	</div>
</div>

<!-- Modal Nuevo Perfil -->
<div class="modal fade" id="perfilModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Nuevo Perfil</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="formPerfil">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" class="form-control" id="display_name" name="display_name" maxlength="50">
                                    <span id="error_display_name" class="help-block"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Area</label>
                                    <select id="area" class="form-control" name="area">
                                        <option selected disabled value="">Selecciona</option>
                                        <option value="Cobranza">Cobranza</option>
                                        <option value="Credito y Riesgos">Credito y Riesgos</option>
                                        <option value="CRM">CRM</option>
                                        <option value="Dirección">Dirección</option>
                                        <option value="Operaciones">Operaciones</option>
                                        <option value="Sistemas">Sistemas</option>
                                        <option value="Ventas">Ventas</option>
                                    </select>
                                    <span id="error_area" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Descripción</label>
                                    <textarea id="description" class="form-control" name="description" maxlength="100"></textarea>
                                    <span id="error_description" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row" style="text-align:right;">
                                <button type="button" class="btn btn-primary" style="margin-right: 15px;" onclick="guardarPerfil()">
                                    Agregar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/js/backoffice/multiselect.min.js"></script>
<script type="text/javascript" src="/back/js/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/backoffice/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="/js/perfiles.js?v=<?php echo microtime(); ?>"></script>
@endsection
