@extends('layouts.app_sitio')

@section('content')

    <section id="carouselCreditoUsa" class="carousel slide carousel-fade CarouselCreditoUsa" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselCreditoUsa" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselCreditoUsa" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('credito-real') }}/img/colaborador.jpg" class="d-block w-100" alt=".Coladoradores Real Usa">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Un financiamiento accesible especialmente <span>para ti.</span></h3>
                    <h5>Consigue el equipo que necesitas crecer</h5>
                    <p>Estabiliza tu flujo de caja, acepta más trabajo y aumenta los beneficios, con el arrendamiento de equipos de Crédito <strong>Real USA Business Capital.</strong></p>
                    <a class="btn Back1"onclick="focusSimulador()">¡Aplica Ahora!</a>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('credito-real') }}/img/maquinaria.jpg" class="d-block w-100" alt="Arrendamiento Real Usa">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Arrendamiento de máquinas accesible especialmente <span>para ti.</span></h3>
                    <h5>Consigue el equipo que necesitas crecer</h5>
                    <p>Estabiliza tu flujo de caja, acepta más trabajo y aumenta los beneficios, con el arrendamiento de equipos de Crédito <strong>Real USA Business Capital.</strong></p>
                    <a class="btn Back1" id="aplica_ahora" onclick="focusSimulador()">¡Aplica Ahora!</a>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselCreditoUsa" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselCreditoUsa" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
            </button>
    </section>

    <section class="Simulacion" id="simulador">
        <form id="formSimulador">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="Title">
                            <h1>Simula tus pagos</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="row Fil">
                            <div class="EstimadoText">
                                <div class="Img">
                                    <img src="{{ asset('credito-real') }}/img/simulador1-costo.svg" alt="">
                                </div>
                                <span>Costo estimado del equipo que necesitas financiar</span>
                            </div>
                            <div class="EstimadoContent">
                                <div class="input-group">
                                    <label for=""></label>
                                    <input name="monto_prestamo" id="monto_prestamo" type="text" placeholder="Hasta ${{ $configuracion['configuracion']['monto_maximo'] }} USD" data-thousands="," data-decimal="." data-prefix="$ " onchange="changePagoMensual()">
                                    <input type="hidden" id="monto_minimo" name="monto_minimo" value="{{ $configuracion['configuracion']['monto_minimo'] }}"></input>
                                    <input type="hidden" id="monto_maximo" name="monto_maximo" value="{{ $configuracion['configuracion']['monto_maximo'] }}"></input>
                                </div>
                            </div>
                        </div>
                        <div class="row Fil">
                            <div class="EstimadoText">
                                <div class="Img">
                                    <img src="{{ asset('credito-real') }}/img/simulador2-equipo.svg" alt="">
                                </div>
                                <span>¿Qué equipo necesitas?</span>
                            </div>
                            <div class="EstimadoContent">
                                <div class="input-group">
                                    <label for=""></label>
                                    <select name="equipo_leasing" id="equipo_leasing" class="form-select" required="required" aria-label="Default select example">
                                        @foreach ($configuracion['equipos_leasing'] as $id => $equipo)
                                            <option value="{{ $id }}">{{ $equipo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row Fil">
                            <div class="EstimadoText">
                                <div class="Img">
                                    <img src="{{ asset('credito-real') }}/img/Calendario.png" alt="">
                                </div>
                                <span>Plazo</span>
                            </div>
                            <div class="EstimadoContent">
                                <div class="input-group">
                                    <label for=""></label>
                                    <select name="plazo" id="plazo" class="form-select" required="required" aria-label="Default select example" onchange="changePagoMensual()">
                                        <option selected="selected" disabled="" value="">Seleccione plazo:</option>
                                        @foreach ($configuracion['plazos'] as $id => $plazo)
                                            <option value="{{ $id }}">{{ $plazo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row Fil">
                            <div class="EstimadoText">
                                <div class="Img">
                                    <img src="{{ asset('credito-real') }}/img/simulador3-pago.svg" alt="">
                                </div>
                                <span>Pago mensual estimado<br> (mas impuestos)</span>
                            </div>
                            <div class="EstimadoContent">
                                <div class="input-group">
                                    <label for=""></label>
                                    <input id="pago_estimado" name="pago_estimado" type="text" placeholder="$350 por 36 meses" data-thousands="," data-decimal="." data-prefix="$ " disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="Opinion">
                            <div class="mb-3">
                                <label for="Areatext" class="form-label">Cuéntanos sobre tu proyecto</label>
                                <textarea class="form-control" id="finalidad_custom" name="finalidad_custom" rows="4" placeholder="Escribe sobre el equipo que necesitas, como lo ocuparás, etc."></textarea>
                                </div>
                                <small id="finalidad-help" class="help"></small>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                    <label id="validacionesSimulador"></label>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8"></div>
                    <div class="col-sm-12 col.md-4 BtnsGo">
                        <a class="btn BtnCredito Back4" onclick="validarSimulador()">Siguiente</a><br><br><br><br>
                    </div>
                </div>
            </div>
            <input id="nueva_solicitud" name="nueva_solicitud" type="hidden" value="false">
        </form>
    </section>

    <section class="AppWeb" id="AppWeb">
        @include('parts.registro')
        @include('parts.verificar-codigo')
        @include('parts.empresa')
        @include('parts.negocio')
        @include('parts.socio')
        @include('parts.info-socios')
        @include('parts.datos_credito')
        @include('parts.puntaje')

    </section>

    <section class="Aplicar" id="aplicar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h2>Para aplicar se requiere:</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="IconThumbs">
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/aplicar1-empresa.svg" alt="">
                            </div>
                            <p>Ser una empresa o contratista legalmente registrado</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/aplicar2-construccion.svg" alt="">
                            </div>
                            <p>Trabajar en construcción o industria relacionada</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/aplicar3-encuesta.svg" alt="">
                            </div>
                            <p>Estados de cuenta bancaria de los últimas 6 meses</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/aplicar4-identificacion.svg" alt="">
                            </div>
                            <p>Contar con identificación oficial vigente</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="Proceso" id="proceso">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h1>Proceso</h1>
                        <h4>¡Empieza hoy!</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="IconThumbs">
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/proceso1-equipo.svg" alt="">
                            </div>
                            <p>Selecciona el equipo usado que necesita para tu negocio del vendedor que quieras</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/proceso2-solicitud.svg" alt="">
                            </div>
                            <p>Llena una solicitud de crédito</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/proceso3-aprobación.svg" alt="">
                            </div>
                            <p>Si te aprobamos compramos el equipo que seleccionaste</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/proceso4-mensual.svg" alt="">
                            </div>
                            <p>Tu usas el equipo y nos pagas mensualmente hasta que vence el contrato</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/proceso5-comprar.svg" alt="">
                            </div>
                            <p>Al final del contrato puedes comprarnos el equipo o regresárnoslos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="Leasing" id="leasing">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Aplicación de Leasing</h2>
                    <h4>Es muy fácil, realiza tu solicitud 100% <span>En Linea</span></h4>
                    <p><a id="aplica_ahora" onclick="focusSimulador()" class="btn Back1 btn-lg">¡Aplica ahora!</a></p>
                </div>
            </div>
        </div>
    </section>

    <section class="Subfooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Img">
                        <img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--<footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Img">
                        <p>(210) 777-1373</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>--}}

@endsection
