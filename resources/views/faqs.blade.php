@extends('layouts.app_sitio')

@section('content')
    <section id="carouselCreditoUsa" class="carousel slide carousel-fade CarouselCreditoUsa" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('credito-real') }}/img/colaborador.jpg" class="d-block w-100" alt=".Coladoradores Real Usa">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Un financiamiento accesible especialmente <span>para ti.</span></h3>
                    <h5>Consigue el equipo que necesitas crecer</h5>
                    <p>Estabiliza tu flujo de caja, acepta más trabajo y aumenta los beneficios, con el arrendamiento de equipos de Crédito <strong>Real USA Business Capital.</strong></p>
                    <a href="/" class="btn Back1" id="aplica_ahora">¡Aplica ahora!</a>
                </div>
            </div>
        </div>
    </section>

    <section class="AppWeb" id="AppWeb">
        @include('parts.registro')
        @include('parts.verificar-codigo')
        @include('parts.empresa')
        @include('parts.negocio')
        @include('parts.socio')
        @include('parts.info-socios')
        @include('parts.datos_credito')
        @include('parts.puntaje')

    </section>
    <section class="QueesArrendar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h2>¿Qué es el arrendamiento de equipo?</h2>
                        <h6>El arrendamiento de equipo es el proceso en el que el propietario de una pequeña empresa trabaja con un prestamista para financiar una o varias piezas de equipo empresarial nuevo o usado. </h6>
                        <h6>A diferencia de un préstamo o financiamiento bancario tradicional, el arrendamiento tiene aprobaciones flexibles para los pagos mensuales, y la opción de propiedad o devolución al final del plazo de arrendamiento.  </h6>

                        <h6>El financiamiento y el arrendamiento de equipo son algunas de las herramientas más valiosas que las pequeñas empresas pueden aprovechar para mejorar el flujo de caja, aumentar los beneficios y crecer.</h6>  
                        
                        <h6>Con todas las opciones de financiamiento de equipo y de préstamos para equipos que hay en el mercado hoy en día, le ayudaremos a decidir qué es lo más adecuado para usted y cómo empezar.  </h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="Subtitle">
                        <h2>Preguntas Frecuentes</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="accordion" id="Faqs">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="DineroEfectivo">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs1">
                                ¿Se pueden arrendar equipos usados o sólo equipos nuevos?
                            </button>
                            </h2>
                            <div id="Faqs1" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>El arrendamiento de equipos de Credíto Real USA Business Capital puede utilizarse tanto para equipos nuevos como usados.  </p>
                            </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="3">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs2">
                                ¿Cómo funciona el arrendamiento de equipos para empresas?
                            </button>
                            </h2>
                            <div id="Faqs2" class="accordion-collapse collapse" data-bs-parent="#Faqs2">
                            <div class="accordion-body">
                                <p>Los programas de arrendamiento de equipos suelen tener dos opciones: </p>
                                <p> La primera es hacer un acuerdo de "lease to own". El equipo será comprado por la institución financiera, y luego el arrendatario pagará cada mes durante un tiempo establecido hasta que el acuerdo de arrendamiento llegue a su fin. Los arrendamientos a menudo no tienen un pago inicial, pero se toma un depósito de seguridad al principio del arrendamiento por aproximadamente 10% del valor que se aplica al final del arrendamiento, lo que resulta en que el arrendatario es dueño del equipo.  </p>
                                
                                <p>El segundo tipo de leasing puede compararse con el alquiler de un apartamento. Pagas por utilizarlo cada mes durante el plazo del contrato de arrendamiento, y al final del mismo se te puede reembolsar el importe pagado. </p>
                                
                                <p>El arrendamiento de equipos suele ser de 2 a 5 años y, al final del mismo, se le da la opción de comprar el artículo por un valor de mercado determinado (normalmente 10%) o puede devolverlo. 
                                </p>
                            </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="Duracion">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faq3">
                                ¿Cómo cobran los prestamistas por el arrendamiento de equipos?
                                </button>
                            </h2>
                            <div id="Faq3" class="accordion-collapse collapse" data-bs-parent="#Faqs3">
                                <div class="accordion-body">
                                <p>El arrendamiento de equipo se diferencia de los préstamos tradicionales porque no se basa en un tipo de interés, en el que los clientes pueden pagar el principal por separado de los intereses. En el caso del arrendamiento, los pagos son estándar en todo momento e incluyen un factor de tasa de arrendamiento que debe pagarse en su totalidad hasta el final del plazo.  </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="Duracion">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs4" >
                                ¿Debo solicitar el arrendamiento o la financiación de un equipo antes de acudir a un concesionario?
                                </button>
                            </h2>
                            <div id="Faqs4" class="accordion-collapse collapse" data-bs-parent="#Faqs4">
                                <div class="accordion-body">
                                <p>Uno de los beneficios de obtener la aprobación para el arrendamiento y la financiación antes de visitar un concesionario, es que usted sabe lo que está pre-aprobado para comprar. En Crédito Real USA Business Capital, los clientes preaprobados disfrutan de las ventajas de buscar las mejores ofertas sabiendo que tienen el dinero en mano para comprar cuando surja la mejor oportunidad.  </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="Duracion">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs5">
                                ¿Puedo pagar el alquiler con una tarjeta de crédito?
                                </button>
                            </h2>
                            <div id="Faqs5" class="accordion-collapse collapse" data-bs-parent="#Faqs5">
                                <div class="accordion-body">
                                <p>No. Los pagos del arrendamiento sólo se pueden pagar desde una cuenta bancaria.</p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="Duracion">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs6">
                                ¿Cuáles son las condiciones típicas de un contrato de arrendamiento?
                                </button>
                            </h2>
                            <div id="Faqs6" class="accordion-collapse collapse" data-bs-parent="#Faqs6">
                                <div class="accordion-body">
                                <p>Estas son algunas de las condiciones típicas de un contrato de arrendamiento. La mayoría de los contratos de arrendamiento requieren un depósito de seguridad de 10% dependiendo de la puntuación de crédito. La mayoría de los contratos de arrendamiento se redactan por 36 meses y al final del contrato, pueden convertir el depósito en el pago final para poseer el equipo. Esto se denomina arrendamiento por el valor justo de mercado y el beneficio es que al final del arrendamiento, no se requiere ningún pago adicional para poseer el equipo. La mayoría de los arrendamientos requieren una garantía personal.  </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="Duracion">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#Faqs7" >
                                ¿Cuál es la diferencia entre un préstamo SBA/tradicional y el arrendamiento de equipos?
                                </button>
                            </h2>
                            <div id="Faqs7" class="accordion-collapse collapse" data-bs-parent="#Faqs7">
                                <div class="accordion-body">
                                <p>Los préstamos para pequeñas empresas y equipos, como los de un prestamista tradicional o la SBA, se basan en gran medida en el crédito de la persona y la empresa para su aprobación. Por el contrario, el arrendamiento de equipos utiliza el propio equipo como garantía, lo que permite unos índices de aprobación mucho más elevados para quienes tienen un bajo puntaje de crédito y un acceso limitado a el financiamiento bancario. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="Subfooter"> <br>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Img">
                        <img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection