@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<!-- <link rel="stylesheet" href="/credito-real/css/webapp/bootstrap-grid.min.css"> -->
<section class="AppWeb">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12" id="navigator">
                <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" style="justify-content: center; display: flex;">
                    <li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
                        <p>Identificación y Selfie</p>
                    </a></li>
                    <li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
                        <p>Comprobante de Ingresos</p>
                    </a></li>
                </ul>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col col-sm col-md col-lg col-xl">
                <div class="card">
                    <hr>
                    <div class="card-header">
                        <div class="row align-items-center">
                        <div class="col" style="text-align: center">
                            <h5 class="card-header-title">
                            ID Reverso <i class="fas fa-info-circle" style="font-size: 15px; color: #44b4f6" onclick="ayuda('tip1')"></i>
                            </h5>
                            @if($desktop === true)
                            <span class="span-card">
                                <ul>
                                    <li>Si la foto no se toma de manera automática, puedes tomarla manualmente dando click en el botón con el icono <i class="fas fa-camera"></i></li>
                                    <li>Asegurate de que la imagen sea clara.</li>
                                </ul>
                            </span>
                            @else
                            <span class="span-card">
                                <ul>
                                    <li>Coloca tu credencial de elector sobre un fondo negro y tomale una foto a la parte de enfrente.</li>
                                    <li>Asegurate de que la imagen sea clara.</li>
                                    <li>Si la foto no se toma automaticamente puedes tomarla con el botón con el icono <i class="fas fa-camera" style="color: #44b4f6"></i></li>
                                </ul>
                            </span>
                            @endif
                        </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body" id="tomar-foto">
                        <div id="cargando" style="display:none">
                        <center>
                            <img src="/credito-real/img/ajax-loader.gif" class="loading-gif">
                            <p> Cargando camara... </p>
                        </center>
                        </div>
                        @if($desktop === true)
                        <div class="videoDiv desktop" id="videoDiv">
                            <video id="videoInput" autoplay="true" class="desktop"></video>
                            <div class="controls">
                                <button class="btn btn-danger play" title="Tomar foto" onclick="IcarSDK.documentCapture.manualTrigger();"><i class="fas fa-camera fa-3x"></i></button>
                            </div>
                        </div>
                        @else
                        <div class="videoDiv mobile" id="videoDiv">
                            <video id="videoInput" autoplay="true" class="mobile"></video>
                            <div class="controls">
                                <button class="btn btn-danger play" title="Tomar foto" onclick="IcarSDK.documentCapture.manualTrigger();"><i class="fas fa-camera fa-3x"></i></button>
                            </div>
                        </div>
                        @endif
                        <div>
                            <input id="ChangeCamera" type="button" value="Change Camera"  style="visibility: hidden; padding: 10px 20px" onclick="askForChangeCameraFunction();"/>
                        </div>
                    </div>
                    <div class="card-body" id="resultado">
                        <canvas id="resultImage"></canvas>
                        <hr>
                        <div class="col-sm-12">
                            <a id="repetirFoto" class="btn btn-secondary btn-lg" onclick="repetirPasoIne()">Repetir Foto</a>
                            <a id="siguientePaso1" class="btn BtnCredito Back4 btn-lg" onclick="saveImage('identificacion_oficial', 'back')">Siguiente</a><br><br><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<div class="SidenavOverlay"></div>
@endsection
<script>
	window.onload = function () {
		$('#cargando').show();
		iniciarProceso();
	}
</script>