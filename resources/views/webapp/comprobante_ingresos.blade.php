@extends('layouts.webapp')
@section('content')
<style>
	.upper {
		text-transform: uppercase;
	}
</style>
<!-- <link rel="stylesheet" href="/credito-real/css/webapp/bootstrap-grid.min.css"> -->
    <section class="AppWeb">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12" id="navigator">
                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" style="justify-content: center; display: flex;">
                        <li role="presentation" class="complete"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><i></i>
                            <p>Identificación y Selfie</p>
                        </a></li>
                        <li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
                            <p>Comprobante de Ingresos</p>
                        </a></li>
                    </ul>
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="TitleComprob">
                        <h2>Comprobante de Ingresos</h2>
                        <h3>(Estados de Cuenta Bancarios)</h3>
                        <h6>En caso de no contar con ellos, envíanos un Whatsapp para ver alternativas</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="Comprobante">
                        <div class="Img">
                            <img src="{{ asset('credito-real') }}/img/subir-doctos.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="IconNumbers">
                        <div class="IconNumber">
                            <div class="IconNum">
                                <p><span>1</span></p>
                            </div>
                            <div class="IconText">
                                <p>Se requieren estados de cuenta bancarios de la empresa los últimos 6 meses</p>
                            </div>
                        </div>
                        <div class="IconNumber">
                            <div class="IconNum">
                                <p><span>2</span></p>
                            </div>
                            <div class="IconText">
                                <p>En foto, escaneo o formato PDF</p>
                            </div>
                        </div>
                        <div class="IconNumber">
                            <div class="IconNum">
                                <p><span>3</span></p>
                            </div>
                            <div class="IconText">
                                <p>Procura que sean lo más legibles posibles</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 offset-sm-3">                    
                    <form id="formComprobanteIngresos" class="dropzone">
                    <div class="DropDrag" id="myDropzone" style="overflow: hidden;">
                        <div class="col-lg-12">
                            <label id="documentosFaltantes" style="color: #16243B;"></label>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 BtnsGo">
                    <button id="subirArchivos" class="btn BtnCredito Back4 btn-lg" style="display:none">Subir Archivos</button><br><br><br><br>
                </div> 
            </div>
        </div>
    </section>

<div class="SidenavOverlay"></div>

@endsection
<script>
	window.onload = function () {
		dropZone('subir');
	}
</script>