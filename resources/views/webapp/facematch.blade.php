@extends('layouts.webapp')
@section('content')
    <section class="AppWeb">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12" id="navigator">
                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist" style="justify-content: center; display: flex;">
                        <li role="presentation" class="active"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab" ><i></i>
                            <p>Identificación y Selfie</p>
                        </a></li>
                        <li role="presentation"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><i></i>
                            <p>Comprobante de Ingresos</p>
                        </a></li>
                    </ul>
                </div>
            </div><br>
            <div class="row">
                <div class="col-12 col-lg-6 col-xl">
                    <hr>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col" style="text-align: center">
                                    <h5 class="card-header-title">
                                        TÓMATE UNA SELFIE <i class="fas fa-info-circle" style="font-size: 15px; color: #44b4f6" onclick="ayuda('tip2')"></i>
                                    </h5>
                                    <span class="span-card">
                                        <ul>
                                            <li>Sonríe y parpadea para tomarte la foto. Con el rostro descubierto, sin lentes ni gorra.</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body" id="tomar-foto">
                            <div id="cargando" style="display:none">
                                <center>
                                <img src="/credito-real/img/ajax-loader.gif" class="loading-gif">
                                <p> Cargando camara... </p>
                                </center>
                            </div>
                            @if($desktop === true)
                                <div class="videoDiv desktop" id="videoDiv">
                                    <video id="videoInput" autoplay="true" class="desktop"></video>
                                </div>
                                @else
                                <div class="videoDiv mobile" id="videoDiv">
                                    <video id="videoInput" autoplay="true" class="mobile"></video>
                                </div>
                                @endif
                            <div>
                                <input id="ChangeCamera" type="button" value="Change Camera"  style="visibility: hidden; padding: 10px 20px" onclick="askForChangeCameraFunction();"  />
                            </div>
                        </div>
                        <div class="card-body" id="resultado">
                            <canvas id="resultImage"></canvas>
                            <hr>
                            <div class="col-sm-12">
                                <div class="col-auto">
                                <a id="repetirFoto" class="btn btn-secondary btn-lg" onclick="repetirPasoSelfie()">Repetir Foto</a>
                                <a id="siguientePaso1" class="btn BtnCredito Back4 btn-lg" onclick="saveImage('identificacion_oficial', 'photo')">Siguiente</a><br><br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </section>
</main>

<div class="SidenavOverlay"></div>
@endsection
<script>
	window.onload = function () {
		iniciarProcesoSelfie();
	}
</script>
