<div class="container" id="datos_credito" style="display:none">
    <form id="formDatosCredito">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Cuéntanos sobre tu negocio</h2>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Depósitos mensuales promedio</label>
                    <input type="text" class="form-control required" id="deposito_mensual_promedio" name="deposito_mensual_promedio">
                    <small id="deposito_mensual_promedio-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Saldo final promedio</label>
                    <input type="text" class="form-control required" id="saldo_final" name="saldo_final">
                    <small id="saldo_final-help" class="help"></small>
                </div>
                
            </div>
            </div> <br>
            <div class="row"> 
                <div class="col-sm-12"><br>
                    <div class="form-check form-check-inline" style="text-align: justify;">
                        <h6>Términos y condiciones</h6>
                        <small id="label_autorizo-help" class="help"></small>
                        <input class="form-check-input required" type="checkbox" id="acepto_consulta" value="acepto" name="acepto_consulta">
                        <label class="form-check-label">Yo certifico que toda la información y documentos previos es correcta y verídica a mi consentimiento. El aplicante(s), autoriza a CR-FED Leasing, LLC, sus oficiales, agentes, representantes o asignados a  CREAL a obtener información crediticia y comercial, a realizar una investigación de antecedentes penales y obtener cualquier otra información crediticia y comercial, a realizar una investigación de antecedentes penales y obtener cualquier otra información adicional que pueda ser utilizada y confiada para ofrecer a la empresa del aplicante asistencia financiera. Toda la información presentada en esta aplicación es estrictamente confidencial. Al dar click en el botón FINALIZAR y marcando la casilla "Acepto los términos y condiciones". Les doy consentimiento a realizar negocios electrónicamente con CREAL en conexión con mi solicitud en línea y doy consentimiento para la recepción de esta divulgación electrónica. De haber co-solicitantes en esta aplicación, los co-solicitantes también están de acuerdo con esta autorización.</label> <br><br>
                        <label class="form-check-label">I certify that all of the above statemnets are true and accurate to the best of my/our knowleadge. The applicant(s) authorizes CR-FED Leasing, LLC, its Officers, Agents, Representatives or Assing (Creal) to obtain commercial and consumer credit information, to conducto a criminal investigation and obtain any other additional information that maybe used and relied upon to offer me or my company financial assistance. All information contained in this aplicaction will be held strictly condifential. By clicking on the FINALIZE button and checking the "Acepto los términos y condiciones". box, I affirmatively consent to conducto business electronically with CREAL in connection with my online request and consent to the receipt of this electronic disclousre. If there are co-applicants on this loan, the co-applicants also agree to this authorization.</label>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                <label id="validacionesCuentasCredito"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a class="btn BtnCredito Back5" onclick="getForm('info_socios', 'datos_credito')">Anterior</a>
                <a class="btn BtnCredito Back4"onclick="datos_credito()">Siguiente</a>
                <br><br><br><br>
            </div> 
        </div>
    </form>
</div>