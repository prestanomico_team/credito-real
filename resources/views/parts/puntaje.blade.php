<div class="container" id="puntaje" style="display:none">
<form id="formDatosPuntaje">
    <div class="row">
        <div class="col-sm-12">
            <div class="Title">
                <h2>Consulta de Puntaje de Crédito</h2>
                <h6>Vamos a verificar tu identidad</h6>
                <h6>Para continuar contesta alguna de las preguntas relacionadas con tu persona.</h6>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        <div class="PuntajeCredito">
            <div class="FormInputOption">
                <label class="form-label">1. ¿En que año se estableció tu préstamo o arrendamiento de auto más reciente?</label> <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio1" value="1">
                    <label class="form-check-label">1997</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio2" value="2">
                    <label class="form-check-label">1998</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio3" value="3">
                    <label class="form-check-label">2000</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio4" value="4">
                    <label class="form-check-label">2001</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio1" id="inlineRadio5" value="5">
                    <label class="form-check-label">Ninguna de las anteriores</label>
                    </div>
            </div>
            <div class="FormInputOption"> <br>
                <label class="form-label">2. De la siguiente lista, selecciona tus empleadores actuales o pasados</label> <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio2" id="inlineRadio1" value="1">
                    <label class="form-check-label">Lec</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio2" id="inlineRadio2" value="2">
                    <label class="form-check-label">Kimberly Horn</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio2" id="inlineRadio3" value="3">
                    <label class="form-check-label">Quicktrip</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio2" id="inlineRadio4" value="4">
                    <label class="form-check-label">Vanguard</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio2" id="inlineRadio5" value="5">
                    <label class="form-check-label">Ninguna de las anteriores</label>
                    </div>
            </div>
            <div class="FormInputOption"> <br>
                <label class="form-label">3. ¿Cuál es tu altura, aproximadamente</label> <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio3" id="inlineRadio1" value="1">
                    <label class="form-check-label">6'1</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio3" id="inlineRadio2" value="2">
                    <label class="form-check-label">6'12"</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio3" id="inlineRadio3" value="3">
                    <label class="form-check-label">6'6"</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio3" id="inlineRadio4" value="4">
                    <label class="form-check-label">6'8"</label>
                    </div>
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Radio3" id="inlineRadio5" value="5">
                    <label class="form-check-label">Ninguna de las anteriores</label>
                    </div>
            </div>
            <div class="row"> 
                <div class="col-sm-12"><br>
                    <div class="form-check form-check-inline" style="text-align: justify;">
                        <h6>Términos y condiciones</h6>
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                        <label class="form-check-label">Yo certifico que toda la información y documentos previos es correcta y verídica a mi concentimiento. El aplicante(s), autoriza a CR-FED Leasing, LLC, sus oficiales, agentes, representantes o asignados a  CREAL a obtener información crediticia y comercial, a realizar una investigación de antecedentes penales y obtener cualquier otra información crediticia y comercial, a realizar una investigación de antecedentes penales y obtener cualquier otra información adicional que pueda ser utilizada y confiada para ofrecer a la empresa del aplicante asistencia financiera. Toda la información presentada en esta aplicación es estrictamente confidencial. Al dar click en el botón FINALIZAR y marcando la casilla "Acepto los términos y condiciones". Les doy consentimiento a realizar negocios electrónicamente con CREAL en conexión con mi solicitud en línea y doy consentimiento para la recepción de esta divulgación electrónica. De haber co-solicitantes en esta aplicación, los co-solicitantes también están de acuerdo con esta autorización.</label> <br><br>
                        <label class="form-check-label">I certify that all of the above statemnets are true and accurate to the best of my/our knowleadge. The applicant(s) authorizes CR-FED Leasing, LLC, its Officers, Agents, Representatives or Assing (Creal) to obtain commercial and consumer credit information, to conducto a criminal investigation and obtain any other additional information that maybe used and relied upon to offer me or my company financial assistance. All information contained in this aplicaction will be held strictly condifential. By clicking on the FINALIZE button and checking the "Acepto los términos y condiciones". box, I affirmatively consent to conducto business electronically with CREAL in connection with my online request and consent to the receipt of this electronic disclousre. If there are co-applicants on this loan, the co-applicants also agree to this authorization.</label>
                        </div>
                </div>
            </div>
            <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                <label id="validacionesPuntaje"></label>
            </div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 BtnsGo">
            <a href="money.html" class="btn BtnCredito Back5">Anterior</a>
            <a href="/aprobacion" class="btn BtnCredito Back4" >Siguiente</a>
            <br><br><br><br>
        </div> 
    </div>
</div>