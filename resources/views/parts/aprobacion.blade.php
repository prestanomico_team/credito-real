<!Doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Crédito Real Usa - Business Capital">
    <meta name="author" description="Prestanomico"/>
    <link rel="pingback" href="Crédito Real Usa"/>
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="{{ asset('credito-real') }}/favicon/credito-real-usa.ico" rel="SHORTCUT ICON">

    <!-- compatilibdad con webapp iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Crédito Real Usa">
    <link rel="apple-touch-icon"  href="{{ asset('credito-real') }}/favicon/apple.png">

    <!-- compatilibdad con webapp Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#004a98">
    <meta name="application-name" content="Conectatest">
    <link rel="icon" type="image/png" href="{{ asset('credito-real') }}/favicon/android.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('credito-real') }}/css/credito-real-usa.css">
    <link rel="stylesheet" href="/css/modals.css?v=<?php echo microtime(); ?>">
    
    <title>Aprobado Crédito Real Usa</title>
    <!-- Facebook Pixel Code -->

    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '4480572125352853');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=4480572125352853&ev=PageView&noscript=1"/>
    </noscript>

    <!-- End Facebook Pixel Code -->
    </head>
    <body class="{{ $class ?? '' }}">
    
    @include('googletagmanager::head')
        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            @if(env('APP_ENV') == 'local' || env('APP_ENV') == 'desarrollo' || env('APP_ENV') == 'desarrollos')
                @verbatim
                    ga('create', 'UA-217784197-1', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @else
                 @verbatim
                    ga('create', 'UA-217784197-2', 'auto', {
                        'cookieDomain': 'none'
                    });
                @endverbatim
            @endif
            ga(function(tracker) {
                var clientId = tracker.get('clientId');
                tracker.set('dimension1', tracker.get('clientId'));
            });
            ga('send', 'pageview');
        </script>
        @include('googletagmanager::body')

        <section class="MenuPrincipal">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="Logo">
                            <a href="/"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-9">
                    <div class="">
                        <ul class="Desktop">
                            <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                            <li><a href="/faqs">Preguntas frecuentes</a></li>
                            @if (!Auth::guard('prospecto')->check())
                                <li><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                                <li><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                                <li><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                            @endif
                            @if (Auth::guard('prospecto')->check())
                            <li><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                            @endif

                        </ul>
                        <ul class="Mobile">
                            <li>
                                <a id="BtnMovil" href="#">
                                    <i class="material-icons" style="vertical-align: middle;">menu</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="SideNav">
            <ul>
                <div class="AvatarLogo">
                    <div class="Background">
                        <div class="Img">
                            <img src="{{ asset('credito-real') }}/img/sidenav.jpg" alt="">
                        </div>
                    </div>
                </div>
                <li><a href="/equipos-leasing">Equipos de Leasing</a></li>
                <li><a href="/faqs">Preguntas frecuentes</a></li>
                @if (!Auth::guard('prospecto')->check())
                    <li class="iniciarSesion"><a><span class="btn BtnCredito" id="aplica_ahora" onclick="focusSimulador()">¡Aplica ahora!</span></a></li>
                    <li class="iniciarSesion"><a href="#" data-bs-toggle="modal" data-bs-target="#LoginForm" class="dropdown-toggle"><span class="Underline">Iniciar sesión</span> </a></li>
                    <li class="sesionIniciada" style="display: none"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                    <li class="sesionIniciada"><a id="nombreProspecto" class="bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display: none"></a></li>
                @endif
                @if (Auth::guard('prospecto')->check())
                <li class="sesionIniciada"><a class="dropdown-item" href="/prospecto/logout">Cerrar Sesión</a></li>
                @endif
            </ul>
        </div>
        <section class="Aprobacion">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3">
                        <div class="Logo">
                            <a href="/"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt=""></a> <br>
                            <h2>¡Felicidades! Tienes una oferta</h2>
                            <h3>{{$nombre}}, gracias por compartirnos tus datos.</h3>
                            <h6>Con base en la información que nos has proporcionado, podemos otorgarte una financiamiento con las siguientes características:</h6>
                        </div>
                        <div class="Aproba">
                            <h3>Resumen de tu financiamiento</h3>
                            <h4>Monto: <span>${{ $monto }} USD </span></h4>
                            <h4>Plazo: <span> {{$plazo}} meses</span> </h4>
                            <h4>Pago mensual estimado:  <span>{{ $pago_estimado}} </span></h4>
                        </div>
                        <div class="Leyenda">
                            <p>Condiciones: Sujeto a comprobación de ingresos declarados y documentación requerida para aprobarlo.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 BtnsGo"><br><br><br>
                        <a class="btn btn-secondary btn-lg" id="rechazar_oferta" data-bs-toggle="modal" data-bs-target="#RechazarOferta">Rechazar</a>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#PreAprobada" id="ofertaAceptada" class="btn BtnCredito Back4 btn-lg">Aceptar</a><br><br><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> <br>
                        <h2>¿Qué Sigue?</h2>
                    </div>
                    <div class="Thumbs">
                        <div class="Thumb">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/que-sigue-1.svg" alt="">
                            </div>
                            <p>1. Carga la documentación solicitada</p>
                        </div>
                        <div class="Thumb">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/que-sigue-2.svg" alt="">
                            </div>
                            <p>2. Validaremos tu información</p>
                        </div>
                        <div class="Thumb">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/que-sigue-3.svg" alt="">
                            </div>
                            <p>3. Si todo marcha bien, pronto tendrás el equipo que necesitas</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="AppWeb">
            <div class="container">
                <div class="row">
                </div>
            </div>
        </section>

    

<!-- Modal Generic -->
<div class="modal fade" id="PreAprobada" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" style="font-size: 1.2em;">¡Tu solicitud fue preaprobada!</h6>
        </div>
        <div class="modal-body">
           <div class="cols">
               <img src="{{ asset('credito-real') }}/img/modal1.svg" style="height: 40px;" alt=""><br>
               <p>Solicitud completada</p>
           </div>
           <div class="cols">
            <img src="{{ asset('credito-real') }}/img/modal2.svg" style="height: 50px;" alt=""><br>
            <p>Carga de documentos</p>
        </div>
        <div class="cols">
            <img src="{{ asset('credito-real') }}/img/modal3.svg" style="height: 40px;" alt=""><br>
            <p>Recibe tu maquinaria</p>
        </div>
           <br>
           <p>Para concluir, adjunta la documentación solicitada.</p>
           <p>Por favor ACEPTA los permisos para acceder a tu dispositivo</p>
           <p>Entre más rápido lo compartas, más rápido obtendrás respuesta.</p>
           <!-- <p>Si tienes dudas envía un Whatsapp al <strong>(55) 8128 9583</strong> y con gusto te apoyaremos</p> -->
           <a onclick="aceptar_oferta()" class="btn BtnCredito Back1">OK</a>
        </div>
      </div>
    </div>
</div>

<!-- Modal Generic -->
<div class="modal fade" id="RechazarOferta" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AvisoImportante">¿Por qué motivos rechazas la oferta? </h5>
            </div>
            <div class="modal-body">
                <div class="Img"><img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" height="120px" alt=""></div> <br>
                    <select class="pre-registro-input" name="motivo_rechazo" id="motivo_rechazo" onchange="cambioMotivoRechazo(this)">
                        <option value="SELECCIONA">SELECCIONA</option>
                        @foreach($motivos as $motivo)
                            <option value="{{ $motivo }}">{{ $motivo }}</option>
                        @endforeach
                    </select>
                    <textarea id="textOtroRechazo" name="textOtroRechazo" maxlength="255" rows="3" style="display:none; margin-top: 10px; margin-bottom: 10px; text-transform: uppercase; padding: 0.5em; width: 100%">
                    </textarea><br><br>
                <button type="button" class="btn BtnCredito Back1" id="confirmaRechazarOferta">Rechazar Oferta</button><br>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('credito-real') }}/js/jquery-3.6.0.min.js"></script>
<script src="{{ asset('credito-real') }}/js/lib/maskmoney.min.js"></script>
<script src="{{ asset('credito-real') }}/js/bootstrap.js"></script>
<script src="{{ asset('credito-real') }}/js/myscripts.js"></script>
<script src="{{ asset('credito-real') }}/js/all.js?v=<?php echo microtime(); ?>"></script>
<script src="{{ asset('credito-real') }}/js/lib/axios.min.js"></script>
<script src="{{ asset('credito-real') }}/js/lib/sweetalert2.all.min.js"></script>

</body>
</html>
