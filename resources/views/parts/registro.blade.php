<style>
    .glyphicon {
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  line-height: 1;

  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
    .glyphicon-ok:before {
  content: "\e013";
}
.glyphicon-remove:before {
  content: "\e014";
}
</style>

<div class="container" id="registro" style="display:none">
    <form id="formRegistro">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Registro</h2>
                    <h6>Abre tu cuenta y sé parte de Crédito Real USA</h6>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Nombre(s)</label>
                    <input type="text" class="form-control required uppercase" id="nombres" name="nombres" maxlength="50">
                    <small id="nombres-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Apellido(s)</label>
                    <input type="text" class="form-control required uppercase" id="apellidos" name="apellidos" maxlength="50">
                    <small id="apellido_paterno-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Email</label>
                    <input type="email" class="form-control required lowercase" id="email" name="email" maxlength="100">
                    <small id="email-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Teléfono Celular</label>
                    <input type="tel" class="form-control required uppercase" id="celular" name="celular" maxlength="10">
                    <small id="celular-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Crear tu contraseña</label>
                    <input type="password" class="form-control required" id="contraseña" name="contraseña" maxlength="25" type="password">
                    <small id="contraseña-help" class="help"></small>
                    <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra mayúscula</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Una letra minúscula</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Un número</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Mínimo 8 caracteres</div>
                </div>
                <div class="FormInput">
                    <label class="form-label">Confirmar contraseña</label>
                    <input type="password" class="form-control required" id="confirmacion_contraseña" maxlength="25" name="confirmacion_contraseña">
                    <small id="confirmacion_contraseña-help" class="help"></small>
                    <div class="col-12 helperPassword" style="font-size:12px; margin-top: 10px;"><span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004; display:none"></span> Una letra mayúscula</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="lcase" class="glyphicon glyphicon-remove" style="color:#FF0004; display:none"></span> Una letra minúscula</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004; display:none"></span> Un número</div>
                    <div class="col-12 helperPassword" style="font-size:12px;"><span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004; display:none"></span> Mínimo 8 caracteres</div>
                </div>
            </div>
            <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                <label id="validacionesRegistro"></label>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 BtnsGo">
                <a class="btn BtnCredito Back4" onclick="registro()">Siguiente</a><br><br><br><br>
            </div> 
        </div>
    </form>
</div>
