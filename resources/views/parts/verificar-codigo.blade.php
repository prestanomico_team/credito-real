<div class="container" id="verificar_codigo" style="display:none">
    <form id="formVerificarSMS">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Verifica tu código</h2>
                    @if(Auth::guard('prospecto')->check())
                    <h6>Te hemos enviado un código de verificación por SMS al número <b>{{ Auth::guard('prospecto')->user()->celular }}</b>. Ingrésalo exactamente como lo recibas.</h6>
                    @else
                    <h6>Te hemos enviado un código de verificación por SMS al número <b id="show_celular"></b>. Ingrésalo exactamente como lo recibas.</h6>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4"> <br>
                <p>Validación de código de verificación</p>
                <div class="Codigo">
                    <input type="text" class="form-control required only_numbers" maxlength="6" id="conf_code" name="conf_code"style="text-align: center;">
                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 BtnsGo">
                <a class="btn BtnCredito Back4" onclick="valida_sms()">Siguiente</a><br><br>
                <a class="" id="reenviarSMS"  onclick="reenviar_sms()"><span>Enviar nuevo código <b id="timerResend"></b></span></a>
            </div>
        </div>
    </form>
</div>
