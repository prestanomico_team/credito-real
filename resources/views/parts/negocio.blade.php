<div class="container" id="datos_negocio" style="display:none">
    <form id="formDatosNegocio">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Cuéntanos sobre tu negocio</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Promedio de ventas mensuales de la empresa</label>
                    <input type="text" class="form-control required" id="ventas_mensuales" name="ventas_mensuales">
                    <small id="ventas_mensuales-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Margen de beneficio promedio de ventas (%)</label>
                    <input type="text" class="form-control required" id="margen_ventas" maxlength="5" name="margen_ventas">
                    <small id="margen_ventas-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Número de empleados</label>
                    <select class="form-select required" id="numero_empleados" name="numero_empleados">
                        <!-- <option selected disabled>Selecciona una opción</option> -->
                        <option value="1 a 20">1 a 20</option>
                        <option value="21 a 60">21 a 60</option>
                        <option value="61 a 100">61 a 100</option>
                        <option value="100">> 100</option>
                    </select>
                    <small id="numero_empleados-help" class="help"></small>
                </div>
                
                <div class="FormInput" style="text-align: center;">
                    <label class="form-label required">¿Eres el único dueño?</label> <br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input required" type="radio" name="unico_dueno" id="unico_dueno" value="Si">
                        <label class="form-check-label" for="inlineRadio1">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input required" type="radio" name="unico_dueno" id="unico_dueno" value="No">
                        <label class="form-check-label" for="inlineRadio2">No</label>
                    </div>
                    <small id="unico_dueno-help" class="help"></small>
                </div> <br>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="FormInput" style="padding: 0 3em;"> <br>
                    <label class="form-label">Enumere cualquier otro nombre comercial que haya utilizado en los últimos 5 años</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="FormInput" style="padding: 0 3em;">
                    <label class="form-label"></label>
                    <input type="text" class="form-control uppercase" placeholder="1.-" id="nombre_comercial_1" name="nombre_comercial_1">
                    <input type="text" class="form-control uppercase" placeholder="2.-" id="nombre_comercial_2" name="nombre_comercial_2">
                    <input type="text" class="form-control uppercase" placeholder="3.-" id="nombre_comercial_3" name="nombre_comercial_3">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="FormInput" style="padding: 0 3em;">
                    <label class="form-label"></label>
                    <input type="text" class="form-control uppercase" placeholder="4.-" id="nombre_comercial_4" name="nombre_comercial_4">
                    <input type="text" class="form-control uppercase" placeholder="5.-" id="nombre_comercial_5" name="nombre_comercial_5">
                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
            <label id="validacionesNegocio"></label>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a class="btn BtnCredito Back5" onclick="getForm('datos_empresa', 'datos_negocio')">Anterior</a>
                <a class="btn BtnCredito Back4" onclick="datos_negocio()">Siguiente</a>
                <br><br><br><br>
            </div> 
        </div>
    </form>
</div>