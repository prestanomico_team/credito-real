<div class="container" id="datos_empresa" style="display:none">
    <form id="formDatosEmpresa">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Datos de la empresa</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Nombre legal del negocio</label>
                    <input type="text" class="form-control uppercase required" id="nombre_empresa" name="nombre_empresa">
                    <small id="nombre_empresa-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Tipo de propiedad empresarial</label>
                    <select class="form-select required" id="tipo_propiedad" name="tipo_propiedad">
                        <option selected disabled>Selecciona una opción</option>
                        <option value="LLC">LLC</option>
                        <option value="Asociacion">Asociacion</option>
                        <option value="Propietario Unico">Propietario Unico</option>
                    </select>
                    <small id="tipo_propiedad-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Fecha de fundación</label>
                    <input type="date" class="form-control required" id="fecha_fundacion" name="fecha_fundacion">
                    <small id="fecha_fundacion-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Tipo de trabajo que realizas</label>
                    <input type="text" class="form-control required" id="descripcion_empresa" name="descripcion_empresa">
                    <small id="descripcion_empresa-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Dirección de la empresa</label>
                    <input name="location" type="text" class="form-control required" placeholder="Address" id="location"/>
                    <small id="location-help" class="help"></small>
                </div>
                <div class="FormInput">
                <label class="form-label">Número interior</label>
                <input id="no_interior" name="no_interior" type="text" class="form-control" placeholder="Apt, Suite, etc (optional)"/>
                <small id="no_interior-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Ciudad</label>
                    <input name="locality" type="text" class="form-control required" id="locality"/>
                    <small id="locality-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Estado</label>
                    <input name="administrative_area_level_1" type="text" class="form-control required" id="administrative_area_level_1"/>
                </div>
                <div class="FormInput">
                    <label class="form-label">Código Postal</label>
                    <input name="postal_code" type="text" class="form-control required" id="postal_code"/>
                    <small id="postal_code-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">País</label>
                    <input name="country" type="text" class="form-control" id="country"/>
                </div>
            </div>
            <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                <label id="validacionesEmpresa"></label>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 BtnsGo">
                <a class="btn BtnCredito Back4" onclick="datos_empresa()">Siguiente</a>
                <br><br><br><br>
            </div> 
        </div>
    </form>
</div>