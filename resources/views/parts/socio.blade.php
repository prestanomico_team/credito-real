<div class="container" id="datos_socio" style="display:none">
    <form id="formDatosSocio">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Cuéntanos sobre tu socio(s)</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Nombre Completo</label>
                    <input type="text" class="form-control required" id="nombre_completo" name="nombre_completo">
                    <small id="nombre_completo-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Fecha de nacimiento del socio</label>
                    <input type="date" class="form-control required" id="fecha_nacimiento" name="fecha_nacimiento">
                    <small id="fecha_nacimiento-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Correo del socio</label>
                    <input type="email" class="form-control required" id="email_socio" name="email_socio">
                    <small id="email_socio-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Dirección de domicilio completo del socio</label>
                    <input type="text" class="form-control required" id="direccion_completa" name="direccion_completa">
                    <small id="direccion_completa-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Teléfono del socio</label>
                    <input type="number" class="form-control required" id="telefono" maxlength="10" name="telefono">
                    <small id="telefono-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Porcentaje de participación (%)</label>
                    <input type="text" class="form-control required" id="porcentaje_participacion" maxlength="2" name="porcentaje_participacion">
                    <small id="porcentaje_participacion-help" class="help"></small>
                </div>
            </div>
            </div>
            <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
                <label id="validacionesSocio"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a class="btn BtnCredito Back5" onclick="getForm('datos_negocio', 'datos_socio')">Anterior</a>
                <a class="btn BtnCredito Back4"onclick="datos_socio()">Siguiente</a>
                <br><br><br><br>
            </div> 
        </div>
    </form>
</div>