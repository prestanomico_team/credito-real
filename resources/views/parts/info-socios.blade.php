<div class="container" id="info_socios" style="display:none">
    <form id="formDatosInfoSocio">
        <div class="row">
            <div class="col-sm-12">
                <div class="Title">
                    <h2>Cuéntanos más sobre tu negocio</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="Formula">
                <div class="FormInput">
                    <label class="form-label">Monto de tus cuentas por cobrar</label>
                    <input type="text" class="form-control required" id="monto_cuentas_cobrar" name="monto_cuentas_cobrar">
                    <small id="monto_cuentas_cobrar-help" class="help"></small>
                </div>
                <div class="FormInput" style="text-align: center;">
                    <label class="form-label">¿Les debes dinero a una empresa comercial de anticipos, efectivo u otro prestamista?</label> <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deudas_impuestos" id="inlineRadio1" value="Si">
                            <label class="form-check-label" for="inlineRadio1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deudas_impuestos" id="inlineRadio2" value="No">
                            <label class="form-check-label" for="inlineRadio2">No</label>
                        </div>
                        <small id="deudas_impuestos-help" class="help"></small>
                </div>
                <div class="FormInput" style="text-align: center;">
                    <label class="form-label">¿Estás al corriente en el pago de tus impuestos y los de tu empresa?</label> <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deudas_comerciales" id="inlineRadio1" value="Si">
                            <label class="form-check-label" for="inlineRadio1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="deudas_comerciales" id="inlineRadio2" value="No">
                            <label class="form-check-label" for="inlineRadio2">No</label>
                        </div>
                        <small id="deudas_comerciales-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Monto de impuestos atrasados o vencidos</label>
                    <input type="text" class="form-control required" id="monto_impuestos" name="monto_impuestos">
                    <small id="monto_impuestos-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Número de Seguridad Social / ITIN</label>
                    <input type="email" class="form-control required" id="nss" maxlength="9" name="nss">
                    <small id="nss-help" class="help"></small>
                </div>
                <div class="FormInput">
                    <label class="form-label">Número de licencia para conducir</label>
                    <input type="text" class="form-control required" id="licencia_conducir" maxlength="10" name="licencia_conducir">
                    <small id="licencia_conducir-help" class="help"></small>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="FormInput" style="padding: 0 3em;"> <br>
                    <label class="form-label">Enumere las 5 deudas más grandes para tu empresa</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="FormInput" style="padding: 0 3em;">
                    <label class="form-label"></label>
                    <input type="text" class="form-control  uppercase" placeholder="1.-" name="deuda_empresa_1">
                    <input type="text" class="form-control  uppercase" placeholder="2.-" name="deuda_empresa_2">
                    <input type="text" class="form-control  uppercase" placeholder="3.-" name="deuda_empresa_3">
                    
                </div>
            </div>
            <div class="col-sm-6">
                <div class="FormInput" style="padding: 0 3em;">
                    <label class="form-label"></label>
                    <input type="text" class="form-control uppercase" placeholder="4.-" name="deuda_empresa_4">
                    <input type="text" class="form-control uppercase" placeholder="5.-" name="deuda_empresa_5">
                    
                </div>
            </div>
        </div>        
        <div class="col-xs-12 txt-center" style="color:red; margin-top: 10px;">
            <label id="validacionesInfoSocio"></label>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a class="btn BtnCredito Back5" onclick="getForm('datos_socio', 'info_socios')">Anterior</a>
                <a class="btn BtnCredito Back4"onclick="info_socios()">Siguiente</a>
                <br><br><br><br>
            </div> 
        </div>
    </form>
</div>