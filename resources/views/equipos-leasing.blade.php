@extends('layouts.app_sitio')

@section('content')
    <section id="carouselCreditoUsa" class="carousel slide carousel-fade CarouselCreditoUsa" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('credito-real') }}/img/maquinaria.jpg" class="d-block w-100" alt="Arrendamiento Real Usa">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Arrendamiento de máquinas accesible especialmente <span>para ti.</span></h3>
                    <h5>Consigue el equipo que necesitas crecer</h5>
                    <p>Estabiliza tu flujo de caja, acepta más trabajo y aumenta los beneficios, con el arrendamiento de equipos de Crédito <strong>Real USA Business Capital.</strong></p>
                    <a href ="/" class="btn Back1" id="aplica_ahora" >¡Aplica ahora!</a>
                </div>
            </div>
        </div>
    </section>

    <section class="AppWeb" id="AppWeb">
        @include('parts.registro')
        @include('parts.verificar-codigo')
        @include('parts.empresa')
        @include('parts.negocio')
        @include('parts.socio')
        @include('parts.info-socios')
        @include('parts.datos_credito')
        @include('parts.puntaje')

    </section>
    <section class="EquiposFinanciar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h2>Tipos de equipos que financiamos</h2>
                        <h6>Crédito Real Usa Business Capital proporciona arrendamientos para una variedad de industrias como:</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="IconThumbs">
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPO-1-INDUSTRIALES.svg" alt="">
                            </div>
                            <p>Equipos Industriales</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-2-CONSTRUCCIÓN.svg" alt="">
                            </div>
                            <p>Equipos de Construcción</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-3-SERVICIOS.svg" alt="">
                            </div>
                            <p>Equipos de Servicios Públicos</p>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-4-PETROLEO.svg" alt="">
                            </div>
                            <p>Equipos de Petróleo y Gas</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="EquiposComunes">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h2>Equipos más comunes</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="IconThumbs">
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-1-MINI-EXCAVATORS.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Mini-Excavators</h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-2-BACK-HOES.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Backhoes</h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-3-REACH.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Reach Forklifts</h6>
                                <p>Forklifts & Material</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-4-SKID.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Skid Steers & Track</h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-5-BULL.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Bull Dozers</h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-6-HORIZONTAL.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Horizontal Drilling Machines </h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-7-TRAILERS.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Trailers</h6>
                                <p>Forklifts & Material</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-COMUNES-8-HEAVY.jpg" alt="">
                            </div>
                            <div class="Info">
                                <h6>Heavy Equipment</h6>
                                <p>Earthmoving Equipment</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12"><br>
                    <span style="color: #888888;font-size: 1.15em;">* Para un financiamiento de 36 meses</span>
                </div>
            </div>
        </div>
    </section>

    <section class="EquiposNO">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Title">
                        <h2>Equipos que <span>NO</span> financiamos</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="IconThumbs">
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-NO-FINANCIADO-1.jpg" alt="">
                            </div>
                            <div class="Info">
                                <p>Revolvedora</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-NO-FINANCIADO-2.jpg" alt="">
                            </div>
                            <div class="Info">
                                <p>Camión de 18 ruedas</p>
                            </div>
                        </div>
                    </div>
                    <div class="IconThumb">
                        <div class="Icon">
                            <div class="Img">
                                <img src="{{ asset('credito-real') }}/img/EQUIPOS-NO-FINANCIADO-3.jpg" alt="">
                            </div>
                            <div class="Info">
                                <p>Autos</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="Subfooter"> <br>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Img">
                        <img src="{{ asset('credito-real') }}/img/credito-real-usa.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection