@extends('crm.app')
@section('content')
<link href="/back/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="/css/modals.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<style>
	.col-form-label {
		margin-bottom: 15px;
	}
	.display-field {
        display: inline-block;
        vertical-align: middle;
    }
    .field_oculto {
        display: none;
    }
 	label {
	    text-transform: none;
	    /* color: #b7b7b7; */
	}
	.form-control {
	    border-radius: none !important;
	    box-shadow: none;
	    border-color: #44b4f6;
	}
	.form-control {
	    border-radius: 3px;
	    box-shadow: none;
	    border-color: #44b4f6;
	}
	.form-control:focus {
	    border-color: #44b4f6;
	    box-shadow: none;
	}
	.btn-primary {
		background-image: none;
	}
</style>
<div class="container" style="width:100%">
	<div id="app" class="content">
		@php
			use App\CatalogoSepomex;
			use Carbon\Carbon;

			$estados = CatalogoSepomex::selectRAW('distinct(estado), id_estado')
				->orderBy('id_estado', 'asc')
				->get();
			$estados = (json_encode($estados));
			use App\MotivoRechazo;
			$motivos = MotivoRechazo::selectRAW('motivo, id')
				->orderBy('id', 'asc')
				->get();
			$motivos = (json_encode($motivos));
			$fecha = Carbon::now()->format('d/m/Y');
			$url = Request::fullUrl();

        @endphp
        
	    <corporativo :estados='{{ $estados }}' :motivos='{{ $motivos }}' :configuracion='{{ json_encode($configuracion) }}' fecha="{{ $fecha }}" url="{{ $url }}"></corporativo>

	</div>
</div>
@endsection
@section('app_backoffice')
	<script type="text/javascript" src="/js/app_backoffice.js"></script>
	<script type="text/javascript" src="/js/statusOferta.js"></script>
@append
