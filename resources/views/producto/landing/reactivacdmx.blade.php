<!DOCTYPE html>
<style>

@import url(https://fonts.googleapis.com/css?family=Montserrat:400,500,80);

body{
  font-family: 'Montserrat', sans-serif;
  background-color: #ffffff;
}

.footer {
	background-color: #fff !important;
	color: #282828 !important;
	font-family: 'Montserrat';
	text-align: center;
}

a {
	color: #282828 !important;
}
.bordes {
  border: 2px solid red;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5);
}

.center {
    margin-left: auto;
    margin-right: auto;
    display: block;
}

.checa-calificas {
  color: #fff !important;
  background-color: #019ebe;
  border-color: #019ebe;
  font-weight: bold !important;
  letter-spacing: 0.05em;
  border-radius: 0;
}

.checa-calificas:hover,
.checa-calificas:active,
.checa-calificas:focus,
.checa-calificas.active {
  /* let's darken #004E64 a bit for hover effect */
  background: #003D4F;
  color: #ffffff;
  border-color: #003D4F;
}

.titulo {
	font-family: 'Montserrat';
	font-weight: lighter;
	color:#000;
	font-size: 2.5vw;
	
}

.subtitulo {
	font-family: 'Montserrat';
	font-weight:  lighter;
	color: #000;
	font-size: 1.75vw;
    text-align: center;
	
}

.texto {
	font-family: 'Montserrat';
	font-weight: lighter;
	font-size: 1.15vw;
	margin-top: 30px;
	margin-bottom: 30px;
}

.leyenda {
	font-family: 'Montserrat';
	font-weight: lighter;
	font-size: 10px;
}

p {
    line-height: 1.5;
}

.div_leyenda {
  position: relative;
  margin-top: 40px;
  text-align: center;
}

.div_leyenda_header {
  position: relative;
  margin-top: 10px;
  text-align: center;
}

.leyenda_text {
  display: inline-block;
  font-weight: bold;
  font-size: .9vw;
  font-family: 'Montserrat';
  line-height: 1.5;
  color: #000;
}

.leyenda_image {
  display: inline-block;
  position: absolute;
  top: 1.55vw;
  margin-left: -9.4vw;
  width: 5vw;
}

.headerfb_text {
  display: inline-block;
  font-weight: bolder;
  font-size: 13px;
  font-family: 'Montserrat';
}

.headerfb_image {
  position: absolute;
  margin-left: 13px;
  width: 25px;
}

.logo_derecha {
	margin-top: 4%;
	width: 25%;
	margin-right: -2%;
	float: right;
}

.logo_izq {
	margin-top: 5%;
	width: 35%;
	margin-left: 1%;
	float: left;
}

.logo_prestanomico {
	/* width: 150px;*/
	width: 50%;
	height: 50%;
	margin-left: 40px;
}

.icon-inline {
	width: 17px !important;
	height: 12px !important;
	margin-right: 10px;
}

.img-left {
	width: 375px;
	height: 325.250px;
}

@media screen and (max-width: 670px) {

	div.description {
		display: none;
	}

	.logo_prestanomico {
		margin-left: 10px;
	}

	.img-left {
		width: 170px;
		height: 170px;
	}

	.titulo {
		font-family: 'Montserrat';
		font-weight: lighter;
		font-size: 4vw;
	}

	.subtitulo {
		font-family: 'Montserrat';
		font-weight:  bold;
		font-size: 3.5vw;
	}

	.texto {
		font-family: 'Montserrat';
		font-weight: lighter;
		font-size: 3.5vw;
		margin-top: 5px;
		margin-bottom: 5px;
        margin-left: 150px;
	}
	.cont {
		background-image: url(/images/landings/corporativo/landing.png);
		background-size: 100% 102%;
		background-attachment: inherit;
		background-repeat: no-repeat;
		height: 264px;
	}
	.leyenda_text {
	  display: inline-block;
	  font-weight: lighter;
	  font-size: 2vw;
	  font-family: 'Montserrat';
	}

	.leyenda_image {
	  display: inline-block;
	  position: absolute;
	  top: 20px;
	  margin-left: -130px;
	  width: 60px;
	}

	.div_leyenda {
	  position: relative;
	  margin-top: 10px;
	  text-align: center
	}

	.div-img-left {
		margin-right: 75px;
	}

	.logo_derecha {
	margin-top: 4%;
	width: 44%;
	margin-right: -36px;
	/*float: right;*/
}

	.logo_izq {
		margin-top: 5%;
		width: 64%;
		margin-left: 1%;
		float: left;
	}

}

@media screen and (min-width: 671px) {
	div.description-mobile {
		display: none;
	}

}

@media screen and (min-width: 900px) and (max-width: 1100px) {
	.img-left {
		width: 320px;
	}

}

@media screen and (min-width: 671px) and (max-width: 899px) {
	.img-left {
		width: 250px;
	}
	.div-img-left {
		margin-right: 25px;
	}

    .div_leyenda_header {
		position: relative;
		margin-top: 10px;
		text-align: right;
	}

}

@media (orientation: landscape) {

	.div_leyenda_header {
		position: relative;
		margin-top: 10px;
		text-align: center;
	}

}


</style>
<html lang='en'>
	<head>
      	<meta charset="UTF-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1.0">
      	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      	<title>Financiera Monte de Piedad | {{ $producto->nombre_producto }}</title>
		<meta name="description" content="Somos una empresa que brinda préstamos flexibles con una tasa de interés menor a la de los bancos regulares.">
		<meta name="keywords" content="Prestamos personales, Préstamos personales, crédito personal, credito personal, préstamos en línea, prestamos en linea, prestamos sin aval, creditos rapidos, créditos rápidos, préstamos móvil, buró de crédito, buro de credito, CAT, IVA">
      	<link rel="icon" type="image/png" href="/images/favicon_prestanomico_negro.png">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
		<meta name="csrf-token" content="{{ csrf_token() }}">
   	</head>
   	<body>
   		<div class="container-fluid">
			<!-- Logos -->
			<div class="row">
				<div class="col-6 logo_izq">
					<img class="logo_prestanomico" src="/images/landings/comunidar/unir.png"></img>
				</div>
				<div class="col-6 logo_derecha">
					<img class="logo_prestanomico" src="/images/landings/comunidar/monte_de_piedad_logo.png"></img>
				</div>
            </div>
            <br >
			<div class="row">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: -50px">
                        <img src="/images/landings/comunidar/logo1.png" class="img-responsive center">
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
						<br >
                        {{-- <p class="subtitulo">Solicita tu préstamo en línea</p> --}}
						<p class="subtitulo">¡Pide tu préstamo con aprobación en línea!</p>
						<p class="subtitulo">Te prestamos hasta $30,000</p>
                        <br >
                        <center>
                        <a href="#" class="btn checa-calificas"> Solicítalo ahora</a>
                        </center>
                        
                       {{--  <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="/images/landings/comunidar/unirydar.png" class="image-responsive center">
                        </div> --}}
                        
                        <div class="div_leyenda">
                            <p>
                                <div class="leyenda_text">* El préstamo es otorgado por Financiera Monte de Piedad S.A de C.V., S.F.P</div>
                            </p>
                        </div>
                    </div>
                </div>
			</div>

   		</div>
		<!-- Footer -->
		<div class="footer">
			<div class="footer__schema">
				<div>
				   <p style="display:inline-table; width: 250px;"><img src="/images/landings/generica/logo-whatsapp.png" class="icon-inline leyenda-text"> (55) 4163 4806</p>

				   <p style="display:inline-table; "><img src="/images/landings/generica/logo-email.png" class="icon-inline leyenda-text"> une@financieramontedepiedad.com.mx</p>
			    </div>
		       <p> Blvd. Manuel Ávila Camacho No. 32, Col. Lomas de Chapultepec III Sección, C.P. 11000	CDMX</p>
		    </div>
		    {{-- <div class="footer__certificate">
		       <span id="siteseal">
		          <script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=fJzgGrLcXSBlMzn2cT5aU0SRFMGfCr7Tpo8P8xkf7gqUdaSDbIjWU7eUFfZ0"></script>
		       </span>
		       <span>
		          <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
		       </span>
		    </div> --}}
		</div>
		<script src='/js/app_backoffice.js' type="text/javascript" charset="utf-8"></script>
		<script src='/js/jquery.cookie.js' type="text/javascript" charset="utf-8"></script>
		<script src='/js/cookie_calificas.js?v=<?php echo microtime(); ?>' type="text/javascript" charset="utf-8"></script>
        @include('layouts.footer_analitycs')
    </body>
</html>