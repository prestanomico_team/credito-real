@extends('crm.app')
@section('content')
<link rel="stylesheet" href="/back/css/font-awesome.min.css">
<link rel="stylesheet" href="/back/css/AdminLTE.min.css">
<link rel="stylesheet" href="/back/css/_all-skins.min.css">
<link rel="stylesheet" href="/back/css/multi-select.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<style>
.yellow {
    color: #FFF500;
}
.green {
    color: #28A745;
}
.glyphicon {
    font-size: 18px;
}
.box.box-primary {
	border-top-color: #f79020;
}
a:visited {
	color: #3c8dbc;
}
.btn-primary:visited {
	color: #fff;
}
label {
	text-transform: none;
}
.help-block {
    color: #dd4b39;
}
</style>

<div class="container" style="width:100%">
	<div id="dashboard-container" class="content">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border" style="text-align:center;">
	            	<h3 class="box-title" style="font-weight: bold;">Listado de Productos</h3>
	            </div>
                @if(Auth::user()->can('administrador-productos'))
				<div class="box-body">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
							<a href="{{ URL::route('productos.nuevo') }}" class="btn btn-primary"> Nuevo </a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#plazosModal">
                                Plazos
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#finalidadesModal">
                                Finalidades
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ocupacionesModal">
                                Ocupaciones
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#equiposModal">
                                Equipos
                            </button>
						</div>
						<hr>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


							<table class="table table-bordered">
								<tbody>
									<tr>
										<th style="width: 5%">#</th>
										<th>Producto</th>
										<th>Landing</th>
										<th>Vigencia</th>
										<th>Editar</th>
										<th style="width: 5%">Default</th>
									</tr>
									@foreach ($productos as $producto)
										@if ($producto->default)
											@php
												$landing = url('/');
												$default = 'glyphicon glyphicon-star yellow';
											@endphp

										@else
											@php
												$landing = url('/').'/landing/'.$producto->alias;
												$default = 'glyphicon glyphicon-star-empty';
											@endphp
										@endif

                                        @if ($producto->vigente == 1 || $producto->vigencia_hasta == null)
                                            @php
                                                $vigencia = 1;
                                            @endphp
                                        @else
                                            @php
                                                $vigencia = 0;
                                            @endphp
                                        @endif

										<tr>
											<td> {{ $loop->iteration }} </td>
											<td> {{ $producto->nombre_producto }} </td>
											<td> <a href="{{ $landing }}"> {{ $landing }} </a> </td>
											<td style="text-align:center"> @if ($vigencia == 1) <li class="glyphicon glyphicon-ok green"></li>  @else <b class="text-danger"> {{ Carbon\Carbon::parse($producto->vigencia_hasta)->format('d-m-Y') }} </b> @endif </td>
											<td style="text-align:center"> <a href="/productos/{{ $producto->id }}"><li class="glyphicon glyphicon-edit"></li></a> </td>
											<td style="text-align:center"> <li class="{{ $default }}"></li>  </td>
										</tr>
									@endforeach


								</tbody>
							</table>
	    				</div>
	  				</div>
	    		</div>
                @else
					<center>
						<br>
						<h4>No tienes privilegios para realizar esta acción</h4>
					</center>
                @endif
			</div>

		</div>
	</div>
</div>

<!-- Modal Plazos -->
<div class="modal fade" id="plazosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Plazos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <table class="table" id="tablaPlazos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Duración</th>
                                    <th>Plazo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($plazos as $plazo)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $plazo->duracion }} </td>
                                    <td> {{ $plazo->plazo }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <form id="formPlazos">
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label class="control-label">Duración</label>
                                    <input type="text" class="form-control" id="duracion" name="duracion">
                                    <span id="error_duracion" class="help-block"></span>
                                </div>
                                <div class="form-group col-md-7">
                                    <label class="control-label">Plazo</label>
                                    <select id="plazo" class="form-control" name="plazo">
                                        <option selected disabled value="">Selecciona</option>
                                        <option value="Meses">Meses</option>
                                        <option value="Quincenas">Quincenas</option>
                                    </select>
                                    <span id="error_plazo" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row" style="text-align:right;">
                                <button type="button" class="btn btn-primary" style="margin-right: 15px;" onclick="agregarPlazo()">
                                    Agregar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Finalidades -->
<div class="modal fade" id="finalidadesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Finalidades</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <table class="table" id="tablaFinalidad">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Finalidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($finalidades as $finalidad)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $finalidad->finalidad }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <form id="formFinalidad">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Finalidad</label>
                                    <input type="text" class="form-control" id="finalidad" name="finalidad">
                                    <span id="error_finalidad" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row" style="text-align:right;">
                                <button type="button" class="btn btn-primary" style="margin: 15px;" onclick="agregarFinalidad()">
                                    Agregar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ocupaciones -->
<div class="modal fade" id="ocupacionesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Ocupaciones</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <table class="table" id="tablaOcupacion">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ocupación</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($ocupaciones as $ocupacion)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $ocupacion->ocupacion }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <form id="formOcupacion">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Ocupación</label>
                                    <input type="text" class="form-control" id="ocupacion" name="ocupacion">
                                    <span id="error_ocupacion" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row" style="text-align:right;">
                                <button type="button" class="btn btn-primary" style="margin: 15px;" onclick="agregarOcupacion()">
                                    Agregar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ocupaciones -->
<div class="modal fade" id="equiposModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Equipos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <table class="table" id="tablaEquipos">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Equipo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($equipos as $equipo)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    <td> {{ $equipo->equipo_leasing}} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-5">
                        <form id="formEquipos">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Equipos Leasing</label>
                                    <input type="text" class="form-control" id="equipo_leasing" name="equipo_leasing">
                                    <span id="error_equipo_leasing" class="help-block"></span>
                                </div>
                            </div>
                            <div class="row" style="text-align:right;">
                                <button type="button" class="btn btn-primary" style="margin: 15px;" onclick="agregarEquipos()">
                                    Agregar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/multiselect.min.js"></script>
<script src="{{ asset('credito-real') }}/js/lib/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="{{ asset('credito-real') }}/js/lib/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="{{ asset('credito-real') }}/js/productos.js?v=<?php echo microtime(); ?>"></script>
@endsection
