@extends('layouts.app_sitio')

@section('content')
    <section class="AppWeb">
        @include('parts.registro')
        @include('parts.verificar-codigo')
        @include('parts.empresa')
        @include('parts.negocio')
        @include('parts.socio')
        @include('parts.info-socios')        
        @include('parts.puntaje')
    </section>
@endsection
