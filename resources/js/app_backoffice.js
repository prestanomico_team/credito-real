
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 import Vue from 'vue/dist/vue'
 
 window.Vue = Vue;
 window.swal = require('sweetalert2');
  

 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 Vue.component('panel', require('./components/Panel.vue').default);
 Vue.component('detalle_prospecto', require('./components/DetalleProspecto.vue').default);
 Vue.component('detalle_solicitud', require('./components/DetalleSolicitud.vue').default);
 
 Vue.component('pagination', require('laravel-vue-pagination'));
 
 const app = new Vue({
     el: '#app',
 });
 