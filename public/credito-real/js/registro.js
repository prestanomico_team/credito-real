$("form").bind("keypress", function(e) {
    if (e.keyCode == 13) {
        return false;
    }
});

function focusSimulador() {
    $("html, body").animate({ scrollTop: $("#simulador").offset().top }, 1000);
}

function validarSimulador() {

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);
    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    datos['monto_prestamo'] = prestamoValue;

    var validaciones = validacionesSimulador();
    var nuevaSolicitud = $('#nueva_solicitud').val();

    $('#validacionesSimulador').html(validaciones);

    var datos_faltantes = '';
    if (validaciones == '') {

        axios.post('/validaciones', {
            datos: datos,
            formulario: 'simulador'
        }).then(function (response) {

            if (response.data.success == false) {

                if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {
                    datos_faltantes += '- Verifica los campos en rojo.';
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                }

            } else {

                if (nuevaSolicitud == 'false' && validaciones == '') {

                    $('#carouselCreditoUsa').hide();
                    $('#simulador').hide();
                    $('#aplicar').hide();
                    $('#proceso').hide();
                    $('#leasing').hide();
                    $('#registro').show();

                    getForm('registro', 'simulador');
                    $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);

                } else if(nuevaSolicitud == 'true' && validaciones == '') {
                    registroNuevaSolicitud()
                }

            }

        }).catch(function (error) {

            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }
}

function registro() {

    var datos = $('#formSimulador,#formRegistro').serializeArray();
    datos = getFormData(datos);
    pagoEstimado = $("#pago_estimado").maskMoney('unmasked')[0];
    datos['pago_estimado'] = pagoEstimado;

    var validaciones = validacionesRegistro();
    $('#validacionesRegistro').html(validaciones);

    if (validaciones == '') {

        var texto = 'Registrando usuario...';
        Swal.fire({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

        var clientId = 0;
        ga(function(tracker) {
            clientId = tracker.get('clientId');
        });
        datos['clientId'] = clientId;

        axios.post('/solicitud/registro', datos)
        .then(function (response) {

            if (response.data.success == true) {

                $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
                $("#aplica_ahora").hide();

                datos['prospecto_id'] = response.data.prospecto_id;

                $('.sesionIniciada').show()
                $('.iniciarSesion').hide()

                if (response.data.hasOwnProperty('eventTM')) {
                    var eventos = response.data.eventTM;
                    $.each(eventos, function(key, evento) {
                        dataLayer.push(evento);
                    });
                }

                if (response.data.siguiente_paso == 'sms') {
                    swal.close();
                    verificar_codigo(datos);
                }

            } else {

                swal.close();
                if (response.data.hasOwnProperty('errores')) {

                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesRegistro').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('siguiente_paso')) {

                    if (response.data.siguiente_paso == 'login') {
                        Swal.fire({
                            title: "Usuario Registrado",
                            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                            type: 'error',
                            showConfirmButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Iniciar Sesión',
                            customClass: 'modalError',
                            cancelButtonText: 'Cancelar',
                            allowOutsideClick: false
                        }).then((result) => {
                            if (result.value) {
                               $('#LoginForm').modal('show');
                           }
                       });
                    }

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Recarga la página para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           location.reload();
                       }
                   });

                } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Reintentar',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                   }).then((result) => {
                      if (result.value) {
                          var texto = 'Registrando usuario...';
                          Swal.fire({
                             html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                             customClass: 'modalLoading',
                             showCancelButton: false,
                             showConfirmButton:false,
                             allowOutsideClick: false
                         });
                         this.registroProspecto(datos);
                      }
                  });

               }
            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });

    }
}


function registroNuevaSolicitud() {

    var datos = $('#formSimulador').serializeArray();
    datos = getFormData(datos);
    pagoEstimado = $("#pago_estimado").maskMoney('unmasked')[0];
    datos['pago_estimado'] = pagoEstimado;

    var texto = 'Registrando solicitud...';
    Swal.fire({
       html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/solicitud/registro/solicitud', datos)
    .then(function (response) {

        if (response.data.success == true) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            $("#aplica_ahora").hide();

            $('.sesionIniciada').show()
            $('.iniciarSesion').hide()

            if (response.data.hasOwnProperty('eventTM')) {
                var eventos = response.data.eventTM;
                $.each(eventos, function(key, evento) {
                    dataLayer.push(evento);
                });
            }

            $('#carouselCreditoUsa').hide();
            $('#simulador').hide();

            if (response.data.siguiente_paso == 'sms') {
                
                var datos = [];
                datos['prospecto_id'] = response.data.prospecto_id;
                datos['celular'] = response.data.celular;
                envia_sms_solicitud(datos)
            }

        } else {

            swal.close();
            if (response.data.hasOwnProperty('errores')) {

                $.each(response.data.errores, function(key, error) {
                    $('#' + key + '-help').html(error);
                });
                $('#validacionesRegistro').html('- Verifica los campos en rojo.');

            } else if (response.data.hasOwnProperty('siguiente_paso')) {

                if (response.data.siguiente_paso == 'login') {
                    Swal.fire({
                        title: "Usuario Registrado",
                        text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                           $('#LoginForm').modal('show');
                       }
                   });
                }

            } else if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Recarga la página para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    confirmButtonText: 'Aceptar',
                    customClass: 'modalError',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                       location.reload();
                   }
               });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Registrando usuario...';
                      Swal.fire({
                         html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                     this.registroProspecto(datos);
                  }
              });

           }
        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

}


function verificar_codigo(datos) {
    $('#registro').hide();
    $('#verificar_codigo').show();
    axios.post('/solicitud/registro/sms', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'registro')
            $('#info_prestamo_personal').show();
            $('#show_celular').html(response.data.celular);
            swal.close();
        } else {
            if (response.data.actualizar_celular == true) {
                
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        var telefonoCelular = document.querySelector('#celularCorreccion');
                        telefonoCelular.addEventListener('keydown', keydownHandler);
                        telefonoCelular.addEventListener('input', inputHandler);
                    },
                    preConfirm: (e) => {
                        var validacion = validacionesCelular()
                        if (validacion == '') {
                          return true;
                        } else {
                          return false;
                        }
                    }
               }).then((result) => {
                  if (result.value) {
                      var datosCorreccion = $('#formCorreccionCelular').serializeArray();
                      datosCorreccion = getFormData(datosCorreccion);
                      datos['celular'] = datosCorreccion['celular'];
                      datos['actualizar_celular'] = 1;
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                      envia_sms(datos);
                  }
              });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });

}

function reenviar_sms() {

    var texto = 'Reenviando SMS...';
    Swal.fire({
       html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });

    axios.post('/solicitud/resend_code')
    .then(function (response) {
        Swal.close();
        $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
        if (response.data.success == true) {
        
            Swal.fire({
                title: 'Envío exitoso...',
                text: response.data.message,
                showConfirmButton: true,
                confirmButtonText: 'Aceptar',
                customClass: 'moda',
                allowOutsideClick: false
           });

        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                  }
              });

            } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'moda',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'moda',
            allowOutsideClick: false
       });

    });

}

function envia_sms_solicitud(datos) {

    axios.post('/solicitud/registro/sms', {
        celular: datos['celular'],
        prospecto_id: datos['prospecto_id']
    })
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
        if (response.data.success == true) {
            getForm('verificar_codigo', 'registro')
            $('#info_prestamo_personal').show();
            $('#show_celular').html(response.data.celular);
            swal.close();
        } else {

            if (response.data.actualizar_celular == true) {
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: 'Por favor ingresa un numero',
                    type: 'error',
                    html: response.data.modal,
                    showConfirmButton: true,
                    confirmButtonText: 'Reintentar',
                    customClass: 'modalError',
                    allowOutsideClick: false,
               }).then((result) => {
                  if (result.value) {
                      var texto = 'Enviando SMS...';
                      Swal.fire({
                         html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
                         customClass: 'modalLoading',
                         showCancelButton: false,
                         showConfirmButton:false,
                         allowOutsideClick: false
                     });
                  }
              });

            } else {

                $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });

            }

        }

    })
    .catch(function (error) {
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });
}

function valida_sms() {
    var datos = $('#formVerificarSMS').serializeArray();
    datos = getFormData(datos);

    var texto = 'Validando SMS...';
    Swal.fire({
       html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
   });

    axios.post('/solicitud/conf_sms_code', datos)
    .then(function (response) {

        $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
        if (response.data.success == true) {
            $('#verificar_codigo').hide();
            getForm(response.data.siguiente_paso.formulario, 'verificar_codigo')
            swal.close();
        } else {

            if (response.data.hasOwnProperty('reload')) {

                Swal.fire({
                    title: "La sesión expiro",
                    text: 'Inicia sesión para continuar',
                    type: 'error',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Iniciar Sesión',
                    customClass: 'modalError',
                    cancelButtonText: 'Cancelar',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        location.href = '/#loginModal';
                        location.reload(true);
                    }
               });

           } else {

                Swal.fire({
                    title: "Ooops.. Surgió un problema",
                    text: response.data.message,
                    type: 'error',
                    showConfirmButton: true,
                    customClass: 'modalError',
                    allowOutsideClick: false
               });

           }

        }

    })
    .catch(function (error) {

        $("html, body").animate({ scrollTop: $("#info_prestamo_personal").offset().top }, 1000);
        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });
}

function datos_empresa() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosEmpresa();
    $('#validacionesEmpresa').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosEmpresa').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosEmpresa').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosEmpresa').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_empresa', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            if (response.data.success == true) {
                if (response.data.hasOwnProperty('eventTM')) {
                    console.log(response.data.eventTM);
                    dataLayer.push(response.data.eventTM);
                }
                //getForm('datos_socios', 'datos_verficar_codigo')
                //datos_negocio()
                $('#datos_empresa').hide();
                $('#datos_negocio').show();
                swal.close();
            } else {

                if (response.data.stat == 'No Califica') {
                    Swal.fire({
                        title: response.data.stat,
                        html: response.data.modal,
                        showConfirmButton: true,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalError',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/';
                        }
                    });
                } else if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

function datos_negocio() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosNegocio();
    
    $('#validacionesNegocio').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosNegocio').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosNegocio').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosNegocio').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_empresa_info', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            if (response.data.success == true) {
                if (response.data.hasOwnProperty('eventTM')) {
                    console.log(response.data.eventTM);
                    dataLayer.push(response.data.eventTM);
                }
                //getForm('datos_socios', 'datos_verficar_codigo')
                var unico = $('input:radio[name="unico_dueno"]:checked').val();
                
                if (unico== 'Si') {
                    
                    $('#datos_negocio').hide();
                    $('#info_socios').show();
                    swal.close();

                } else {
                    
                    $('#datos_negocio').hide();
                    $('#datos_socio').show();
                    swal.close();
                }

            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

function datos_socio() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosSocio();
    $('#validacionesSocio').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosSocio').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosSocio').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosSocio').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_socio', datos)
        .then(function (response) {
            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            if (response.data.success == true) {
                if (response.data.hasOwnProperty('eventTM')) {
                    console.log(response.data.eventTM);
                    dataLayer.push(response.data.eventTM);
                }
                //getForm('datos_socios', 'datos_verficar_codigo')
                $('#datos_socio').hide();
                $('#info_socios').show();

                swal.close();
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

function info_socios() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesInfoSocio();
    $('#validacionesInfoSocio').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosInfoSocio').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosInfoSocio').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosInfoSocio').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_socio_info', datos)
        .then(function (response) {

            if (response.data.success == true) {
                console.log(response.data);
                if (response.data.hasOwnProperty('eventTM')) {
                    console.log(response.data.eventTM);
                    dataLayer.push(response.data.eventTM);
                }
                $('#info_socios').hide();
                $('#datos_credito').show();
                swal.close();
                //location.href ='/solicitud/oferta';
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

function datos_credito() {

    var clientId = 0;
    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

    var validaciones = validacionesDatosCredito();
    $('#validacionesCuentasCredito').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosCredito').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosCredito').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosCredito').serializeArray();
        datos = getFormData(datos);
        datos['clientId'] = clientId;

        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_credito', datos)
        .then(function (response) {
            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            if (response.data.success == true) {
                if (response.data.hasOwnProperty('eventTM')) {
                    console.log(response.data.eventTM);
                    
                    var eventos = response.data.eventTM;
                    
                        dataLayer.push(eventos);
                
                }
                swal.close();
                location.href ='/solicitud/oferta';
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesCuentasCredito').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

function puntaje() {
    $('#info_socios').hide();
    $('#puntaje').show();

    var validaciones = validacionesPuntaje();
    $('#validacionesPuntaje').html(validaciones);

    if (validaciones == '') {

        var disabledSelect = $('#formDatosPuntaje').find('option:disabled').removeAttr('disabled');
        var disabled = $('#formDatosPuntaje').find('input:disabled').removeAttr('disabled');

        var datos = $('#formDatosPuntaje').serializeArray();
        datos = getFormData(datos);
        disabledSelect.attr('disabled','disabled');
        disabled.attr('disabled','disabled');

        var texto = 'Guardando datos...';
        swal({
           html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
           customClass: 'modalLoading',
           showCancelButton: false,
           showConfirmButton:false,
           allowOutsideClick: false
       });

       axios.post('/solicitud/datos_puntaje', datos)
        .then(function (response) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            if (response.data.success == true) {
                //getForm('datos_socios', 'datos_verficar_codigo')
                puntaje()
                swal.close();
            } else {

                if (response.data.hasOwnProperty('errores')) {
                    swal.close();
                    $.each(response.data.errores, function(key, error) {
                        $('#' + key + '-help').html(error);
                    });
                    $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');

                } else if (response.data.hasOwnProperty('reload')) {

                    Swal.fire({
                        title: "La sesión expiro",
                        text: 'Inicia sesión para continuar',
                        type: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Iniciar Sesión',
                        customClass: 'modalError',
                        cancelButtonText: 'Cancelar',
                        allowOutsideClick: false
                    }).then((result) => {
                        if (result.value) {
                            location.href = '/#loginModal';
                            location.reload(true);
                        }
                   });

               } else {

                    Swal.fire({
                        title: "Ooops.. Surgió un problema",
                        text: response.data.message,
                        type: 'error',
                        showConfirmButton: true,
                        customClass: 'modalError',
                        allowOutsideClick: false
                    });

                }

            }

        })
        .catch(function (error) {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error.response.status + ': ' + error.response.responseText,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });

        });
    }
}

/*function getForm(siguientePaso = null, anterior = null){
    if (anterior != null) {
        $('#' + anterior).hide();
        $('#' + siguientePaso).show();
    }
}*/

function aprobacion() {
    $('#puntaje').hide();
    $('#aprobacion').show();
}

function initMap() {
    const componentForm = [
      'location',
      'locality',
      'administrative_area_level_1',
      'country',
      'postal_code',
    ];
    const autocompleteInput = document.getElementById('location');
    console.log(autocompleteInput);
    const autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
      fields: ["address_components", "geometry", "name"],
      types: ["address"],
    });
    console.log(autocomplete);
    autocomplete.addListener('place_changed', function () {
      const place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert('No details available for input: \'' + place.name + '\'');
        return;
      }
      fillInAddress(place);
    });

    function fillInAddress(place) {  // optional parameter
      const addressNameFormat = {
        'street_number': 'short_name',
        'route': 'long_name',
        'locality': 'long_name',
        'administrative_area_level_1': 'short_name',
        'country': 'long_name',
        'postal_code': 'short_name',
      };
      const getAddressComp = function (type) {
        for (const component of place.address_components) {
          if (component.types[0] === type) {
            return component[addressNameFormat[type]];
          }
        }
        return '';
      };
      document.getElementById('location').value = getAddressComp('street_number') + ' '
                + getAddressComp('route');
      for (const component of componentForm) {
        // Location field is handled separately above as it has different logic.
        if (component !== 'location') {
          document.getElementById(component).value = getAddressComp(component);
        }
      }
    }
}

function changePagoMensual(){

	mesesTexto = $("#plazo option:selected").text();
	mesesTexto = $("#plazo").find('option:selected').text();
    mesesTexto = mesesTexto.split(' ', 2);

	if ($.isNumeric(mesesTexto[0])) {


		if (mesesTexto[0] == 36) {
            prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
		    prestamoValue = prestamoValue * 1000;

            prestamoValue = prestamoValue * 0.045804;
		    finalidadValue = $( "#finalidad option:selected" ).text();

        } else {
            prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
		    prestamoValue = prestamoValue * 1000;

            prestamoValue = prestamoValue * 0.039578;
		    finalidadValue = $( "#finalidad option:selected" ).text();
        }

		if(prestamoValue != null){

			$("#pago_estimado").text(moneyFormat(parseInt(prestamoValue)));

			$('#pago_estimado').val(moneyFormat(parseInt(prestamoValue)));

		}
	}

}

function aceptar_oferta() {

    var clientId = 0;
    var texto = 'Guardando oferta...'
    Swal.fire({
        html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton:false,
        allowOutsideClick: false
    });

    ga(function(tracker) {
    	clientId = tracker.get('clientId');
    });

     axios.post('/solicitud/status_oferta', {
         'status_oferta' : 'Oferta Aceptada',
         'clientId'      : clientId

     })
     .then(function (response) {

         if (response.data.hasOwnProperty('reload')) {

             Swal.fire({
                 title: "La sesión expiro",
                 text: 'Inicia sesión para continuar',
                 type: 'error',
                 showConfirmButton: true,
                 showCancelButton: true,
                 confirmButtonText: 'Iniciar Sesión',
                 customClass: 'modalError',
                 cancelButtonText: 'Cancelar',
                 allowOutsideClick: false
             }).then((result) => {
                 if (result.value) {
                     location.href = '/#loginModal';
                     location.reload(true);
                 }
            });

        } else {

             if (response.data.hasOwnProperty('eventTM')) {
                 var eventos = response.data.eventTM;
                 $.each(eventos, function(key, evento) {
                     dataLayer.push(evento);
                 });
             }

             if (response.data.carga_documentos == true) {
                 console.log(response.data);
                 var siguiente_paso = response.data.siguiente_paso
                 location.href = '/webapp/' + siguiente_paso;
             } else {

                 Swal.fire({
                     html: response.data.modal,
                     showConfirmButton: false,
                     allowOutsideClick: false,
                     customClass: 'modalstatus'
                 });
             }

         }

     })
     .catch(function (error) {

         Swal.fire({
             title: "Ooops.. Surgió un problema",
             text: error.response.status + ': ' + error.response.responseText,
             type: 'error',
             showConfirmButton: true,
             customClass: 'modalError',
             allowOutsideClick: false
        });

     });
}

$(document).on('click', "#rechazarOferta", function() {

    $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {

    if(select.value == 'Otro') {
        $('#textOtroRechazo').show();
        $('#textOtroRechazo').val('');
        $('#textOtroRechazo').focus();
    } else {
        $('#textOtroRechazo').hide();
    }

}

$(document).on('click', "#cancelarRechazarOferta", function() {

    $('#botonesRechazo').hide();
    $('#seccionRechazo').hide();
    $('#tituloRechazo').hide();
    $('#textOtroRechazo').hide();
    $("#motivo_rechazo").val('SELECCIONA');
    $('#textOtroRechazo').val('');

});

$(document).on('click', "#confirmaRechazarOferta", function() {

    var motivo_rechazo = $('#motivo_rechazo').val();
    var descripcion_otro = $('#textOtroRechazo').val();
    var modelo = $('#oferta_modelo').val();

    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });
    
    var texto = 'Guardando status de la oferta...';
    Swal.fire({
        html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton:false,
        allowOutsideClick: false
    });

    axios.post('/solicitud/rechazar_oferta', {
        'status_oferta'     : 'Oferta Rechazada',
        'modelo'            : modelo,
        'motivo'            : true,
        'motivo_rechazo'    : motivo_rechazo,
        'descripcion_otro'  : descripcion_otro,
        'clientId'          : clientId
    })
    .then(function (response) {

        if (response.data.success == true) {
            window.location.href = '/';
        } else {
            Swal.fire({
                title: "Ooops.. Surgió un problema",
                text: error,
                type: 'error',
                showConfirmButton: true,
                customClass: 'modalError',
                allowOutsideClick: false
           });
        }
    })
    .catch(function (error) {

        Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: error.response.status + ': ' + error.response.responseText,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
       });

    });

});

function moneyFormat(value, decimals){
    if (decimals === undefined) { decimals = 2; }
    var price = (value).toFixed(decimals);
    return '$'+price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function money(){
    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    console.log(prestamoValue);
    $("#monto_prestamo").text(moneyFormat(parseInt(prestamoValue)));
    $("#monto_prestamo").val(moneyFormat(parseInt(prestamoValue)));

}

function getFormData(datos){
    var unindexed_array = datos;
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

$("input").keydown(function (e){
    // Capturamos qué telca ha sido
    var keyCode= e.which;
    // Si la tecla es el Intro/Enter
    if (keyCode == 13){
      // Evitamos que se ejecute eventos
      event.preventDefault();
      // Devolvemos falso
      return false;
    }
  });

function getForm(siguientePaso = null, anterior = null) {


	axios.get('/getForm', {
      	params: {
      		paso: siguientePaso,
		}
	})
	.then(function(response) {
        //$('#loading-form').hide();
		$('#' + response.data.formulario).show();
		if (anterior != null) {
			$('#' + anterior).hide();
		} else if (response.data.oculta != null && response.data.nueva_solicitud == false) {

            $('#' + response.data.oculta).hide();

        }

        if (response.data.formulario == 'verificar_codigo') {

            $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
            $('#nombreProspecto').html(response.data.nombre_prospecto);
            playinterval();
        }

        if (response.data.view == 'view') {
            html(response.data.modal);
        }

        if (response.data.nueva_solicitud == true) {
            $('#nueva_solicitud').val('true');
        }

        if (response.data.hasOwnProperty('ultimo_status')) {
            if (response.data.ultimo_status == 'Registro') {
                $('#nombreProspecto').html(response.data.nombre_prospecto);
            }
        }

        if (response.data.hasOwnProperty('sesionIniciada')) {
            $('#nombreProspecto').html(response.data.nombre_prospecto);
        }

        if (response.data.redirect != null) {
            var texto = 'Faltan documentos por completar </br> Redirigiendo...';
            Swal.fire({
               html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
               customClass: 'modalLoading',
               showCancelButton: false,
               showConfirmButton:false,
               allowOutsideClick: false
           });
            location.href = response.data.redirect;
        } else if (response.data.mostrar_oferta == true) {

            location.href = '/solicitud/oferta';

        }

	})
	.catch(function(error) {

	});

}
