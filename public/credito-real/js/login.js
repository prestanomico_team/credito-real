function iniciarSesion() {

    $("#loginError").html('');
    var texto = 'Iniciando sesión...';
    Swal.fire({
       html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>'+ texto +'</p></center>',
       customClass: 'modalLoading',
       showCancelButton: false,
       showConfirmButton:false,
       allowOutsideClick: false
    });

    var datos = $('#formLogin *').serializeArray();
    datos = getFormData(datos);

    var clientId = 0;
    ga(function(tracker) {
        clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;

    axios.post('/login/prospecto', datos)
    .then(function (response) {

        if (response.data.success == true) {

            var scrolTop = '#simulador';
            $('#LoginForm').modal('hide');
            $('#formRegistro').trigger("reset");
            $('#formSimulador').trigger("reset");
            $('#formVerificarSMS').trigger("reset");
            $('#prestamoPersonalPagoFIjo').html('$0.00');

            $('.sesionIniciada').show()
            $('.iniciarSesion').hide()

            if (response.data.hasOwnProperty('eventTM')) {
                ga(function(tracker) {
                    tracker.set('dimension1', response.data.eventTM[0].clientId);
                    tracker.set('dimension2', response.data.eventTM[0].userId);
                    tracker.set('dimension3', response.data.eventTM[0].solicId);
                });
                ga('send', 'pageview');
            }

            if (response.data.nueva_solicitud == false) {
                $('#' + response.data.formulario).show();
                $('#' + response.data.oculta).hide();

                swal.close();
                $("html, body").animate({ scrollTop: $("#AppWeb").offset().top }, 1000);
                $('#info_prestamo_personal').show();
                $("#info_general_prestamo").text(response.data.prestamo);
                $("#info_general_plazo").text(response.data.plazo);
                $("#info_general_finalidad").text(response.data.finalidad);

            } else {
                $('#' + response.data.formulario).show();
                $('#formRegistro').hide();
                swal.close();
                $('#nueva_solicitud').val('true');
            }

            $('#nombreProspecto').html(response.data.nombre_prospecto);
            $('#show_celular').html(response.data.celular);

            if (response.data.modal != null) {

                if (response.data.hasOwnProperty('mostrar_oferta')) {
                    if (response.data.hasOwnProperty('view')) {
                        if (response.data.view != '') {
                            html(response.data.view);
                        }
                    }

                    if (response.data.mostrar_oferta == true) {

                    } else {
                        Swal.fire({
                            html: response.data.modal,
                            showConfirmButton: true,
                            allowOutsideClick: false,
                            confirmButtonText: 'Aceptar',
                            customClass: 'modalstatus'
                        }).then((result) => {
                           if (result.value) {
                              $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                           }
                       });
                   }

                } else {

                    Swal.fire({
                        html: response.data.modal,
                        showConfirmButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Aceptar',
                        customClass: 'modalstatus'
                    }).then((result) => {
                       if (result.value) {
                          $("html, body").animate({ scrollTop: $(scrolTop).offset().top - 50 }, 1000);
                       }
                   });

                }


            }

        } else {

            $("#loginError").html(response.data.message);
            swal.close();
        }
    })
    .catch(function (error) {

        var descripcionError = '';
        if (error.hasOwnProperty('response')) {

            if (error.response.hasOwnProperty('data')) {
                descripcionError = error.response.data.message;
            } else {
                descripcionError = error.response.status + ': ' + error.response.responseText;
            }

        } else {
            descripcionError = error;
        }

        $("#loginError").html(descripcionError);
        swal.close();

    });

}
