$ = jQuery.noConflict();

$(document).ready(function(){
	$('#BtnMovil').on('click', function(){
		$('.SideNav').addClass('SideNavActive');
		$('.SidenavOverlay').css("display", "block");
	});
	$('.SidenavOverlay').on('click', function(){
		$('.SideNav').removeClass('SideNavActive');
		$('.SidenavOverlay').css("display", "none");
	});
	 

	// Scroll en la web
	$('#irArriba').click(function() { 
		$('html, body').animate({
			scrollTop: 0
		}, 600);
		return false;
	});

	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('#irArriba').slideDown(400);
		} else {
			$('#irArriba').slideUp(400);
		}
	});

	// $('#ContentBuscar').hide();
	$('#btn_buscar').on('click', function(){
		$('#ContentBuscar').show('400');

	});
	$('#CloseSearch').on('click', function(){
		$('#ContentBuscar').hide();
	});

	// Al hacer scroll, el menu aumenta o disminuye

	const nav = document.querySelector('#MenuPage');
	const topOfNav = nav.offsetTop;
	function fixnav() {
		if(window,scrollY >= topOfNav) {
			// document.body.style.paddingTop = nav.offsetHeight + 'px';
			document.body.classList.add('fixed-nav');
			// document.body.classList.add('animate__animated animate__fadeIn');
		} else {
			document.body.style.paddingTop = 0;
			document.body.classList.remove('fixed-nav');
			// document.body.classList.remove('animate__animated animate__fadeIn');
		}

	}
	window.addEventListener('scroll', fixnav);


});