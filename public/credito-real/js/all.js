function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

$("form").bind("keypress", function (e) {
  if (e.keyCode == 13) {
    return false;
  }
});

function focusSimulador() {
  $("html, body").animate({
    scrollTop: $("#simulador").offset().top
  }, 1000);
}

function validarSimulador() {
  var datos = $('#formSimulador').serializeArray();
  datos = getFormData(datos);
  prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
  prestamoValue = prestamoValue * 1000;
  datos['monto_prestamo'] = prestamoValue;
  var validaciones = validacionesSimulador();
  var nuevaSolicitud = $('#nueva_solicitud').val();
  $('#validacionesSimulador').html(validaciones);
  var datos_faltantes = '';

  if (validaciones == '') {
    axios.post('/validaciones', {
      datos: datos,
      formulario: 'simulador'
    }).then(function (response) {
      if (response.data.success == false) {
        if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Recarga la página para continuar',
            type: 'error',
            showConfirmButton: true,
            confirmButtonText: 'Aceptar',
            customClass: 'modalError',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.reload();
            }
          });
        } else {
          datos_faltantes += '- Verifica los campos en rojo.';
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
        }
      } else {
        if (nuevaSolicitud == 'false' && validaciones == '') {
          $('#carouselCreditoUsa').hide();
          $('#simulador').hide();
          $('#aplicar').hide();
          $('#proceso').hide();
          $('#leasing').hide();
          $('#registro').show();
          getForm('registro', 'simulador');
          $("html, body").animate({
            scrollTop: $("#AppWeb").offset().top
          }, 1000);
        } else if (nuevaSolicitud == 'true' && validaciones == '') {
          registroNuevaSolicitud();
        }
      }
    })["catch"](function (error) {
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function registro() {
  var datos = $('#formSimulador,#formRegistro').serializeArray();
  datos = getFormData(datos);
  pagoEstimado = $("#pago_estimado").maskMoney('unmasked')[0];
  datos['pago_estimado'] = pagoEstimado;
  var validaciones = validacionesRegistro();
  $('#validacionesRegistro').html(validaciones);

  if (validaciones == '') {
    var texto = 'Registrando usuario...';
    Swal.fire({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    var clientId = 0;
    ga(function (tracker) {
      clientId = tracker.get('clientId');
    });
    datos['clientId'] = clientId;
    axios.post('/solicitud/registro', datos).then(function (response) {
      var _this = this;

      if (response.data.success == true) {
        $("html, body").animate({
          scrollTop: $("#AppWeb").offset().top
        }, 1000);
        $("#aplica_ahora").hide();
        datos['prospecto_id'] = response.data.prospecto_id;
        $('.sesionIniciada').show();
        $('.iniciarSesion').hide();

        if (response.data.hasOwnProperty('eventTM')) {
          var eventos = response.data.eventTM;
          $.each(eventos, function (key, evento) {
            dataLayer.push(evento);
          });
        }

        if (response.data.siguiente_paso == 'sms') {
          swal.close();
          verificar_codigo(datos);
        }
      } else {
        swal.close();

        if (response.data.hasOwnProperty('errores')) {
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesRegistro').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('siguiente_paso')) {
          if (response.data.siguiente_paso == 'login') {
            Swal.fire({
              title: "Usuario Registrado",
              text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
              type: 'error',
              showConfirmButton: true,
              showCancelButton: true,
              confirmButtonText: 'Iniciar Sesión',
              customClass: 'modalError',
              cancelButtonText: 'Cancelar',
              allowOutsideClick: false
            }).then(function (result) {
              if (result.value) {
                $('#LoginForm').modal('show');
              }
            });
          }
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Recarga la página para continuar',
            type: 'error',
            showConfirmButton: true,
            confirmButtonText: 'Aceptar',
            customClass: 'modalError',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.reload();
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Reintentar',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              var texto = 'Registrando usuario...';
              Swal.fire({
                html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
                customClass: 'modalLoading',
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false
              });

              _this.registroProspecto(datos);
            }
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#pasosSolicitud").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function registroNuevaSolicitud() {
  var datos = $('#formSimulador').serializeArray();
  datos = getFormData(datos);
  pagoEstimado = $("#pago_estimado").maskMoney('unmasked')[0];
  datos['pago_estimado'] = pagoEstimado;
  var texto = 'Registrando solicitud...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  datos['clientId'] = clientId;
  axios.post('/solicitud/registro/solicitud', datos).then(function (response) {
    var _this2 = this;

    if (response.data.success == true) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      $("#aplica_ahora").hide();
      $('.sesionIniciada').show();
      $('.iniciarSesion').hide();

      if (response.data.hasOwnProperty('eventTM')) {
        var eventos = response.data.eventTM;
        $.each(eventos, function (key, evento) {
          dataLayer.push(evento);
        });
      }

      $('#carouselCreditoUsa').hide();
      $('#simulador').hide();

      if (response.data.siguiente_paso == 'sms') {
        var datos = [];
        datos['prospecto_id'] = response.data.prospecto_id;
        datos['celular'] = response.data.celular;
        envia_sms_solicitud(datos);
      }
    } else {
      swal.close();

      if (response.data.hasOwnProperty('errores')) {
        $.each(response.data.errores, function (key, error) {
          $('#' + key + '-help').html(error);
        });
        $('#validacionesRegistro').html('- Verifica los campos en rojo.');
      } else if (response.data.hasOwnProperty('siguiente_paso')) {
        if (response.data.siguiente_paso == 'login') {
          Swal.fire({
            title: "Usuario Registrado",
            text: 'Ya existe un usuario con el email que intentas registrar. Inicia sesión para continuar con tu solicitud',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              $('#LoginForm').modal('show');
            }
          });
        }
      } else if (response.data.hasOwnProperty('reload')) {
        Swal.fire({
          title: "La sesión expiro",
          text: 'Recarga la página para continuar',
          type: 'error',
          showConfirmButton: true,
          confirmButtonText: 'Aceptar',
          customClass: 'modalError',
          allowOutsideClick: false
        }).then(function (result) {
          if (result.value) {
            location.reload();
          }
        });
      } else {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: response.data.message,
          type: 'error',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonText: 'Reintentar',
          customClass: 'modalError',
          cancelButtonText: 'Cancelar',
          allowOutsideClick: false
        }).then(function (result) {
          if (result.value) {
            var texto = 'Registrando usuario...';
            Swal.fire({
              html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
              customClass: 'modalLoading',
              showCancelButton: false,
              showConfirmButton: false,
              allowOutsideClick: false
            });

            _this2.registroProspecto(datos);
          }
        });
      }
    }
  })["catch"](function (error) {
    $("html, body").animate({
      scrollTop: $("#pasosSolicitud").offset().top
    }, 1000);
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'modalError',
      allowOutsideClick: false
    });
  });
}

function verificar_codigo(datos) {
  $('#registro').hide();
  $('#verificar_codigo').show();
  axios.post('/solicitud/registro/sms', datos).then(function (response) {
    $("html, body").animate({
      scrollTop: $("#AppWeb").offset().top
    }, 1000);

    if (response.data.success == true) {
      getForm('verificar_codigo', 'registro');
      $('#info_prestamo_personal').show();
      $('#show_celular').html(response.data.celular);
      swal.close();
    } else {
      if (response.data.actualizar_celular == true) {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: 'Por favor ingresa un numero',
          type: 'error',
          html: response.data.modal,
          showConfirmButton: true,
          confirmButtonText: 'Reintentar',
          customClass: 'modalError',
          allowOutsideClick: false,
          onBeforeOpen: function onBeforeOpen() {
            var telefonoCelular = document.querySelector('#celularCorreccion');
            telefonoCelular.addEventListener('keydown', keydownHandler);
            telefonoCelular.addEventListener('input', inputHandler);
          },
          preConfirm: function preConfirm(e) {
            var validacion = validacionesCelular();

            if (validacion == '') {
              return true;
            } else {
              return false;
            }
          }
        }).then(function (result) {
          if (result.value) {
            var datosCorreccion = $('#formCorreccionCelular').serializeArray();
            datosCorreccion = getFormData(datosCorreccion);
            datos['celular'] = datosCorreccion['celular'];
            datos['actualizar_celular'] = 1;
            var texto = 'Enviando SMS...';
            Swal.fire({
              html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
              customClass: 'modalLoading',
              showCancelButton: false,
              showConfirmButton: false,
              allowOutsideClick: false
            });
            envia_sms(datos);
          }
        });
      } else {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: response.data.message,
          type: 'error',
          showConfirmButton: true,
          customClass: 'moda',
          allowOutsideClick: false
        });
      }
    }
  })["catch"](function (error) {
    $("html, body").animate({
      scrollTop: $("#info_prestamo_personal").offset().top
    }, 1000);
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'moda',
      allowOutsideClick: false
    });
  });
}

function reenviar_sms() {
  var texto = 'Reenviando SMS...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  axios.post('/solicitud/resend_code').then(function (response) {
    Swal.close();
    $("html, body").animate({
      scrollTop: $("#AppWeb").offset().top
    }, 1000);

    if (response.data.success == true) {
      Swal.fire({
        title: 'Envío exitoso...',
        text: response.data.message,
        showConfirmButton: true,
        confirmButtonText: 'Aceptar',
        customClass: 'moda',
        allowOutsideClick: false
      });
    } else {
      if (response.data.actualizar_celular == true) {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: 'Por favor ingresa un numero',
          type: 'error',
          html: response.data.modal,
          showConfirmButton: true,
          confirmButtonText: 'Reintentar',
          customClass: 'modalError',
          allowOutsideClick: false
        }).then(function (result) {
          if (result.value) {
            var texto = 'Enviando SMS...';
            Swal.fire({
              html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
              customClass: 'modalLoading',
              showCancelButton: false,
              showConfirmButton: false,
              allowOutsideClick: false
            });
          }
        });
      } else {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: response.data.message,
          type: 'error',
          showConfirmButton: true,
          customClass: 'moda',
          allowOutsideClick: false
        });
      }
    }
  })["catch"](function (error) {
    $("html, body").animate({
      scrollTop: $("#AppWeb").offset().top
    }, 1000);
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'moda',
      allowOutsideClick: false
    });
  });
}

function envia_sms_solicitud(datos) {
  axios.post('/solicitud/registro/sms', {
    celular: datos['celular'],
    prospecto_id: datos['prospecto_id']
  }).then(function (response) {
    $("html, body").animate({
      scrollTop: $("#AppWeb").offset().top
    }, 1000);

    if (response.data.success == true) {
      getForm('verificar_codigo', 'registro');
      $('#info_prestamo_personal').show();
      $('#show_celular').html(response.data.celular);
      swal.close();
    } else {
      if (response.data.actualizar_celular == true) {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: 'Por favor ingresa un numero',
          type: 'error',
          html: response.data.modal,
          showConfirmButton: true,
          confirmButtonText: 'Reintentar',
          customClass: 'modalError',
          allowOutsideClick: false
        }).then(function (result) {
          if (result.value) {
            var texto = 'Enviando SMS...';
            Swal.fire({
              html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
              customClass: 'modalLoading',
              showCancelButton: false,
              showConfirmButton: false,
              allowOutsideClick: false
            });
          }
        });
      } else {
        $("html, body").animate({
          scrollTop: $("#AppWeb").offset().top
        }, 1000);
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: response.data.message,
          type: 'error',
          showConfirmButton: true,
          customClass: 'modalError',
          allowOutsideClick: false
        });
      }
    }
  })["catch"](function (error) {
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'modalError',
      allowOutsideClick: false
    });
  });
}

function valida_sms() {
  var datos = $('#formVerificarSMS').serializeArray();
  datos = getFormData(datos);
  var texto = 'Validando SMS...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  axios.post('/solicitud/conf_sms_code', datos).then(function (response) {
    $("html, body").animate({
      scrollTop: $("#AppWeb").offset().top
    }, 1000);

    if (response.data.success == true) {
      $('#verificar_codigo').hide();
      getForm(response.data.siguiente_paso.formulario, 'verificar_codigo');
      swal.close();
    } else {
      if (response.data.hasOwnProperty('reload')) {
        Swal.fire({
          title: "La sesión expiro",
          text: 'Inicia sesión para continuar',
          type: 'error',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonText: 'Iniciar Sesión',
          customClass: 'modalError',
          cancelButtonText: 'Cancelar',
          allowOutsideClick: false
        }).then(function (result) {
          if (result.value) {
            location.href = '/#loginModal';
            location.reload(true);
          }
        });
      } else {
        Swal.fire({
          title: "Ooops.. Surgió un problema",
          text: response.data.message,
          type: 'error',
          showConfirmButton: true,
          customClass: 'modalError',
          allowOutsideClick: false
        });
      }
    }
  })["catch"](function (error) {
    $("html, body").animate({
      scrollTop: $("#info_prestamo_personal").offset().top
    }, 1000);
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'modalError',
      allowOutsideClick: false
    });
  });
}

function datos_empresa() {
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var validaciones = validacionesDatosEmpresa();
  $('#validacionesEmpresa').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosEmpresa').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosEmpresa').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosEmpresa').serializeArray();
    datos = getFormData(datos);
    datos['clientId'] = clientId;
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_empresa', datos).then(function (response) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);

      if (response.data.success == true) {
        if (response.data.hasOwnProperty('eventTM')) {
          console.log(response.data.eventTM);
          dataLayer.push(response.data.eventTM);
        } //getForm('datos_socios', 'datos_verficar_codigo')
        //datos_negocio()


        $('#datos_empresa').hide();
        $('#datos_negocio').show();
        swal.close();
      } else {
        if (response.data.stat == 'No Califica') {
          Swal.fire({
            title: response.data.stat,
            html: response.data.modal,
            showConfirmButton: true,
            confirmButtonText: 'Aceptar',
            customClass: 'modalError',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/';
            }
          });
        } else if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function datos_negocio() {
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var validaciones = validacionesDatosNegocio();
  $('#validacionesNegocio').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosNegocio').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosNegocio').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosNegocio').serializeArray();
    datos = getFormData(datos);
    datos['clientId'] = clientId;
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_empresa_info', datos).then(function (response) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);

      if (response.data.success == true) {
        if (response.data.hasOwnProperty('eventTM')) {
          console.log(response.data.eventTM);
          dataLayer.push(response.data.eventTM);
        } //getForm('datos_socios', 'datos_verficar_codigo')


        var unico = $('input:radio[name="unico_dueno"]:checked').val();

        if (unico == 'Si') {
          $('#datos_negocio').hide();
          $('#info_socios').show();
          swal.close();
        } else {
          $('#datos_negocio').hide();
          $('#datos_socio').show();
          swal.close();
        }
      } else {
        if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function datos_socio() {
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var validaciones = validacionesDatosSocio();
  $('#validacionesSocio').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosSocio').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosSocio').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosSocio').serializeArray();
    datos = getFormData(datos);
    datos['clientId'] = clientId;
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_socio', datos).then(function (response) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);

      if (response.data.success == true) {
        if (response.data.hasOwnProperty('eventTM')) {
          console.log(response.data.eventTM);
          dataLayer.push(response.data.eventTM);
        } //getForm('datos_socios', 'datos_verficar_codigo')


        $('#datos_socio').hide();
        $('#info_socios').show();
        swal.close();
      } else {
        if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function info_socios() {
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var validaciones = validacionesInfoSocio();
  $('#validacionesInfoSocio').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosInfoSocio').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosInfoSocio').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosInfoSocio').serializeArray();
    datos = getFormData(datos);
    datos['clientId'] = clientId;
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_socio_info', datos).then(function (response) {
      if (response.data.success == true) {
        console.log(response.data);

        if (response.data.hasOwnProperty('eventTM')) {
          console.log(response.data.eventTM);
          dataLayer.push(response.data.eventTM);
        }

        $('#info_socios').hide();
        $('#datos_credito').show();
        swal.close(); //location.href ='/solicitud/oferta';
      } else {
        if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function datos_credito() {
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var validaciones = validacionesDatosCredito();
  $('#validacionesCuentasCredito').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosCredito').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosCredito').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosCredito').serializeArray();
    datos = getFormData(datos);
    datos['clientId'] = clientId;
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_credito', datos).then(function (response) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);

      if (response.data.success == true) {
        if (response.data.hasOwnProperty('eventTM')) {
          console.log(response.data.eventTM);
          var eventos = response.data.eventTM;
          dataLayer.push(eventos);
        }

        swal.close();
        location.href = '/solicitud/oferta';
      } else {
        if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesCuentasCredito').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}

function puntaje() {
  $('#info_socios').hide();
  $('#puntaje').show();
  var validaciones = validacionesPuntaje();
  $('#validacionesPuntaje').html(validaciones);

  if (validaciones == '') {
    var disabledSelect = $('#formDatosPuntaje').find('option:disabled').removeAttr('disabled');
    var disabled = $('#formDatosPuntaje').find('input:disabled').removeAttr('disabled');
    var datos = $('#formDatosPuntaje').serializeArray();
    datos = getFormData(datos);
    disabledSelect.attr('disabled', 'disabled');
    disabled.attr('disabled', 'disabled');
    var texto = 'Guardando datos...';
    swal({
      html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
      customClass: 'modalLoading',
      showCancelButton: false,
      showConfirmButton: false,
      allowOutsideClick: false
    });
    axios.post('/solicitud/datos_puntaje', datos).then(function (response) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);

      if (response.data.success == true) {
        //getForm('datos_socios', 'datos_verficar_codigo')
        puntaje();
        swal.close();
      } else {
        if (response.data.hasOwnProperty('errores')) {
          swal.close();
          $.each(response.data.errores, function (key, error) {
            $('#' + key + '-help').html(error);
          });
          $('#validacionesDatosEmpresa').html('- Verifica los campos en rojo.');
        } else if (response.data.hasOwnProperty('reload')) {
          Swal.fire({
            title: "La sesión expiro",
            text: 'Inicia sesión para continuar',
            type: 'error',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Iniciar Sesión',
            customClass: 'modalError',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false
          }).then(function (result) {
            if (result.value) {
              location.href = '/#loginModal';
              location.reload(true);
            }
          });
        } else {
          Swal.fire({
            title: "Ooops.. Surgió un problema",
            text: response.data.message,
            type: 'error',
            showConfirmButton: true,
            customClass: 'modalError',
            allowOutsideClick: false
          });
        }
      }
    })["catch"](function (error) {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error.response.status + ': ' + error.response.responseText,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    });
  }
}
/*function getForm(siguientePaso = null, anterior = null){
    if (anterior != null) {
        $('#' + anterior).hide();
        $('#' + siguientePaso).show();
    }
}*/


function aprobacion() {
  $('#puntaje').hide();
  $('#aprobacion').show();
}

function initMap() {
  var componentForm = ['location', 'locality', 'administrative_area_level_1', 'country', 'postal_code'];
  var autocompleteInput = document.getElementById('location');
  console.log(autocompleteInput);
  var autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
    fields: ["address_components", "geometry", "name"],
    types: ["address"]
  });
  console.log(autocomplete);
  autocomplete.addListener('place_changed', function () {
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert('No details available for input: \'' + place.name + '\'');
      return;
    }

    fillInAddress(place);
  });

  function fillInAddress(place) {
    // optional parameter
    var addressNameFormat = {
      'street_number': 'short_name',
      'route': 'long_name',
      'locality': 'long_name',
      'administrative_area_level_1': 'short_name',
      'country': 'long_name',
      'postal_code': 'short_name'
    };

    var getAddressComp = function getAddressComp(type) {
      var _iterator = _createForOfIteratorHelper(place.address_components),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var component = _step.value;

          if (component.types[0] === type) {
            return component[addressNameFormat[type]];
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return '';
    };

    document.getElementById('location').value = getAddressComp('street_number') + ' ' + getAddressComp('route');

    var _iterator2 = _createForOfIteratorHelper(componentForm),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var component = _step2.value;

        // Location field is handled separately above as it has different logic.
        if (component !== 'location') {
          document.getElementById(component).value = getAddressComp(component);
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }
}

function changePagoMensual() {
  mesesTexto = $("#plazo option:selected").text();
  mesesTexto = $("#plazo").find('option:selected').text();
  mesesTexto = mesesTexto.split(' ', 2);

  if ($.isNumeric(mesesTexto[0])) {
    if (mesesTexto[0] == 36) {
      prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
      prestamoValue = prestamoValue * 1000;
      prestamoValue = prestamoValue * 0.045804;
      finalidadValue = $("#finalidad option:selected").text();
    } else {
      prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
      prestamoValue = prestamoValue * 1000;
      prestamoValue = prestamoValue * 0.039578;
      finalidadValue = $("#finalidad option:selected").text();
    }

    if (prestamoValue != null) {
      $("#pago_estimado").text(moneyFormat(parseInt(prestamoValue)));
      $('#pago_estimado').val(moneyFormat(parseInt(prestamoValue)));
    }
  }
}

function aceptar_oferta() {
  var clientId = 0;
  var texto = 'Guardando oferta...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  axios.post('/solicitud/status_oferta', {
    'status_oferta': 'Oferta Aceptada',
    'clientId': clientId
  }).then(function (response) {
    if (response.data.hasOwnProperty('reload')) {
      Swal.fire({
        title: "La sesión expiro",
        text: 'Inicia sesión para continuar',
        type: 'error',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Iniciar Sesión',
        customClass: 'modalError',
        cancelButtonText: 'Cancelar',
        allowOutsideClick: false
      }).then(function (result) {
        if (result.value) {
          location.href = '/#loginModal';
          location.reload(true);
        }
      });
    } else {
      if (response.data.hasOwnProperty('eventTM')) {
        var eventos = response.data.eventTM;
        $.each(eventos, function (key, evento) {
          dataLayer.push(evento);
        });
      }

      if (response.data.carga_documentos == true) {
        console.log(response.data);
        var siguiente_paso = response.data.siguiente_paso;
        location.href = '/webapp/' + siguiente_paso;
      } else {
        Swal.fire({
          html: response.data.modal,
          showConfirmButton: false,
          allowOutsideClick: false,
          customClass: 'modalstatus'
        });
      }
    }
  })["catch"](function (error) {
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'modalError',
      allowOutsideClick: false
    });
  });
}

$(document).on('click', "#rechazarOferta", function () {
  $('#botonesRechazo').show();
});

function cambioMotivoRechazo(select) {
  if (select.value == 'Otro') {
    $('#textOtroRechazo').show();
    $('#textOtroRechazo').val('');
    $('#textOtroRechazo').focus();
  } else {
    $('#textOtroRechazo').hide();
  }
}

$(document).on('click', "#cancelarRechazarOferta", function () {
  $('#botonesRechazo').hide();
  $('#seccionRechazo').hide();
  $('#tituloRechazo').hide();
  $('#textOtroRechazo').hide();
  $("#motivo_rechazo").val('SELECCIONA');
  $('#textOtroRechazo').val('');
});
$(document).on('click', "#confirmaRechazarOferta", function () {
  var motivo_rechazo = $('#motivo_rechazo').val();
  var descripcion_otro = $('#textOtroRechazo').val();
  var modelo = $('#oferta_modelo').val();
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  var texto = 'Guardando status de la oferta...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  axios.post('/solicitud/rechazar_oferta', {
    'status_oferta': 'Oferta Rechazada',
    'modelo': modelo,
    'motivo': true,
    'motivo_rechazo': motivo_rechazo,
    'descripcion_otro': descripcion_otro,
    'clientId': clientId
  }).then(function (response) {
    if (response.data.success == true) {
      window.location.href = '/';
    } else {
      Swal.fire({
        title: "Ooops.. Surgió un problema",
        text: error,
        type: 'error',
        showConfirmButton: true,
        customClass: 'modalError',
        allowOutsideClick: false
      });
    }
  })["catch"](function (error) {
    Swal.fire({
      title: "Ooops.. Surgió un problema",
      text: error.response.status + ': ' + error.response.responseText,
      type: 'error',
      showConfirmButton: true,
      customClass: 'modalError',
      allowOutsideClick: false
    });
  });
});

function moneyFormat(value, decimals) {
  if (decimals === undefined) {
    decimals = 2;
  }

  var price = value.toFixed(decimals);
  return '$' + price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function money() {
  prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
  prestamoValue = prestamoValue * 1000;
  console.log(prestamoValue);
  $("#monto_prestamo").text(moneyFormat(parseInt(prestamoValue)));
  $("#monto_prestamo").val(moneyFormat(parseInt(prestamoValue)));
}

function getFormData(datos) {
  var unindexed_array = datos;
  var indexed_array = {};
  $.map(unindexed_array, function (n, i) {
    indexed_array[n['name']] = n['value'];
  });
  return indexed_array;
}

$("input").keydown(function (e) {
  // Capturamos qué telca ha sido
  var keyCode = e.which; // Si la tecla es el Intro/Enter

  if (keyCode == 13) {
    // Evitamos que se ejecute eventos
    event.preventDefault(); // Devolvemos falso

    return false;
  }
});

function getForm() {
  var siguientePaso = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var anterior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  axios.get('/getForm', {
    params: {
      paso: siguientePaso
    }
  }).then(function (response) {
    //$('#loading-form').hide();
    $('#' + response.data.formulario).show();

    if (anterior != null) {
      $('#' + anterior).hide();
    } else if (response.data.oculta != null && response.data.nueva_solicitud == false) {
      $('#' + response.data.oculta).hide();
    }

    if (response.data.formulario == 'verificar_codigo') {
      $("html, body").animate({
        scrollTop: $("#AppWeb").offset().top
      }, 1000);
      $('#nombreProspecto').html(response.data.nombre_prospecto);
      playinterval();
    }

    if (response.data.view == 'view') {
      html(response.data.modal);
    }

    if (response.data.nueva_solicitud == true) {
      $('#nueva_solicitud').val('true');
    }

    if (response.data.hasOwnProperty('ultimo_status')) {
      if (response.data.ultimo_status == 'Registro') {
        $('#nombreProspecto').html(response.data.nombre_prospecto);
      }
    }

    if (response.data.hasOwnProperty('sesionIniciada')) {
      $('#nombreProspecto').html(response.data.nombre_prospecto);
    }

    if (response.data.redirect != null) {
      var texto = 'Faltan documentos por completar </br> Redirigiendo...';
      Swal.fire({
        html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
        customClass: 'modalLoading',
        showCancelButton: false,
        showConfirmButton: false,
        allowOutsideClick: false
      });
      location.href = response.data.redirect;
    } else if (response.data.mostrar_oferta == true) {
      location.href = '/solicitud/oferta';
    }
  })["catch"](function (error) {});
}

function validacionesSimulador(datos) {
  var monto_minimo = parseInt($('#monto_minimo').val());
  var monto_maximo = parseInt($('#monto_maximo').val());
  var datos_faltantes = '';
  $(".error").removeClass("error");

  if ($("#plazo").val() == null) {
    datos_faltantes += '- Selecciona un Plazo. <br>';
    $("#plazo").addClass('error');
  }

  prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
  prestamoValue = prestamoValue * 1000;

  if (prestamoValue == 0 || parseInt(prestamoValue) < monto_minimo || parseInt(prestamoValue) > monto_maximo) {
    datos_faltantes += '- El Monto del costo estimado del equipo debe ser entre ' + formatoMoneda(monto_minimo) + ' y ' + formatoMoneda(monto_maximo) + '. <br>';
    $("#monto_prestamo").addClass('error');
  }

  if ($("#finalidad_custom").val().length == 0 || $("#finalidad_custom").val().length < 20) {
    datos_faltantes += '- El campo Cuéntanos sobre tu proyecto debe contener mínimo 20 caracteres y máximo 200. <br>';
    $("#finalidad_custom").addClass('error');
  }

  return datos_faltantes;
}

function validacionesRegistro() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formRegistro .required').each(function () {
    if (this.value == '') {
      campo = this.placeholder;
      $('#' + this.id).addClass('error');

      switch (this.name) {
        case 'nombres':
          campo = 'Nombres';
          break;

        case 'apellidos':
          campo = 'Apellidos';
          break;

        case 'email':
          campo = 'Email';
          break;

        case 'celular':
          campo = 'Teléfono celular';
          break;

        case 'contraseña':
          campo = 'Contraseña';
          break;

        case 'confirmacion_contraseña':
          campo = 'Confirma tu contraseña';
          break;
      }

      datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
    }

    if (this.name == 'email' && this.value != '') {
      var email = IsEmail(this.value);

      if (email == false) {
        $('#' + this.id).addClass('error');
        $('#' + this.id + '-help').html('Ingresa un correo válido');
        check_especial = true;
      }
    }

    if (this.name == 'celular' && this.value != '' && this.value.length < 10) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('El teléfono celular debe ser de 10 dígitos');
      check_especial = true;
    }

    if (this.name == 'confirmacion_contraseña' && this.value != '' && $('#contraseña').val() != '') {
      if (this.value != $('#contraseña').val()) {
        $('#' + this.id).addClass('error');
        $('#contraseña').addClass('error');
        $('#' + this.id + '-help').html('El campo contraseña y confirmación de contraseña no son iguales');
        $('#contraseña-help').html('El campo contraseña y confirmación de contraseña no son iguales');
        check_especial = true;
      }
    }

    if ((this.name == 'nombres' || this.name == 'apellidos') && this.value != '' && this.value.length < 2) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('El campo debe tener al menos 2 caracteres');
      check_especial = true;
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

function validacionesDatosEmpresa() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formDatosEmpresa .required').each(function () {
    if (this.value == '' || this.value == null) {
      campo = this.placeholder;

      switch (this.name) {
        case 'nombre_empresa':
          campo = 'Nombre del Negocio';
          break;

        case 'tipo_trabajo':
          campo = 'Tipo de Trabajo';
          break;

        case 'fecha_fundacion':
          campo = 'Fecha fundacion';
          break;

        case 'location':
          campo = 'Dirección de la Empresa';
          break;

        case 'locality':
          campo = 'Ciudad';
          break;

        case 'administrative_area_level_1':
          campo = 'Estado';
          break;

        case 'postal_code':
          campo = 'Codigo Postal';
          break;
      }
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

function validacionesDatosNegocio() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formDatosNegocio .required').each(function () {
    if (this.value == '' || this.value == null) {
      campo = this.placeholder;

      switch (this.name) {
        case 'ventas_mensuales':
          campo = 'Ventas Mensuales';
          break;

        case 'margen_ventas':
          campo = 'Margen promedio de ventas';
          break;

        case 'numero_empleados':
          campo = 'Numero de Empleados';
          break;

        case 'unico_dueno':
          campo = 'Unico dueño';
          break;
      }
    }

    if (this.name == 'ventas_mensuales' && this.value != '') {
      valor = $("#ventas_mensuales").maskMoney('unmasked')[0];
      valor = valor * 1000;

      if (valor < 1000) {
        $('#' + this.id).addClass('error');
        $('#' + this.id + '-help').html('El ingreso minimo es de $ 1,000.00');
        check_especial = true;
      }
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

function validacionesDatosSocio() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formDatosSocio .required').each(function () {
    if (this.value == '' || this.value == null) {
      campo = this.placeholder;

      switch (this.name) {
        case 'nombre_completo':
          campo = 'Nombre Completo';
          break;

        case 'email_socio':
          campo = 'Email';
          break;

        case 'telefono':
          campo = 'Teléfono del socio';
          break;

        case 'direccion_completa':
          campo = 'Direccion Completa';
          break;
      }
    }

    if (this.name == 'email_socio' && this.value != '') {
      var email = IsEmail(this.value);

      if (email == false) {
        $('#' + this.id).addClass('error');
        $('#' + this.id + '-help').html('Ingresa un correo válido');
        check_especial = true;
      }
    }

    if (this.name == 'telefono' && this.value != '' && this.value.length < 10) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('El teléfono celular debe ser de 10 dígitos');
      check_especial = true;
    }

    if (this.name == 'direccion_completa' && this.value != '' && this.value.length < 2) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('El campo debe tener al menos 2 caracteres');
      check_especial = true;
    }

    if (this.name == 'nombre_completo' && this.value != '' && this.value.length < 2) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('El campo debe tener al menos 2 caracteres');
      check_especial = true;
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

function validacionesInfoSocio() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formDatosInfoSocio .required').each(function () {
    if (this.value == '' || this.value == null) {
      campo = this.placeholder;

      switch (this.name) {
        case 'monto_cuentas_cobrar':
          campo = 'Monto cuentas por cobrar';
          break;

        case 'monto_impuestos':
          campo = 'Monto de impuestos atrasados';
          break;

        case 'duedas_comerciales':
          campo = 'Deudas comerciales';
          break;

        case 'duedas_impuestos':
          campo = 'Deudas de impuestos';
          break;

        case 'nss':
          campo = 'Numero de seguridad social';
          break;

        case 'licencia_conducir':
          campo = 'Licencia de conducir';
          break;
      }
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

function validacionesDatosCredito() {
  var datos_faltantes = '';
  var check_especial = false;
  $('#formDatosCredito .required').each(function () {
    if (this.checked == false) {
      if (this.name == 'acepto_consulta') {
        console.log(this.checked);
        $('#label_autorizo').addClass('errorTitle');
        datos_faltantes += '- Se debe autorizar los términos y condiciones. <br>';
      }
    }

    if (this.value == '' || this.value == null) {
      campo = this.placeholder;

      switch (this.name) {
        case 'deposito_mensual_promedio':
          campo = 'Depositos mensuales';
          break;

        case 'saldo_final':
          campo = 'Saldo final mensual';
          break;
      }
    }
  });

  if (check_especial == true) {
    datos_faltantes += '- Verifica los campos en rojo.';
  }

  return datos_faltantes;
}

var formatoMoneda = function formatoMoneda(input) {
  return '$' + numeroComas(input);
};

var numeroComas = function numeroComas(n) {
  var parts = n.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
};

function IsEmail(valor) {
  return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/.test(valor);
}

$('#contraseña').on('keyup', function () {
  console.log(this.value);
  var pass = IsValidPwd(this.value);
});
$('#new_password').on('keyup', function () {
  var pass = IsValidPwdAct(this.value);
});
$('#password_login').on('blur', function () {
  if (this.value != '') {
    var pass = IsValidPwdLogin(this.value);

    if (pass == false) {
      $('#' + this.id).addClass('error');
      $('#' + this.id + '-help').html('La contraseña no cumple con el formato: <br> Al menos 1 letra mayúscula, 1 letra minúscula, 1 número y al menos 8 carateres.');
      $("#buttonLogin").prop("disabled", true);
    } else {
      $("#buttonLogin").prop("disabled", false);
    }
  }
});
$('#monto_prestamo').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
$('#ventas_mensuales').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
$('#monto_cuentas_cobrar').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
$('#monto_impuestos').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
$('#deposito_mensual_promedio').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
$('#saldo_final').maskMoney({
  prefix: '$ ',
  precision: 0,
  reverse: false,
  selectAllOnFocus: true,
  formatOnBlur: true,
  decimal: '.',
  thousands: ',',
  allowEmpty: true
});
/**
 * Verifica que la contraseña que ingresa el prospecto cumpla con la politica
 * de contraseñas
 *
 * @return {boolean} Resultado de la validación de la contraseña
 */

function IsValidPwd(valor) {
  $('.helperPassword').show();
  var ucase = new RegExp("[A-Z]+");
  var lcase = new RegExp("[a-z]+");
  var num = new RegExp("[0-9]+");
  var valid_len = false;
  var valid_ucase = false;
  var valid_lcase = false;
  var valid_num = false;

  if (valor.length >= 8) {
    $("#8char").removeClass("glyphicon-remove");
    $("#8char").addClass("glyphicon-ok");
    $("#8char").css("color", "#00A41E");
    valid_len = true;
  } else {
    $("#8char").removeClass("glyphicon-ok");
    $("#8char").addClass("glyphicon-remove");
    $("#8char").css("color", "#FF0004");
  }

  if (ucase.test(valor)) {
    $("#ucase").removeClass("glyphicon-remove");
    $("#ucase").addClass("glyphicon-ok");
    $("#ucase").css("color", "#00A41E");
    valid_ucase = true;
  } else {
    $("#ucase").removeClass("glyphicon-ok");
    $("#ucase").addClass("glyphicon-remove");
    $("#ucase").css("color", "#FF0004");
  }

  if (lcase.test(valor)) {
    $("#lcase").removeClass("glyphicon-remove");
    $("#lcase").addClass("glyphicon-ok");
    $("#lcase").css("color", "#00A41E");
    valid_lcase = true;
  } else {
    $("#lcase").removeClass("glyphicon-ok");
    $("#lcase").addClass("glyphicon-remove");
    $("#lcase").css("color", "#FF0004");
  }

  if (num.test(valor)) {
    $("#num").removeClass("glyphicon-remove");
    $("#num").addClass("glyphicon-ok");
    $("#num").css("color", "#00A41E");
    valid_num = true;
  } else {
    $("#num").removeClass("glyphicon-ok");
    $("#num").addClass("glyphicon-remove");
    $("#num").css("color", "#FF0004");
  }

  if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
    return true;
  } else {
    return false;
  }
}

function IsValidPwdAct(valor) {
  $('.helper').show();
  var ucase = new RegExp("[A-Z]+");
  var lcase = new RegExp("[a-z]+");
  var num = new RegExp("[0-9]+");
  var valid_len = false;
  var valid_ucase = false;
  var valid_lcase = false;
  var valid_num = false;

  if (valor.length >= 8) {
    $("#char").removeClass("glyphicon-remove");
    $("#char").addClass("glyphicon-ok");
    $("#char").css("color", "#00A41E");
    valid_len = true;
  } else {
    $("#char").removeClass("glyphicon-ok");
    $("#char").addClass("glyphicon-remove");
    $("#char").css("color", "#FF0004");
  }

  if (ucase.test(valor)) {
    $("#Ucase").removeClass("glyphicon-remove");
    $("#Ucase").addClass("glyphicon-ok");
    $("#Ucase").css("color", "#00A41E");
    valid_ucase = true;
  } else {
    $("#Ucase").removeClass("glyphicon-ok");
    $("#Ucase").addClass("glyphicon-remove");
    $("#Ucase").css("color", "#FF0004");
  }

  if (lcase.test(valor)) {
    $("#Lcase").removeClass("glyphicon-remove");
    $("#Lcase").addClass("glyphicon-ok");
    $("#Lcase").css("color", "#00A41E");
    valid_lcase = true;
  } else {
    $("#Lcase").removeClass("glyphicon-ok");
    $("#Lcase").addClass("glyphicon-remove");
    $("#Lcase").css("color", "#FF0004");
  }

  if (num.test(valor)) {
    $("#Num").removeClass("glyphicon-remove");
    $("#Num").addClass("glyphicon-ok");
    $("#Num").css("color", "#00A41E");
    valid_num = true;
  } else {
    $("#Num").removeClass("glyphicon-ok");
    $("#Num").addClass("glyphicon-remove");
    $("#Num").css("color", "#FF0004");
  }

  if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
    return true;
  } else {
    return false;
  }
}

function iniciarSesion() {
  $("#loginError").html('');
  var texto = 'Iniciando sesión...';
  Swal.fire({
    html: '<center><img src="/credito-real/img/ajax-loader.gif" class="loading-gif"><p>' + texto + '</p></center>',
    customClass: 'modalLoading',
    showCancelButton: false,
    showConfirmButton: false,
    allowOutsideClick: false
  });
  var datos = $('#formLogin *').serializeArray();
  datos = getFormData(datos);
  var clientId = 0;
  ga(function (tracker) {
    clientId = tracker.get('clientId');
  });
  datos['clientId'] = clientId;
  axios.post('/login/prospecto', datos).then(function (response) {
    if (response.data.success == true) {
      var scrolTop = '#simulador';
      $('#LoginForm').modal('hide');
      $('#formRegistro').trigger("reset");
      $('#formSimulador').trigger("reset");
      $('#formVerificarSMS').trigger("reset");
      $('#prestamoPersonalPagoFIjo').html('$0.00');
      $('.sesionIniciada').show();
      $('.iniciarSesion').hide();

      if (response.data.hasOwnProperty('eventTM')) {
        ga(function (tracker) {
          tracker.set('dimension1', response.data.eventTM[0].clientId);
          tracker.set('dimension2', response.data.eventTM[0].userId);
          tracker.set('dimension3', response.data.eventTM[0].solicId);
        });
        ga('send', 'pageview');
      }

      if (response.data.nueva_solicitud == false) {
        $('#' + response.data.formulario).show();
        $('#' + response.data.oculta).hide();
        swal.close();
        $("html, body").animate({
          scrollTop: $("#AppWeb").offset().top
        }, 1000);
        $('#info_prestamo_personal').show();
        $("#info_general_prestamo").text(response.data.prestamo);
        $("#info_general_plazo").text(response.data.plazo);
        $("#info_general_finalidad").text(response.data.finalidad);
      } else {
        $('#' + response.data.formulario).show();
        $('#formRegistro').hide();
        swal.close();
        $('#nueva_solicitud').val('true');
      }

      $('#nombreProspecto').html(response.data.nombre_prospecto);
      $('#show_celular').html(response.data.celular);

      if (response.data.modal != null) {
        if (response.data.hasOwnProperty('mostrar_oferta')) {
          if (response.data.hasOwnProperty('view')) {
            if (response.data.view != '') {
              html(response.data.view);
            }
          }

          if (response.data.mostrar_oferta == true) {} else {
            Swal.fire({
              html: response.data.modal,
              showConfirmButton: true,
              allowOutsideClick: false,
              confirmButtonText: 'Aceptar',
              customClass: 'modalstatus'
            }).then(function (result) {
              if (result.value) {
                $("html, body").animate({
                  scrollTop: $(scrolTop).offset().top - 50
                }, 1000);
              }
            });
          }
        } else {
          Swal.fire({
            html: response.data.modal,
            showConfirmButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Aceptar',
            customClass: 'modalstatus'
          }).then(function (result) {
            if (result.value) {
              $("html, body").animate({
                scrollTop: $(scrolTop).offset().top - 50
              }, 1000);
            }
          });
        }
      }
    } else {
      $("#loginError").html(response.data.message);
      swal.close();
    }
  })["catch"](function (error) {
    var descripcionError = '';

    if (error.hasOwnProperty('response')) {
      if (error.response.hasOwnProperty('data')) {
        descripcionError = error.response.data.message;
      } else {
        descripcionError = error.response.status + ': ' + error.response.responseText;
      }
    } else {
      descripcionError = error;
    }

    $("#loginError").html(descripcionError);
    swal.close();
  });
}
