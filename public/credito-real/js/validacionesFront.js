
function validacionesSimulador(datos) {

    var monto_minimo = parseInt($('#monto_minimo').val());
    var monto_maximo = parseInt($('#monto_maximo').val());
    var datos_faltantes = '';
    $(".error").removeClass("error");

    if ($("#plazo").val() == null) {
        datos_faltantes += '- Selecciona un Plazo. <br>';
        $("#plazo").addClass('error');
    }

    prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
    prestamoValue = prestamoValue * 1000;
    
    if (prestamoValue == 0 || ( parseInt(prestamoValue) < monto_minimo || parseInt(prestamoValue) > monto_maximo)) {
        datos_faltantes += '- El Monto del costo estimado del equipo debe ser entre ' + formatoMoneda(monto_minimo) + ' y ' + formatoMoneda(monto_maximo) + '. <br>';
        $("#monto_prestamo").addClass('error');
    }

    if ($("#finalidad_custom").val().length == 0 || $("#finalidad_custom").val().length < 20) {
        datos_faltantes += '- El campo Cuéntanos sobre tu proyecto debe contener mínimo 20 caracteres y máximo 200. <br>';
        $("#finalidad_custom").addClass('error');
    }

    return datos_faltantes;
}

function validacionesRegistro() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formRegistro .required').each(function() {
        if (this.value == '') {
            campo = this.placeholder;
            $('#' + this.id).addClass('error');
            switch (this.name) {
                case 'nombres':
                    campo = 'Nombres';
                    break;
                case 'apellidos':
                    campo = 'Apellidos';
                    break;
                case 'email':
                    campo = 'Email';
                    break;
                case 'celular':
                    campo = 'Teléfono celular';
                    break;
                case 'contraseña':
                    campo = 'Contraseña';
                    break;
                case 'confirmacion_contraseña':
                    campo = 'Confirma tu contraseña';
                    break;

            }
            datos_faltantes += ' - El campo ' + campo + ' es requerido. <br>';
        }

        if (this.name == 'email' && (this.value != '')) {

            var email = IsEmail(this.value);
            if (email == false) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('Ingresa un correo válido');
                check_especial = true;
            }

        }

        if (this.name == 'celular' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }

        if (this.name == 'confirmacion_contraseña' && (this.value != '' && $('#contraseña').val() != '')) {
            if (this.value != $('#contraseña').val()) {
                $('#' + this.id).addClass('error');
                $('#contraseña').addClass('error');
                $('#'+this.id+'-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                $('#contraseña-help').html('El campo contraseña y confirmación de contraseña no son iguales');
                check_especial = true;
            }
        }

        if ((this.name == 'nombres' || this.name == 'apellidos') && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }

    });
    
    
    
    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosEmpresa() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosEmpresa .required').each(function() {
        
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            switch (this.name) {
                case 'nombre_empresa':
                    campo = 'Nombre del Negocio';
                    break;
                case 'tipo_trabajo':
                    campo = 'Tipo de Trabajo';
                    break;
                case 'fecha_fundacion':
                    campo = 'Fecha fundacion';
                    break;
                case 'location':
                    campo = 'Dirección de la Empresa';
                    break;
                case 'locality':
                    campo = 'Ciudad';
                    break;
                case 'administrative_area_level_1':
                    campo = 'Estado';
                    break;
                case 'postal_code':
                    campo = 'Codigo Postal';
                break;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;
}

function validacionesDatosNegocio() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosNegocio .required').each(function() {
        
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            switch (this.name) {
                case 'ventas_mensuales':
                    campo = 'Ventas Mensuales';
                    break;
                case 'margen_ventas':
                    campo = 'Margen promedio de ventas';
                    break;
                case 'numero_empleados':
                    campo = 'Numero de Empleados';
                    break;
                case 'unico_dueno':
                    campo = 'Unico dueño';
                    break;
            }
        }
        
        if (this.name == 'ventas_mensuales' && this.value != '') {
            valor = $("#ventas_mensuales").maskMoney('unmasked')[0];
            valor = valor * 1000;
            if (valor < 1000) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('El ingreso minimo es de $ 1,000.00');
                check_especial = true;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

function validacionesDatosSocio() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosSocio .required').each(function() {
        
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            switch (this.name) {
                case 'nombre_completo':
                    campo = 'Nombre Completo';
                    break;
                case 'email_socio':
                    campo = 'Email';
                    break;
                case 'telefono':
                    campo = 'Teléfono del socio';
                    break;
                case 'direccion_completa':
                    campo = 'Direccion Completa';
                    break;
            }
        }

        if (this.name == 'email_socio' && (this.value != '')) {

            var email = IsEmail(this.value);
            if (email == false) {
                $('#' + this.id).addClass('error');
                $('#'+this.id+'-help').html('Ingresa un correo válido');
                check_especial = true;
            }
        }

        if (this.name == 'telefono' && (this.value != '' && this.value.length < 10)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El teléfono celular debe ser de 10 dígitos');
            check_especial = true;
        }

        if (this.name == 'direccion_completa' && (this.value != '' && this.value.length < 2 )) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }

        if ((this.name == 'nombre_completo') && (this.value != '' && this.value.length < 2)) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('El campo debe tener al menos 2 caracteres');
            check_especial = true;
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

function validacionesInfoSocio() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosInfoSocio .required').each(function() {
        
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            switch (this.name) {
                case 'monto_cuentas_cobrar':
                    campo = 'Monto cuentas por cobrar';
                    break;
                case 'monto_impuestos':
                    campo = 'Monto de impuestos atrasados';
                    break;
                case 'duedas_comerciales':
                    campo = 'Deudas comerciales';
                    break;
                case 'duedas_impuestos':
                    campo = 'Deudas de impuestos';
                    break;
                case 'nss':
                    campo = 'Numero de seguridad social';
                    break;
                case 'licencia_conducir':
                    campo = 'Licencia de conducir';
                    break;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

function validacionesDatosCredito() {

    var datos_faltantes = '';
    var check_especial = false;

    $('#formDatosCredito .required').each(function() {

        if (this.checked == false) {
            if (this.name == 'acepto_consulta') {
                console.log(this.checked);
                $('#label_autorizo').addClass('errorTitle');
                datos_faltantes += '- Se debe autorizar los términos y condiciones. <br>';
            }
        }
        
        if (this.value == '' || this.value == null) {
            campo = this.placeholder;

            switch (this.name) {
                case 'deposito_mensual_promedio':
                    campo = 'Depositos mensuales';
                    break;
                case 'saldo_final':
                    campo = 'Saldo final mensual';
                    break;
            }
        }
    });

    if (check_especial == true) {
        datos_faltantes += '- Verifica los campos en rojo.';
    }

    return datos_faltantes;

}

var formatoMoneda = function(input) {
	return '$' + numeroComas(input);
}

var numeroComas = function(n){
	var parts = n.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

function IsEmail(valor) {
    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/.test(valor)
}

$('#contraseña').on('keyup', function() {
    console.log(this.value);
    var pass = IsValidPwd(this.value);
 });
 
 $('#new_password').on('keyup', function() {
     var pass = IsValidPwdAct(this.value);
 });
 
 $('#password_login').on('blur', function() {
     if (this.value != '') {
         var pass = IsValidPwdLogin(this.value);
         if (pass == false) {
            $('#' + this.id).addClass('error');
            $('#'+this.id+'-help').html('La contraseña no cumple con el formato: <br> Al menos 1 letra mayúscula, 1 letra minúscula, 1 número y al menos 8 carateres.');
            $("#buttonLogin").prop("disabled", true);
         } else {
            $("#buttonLogin").prop("disabled", false);
         }
     }
 });

 $('#monto_prestamo').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#ventas_mensuales').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#monto_cuentas_cobrar').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#monto_impuestos').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#deposito_mensual_promedio').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

$('#saldo_final').maskMoney({
    prefix: '$ ',
    precision: 0,
    reverse: false,
    selectAllOnFocus: true,
    formatOnBlur: true,
    decimal: '.',
    thousands: ',',
    allowEmpty: true,
});

/**
 * Verifica que la contraseña que ingresa el prospecto cumpla con la politica
 * de contraseñas
 *
 * @return {boolean} Resultado de la validación de la contraseña
 */
function IsValidPwd(valor) {

    $('.helperPassword').show();
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if (valor.length >= 8) {
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
        valid_len = true;
	} else {
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}

	if (ucase.test(valor)) {
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
        valid_ucase = true;
	} else {
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}

	if (lcase.test(valor)) {
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
        valid_lcase = true;
	} else {
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}

	if (num.test(valor)) {
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
        valid_num = true;
	} else {
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}

function IsValidPwdAct(valor) {

    $('.helper').show();
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");

    var valid_len = false;
    var valid_ucase = false;
    var valid_lcase = false;
    var valid_num = false;

	if (valor.length >= 8) {
		$("#char").removeClass("glyphicon-remove");
		$("#char").addClass("glyphicon-ok");
		$("#char").css("color","#00A41E");
        valid_len = true;
	} else {
		$("#char").removeClass("glyphicon-ok");
		$("#char").addClass("glyphicon-remove");
		$("#char").css("color","#FF0004");
	}

	if (ucase.test(valor)) {
		$("#Ucase").removeClass("glyphicon-remove");
		$("#Ucase").addClass("glyphicon-ok");
		$("#Ucase").css("color","#00A41E");
        valid_ucase = true;
	} else {
		$("#Ucase").removeClass("glyphicon-ok");
		$("#Ucase").addClass("glyphicon-remove");
		$("#Ucase").css("color","#FF0004");
	}

	if (lcase.test(valor)) {
		$("#Lcase").removeClass("glyphicon-remove");
		$("#Lcase").addClass("glyphicon-ok");
		$("#Lcase").css("color","#00A41E");
        valid_lcase = true;
	} else {
		$("#Lcase").removeClass("glyphicon-ok");
		$("#Lcase").addClass("glyphicon-remove");
		$("#Lcase").css("color","#FF0004");
	}

	if (num.test(valor)) {
		$("#Num").removeClass("glyphicon-remove");
		$("#Num").addClass("glyphicon-ok");
		$("#Num").css("color","#00A41E");
        valid_num = true;
	} else {
		$("#Num").removeClass("glyphicon-ok");
		$("#Num").addClass("glyphicon-remove");
		$("#Num").css("color","#FF0004");
	}

    if (valid_len == true && valid_ucase == true && valid_lcase == true && valid_num == true) {
        return true;
    } else {
        return false;
    }
}