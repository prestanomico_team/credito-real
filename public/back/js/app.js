(function(){
	var app = angular.module('prestaburo',[],function($interpolateProvider){
		 	$interpolateProvider.startSymbol('<%');
      $interpolateProvider.endSymbol('%>');
	});

	app.controller('MainController', function(){
		
	});

	app.filter('stringLength', function () {
	  return function (item) {
	  	if(typeof(item) == 'string'){
	    	return item.length;
	    }
	  };
	});


})();