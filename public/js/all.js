function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function focusSimulador() {
  $("html, body").animate({
    scrollTop: $("#simulador").offset().top
  }, 1000);
}

function validarSimulador() {
  var datos = $('#formSimulador').serializeArray();
  console.log(datos);
  datos = getFormData(datos);
  console.log(datos);
  /*prestamoValue = $("#monto_prestamo").maskMoney('unmasked')[0];
  prestamoValue = prestamoValue * 1000;
  datos['monto_prestamo'] = prestamoValue;
    var validaciones = validacionesSimulador();
  var nuevaSolicitud = $('#nueva_solicitud').val();
  $('#validacionesSimulador').html(validaciones);
    var datos_faltantes = '';
  if (validaciones == '') {
        axios.post('/validaciones', {
          datos: datos,
          formulario: 'simulador'
      }).then(function (response) {
            if (response.data.success == false) {
                if (response.data.hasOwnProperty('reload')) {
                    Swal.fire({
                      title: "La sesión expiro",
                      text: 'Recarga la página para continuar',
                      type: 'error',
                      showConfirmButton: true,
                      confirmButtonText: 'Aceptar',
                      customClass: 'modalError',
                      allowOutsideClick: false
                  }).then((result) => {
                      if (result.value) {
                         location.reload();
                     }
                 });
                } else {
                  datos_faltantes += '- Verifica los campos en rojo.';
                  $.each(response.data.errores, function(key, error) {
                      $('#' + key + '-help').html(error);
                  });
              }
            } else {
                if (nuevaSolicitud == 'false' && validaciones == '') {
                  getForm('registro', 'simulador');
                  $("html, body").animate({ scrollTop: $("#pasosSolicitud").offset().top }, 1000);
              } else if(nuevaSolicitud == 'true' && validaciones == '') {
                  registroNuevaSolicitud()
              }
            }
        }).catch(function (error) {
            Swal.fire({
              title: "Ooops.. Surgió un problema",
              text: error,
              type: 'error',
              showConfirmButton: true,
              customClass: 'modalError',
              allowOutsideClick: false
         });
        });
    }*/
}

function verificar_codigo() {
  $('#registro').hide();
  $('#verificar_codigo').show();
}

function datos_empresa() {
  $('#verificar_codigo').hide();
  $('#datos_empresa').show();
}

function datos_negocio() {
  $('#datos_empresa').hide();
  $('#datos_negocio').show();
}

function datos_socio() {
  $('#datos_negocio').hide();
  $('#datos_socio').show();
}

function info_socios() {
  $('#datos_socio').hide();
  $('#info_socios').show();
}

function puntaje() {
  $('#info_socios').hide();
  $('#puntaje').show();
}

function aprobacion() {
  $('#puntaje').hide();
  $('#aprobacion').show();
}

function initMap() {
  var componentForm = ['location', 'locality', 'administrative_area_level_1', 'country', 'postal_code'];
  var autocompleteInput = document.getElementById('location');
  console.log(autocompleteInput);
  var autocomplete = new google.maps.places.Autocomplete(autocompleteInput, {
    fields: ["address_components", "geometry", "name"],
    types: ["address"]
  });
  console.log(autocomplete);
  autocomplete.addListener('place_changed', function () {
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      // User entered the name of a Place that was not suggested and
      // pressed the Enter key, or the Place Details request failed.
      window.alert('No details available for input: \'' + place.name + '\'');
      return;
    }

    fillInAddress(place);
  });

  function fillInAddress(place) {
    // optional parameter
    var addressNameFormat = {
      'street_number': 'short_name',
      'route': 'long_name',
      'locality': 'long_name',
      'administrative_area_level_1': 'short_name',
      'country': 'long_name',
      'postal_code': 'short_name'
    };

    var getAddressComp = function getAddressComp(type) {
      var _iterator = _createForOfIteratorHelper(place.address_components),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var component = _step.value;

          if (component.types[0] === type) {
            return component[addressNameFormat[type]];
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return '';
    };

    document.getElementById('location').value = getAddressComp('street_number') + ' ' + getAddressComp('route');

    var _iterator2 = _createForOfIteratorHelper(componentForm),
        _step2;

    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var component = _step2.value;

        // Location field is handled separately above as it has different logic.
        if (component !== 'location') {
          document.getElementById(component).value = getAddressComp(component);
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }
}

function getFormData(datos) {
  var unindexed_array = datos;
  var indexed_array = {};
  $.map(unindexed_array, function (n, i) {
    indexed_array[n['name']] = n['value'];
  });
  return indexed_array;
}
