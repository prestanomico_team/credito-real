<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespuestasMaquinaRiesgosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas_maquina_riesgos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->boolean('ejecucion_sp')->nullable();
            $table->mediumText('status_ejecucion_sp')->nullable();
            $table->string('decision', 255)->nullable();
            $table->string('stored_procedure', 50)->nullable();
            $table->string('status_oferta', 25)->nullable();
            $table->string('motivo_rechazo', 100)->nullable();
            $table->string('descripcion_otro', 255)->nullable();
            $table->integer('plantilla_comunicacion')->unsigned()->nullable();
            $table->boolean('pantallas_extra')->nullable();
            $table->boolean('oferta_minima')->nullable();
            $table->boolean('simplificado')->nullable();
            $table->boolean('facematch')->nullable();
            $table->boolean('carga_identificacion_selfie')->nullable();
            $table->boolean('subir_documentos')->nullable();
            $table->string('situaciones',255)->nullable();
            $table->boolean('elegida')->nullable();
            $table->string('tipo_poblacion', 50)->nullable();
            $table->string('tipo_oferta', 50)->nullable();
            $table->string('monto', 100)->nullable();
            $table->string('plazo', 100)->nullable();
            $table->string('pago', 100)->nullable();
            $table->string('tasa', 100)->nullable();
            $table->boolean('cuestionario_dinamico_guardado')->nullable();
            $table->mediumText('status_guardado')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas_maquina_riesgos');
    }
}
