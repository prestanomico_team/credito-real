<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('nombres');
            $table->longText('apellido_paterno');
            $table->longText('apellido_materno');
            $table->string('email', 100)->unique();
            $table->string('celular', 15);
            $table->string('password', 255);
            $table->boolean('cognito_id');
            $table->string('sms_verificacion', 10);
            $table->boolean('usuario_confirmado');
            $table->boolean('login')->default(0);
            $table->string('referencia', 100);
            $table->longText('encryptd');
            $table->timestamps();

            $table->index(['email'], 'email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospectos');
    }
}
