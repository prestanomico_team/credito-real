<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuestionariosDinamicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios_dinamicos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('situacion_id')->unsigned()->nullable();
            $table->longText('pregunta')->nullable();
            $table->string('tipo', 50)->nullable();
            $table->longText('opciones')->nullable();
            $table->longText('depende')->nullable();
            $table->longText('respuesta_depende')->nullable();
            $table->boolean('oculto')->nullable();
            $table->longText('grid_estilo')->nullable();
            $table->string('ancho_estilo', 10)->nullable();
            $table->timestamps();

            $table->index(['situacion_id'], 'situacion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios_dinamicos');
    }
}
