<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlazoProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plazo_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plazo_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('plazo_producto', function (Blueprint $table) {
            Schema::dropIfExists('plazo_producto');
        });
    }
}
