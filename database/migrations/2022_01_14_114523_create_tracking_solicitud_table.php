<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('solicitud_id');
            $table->unsignedBigInteger('status_id');
            $table->string('status', 100);
            $table->string('sub_status', 100);
            $table->boolean('success');
            $table->mediumText('descripcion');
            $table->mediumText('extra')->nullable();
            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
            $table->index(['status', 'sub_status'], 'status_substatus');
            $table->index(['status_id'], 'status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_solicitud');
    }
}
