<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_producto', 50);
            $table->string('alias', 50)->nullable();
            $table->string('tipo', 50)->nullable();
            $table->string('vista', 50)->nullable();
            $table->string('empresa', 150)->nullable();
            $table->integer('monto_minimo');
            $table->integer('monto_maximo');
            $table->integer('bc_score');
            $table->string('condicion', 10)->nullable();
            $table->integer('micro_score')->nullable();
            $table->boolean('sin_historial')->nullable();
            $table->boolean('sin_cuentas_recientes')->nullable();
            $table->integer('edad_minima');
            $table->integer('edad_maxima');
            $table->float('cat', 8, 2);
            $table->float('tasa_minima', 8, 2);
            $table->boolean('tasa_fija')->nullable();
            $table->float('tasa_maxima', 8, 2);
            $table->float('comision_apertura', 8, 2);
            $table->string('stored_procedure', 50);
            $table->boolean('doble_oferta')->nullable();
            $table->string('tipo_monto_do', 20)->nullable();
            $table->double('do_monto_maximo')->nullable();
            $table->string('tipo_monto_simplificado', 20)->nullable();
            $table->double('simplificado_monto_minimo')->nullable();
            $table->double('simplificado_monto_maximo')->nullable();
            $table->boolean('facematch_simplificado')->nullable();
            $table->boolean('carga_identificacion_selfie_simplificado')->nullable();
            $table->boolean('facematch')->nullable();
            $table->boolean('carga_identificacion_selfie')->nullable();
            $table->boolean('carga_comporbante_ingresos')->nullable();
            $table->text('logo');
            $table->string('redireccion', 255)->nullable();
            $table->string('campo_cobertura', 30)->nullable();
            $table->boolean('consulta_alp')->nullable();
            $table->boolean('consulta_buro')->nullable();
            $table->boolean('garantia')->nullable();
            $table->boolean('seguro')->nullable();
            $table->date('vigencia_de')->nullable();
            $table->boolean('vigente')->nullable();
            $table->date('vigencia_hasta')->nullable();
            $table->boolean('default')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
