<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesAltaEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes_alta_empresa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id')->nullable();
            $table->unsignedBigInteger('solicitud_id')->nullable();
            $table->string('panel_operativo_id', 50)->nullable();
            $table->text('fecha_alta')->nullable();
            $table->string('SHORTNAME', 100)->nullable();
            $table->string('NAME1', 100)->nullable();
            $table->string('NAME2', 100)->nullable();
            $table->string('STREET', 250)->nullable();
            $table->string('DIRNUMEXT', 5)->nullable();
            $table->string('DIRCDEDO', 20)->nullable();
            $table->string('DIRDELMUNI', 20)->nullable();
            $table->string('DIRCOLONIA', 20)->nullable();
            $table->string('DIRPAIS', 2)->nullable(); 
            $table->string('TELCEL', 15)->nullable();
            $table->string('EMAIL', 100)->nullable();
            $table->string('FORMERNAME', 100)->nullable();
            $table->string('DIRCODPOS', 5)->nullable();
            $table->string('MAININCOME', 10)->nullable();//total ventas
            $table->string('LOANAMOUNT', 10)->nullable();
            $table->string('TERM', 20)->nullable();
            $table->string('LOANPURPOSE', 255)->nullable();
            $table->string('INCSNDSOURCE', 50)->nullable();
            $table->string('IDTYPE', 50)->nullable();
            $table->boolean('aplica_cliente')->nullable();
            $table->boolean('alta_cliente')->default(0);
            $table->datetime('alta_cliente_at')->nullable();
            $table->string('no_cliente_t24', 20)->nullable();
            $table->boolean('aplica_solicitud')->nullable();
            $table->boolean('alta_solicitud')->default(0)->nullable();
            $table->datetime('alta_solicitud_at')->nullable();
            $table->string('no_solicitud_t24', 20)->nullable();
            $table->boolean('aplica_ligue')->nullable();
            $table->boolean('usuario_ligado')->default(0);
            $table->datetime('ligue_usuario_at')->nullable();
            $table->boolean('aplica_email')->nullable();
            $table->boolean('email_enviado')->nullable();
            $table->string('id_email', 20)->nullable();
            $table->string('status_email', 20)->nullable();
            $table->string('status_sms', 20)->nullable();
            $table->datetime('envio_notificaciones_at')->nullable();
            $table->boolean('aplica_facematch')->nullable();
            $table->boolean('facematch')->nullable();
            $table->boolean('solo_carga_identificacion_selfie')->nullable();
            $table->boolean('documentos_facematch')->nullable();
            $table->boolean('registro_facematch')->nullable();
            $table->longText('facematch_error')->nullable();
            $table->dateTime('facematch_at')->nullable();
            $table->boolean('aplica_comprobante_ingresos')->nullable();
            $table->boolean('alta_comprobante_ingresos')->nullable();
            $table->longText('comprobante_ingresos_error')->nullable();
            $table->dateTime('alta_comprobante_ingresos_at')->nullable();

            $table->longText('error')->nullable();
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes_alta_empresa');
    }
}
