<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargaDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carga_documentos', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('solicitud_id');
            $table->unsignedBigInteger('prospecto_id');
            $table->boolean('aplica_facematch')->nullable();
            $table->boolean('id_front')->nullable();
            $table->boolean('id_back')->nullable();
            $table->boolean('selfie')->nullable();
            $table->boolean('facematch_completo')->nullable();
            
            $table->boolean('aplica_comprobante_ingresos')->nullable();
            $table->string('frecuencia', 25)->nullable();
            $table->string('tipo_comprobante', 25)->nullable();
            $table->string('detalle_documento', 25)->nullable();
            $table->integer('numero_comprobantes')->nullable();
            $table->boolean('comprobante_ingresos_completo')->nullable();

            $table->timestamps();

            $table->index(['solicitud_id'], 'solicitud');
            $table->index(['prospecto_id'], 'prospecto');
            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carga_documentos');
    }
}
