<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->longText('nombre_empresa');
            $table->string('tipo_propiedad', 10);
            $table->date('fecha_fundacion');
            $table->longText('descripcion_empresa');
            $table->string('calle', 150);
            $table->string('ciudad', 150);
            $table->string('estado', 150);
            $table->string('cp', 5);
            $table->string('pais', 150);
            $table->string('ventas_mensuales', 20);
            $table->string('margen_ventas', 20);
            $table->string('numero_empleados', 10);
            $table->boolean('unico_dueño');
            $table->longText('otros_nombres_comerciales');
            $table->string('monto_cuentas_cobrar', 20);
            $table->string('deudas_impuestos', 20);
            $table->string('deudas_comerciales', 20);
            $table->string('monto_impuestos', 20);
            $table->longText('deudas_empresa');
            $table->string('nss', 10);
            $table->string('licencia_conducir', 11);
            $table->string('depositos_mensuales', 20);
            $table->string('saldo_mensual', 20);
            $table->timestamps();

            $table->index(['prospecto_id', 'solicitud_id'], 'prospecto_solicitud');
            $table->index(['solicitud_id'], 'solicitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
