<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->string('status', 100);
            $table->string('sub_status', 100);
            $table->string('tipo_persona', 20);
            $table->integer('prestamo');
            $table->integer('plazo');
            $table->string('finalidad', 150);
            $table->float('pago_estimado');
            $table->string('equipo_leasing');
            $table->timestamps();

            $table->index(['prospecto_id'], 'prospecto_id');
            $table->index(['status', 'sub_status'], 'status_substatus');
            $table->index(['prospecto_id', 'status', 'sub_status'], 'prospecto_status_substatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
