<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantillasComunicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantillas_comunicaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plantilla_id')->nullable();
            $table->mediumText('modal_encabezado')->nullable();
            $table->mediumText('modal_img')->nullable();
            $table->longText('modal_cuerpo')->nullable();
            $table->longText('sms')->nullable();
            $table->string('email_asunto', 100)->nullable();
            $table->longText('email_cuerpo')->nullable();
            $table->timestamps();

            $table->index(['plantilla_id'], 'plantilla_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantillas_comunicaciones');
    }
}
