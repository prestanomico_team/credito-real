<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prospecto_id');
            $table->unsignedBigInteger('solicitud_id');
            $table->string('client_id', 50);
            $table->string('email', 150);
            $table->string('ip', 50)->nullable();
            $table->string('telefono', 150)->nullable();
            $table->string('navegador', 30)->nullable();
            $table->string('navegador_version', 10)->nullable();
            $table->string('sistema_operativo', 30)->nullable();
            $table->string('sistema_version', 10)->nullable();
            $table->string('tipo_dispositivo', 25)->nullable();
            $table->string('marca', 30)->nullable();
            $table->string('modelo', 30)->nullable();
            $table->string('modelo_detalle',30)->nullable();
            $table->string('idioma', 15)->nullable();
            $table->string('campaña', 30)->nullable();
            $table->string('origen', 30)->nullable();
            $table->string('medio', 30)->nullable();
            $table->string('contenido_anuncio', 255)->nullable();
            $table->integer('duracion_sesion')->nullable();
            $table->string('pais', 30)->nullable();
            $table->string('estado', 50)->nullable();
            $table->string('ciudad', 50)->nullable();
            $table->string('proveedor_internet', 100)->nullable();
            $table->string('fecha_hora', 30)->nullable();
            $table->string('datos_obtenidos', 30)->nullable();
            $table->timestamps();

            $table->index(['prospecto_id'], 'prospecto');
            $table->index(['solicitud_id'], 'solicitud');
            $table->index(['client_id'], 'client');
            $table->index(['prospecto_id', 'solicitud_id', 'client_id'], 'prospecto_solicitud_client');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics');
    }
}
