<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogoZipCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogo_zip_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('zip_code', 5);
            $table->string('type', 20);
            $table->string('tipo_asentamiento', 50);
            $table->string('primary_city', 100);
            $table->string('acceptable_cities', 250);
            $table->string('unacceptable_cities', 250);
            $table->string('state', 2);
            $table->string('timezone', 20);
            $table->string('area_codes', 20);
            $table->string('world_region', 2);
            $table->string('country', 2);
            $table->string('latitud', 7);
            $table->string('longitude', 7);
            $table->string('irs_estimated_population', 10);
            
            $table->timestamps();

            $table->index(['zip_code'], 'zip_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogo_zip_codes');
    }
}
